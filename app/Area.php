<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Area extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'area';

    //Prevenir Asignacion masiva en:
    protected $guarded = [ ];

    //relaciones
    public function proceso()
    {
        return $this->hasMany('App\Proceso');
    }
}
