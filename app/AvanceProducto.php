<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Support\Facades\DB;

class AvanceProducto extends Model implements AuditableContract
{
    use Auditable;
    protected $table = "avance_productos";
    protected $guarded = [];

    protected $casts = [
        'id' => 'integer',
        'cantidad_registrada' => 'double',
        'producto_id' => 'integer',
        'registro_id' => 'integer',
    ];

    public function producto()
    {
        return $this->belongsTo('App\Producto', 'producto_id', 'id');
    }

    public function registro()
    {
        return $this->belongsTo('App\RegistroAvance', 'registro_id', 'id');
    }

    public function asignarCantidad($cantidad)
    {
        if($this->producto){

        }else{

        }
    }
}
