<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Cargo extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'cargo';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function persona()
    {
        return $this->hasMany('App\Persona');
    }
}
