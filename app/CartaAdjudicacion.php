<?php

namespace App;

use Auth;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

//use Illuminate\Database\Eloquent\Model;

class CartaAdjudicacion extends Contrato
{
    protected static $singleTableType = 'carta_adjudicacion';

    public function tester_text()
    {
        return "Soy una puta carta";
    }

    public function pdf(){
		ini_set('max_execution_time', 300);
        //$pdf = new \mPDF('c','A4','12','Arial', 22, 15, 1, 1, 5, 0);
        $param = [
			'mode' => 'c',
			'format' => 'A4',
			'default_font_size' => 12,
			'default_font' => 'Arial',
			'margin_left' => 22,
			'margin_right' => 15,
			'margin_top' => 1,
			'margin_bottom' => 1,
			'margin_header' => 5,
			'margin_footer' => 0,
			'orientation' => 'P',
        ];
        $pdf = new \Mpdf\Mpdf($param);
        $pdf->setAutoTopMargin = 'stretch';
        $pdf->setAutoBottomMargin = 'stretch';
        $header = \View::make('contratos.plantilla_pdf_encabezado_carta')->render();
        $footer = \View::make('contratos.plantilla_pdf_pie')->render();
        //$html = $this->html_carta;
        $html = \View::make('contratos.plantilla_pdf_cuerpo_carta', ['contrato' => $this])->render();
        $pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);
        $pdf->WriteHTML($html);
        return $pdf->Output();
	}


    public function info_temporal(Request $request){
		//$temp = new CartaAdjudicacion();
		$this->html_carta = $request->html_carta;
        $this->proyecto = Auth::user()->persona->id;
		//return $temp;
	}

    public function guardar_cuerpo(Request $request){
        $transaccion = DB::transaction(function() use ($request){
            $this->info_temporal($request);
            $this->estado_doc = ($request->accion == 2) ? 2 : 3;//guardar o procesar segun el caso;
            $this->save();
        });
    }

    public function crear_otrosi()
    {
        $otrosi = new CartaAdjudicacion;
        $otrosi = $this;
        return $otrosi;
    }

    public function Objeto()
	{
		return "Objeto no disponible";
    }
    
    public function ObjetoRaw()
	{
		return "Objeto no disponible";
	}

}
