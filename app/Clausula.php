<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Clausula extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'clausulas';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function tipo()
    {
        return $this->belongsTo('App\TipoClausula', 'tipo_clausula_id', 'id');
    }

    public function contrato()
    {
        return $this->belongsTo('App\Contrato');
    }
}
