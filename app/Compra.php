<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Compra extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'plan_de_compras';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

	//Custom attributes
    protected $appends = ['vigencia']; 

	public function getVigenciaAttribute()
    {
		return $this->rubro->vigencia->anio;
	}
	
    //relaciones
    public function proceso()
    {
        return $this->belongsTo('App\Proceso');
    }

    public function modalidad()
    {
        return $this->belongsTo('App\ModalidadSeleccion');
    }
	
    public function estado()
    {
        return $this->belongsTo('App\EstadoCompra');
    }

	public function rubro()
    {
        return $this->belongsTo('App\Presupuesto', 'rubro_id', 'id');
    }

    public function estudio()
    {
        return $this->hasOne('App\EstudioMercado');
    }

    public function documento($value='')
    {
        return $this->belongsToMany('App\DocumentoBase', 'documento_compra', 'compra_id', 'doc_id');
    }
}
