<?php

namespace App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\DB;

//use Illuminate\Database\Eloquent\Model;
//use OwenIt\Auditing\Auditable;

class Contrato extends DocumentoBase
{
    //use Auditable;
	protected static $singleTableType = 'contrato';
	//relaciones
	//Deshabilitadas temporalmente
	// public function rp()
	// {
	// 	return $this->hasMany('App\RegistroPresupuestal');
	// }
	
	// public function cdp()
	// {
	// 	return $this->belongsToMany('App\ProvisionPresupuestal', 'contrato_cdp', 'contrato_id', 'cdp_id');
	// }

	public function tester_text()
	{
		return "Soy un maldito contrato";
	}

	public function persona()
	{
		return $this->belongsTo('App\Persona', 'supervisor', 'id');
	}

	public function tercero()
	{
		return $this->belongsTo('App\Tercero')->where('tipo_id', $this->tipo_tercero);
	}

	public function modalidad()
	{
		return $this->belongsTo('App\ModalidadSeleccion');
	}

	public function tipo_contrato()
	{
		return $this->belongsTo('App\TipoContrato');
	}


	public function estado()
	{
		return $this->belongsTo('App\EstadoContrato');
	}

	
	public function clausulas()
	{
		return $this->hasMany('App\Clausula', 'contrato_id', 'id');
	}

	public function otrosi()
	{
		return $this->hasMany('App\Otrosi', 'contrato_id', 'id');
	}

    
    

	public function ClausulaObjeto()
	{
		return $this->clausulas->where('tipo_clausula_id', 1)->first();
	}
	//Objeto HTML
	public function Objeto()
	{
		return $this->clausulas->where('tipo_clausula_id', 1)->first()->contenido_html;
	}
	//Objeto TEXTO PLANO
	public function ObjetoRaw()
	{
		$html = $this->clausulas->where('tipo_clausula_id', 1)->first()->contenido_html;
		$texto = preg_replace( "/\n\s+/", "\n", rtrim(html_entity_decode(strip_tags($html))) );
		return $texto;
	}

	public function pdf(){
		ini_set('max_execution_time', 300);
        //$pdf = new \Mpdf('c','A4','12','Arial', 22, 15, 1, 1, 5, 0);
		$param = [
			'mode' => 'c',
			'format' => 'A4',
			'default_font_size' => 12,
			'default_font' => 'Arial',
			'margin_left' => 22,
			'margin_right' => 15,
			'margin_top' => 1,
			'margin_bottom' => 1,
			'margin_header' => 5,
			'margin_footer' => 0,
			'orientation' => 'P',
		];
		$pdf = new \Mpdf\Mpdf($param);
        $pdf->setAutoTopMargin = 'stretch';
        $pdf->setAutoBottomMargin = 'stretch';
        $header = \View::make('contratos.plantilla_pdf_encabezado',['contrato' => $this])->render();
        $footer = \View::make('contratos.plantilla_pdf_pie')->render();
		$html = \View::make('contratos.plantilla_pdf_cuerpo', ['contrato' => $this])->render();
		$pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);
        $pdf->WriteHTML($html);
        return $pdf->Output();
	}
	//Funcion retorna un temporal de la misma clase de documento
	//Se usa para pasar este objeto temporal a la vista previa (PDF)
	public function info_temporal(Request $request){
		
		foreach ($this->clausulas as $key => $value) {
        	$this->clausulas->forget($key);
		}
        
        foreach ($this->obligaciones as $key => $value) {
        	$this->obligaciones->forget($key);
		}
        
        foreach ($request->clausula as $key => $clausula) {
            $cl = new \App\Clausula;                    
            if (is_array($clausula)) {
                if(array_key_exists('objeto', $clausula)){
                    $cl->tipo_clausula_id = 1;
                    $cl->contenido_html = $clausula['objeto'];
                    $cl->ordinal = ($key);
                }
                if(array_key_exists('obligaciones', $clausula)){
                    $cl->tipo_clausula_id = 2;
                    $cl->contenido_html = $clausula['obligaciones'];
                    $cl->ordinal = ($key);
                    if (isset($request->obligaciones['obligacion_espf']) && isset($request->obligaciones['obligacion_gnrl'])) {
                        foreach ($request->obligaciones['obligacion_espf'] as $index => $obsp) {
                            //SPCF
                            $obligacion = new \App\Obligacion;
                            $obligacion->tipo_obligacion_id = 2;
                            $obligacion->ordinal = ($index+1);
                            $obligacion->obligacion = $obsp;
                            $this->obligaciones->add($obligacion);
                        }
                        foreach ($request->obligaciones['obligacion_gnrl'] as $index => $obgn) {
                            //GNRL
                            $obligacion = new \App\Obligacion;
                            $obligacion->tipo_obligacion_id = 1;
                            $obligacion->ordinal = ($index+1);
                            $obligacion->obligacion = $obgn;
                            $this->obligaciones->add($obligacion);
                        }
                    }
                }
            } else {
                $cl->tipo_clausula_id = 3;
                $cl->contenido_html = $clausula;
                $cl->ordinal = $key;                                     
            }
            $this->clausulas->add($cl);
        }
        $this->consecutivo_cpe = $request->consecutivo_cpe;
        $this->encabezado_consideraciones = $request->encabezado;
        $this->nombre_contratante = $request->nombre_contratante;
        $this->cargo_contratante = $request->cargo_contratante;
        $this->nombre_contratista = $request->nombre_contratista;
        $this->cargo_contratista = $request->cargo_contratista;                
        $this->proyecto = Auth::user()->persona->id;
		//return $temp;
	}
	//Guardar cuerpo | Clausulas y Obligaciones
	public function guardar_cuerpo(Request $request){
		$transaccion = DB::transaction(function() use ($request){
			$this->obligaciones()->delete();
			$this->clausulas()->delete();
			$this->info_temporal($request);
			$this->obligaciones()->saveMany($this->obligaciones);
			$this->clausulas()->saveMany($this->clausulas);
            $this->estado_doc = ($request->accion == 2) ? 2 : 3;//guardar o procesar segun el caso;
            $this->save();
        });
	}

	public function crear_otrosi()
	{
		$otrosi = new Contrato;
        $otrosi = $this;
        return $otrosi;
	}
}
