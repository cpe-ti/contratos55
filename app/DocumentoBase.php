<?php

namespace App;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Database\Eloquent\Model;

use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use Illuminate\Support\Facades\DB;

use Auth;

class DocumentoBase extends Model implements AuditableContract
{
    use Auditable;
    
    use SingleTableInheritanceTrait;

	// protected $dates = [
    //  'created_at',
	// 	'updated_at',
	// 	'fecha_suscripcion',
	// 	'fecha_legalizacion',
	// 	'fecha_inicio',
	// 	'fecha_terminacion',
	// 	'fecha_rp'
    // ];
	
	// protected $casts = [
    //     'fecha_rp' => 'date',
    //     'fecha_suscripcion' => 'date',
    //     'fecha_legalizacion' => 'date',
    //     'fecha_inicio' => 'date',
    //     'fecha_terminacion' => 'date',
    // ];

	//protected $dateFormat = 'Y-m-d';

	protected $table = "documento_base";

    protected $guarded = [];

    protected static $singleTableTypeField = 'tipo';

    protected static $singleTableSubclasses = [Contrato::class, OrdenCompra::class, CartaAdjudicacion::class, Otrosi::class];

	// public function getDateFormat() {
    	// return 'Y-d-m H:i:s';
	// }

	//public function __construct()
    //{
       //if ($this->tipo == 'carta_adjudicacion') {
       //    return CartaAdjudicacion::findOrFail($this->id);
          //$doctemp = new CartaAdjudicacion;
       //} else {
       //    return Contrato::findOrFail($this->id);
          //$doctemp = new Contrato;
       //}
	//}


	//Custom attribute
    protected $appends = ['tipo_documento', 'nombre_completo', 'text']; 

	public function getTipoDocumentoAttribute()
    {
		$clase = get_class($this);
		switch ($clase) {
			case 'App\Contrato':
				return "Contrato";
				break;
			case 'App\CartaAdjudicacion':
				return "Carta de Adjudicación";
				break;
			case 'App\Otrosi':
				return "Otrosí";
				break;			
			default:
				return "Documento Base";
				break;
		}
		return 'palito';
	}
	
	public function getNombreCompletoAttribute()
    {
		return $this->NombreRapido();
	}
	
	public function getTextAttribute()
    {
		return $this->NombreRapido();
    }
	
	// Habilitar chequeo numerico en JSON
	public function toJson($options = JSON_NUMERIC_CHECK)
    {
        return json_encode($this->toArray(), $options);
    }

	public function estado_documento()
	{
		return $this->belongsTo('App\EstadoDocumento', 'estado_doc', 'id');
	}

	public function compra(){
		return $this->belongsToMany('App\Compra', 'documento_compra', 'doc_id', 'compra_id');
	}

	public function proceso()
    {
        return $this->belongsToMany('App\Proceso', 'lideres_procesos', 'lider_id', 'proceso_id');
    }

	public function persona()
	{
		return $this->belongsTo('App\Persona', 'supervisor', 'id');
	}

	public function modalidad()
	{
		return $this->belongsTo('App\ModalidadSeleccion');
	}

	public function estado()
	{
		return $this->belongsTo('App\EstadoContrato');
	}

	public function tipo_contrato()
	{
		return $this->belongsTo('App\TipoContrato');
	}

    public function tercero()
	{
		return $this->belongsTo('App\Tercero', 'tercero_id', 'id')->where('tipo_id', $this->tipo_tercero);
	}

    public function polizas()
	{
		return $this->hasMany('App\Poliza', 'doc_id', 'id');
	}

	public function revisado_por()
	{
		return $this->belongsTo('App\Persona', 'reviso', 'id');
	}

	public function proyectado_por()
	{
		return $this->belongsTo('App\Persona', 'proyecto', 'id');
	}

	public function super()
	{
		return $this->belongsTo('App\Persona', 'supervisor', 'id');
	}

	public function productos()
	{
		return $this->hasMany('App\Producto', 'contrato_id', 'id');
	}

	public function programacion_tecnica()
    {
        return $this->hasManyThrough('App\ProgramacionProducto', 'App\Producto', 'contrato_id', 'id');
    }

	public function obligaciones()
	{
		return $this->hasMany('App\Obligacion', 'contrato_id', 'id');
	}

	public function pdf(){
		return "No hay metodo definido";
	}

	public function aprobar(){
		
	}

	public function pagos()
	{
		return $this->hasMany('App\Pagos');
	}

	public function pagos_programados()
    {
    	return $this->hasMany('App\ProgramacionPago', 'doc_id', 'id');
    }

    public function Objeto()
	{
		return "No hay Objeto... sorry";
	}

 	//public function programacion_tecnica()
	// {
	// 	return $this->productos->toArray();
	// }
	public function test()
	{
		return $this->productos->toArray();
	}

	
	/**
     * Get all of the ACTIVED models from the database.
     *
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function allActive($columns = ['*'])
    {
        $columns = is_array($columns) ? $columns : func_get_args();

        $instance = new static;

        return $instance->newQuery()->where('activo','1')->get($columns);
	}
	
	//Obtener la coleccion de documentos a los que se les puede hacer otrosis
	//Contratos legalizados y que no sean otrosis.
	public static function otrosiables($columns = ['*'])
	{
		$columns = is_array($columns) ? $columns : func_get_args();

        $instance = new static;

        return $instance->newQuery()->where('estado_doc', '>=', 6)->where('tipo', '!=', 'otrosi')->get($columns);
		//return self::all()->where('estado_doc', '>=', 6)->where('tipo', '!=', 'otrosi')->get($columns);
		//estado_doc = 6;
	}

	public function getLastData()
	{
		if (count($this->otrosis)) {
			# code...
		}
	}

	public function getLastOtrosi()
	{
		if (count($this->otrosis)) {
			//$this->otrosis()->where('')
		}
		return NULL;
	}

	//Funciones y metodos especificos
	public function NombreRapido() {
		return $this->consecutivo_cpe." - ".$this->tercero->NombreCompleto();
        //return ucfirst($this->persona->primer_nombre).($this->persona->segundo_nombre == "" ? "" : " ".ucfirst($this->persona->segundo_nombre))." ".ucfirst($this->persona->primer_apellido).ucfirst($this->persona->segundo_apellido == "" ? "" : " ".ucfirst($this->persona->segundo_apellido));
	}

	public function CreadoPor()
	{
		$message = "";
		try {
			$message = $this->audits()->with('user')->where('event','created')->first()->user->NombreCompleto();
		} catch (\Exception $e) {
			//echo 'Caught \exception: ',  $e->getMessage(), "\n";
			$message = "Error";
		}
		return $message;
	}

	public function ProyectadoPor()
	{
		$message = "";
		try {
			$message = $this->audits()->with('user')->where([['event','updated'],['new_values','LIKE','%"estado_doc":3%']])->latest()->first()->user->NombreCompleto();
		} catch (\Exception $e) {
			//echo 'Caught \exception: ',  $e->getMessage(), "\n";
			$message = "Error";
		}
		return $message;
	}

	public function RechazadoPor()
	{
		$message = "";
		try {
			$message = $this->audits()->with('user')->where([['event','updated'],['new_values','LIKE','%"estado_doc":4%']])->latest()->first()->user->NombreCompleto();
		} catch (\Exception $e) {
			//echo 'Caught \exception: ',  $e->getMessage(), "\n";
			$message = "Error";
		}
		return $message;
	}

	public function AprobadoPor()
	{
		$message = "";
		try {
			$message = $this->audits()->with('user')->where([['event','updated'],['new_values','LIKE','%"estado_doc":5%']])->latest()->first()->user->NombreCompleto();
		} catch (\Exception $e) {
			//echo 'Caught \exception: ',  $e->getMessage(), "\n";
			$message = "Error";
		}
		return $message;
	}



	public function reviso()
	{
		$str = "";
		try {
			if ($this->revisado_por) {
				$str = $this->revisado_por->NombreyCargo();
			}			
		} catch (\Exception $e) {
			//echo 'Caught \exception: ',  $e->getMessage(), "\n";
			//$message = "Error";
			$str = "";
		}
		return $str;	
	}

	public function proyecto()
	{
		$str = "";
		try {
			if ($this->proyectado_por) {
				$str = $this->proyectado_por->NombreyCargo();
			}
		} catch (\Exception $e) {
			//echo 'Caught \exception: ',  $e->getMessage(), "\n";
			//$message = "Error";
		}
		return $str;	
	}

	public function info_temporal(Request $request){
		//$temp = new DocuemntoBase();
		//return $temp;
	}

	public function puede_editarse()
	{
		$r = true;
		$m = "";
		if ($this->estado_doc == 3) {
            $r = false;
			$m = "El contrato esta en revisión y solo podrá editarse si es rechazado.";
        }        
        if ($this->estado_doc == 5 || $this->estado_doc == 6) {
            $r = false;
			$m = "El contrato ya no puede editarse";
        }
        return array($r, $m);
	}

	public function crear_otrosi()
	{
		return false;
	}

	public function tiene_productos()
	{
		if (count($this->productos)) {
			return true;
		} else {
			return false;
		}
		
	}

	public function tiene_programacion_tecnica()
	{
		if ($this->tiene_productos()) {
			return $this->productos->first()->esta_programado();
		} else {
			return false;
		}		
	}

	public function tiene_programacion_financiera()
	{
		if ($this->tiene_productos()) {
			return $this->productos->first()->esta_programado_pago();
		} else {
			return false;
		}		
	}

	public function programacion_completa(){
		$suma_ponderada = 0;
		$meses = Array();
		foreach ($this->meses() as $mes) {
			$mestxt = strftime("%d-%m-%Y", $mes->getTimeStamp());
			$meses[$mestxt]['mes'] = $mes;
			$meses[$mestxt]['prog_tec'] = $this->programacion_tecnica()->whereDate('fecha','=', $mes)->get();
			//$meses[$mestxt]['prog_fin'];
			foreach ($this->programacion as $key => $value) {
				# code...
			}
			$suma = 0;
			foreach ($meses[$mestxt]['prog_tec'] as $prog_t) {
				$suma += $prog_t->ponderado_mes();				
			}
			$suma_ponderada += $suma;
			$meses[$mestxt]['prog_tec']['ponderado_mes'] = $suma;
			$meses[$mestxt]['prog_tec']['avance_mes'] = $suma_ponderada;
		}
		
		return $meses;
	}

	public function sumatoria_productos_pago()
    {
        $pagos = Array();
        $producto = Array();

        foreach ($this->pagos_programados()->orderBy('orden', 'ASC')->get() as $pg) {
        	foreach ($pg->productos as $pr) {
        		if (array_key_exists($pr->id, $producto)) {
        			$producto[$pr->id] += $pr->pivot->cantidad;
        		}else{
        			$producto[$pr->id] = $pr->pivot->cantidad;
        		}
        	}
        	$pagos[$pg->orden]['productos'] = $producto;
        }
        return $pagos;
	}

	// ╦╔╗╔╔═╗╔═╗╦═╗╔╦╗╔═╗╔═╗╦╔═╗╔╗╔  ╔═╗╦ ╦╔═╗╔═╗╦═╗╦  ╦╦╔═╗╦╔═╗╔╗╔
	// ║║║║╠╣ ║ ║╠╦╝║║║╠═╣║  ║║ ║║║║  ╚═╗║ ║╠═╝║╣ ╠╦╝╚╗╔╝║╚═╗║║ ║║║║
	// ╩╝╚╝╚  ╚═╝╩╚═╩ ╩╩ ╩╚═╝╩╚═╝╝╚╝  ╚═╝╚═╝╩  ╚═╝╩╚═ ╚╝ ╩╚═╝╩╚═╝╝╚╝

	public function meses()
	{
		$inicio = new \DateTime($this->fecha_inicio);
		$inicio->modify('first day of this month');
		$fin = new \DateTime($this->fecha_terminacion);
		$fin->modify('first day of next month');
		$intervalo = \DateInterval::createFromDateString('1 month');
		$periodo = new \DatePeriod($inicio, $intervalo, $fin);
		return $periodo;
	}

	public function mesesAsAray(){
		$periodo = $this->meses();
		$arraymeses = Array();
		setlocale(LC_TIME, 'es-CO', 'es_CO');
		$cm = new \DateTime();
		foreach ($periodo as $llave => $mes) {
			$mes_fin = clone $mes;
			$mes_fin->modify('last day of this month');
			//$esMesActual = ($mes_fin->format('Y-m') == $cm->format('Y-m'));
			$esMesActual = true;//habilitar todos los meses
			array_push($arraymeses, ["meshabil"=>$esMesActual, "date" => $mes->format('Y-m-d'), "dateini" => $mes->format('Y-m-d'), "datefin" => $mes_fin->format('Y-m-d'),"texto" => ucfirst(utf8_encode(strftime("%B de %Y", $mes->getTimeStamp())))]);
		}
		return $arraymeses;
	}


	public function informacion_supervision(){
		$arrayMeses = $this->mesesAsAray();
		foreach ($arrayMeses as $key => $mes) {
			$resgistroAvance = $this->registro_avance()->whereBetween('fecha_registro', [$mes["dateini"], $mes["datefin"]])->orderBy('id', 'asc')->with(['avance_productos', 'solicitud_pagos'])->get();
			$productosProgramados = $this->programacion_tecnica()->whereBetween('fecha', [$mes["dateini"], $mes["datefin"]])->with('producto')->get();
			$arrayMeses[$key]["registro_avance"] = $resgistroAvance;
			$arrayMeses[$key]['productos_programados'] = $productosProgramados;
		}
		return $arrayMeses;
		
	}

	public function ultimoRegistro()
	{
		$ultimoregistro = $this->registro_avance()->orderBy('orden','desc')->first();
		return $ultimoregistro;
	}

	public function registrosProcesados()
	{
		$registros = $this->registro_avance()->where('procesado', 1)->orderBy('orden','asc')->get();
		return $registros;
	}
	
	public function ultimoRegistroRatificado()
	{
		$ultimoregistro = $this->registro_avance()->where('procesado', 1)->orderBy('orden','desc')->first();
		return $ultimoregistro;
	}

	public function ultimoNumeroInforme()
	{
		//$ultimoregistro = $this->registro_avance()->orderBy('orden','desc')->first();
		if($this->ultimoRegistroRatificado() == null){
			return 0;
		}
		return $this->ultimoRegistroRatificado()->orden;
	}

	//╦═╗╔═╗╔═╗╦╔═╗╔╦╗╦═╗╔═╗  ╔╦╗╔═╗  ╔═╗╦  ╦╔═╗╔╗╔╔═╗╔═╗  ╦ ╦  ╔═╗╦ ╦╔═╗╔═╗╦═╗╦  ╦╦╔═╗╦╔═╗╔╗╔
	//╠╦╝║╣ ║ ╦║╚═╗ ║ ╠╦╝║ ║   ║║║╣   ╠═╣╚╗╔╝╠═╣║║║║  ║╣   ╚╦╝  ╚═╗║ ║╠═╝║╣ ╠╦╝╚╗╔╝║╚═╗║║ ║║║║
	//╩╚═╚═╝╚═╝╩╚═╝ ╩ ╩╚═╚═╝  ═╩╝╚═╝  ╩ ╩ ╚╝ ╩ ╩╝╚╝╚═╝╚═╝   ╩   ╚═╝╚═╝╩  ╚═╝╩╚═ ╚╝ ╩╚═╝╩╚═╝╝╚╝
	public function registro_avance()
    {
        return $this->hasMany('App\RegistroAvance', 'contrato_id', 'id');
	}

	public function totalPagadoRegistros()
	{
		$registrosProcesados = $this->registrosProcesados();
		$total = 0;
		foreach ($registrosProcesados as $registro) {
			foreach ($registro->solicitud_pagos as $pago) {
				$total += $pago->valor_solicitado;
			}
		}

		return $total;
	}

	//  ██████╗ ████████╗██████╗  ██████╗ ███████╗██╗███████╗
	// ██╔═══██╗╚══██╔══╝██╔══██╗██╔═══██╗██╔════╝██║██╔════╝
	// ██║   ██║   ██║   ██████╔╝██║   ██║███████╗██║███████╗
	// ██║   ██║   ██║   ██╔══██╗██║   ██║╚════██║██║╚════██║
	// ╚██████╔╝   ██║   ██║  ██║╚██████╔╝███████║██║███████║
	//  ╚═════╝    ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚═╝╚══════╝
	
	
	//Retorna objeto tipo Otrosi Guardado creado a partir de la info del actual objeto
	public function crearOtrosi()
	{
		$class = get_class($this);
		switch ($class) {
			case Contrato::class:
				return "Contrato";
				break;
			case CartaAdjudicacion::class:
				return "Carta";
				break;
			case Otrosi::class:
				return "Otrosi";
				break;			
			default:
				return "Nada";
				break;
		}
		
		return is_a($this, 'App\Contrato');
		return new Otrosi();
	}

	public function otrosis()
    {
        return $this->hasMany('App\Otrosi', 'contrato_id', 'id');
    }
	
}
