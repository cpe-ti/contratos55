<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EstadoCompra extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'estado_compra';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function compra()
    {
        return $this->hasMany('App\Compra');
    }
}
