<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EstadoDocumento extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'estado_documento';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function documento()
    {
        return $this->hasMany('App\DocumentoBase', 'estado_doc', 'id');
    }
}
