<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudioMercado extends Model
{
    //nombre de tabla
    protected $table = 'estudio_mercado';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function compra()
    {
        return $this->belongsTo('App\Compra');
    }

    public function cdp()
    {
        return $this->hasOne('App\ProvisionPresupuestal');
    }
}
