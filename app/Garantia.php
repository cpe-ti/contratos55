<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Garantia extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'garantia';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function poliza()
    {
        return $this->belongsTo('App\Poliza', 'poliza_id', 'id');
    }
}
