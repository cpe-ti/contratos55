<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Grupo extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'grupo';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function permiso()
    {
        return $this->hasMany('App\Permiso');
    }
}
