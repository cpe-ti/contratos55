<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Persona;

use App\Rol;

use App\Grupo;

use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:menu-administrar');
    }
    
    public function index()
    {
        return view('administracion.inicio');
    }

    public function Usuarios()
    {
        return view('administracion.usuarios', ['usuarios' => User::all()]);
    }

    public function VerUsuario($id)
    {
        return view('administracion.usuario', ['usuario' => User::findOrFail($id)]);
    }

    public function EditarUsuario($id)
    {
        return view('administracion.editar-usuario', ['usuario' => User::findOrFail($id), 'personas' => Persona::all(), 'roles' => Rol::all()]);
    }
    
    public function GuardarUsuario($id, Request $request)
    {
        $usuario = User::findOrFail($id);
        
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,'.$id.'',
            'persona' => 'required|exists:persona,id|numeric|persona_libre:'.$usuario->persona_id.'',
            'rol.*' => 'required',
        ],[
            'email.required' => 'El correo es requerido',
            'email.email' => 'El correo no es válido',
            'persona.required' => 'persona es requerido',
            'persona.exists' => 'Esta persona no existe en la base de datos',
            'persona.persona_libre' => 'Esta persona ya tiene un usuario asociado',
            'rol.*.required' => 'El rol es requerido',
        ]);
        //Pasada la validación inicial inicia transaccion
        $transaccion = DB::transaction(function() use ($request, $usuario){
            //cambiar correo de usuario
             $usuario->email = $request->email;
            //cambiar estado activo de usuario
            $usuario->activo = ($request->activo ? true : false); 
            //cambiar persona asociada a usuario
            if (!($request->persona == $usuario->persona->id)) {
                $usuario->persona_id = $request->persona;
            }
            //Despegar todos los roles del usuario
            $usuario->detachRoles($usuario->roles);
            //asignar uno a uno los nuevos roles
            foreach ($request->rol as $key => $rol) {
                $usuario->attachRole($key);
            }
            //guardar el usuario
            $usuario->save();
        });        
        if ($request->wantsJson()) {
            return response()->json([
                'success' => true,
                'mensaje' => 'Cambios de usuario guardados correctamente'
            ]);
        }
        return view('administracion.editar-usuario', ['usuario' => User::findOrFail($id), 'personas' => Persona::all(), 'roles' => Rol::all()]);
        //return view('administracion.editar-usuario', ['usuario' => User::findOrFail($id), 'personas' => Persona::all(), 'roles' => Rol::all()]);
    }

    public function VerRoles()
    {
        return view('administracion.roles-permisos', ['roles' => Rol::all()]);
    }

    public function CrearRoles(){
        //return Rol::all();
        return view('administracion.crear.rol');
    }

    public function EditarPermisos($rol_id)
    {
        return view('administracion.permisos', ['rol' => Rol::findOrFail($rol_id), 'grupos' => Grupo::all()]);
    }

    public function GuardarPermisos(Request $request)
    {
        $this->validate($request, [
            'permiso.*' => 'required',
            'rol' => 'required',
        ],[
            'producto.*.required' => ' El nombre es obligatorio',
            'rol.required' => 'No se definió el Rol',
        ]);
        //Obtener Rol
        $rol = Rol::find($request->rol);
        //Borrar todos los permisos del Rol
        $rol->perms()->sync([]);
        //Asignar permisos marcados
        if (!is_null($request->input('permiso'))) {
            foreach ($request->input('permiso') as $key => $value) {
                $rol->attachPermission($key);
            }
        };
        if ($request->wantsJson()) {
            return response()->json([
                'success' => true,
                'mensaje' => 'Permisos guardados correctamente'
            ]);
        }
        return view('administracion.permisos', ['rol' => Rol::findOrFail($rol_id), 'grupos' => Grupo::all()]);
    }

    public function documentos()
    {
        return view('administracion.documentos');
    }
}
