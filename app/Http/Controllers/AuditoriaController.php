<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DocumentoBase;
use App\RegistroAvance;

class AuditoriaController extends Controller
{
    //

    public function avanceContrato(DocumentoBase $documento)
    {
        return view('auditoria.contratos.registrosavance', ['contrato' => $documento]);
    }

    public function informeRegistro(RegistroAvance $registro)
    {
        return $registro->informe;
    }
}
