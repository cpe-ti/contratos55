<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Persona;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/inicio';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'persona_id' => 'required|digits_between:7,11|exists:persona,id|numeric|persona_libre',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ],[
            'exists' => 'Este número de identificación no se encuentra registrado.',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        //1010202030
        //Rol predeterminado asignado a la persona
        $rol = Persona::find($data['persona_id'])->rol_pre();
        //Crear usuario
        $transaccion = DB::transaction(function() use ($data, $rol){
            $nuevo_usuario = User::create([
                'persona_id' => $data['persona_id'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
            $nuevo_usuario->attachRole($rol);
            return $nuevo_usuario;
        });
        return $transaccion;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        if ($request->ajax()) {
            return response()->json([
                "mensaje" => 'Nuevo Usuario Creado Correctamente',
                "menu" => (string) view('partials.menu')->render(),
                "barrausuario" => (string) view('partials.barra-seccion-usuario')->render(),
                "intento_url" => redirect()->intended()->getTargetUrl(),
            ], 200);
        }
        return redirect($this->redirectPath());
    }
}
