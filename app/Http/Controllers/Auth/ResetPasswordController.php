<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Response;
use Illuminate\Http\Request as RequestX;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/inicio';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function sendResetResponse($response)
    {
        if(Request::ajax()){
            return response()->json([
                "mensaje" => 'Contraseña reestablecida correctamente',
                "menu" => (string) view('partials.menu'),
                "barrausuario" => (string) view('partials.barra-seccion-usuario')
            ], 200);
        }
        return redirect($this->redirectPath())
                            ->with('status', trans($response));
    }


    protected function sendResetFailedResponse(RequestX $request, $response)
    {
        if(Request::ajax()){
            return response()->json(['error' => trans($response)], 401);
        }
        return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => trans($response)]);
    }
}
