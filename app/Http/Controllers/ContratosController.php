<?php

namespace App\Http\Controllers;

use App\Notifications\CheckDoc;
use App\Notifications\NoCoordinador;
use App\Notifications\DocAprobado;
use App\Notifications\NuevoInformeProcesado;
use App\Notifications\DocRechazado;
use App\Notifications\ContratoAsignado;
use App\Notifications\NoSupervisorUser;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\DocumentoBase;
use App\Contrato;
use App\CartaAdjudicacion;
use App\Compra;
use App\Proceso;
use App\Persona;
use App\ModalidadSeleccion;
use App\Tercero;
use App\TipoPersona;
use App\Poliza;
use App\Garantia;
use App\TipoContrato;
Use App\User;
use App\RegistroAvance;
use App\AvanceProducto;
use App\SolicitudPago;
use App\InformeSupervision;

use View;
use Auth;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;


//use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;

/* Trait informes en PDF */
use App\Traits\InformeSupervisionPDF;

class ContratosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */

    use InformeSupervisionPDF;

    public function __construct()
    {
        
    }

    public function index()
    {
        //return view('contratos.todos', ['contratos' => Contrato::where('supervisor', Auth::user()->persona_id)->get()]);
        // return DocumentoBase::allActive();
        return view('contratos.todos', ['contratos' => DocumentoBase::allActive()]);
    }

    public function indexSuper()
    {
        // return view('contratos.todos', ['contratos' => Contrato::where('supervisor', Auth::user()->persona_id)->where('estado_doc', 6)->get()]);
        return view('contratos.todos', ['contratos' => DocumentoBase::where('supervisor', Auth::user()->persona_id)->where('estado_doc', 6)->get()]);
    }


    public function show($id)
    {
        $doc = DocumentoBase::findOrFail($id);
        return view('contratos.contrato', ['contrato' => $doc]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        return view('contratos.nuevo.crear', ['tipos' => TipoPersona::all(), 'tipo_cto' => TipoContrato::all(),'procesos' => Proceso::all(), 'modalidades' => ModalidadSeleccion::all(), 'compras' => Compra::all()]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(Request $request)
    {
        $this->validar_nuevo_doc($request);
        $transaccion = DB::transaction(function() use ($request){
            $clase = 'App\\'.TipoContrato::findOrFail($request->tipo_cto)->clase;
            $doc = new $clase;
            $doc->tipo_contrato_id = $request->tipo_cto;
            $doc->modalidad_id = $request->modalidad_cto;
            $doc->tercero_id = $request->tercero_id;
            $doc->tipo_tercero = $request->tercero_tipo;
            $doc->valor = $request->valor_cto;
            $doc->estado_id = 1;//Estado Adjudicado
            $doc->cdp = $request->cdp_cto;
            $doc->activo = true;
            $doc->estado_doc = 1;//estado creado
            $doc->save();
            $doc->compra()->attach($request->compra);
        });
        //Contrato guardado
        if ($request->wantsJson()) {
            return response()->json([
                'success' => true,
                'mensaje' => 'Nuevo contrato creado correctamente'
            ]);
        }
        return redirect('contratacion/contratos');
    }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
        $doc = DocumentoBase::findOrFail($id);
        list($res, $mensaje) = $doc->puede_editarse();
        if ($res === false) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => $mensaje]);
        }
        return view('contratos.editar', ['contrato' => $doc, 'tipos' => TipoPersona::all(), 'tipo_cto' => TipoContrato::all(),'procesos' => Proceso::all(), 'modalidades' => ModalidadSeleccion::all(), 'compras' => Compra::all()]);
        
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(Request $request,$id)
    {
        $doc = DocumentoBase::findOrFail($id);
        $contrato;
        if ($doc->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
            # code...
        } else {
            $contrato = Contrato::findOrFail($id);
            # code...
        }
        unset($doc);        
        if ($contrato->estado_doc == 1) {
            //return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato aun no tiene un cuerpo creado para editar"]);
        }
        if ($contrato->estado_doc == 2) {
            //return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya no puede editarse"]);
        }
        $codigo = 0;
        $mensaje = "";
        $error = false;
        //Validar estado de documento
        switch ($contrato->estado_doc) {
            case 3:
                $codigo = 409;
                $mensaje = "El contrato esta en revisión y solo podrá editarse si es rechazado.";
                $error = true;
                break;
            case 4:
                $codigo = 403;
                $mensaje = "El contrato ya no puede editarse.";
                $error = true;
                break;
            case 5:
                $codigo = 403;
                $mensaje = "El contrato ya no puede editarse.";
                $error = true;
                break;
            case 6:
                $codigo = 403;
                $mensaje = "El contrato ya no puede editarse.";
                $error = true;
                break;            
            default:
                $codigo = 0;
                $mensaje = "";
                $error = false;
                break;
        }
        if ($error) {
            if ($request->wantsJson()) {
                return \Response::json([
                    'success' => false,
                    'mensaje' => $mensaje,
                ], $codigo);
            }
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => $mensaje]);
        }
        //Validar datos enviados
        $this->validar_nuevo_doc($request);
        //Guardar Actualizacion
        $transaccion = DB::transaction(function() use ($contrato, $request){
            if($request->tipo_cto == 8){
                $contrato->tipo = 'carta_adjudicacion';
            }else{
                $contrato->tipo = 'contrato';
            }
            $contrato->tipo_contrato_id = $request->tipo_cto;
            $contrato->modalidad_id = $request->modalidad_cto;
            $contrato->tercero_id = $request->tercero_id;
            $contrato->tipo_tercero = $request->tercero_tipo;
            $contrato->valor = $request->valor_cto;
            $contrato->cdp = $request->cdp_cto;
            $contrato->compra()->detach();
            $contrato->compra()->attach($request->compra);
            $contrato->save();
        });

        if ($request->wantsJson()) {
            return \Response::json([
                'success' => true,
                'mensaje' => "Datos guardados",
            ], 201);//Created
        }
        return redirect('contratacion/contratos');
        //return "\"What about the reality where Hitler cured cancer, Morty? The answer is: Don't think about it.\"";
    }

    #Funcion mostrar vista cuerpo documento
    #Contrato / Carta / Etc

    public function generar_cuerpo($id)
    {
        $doc = DocumentoBase::findOrFail($id);
        $contrato;
        if ($doc->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
        } else {
            $contrato = Contrato::findOrFail($id);
        }
        unset($doc);
        if ($contrato->estado_doc != 1 && $contrato->estado_doc != 2 && $contrato->estado_doc != 4) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "No puede generarse un cuerpo porque el estado del documento es¨: ".$contrato->estado_documento->estado]);
        }
        if ($contrato->tipo_contrato->id == 8) {//Carta de adjudicacion
            return view('contratos.nuevo.cuerpo-carta-adjudicacion', ['contrato' => $contrato]); 
        }
        return view('contratos.nuevo.cuerpo_contrato', ['contrato' => $contrato]);
                    
    }
    //guardar cuerpo generado
    public function guardar_cuerpo(Request $request, $id)
    {
        // print_r($request->all());
        // exit(0);
        $doc = DocumentoBase::findOrFail($id);
        $documento;
        //$doctemp;
        if ($doc->tipo == 'carta_adjudicacion') {
            $documento = CartaAdjudicacion::findOrFail($id);
            //$doctemp = new CartaAdjudicacion;
        } else {
            $documento = Contrato::findOrFail($id);
            //$doctemp = new Contrato;
        }
        unset($doc);
        
        if ($documento->estado_doc != 1 && $documento->estado_doc != 2 && $documento->estado_doc != 4) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "No puede generarse un cuerpo porque el estado del documento es¨: ".$documento->estado_documento->estado]);
        }
        //Validar accion solicitada
        $mensaje = '';
        $codigo = 0;
        $error = false;

        switch ($request->accion) {
            case 1:
                if ($request->wantsJson()) {
                    $error = true;
                    $mensaje = "El pdf se debe ver a través de una peticion corriente";
                    $codigo = 405;
                    break;
                }
                $error = false;
                $documento->info_temporal($request);
                //$doctemp->info_temporal($request);
                return \Response::make($documento->pdf(), 201);
                //return \Response::make($this->imprimir_carta($contrato), 201);
                break;
            case 2:
                $mensaje = "Guardado Correctamente";
                $codigo = 201;
                $error = false;
                break;
            case 3:
                //Validaciones correspondientes al Request
                if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
                    $this->validar_cuerpo_carta($request);
                }
                if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
                    $this->validar_cuerpo_cto($request);   
                }
                $mensaje = "Procesado Correctamente";
                $codigo = 201;
                $error = false;
                break;                
            default:
                $mensaje = "Accion no válida";
                $codigo = 405;
                $error = true;
                break;
        }
        //Si la accion no es valida       
        if ($error) {
            if ($request->wantsJson()) {
                return \Response::json([
                    'success' => false,
                    'mensaje' => $mensaje,
                ], $codigo);
            }
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => $mensaje]);
        }
        //Si todo esta ok guarda o procesa
        $documento->guardar_cuerpo($request);
        
        //notificar si es procesar;
        if ($request->accion == 3) {
            $this->NotificarDocProcesado($documento->id);
        }        
        // Si todo salio OK
        if ($request->wantsJson()) {
            return \Response::json([
                'success' => !$error,
                'mensaje' => $mensaje,
            ], $codigo);//Created
        }
        return redirect('contratacion/contratos');
    }

    public function editar_cuerpo($id){
        $doc = DocumentoBase::findOrFail($id);
        if ($doc->estado_doc == 1) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato aun no tiene un cuerpo creado para editar"]);
        }
        if ($doc->estado_doc == 2) {
            //return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya no puede editarse"]);
        }        
        if ($doc->estado_doc == 3) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato esta en revisión y solo podrá editarse si es rechazado."]);
        }        
        if ($doc->estado_doc == 4) {
            //return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya no puede editarse"]);
        }        
        if ($doc->estado_doc == 5) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya no puede editarse"]);
        }        
        if ($doc->estado_doc == 6) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya no puede editarse"]);
        }
        unset($doc);
        return $this->generar_cuerpo($id);        
    }

    public function update_cuerpo(Request $request, $id){
        return $this->guardar_cuerpo($request, $id);    
    }

    public function aprobar($id){
        $documento = DocumentoBase::findOrFail($id);
        $contrato = NULL;

        if ($documento->estado_doc != 3) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato debe estar en estado 'revision' para poder aprobarse. Estado:".$documento->estado_documento->estado]);
        }

        if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
        }

        if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
            $contrato = Contrato::findOrFail($id);
        }

        return view('contratos.nuevo.vistobueno', ['contrato' => $contrato]);
    }

    public function aprobar_direccion($id)
    {
        # code...
        //return "Aprobacion de dirección";
        return view('contratos.nuevo.vistobueno', ['contrato' => $contrato]);
    }

    public function save_aprobar(Request $request, $id){
        //exit(print_r($request->all()));
        $documento = DocumentoBase::findOrFail($id);
        $contrato = NULL;

        if ($documento->estado_doc != 3) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato debe estar en estado 'revision' para poder aprobarse. Estado:".$documento->estado_documento->estado]);
        }

        if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
        }
         
        if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
            $contrato = Contrato::findOrFail($id);
        }

        $mensaje = "";
        if ($request->accion == 1) {
            $contrato->estado_doc = 5;
            $contrato->reviso = Auth::user()->persona->id;
            $mensaje = "Documento Aprobado";
            $contrato->proyectado_por->user->notify(new DocAprobado(url('contratacion/contratos/'.$id)));
        } elseif($request->accion == 2) {
            $contrato->estado_doc = 4;
            $mensaje = "Documento Rechazado";
            $contrato->proyectado_por->user->notify(new DocRechazado(url('contratacion/contratos/'.$id), $request->observaciones));
        }
        $contrato->observaciones = $request->observaciones;
        $contrato->save();        
        //ass
        if ($request->wantsJson()) {
            return \Response::json([
                'success' => true,
                'mensaje' => $mensaje,
            ], 200);//Created
        }
        return redirect('contratacion/contratos');
    }


    public function save_aprobar_direccion(Request $request, $id)
    {
        # code...
    }

    public function legalizar(Request $request, $id){
        $documento = DocumentoBase::findOrFail($id);
        $contrato = NULL;

        if ($documento->estado_doc != 5) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato debe estar en estado 'Aprobado' para poder legalizarse. Estado: ".$documento->estado_documento->estado]);
        }

        if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
        }

        if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
            $contrato = Contrato::findOrFail($id);
        }

        $supervisores = Persona::all();
        return view('contratos.nuevo.legalizar', ['contrato' => $contrato, 'supervisores' => $supervisores]);
    }

    public function StoreLegalizar(Request $request, $id){
        //print_r($request->all());
        //exit(0);
        $documento = DocumentoBase::findOrFail($id);
        $contrato = NULL;

        if ($documento->estado_doc != 5) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato debe estar en estado 'Aprobado' para poder legalizarse. Estado: ".$documento->estado_documento->estado]);
        }

        if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
            // throw new \Exception('Soy una carta');
            $contrato = CartaAdjudicacion::findOrFail($id);
        }

        if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
            //throw new \Exception('Soy un contrato');
            $contrato = Contrato::findOrFail($id);
        }

        //$contrato = Contrato::findOrFail($id);
        if ($request->accion == 1) {
            $this->validate($request, [
                'id_oasis' => 'integer|numeric|unique:documento_base,id_oasis,'.$id.'',
                'consecutivo_cpe' => 'regex:/^([0-9]){3}-([0-9]){2}[A-Z]{0,1}$/|unique:documento_base,consecutivo_cpe,'.$id.'',
                'fecha_suscripcion' => 'date',
                'fecha_legalizacion' => 'date',
                'rp' => 'numeric|integer',
                'fecha_rp' => 'date',
                'url_secop' => 'url',
                'supervisor' => 'exists:persona,id',
                'polizas.*.numero_poliza' => 'numeric|integer',
                'polizas.*.fecha_expedicion' => 'date',
                'polizas.*.garantia.*.valor_amparo' => 'numeric|min:0',
                'polizas.*.garantia.*.fecha_inicio' => 'date',
                'polizas.*.garantia.*.fecha_fin' => 'date|after:polizas.*.garantia.*.fecha_inicio',
            ],[
                'polizas.*.garantia.*.fecha_fin.after' => 'La fecha de terminación :attribute debe ser mayor a la de inicio',
            ]);

            $request->consecutivo_cpe = !empty($request->consecutivo_cpe) ? $request->consecutivo_cpe : NULL;
            $request->id_oasis = !empty($request->id_oasis) ? $request->id_oasis : NULL;
            $request->rp = !empty($request->rp) ? $request->rp : NULL;
            $request->fecha_suscripcion = !empty($request->fecha_suscripcion) ? $request->fecha_suscripcion : NULL;
            $request->fecha_legalizacion = !empty($request->fecha_legalizacion) ? $request->fecha_legalizacion : NULL;
            $request->fecha_rp = !empty($request->fecha_rp) ? $request->fecha_rp : NULL;
            $request->url_secop = !empty($request->url_secop) ? $request->url_secop : NULL;
            $request->supervisor = !empty($request->supervisor) ? $request->supervisor : NULL;
            $request->accion = !empty($request->accion) ? $request->accion : NULL;
            $request->accion = !empty($request->accion) ? $request->accion : NULL;
            $request->polizas = !empty($request->polizas) ? $request->polizas : NULL;
        }else{
            $this->validate($request, [
                'id_oasis' => 'required|integer|numeric|unique:documento_base,id_oasis,'.$id.'',
                'consecutivo_cpe' => 'required|regex:/^([0-9]){3}-([0-9]){2}[A-Z]{0,1}$/|unique:documento_base,consecutivo_cpe,'.$id.'',
                'fecha_suscripcion' => 'required|date',
                'fecha_legalizacion' => 'required|date',
                'rp' => 'required|numeric|integer',
                'fecha_rp' => 'required|date',
                'url_secop' => 'required|url',
                'supervisor' => 'required|exists:persona,id',
                'polizas.*.aseguradora' => 'required',
                'polizas.*.numero_poliza' => 'required|numeric|integer',
                'polizas.*.fecha_expedicion' => 'required|date',
                'polizas.*.garantia' => 'required',
                'polizas.*.garantia.*.amparo' => 'required',
                'polizas.*.garantia.*.valor_amparo' => 'required|numeric|min:0',
                'polizas.*.garantia.*.fecha_inicio' => 'required|date',
                'polizas.*.garantia.*.fecha_fin' => 'required|date|after:polizas.*.garantia.*.fecha_inicio',
            ],[
                'fecha_suscripcion.required' => 'La fecha de suscripcion es requerida.',
                'fecha_polizas.required' => 'La fecha de polizas es requerida.',
                'rp.required' => 'El RP es requerido.',
                'fecha_rp.required' => 'La fecha del RP es requerida.',
                'supervisor.required' => 'El supervisor es requerido.',
                'supervisor.exists' => 'El supervisor no existe en la base de datos.',
                'polizas.*.garantia.*.fecha_fin.after' => 'La fecha de terminación :attribute debe ser mayor a la de inicio',
            ]);
        }

        $transaccion = DB::transaction(function() use ($contrato, $request){
            $contrato->consecutivo_cpe = $request->consecutivo_cpe;
            $contrato->id_oasis = $request->id_oasis;
            $contrato->rp = $request->rp;
            $contrato->fecha_suscripcion = $request->fecha_suscripcion;
            $contrato->fecha_legalizacion = $request->fecha_legalizacion;
            $contrato->fecha_rp = $request->fecha_rp;
            $contrato->url_secop = $request->url_secop;
            $contrato->supervisor = $request->supervisor;
            $contrato->polizas()->delete();
            if ($request->accion == 1) {
                //$contrato->estado_doc = 6;//estado legalizado
            } elseif($request->accion == 2){
                $contrato->estado_doc = 6;//estado legalizado
            }
            
            if ($request->polizas) {
                foreach ($request->polizas as $key => $poliza) {
                    $pol = new Poliza;
                    $pol->aseguradora = !empty($poliza['aseguradora']) ? $poliza['aseguradora'] : NULL;
                    $pol->numero_poliza = !empty($poliza['numero_poliza']) ? $poliza['numero_poliza'] : NULL;
                    $pol->fecha_expedicion = !empty($poliza['fecha_expedicion']) ? $poliza['fecha_expedicion'] : NULL;
                    $contrato->polizas()->save($pol);
                    if ($poliza['garantia']) {
                        foreach ($poliza['garantia'] as $key => $garantia) {
                            $ga = new Garantia;
                            $ga->amparo = !empty($garantia['amparo']) ? $garantia['amparo'] : NULL;
                            $ga->valor_amparo = !empty($garantia['valor_amparo']) ? $garantia['valor_amparo'] : NULL;
                            $ga->fecha_inicio = !empty($garantia['fecha_inicio']) ? $garantia['fecha_inicio'] : NULL;
                            $ga->fecha_fin = !empty($garantia['fecha_fin']) ? $garantia['fecha_fin'] : NULL;
                            $pol->garantias()->save($ga);
                        }
                    }
                }
            }
            $contrato->save();
            if ($request->accion == 2) {
                if ($contrato->super) {
                    //throw new \Exception('Error');
                    $contrato->super->notify(new ContratoAsignado($contrato));
                }else{
                    //throw new \Exception('Error');
                    Auth::user()->notify(new NoSupervisorUser());    
                }
            }
        });
        //Contrato guardado
        if ($request->wantsJson()) {
            return \Response::json([
                'success' => true,
                'mensaje' => "Datos guardados",
            ], 201);//Created
        }
        return redirect('contratacion/contratos');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    

    public function showSuper($id)
    {
        // return view('contratos.contrato', ['contrato' => Contrato::where('supervisor', Auth::user()->persona_id)->Where('id', $id)->where('estado_doc', 6)->firstOrFail() ]);
        return view('contratos.contrato', ['contrato' => DocumentoBase::where('supervisor', Auth::user()->persona_id)->Where('id', $id)->where('estado_doc', 6)->firstOrFail() ]);

    }

    public function plazo_ejecucion($id, Request $request)
    {
        // $contrato = Contrato::findOrFail($id);
        $contrato = DocumentoBase::findOrFail($id);
        if ($contrato->fecha_inicio && $contrato->fecha_terminacion) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya tiene fechas de inicio y terminacion definidas."]);
        }
        
        return view('supervision.plazo-ejecucion');
    }    

    public function store_plazo_ejecucion($id, Request $request)
    {
        // $contrato = Contrato::findOrFail($id);
        $contrato = DocumentoBase::findOrFail($id);
        if ($contrato->fecha_inicio && $contrato->fecha_terminacion) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya tiene fechas de inicio y terminacion definidas."]);
        }
        $this->validate($request, [
            'fecha_inicio' => 'required|fecha_mayor_que:fecha_fin',
            'fecha_fin' => 'required',
        ],[
            'fecha_inicio.required' => 'La fecha de inicio es requerida',
            'fecha_fin.required' => 'La fecha de terminación es requerida',
            'fecha_inicio.fecha_mayor_que' => 'La fecha de terminación debe ser mayor que la de inicio.',
        ]);
        
        $transaccion = DB::transaction(function() use ($contrato, $request){
            $contrato->fecha_inicio = $request->fecha_inicio;
            $contrato->fecha_terminacion = $request->fecha_fin;
            $contrato->save();
        });

        if ($request->wantsJson()) {
            return \Response::json([
                'success' => true,
                'mensaje' => "Fechas guardadas",
            ], 201);//Created
        }
        return redirect('supervision/contratos/'.$id);
    }    

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */

    public function show_reversar($id){
        $documento = DocumentoBase::findOrFail($id);
        $contrato = NULL;

        if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
        }

        if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
            $contrato = Contrato::findOrFail($id);
        }
        unset($documento);
        return view('contratos.reversar')->with('contrato', $contrato);
    }
    
    public function reversar_aprobado(Request $request, $id){
        $documento = DocumentoBase::findOrFail($id);
        $contrato = NULL;

        if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
        }

        if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
            $contrato = Contrato::findOrFail($id);
        }
        unset($documento);
        
        if ($contrato->estado_doc == 5) {
            $contrato->estado_doc = 4;
            $contrato->observaciones = $request->observaciones;
            $contrato->save();
            if ($request->wantsJson()) {
                return \Response::json([
                    'success' => true,
                    'mensaje' => "Documento Revertido",
                ], 201);//Created
            }
            return redirect('contratacion/contratos/'.$id);
        } else {
            if ($request->wantsJson()) {
                return response("El contrato debe estar aprobado para poder reversarse. Estado: ".$contrato->estado_documento->estado, 200);
                //return \Response::json([
                //    'success' => true,
                //    'mensaje' => "El contrato debe estar aprobado para poder reversarse. Estado: ".$contrato->estado_documento->estado,
                //], 403);
            }
            return redirect('contratacion/contratos/'.$id);
        }
        
    }


    public function destroy($id)
    {
        //
    }

    public function asignarRp()
    {

    }
    
    public function registrarRP()
    {

    }

    public function ejecucion()
    {

    }

    public function registrarAvance($id)
    {
        $documento = DocumentoBase::findOrFail($id);
        $this->authorizeForUser(Auth::user(), 'supervisar', $documento);
        // $contrato = NULL;

        // if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
        //     $contrato = CartaAdjudicacion::findOrFail($id);
        // }

        // if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
        //     $contrato = Contrato::findOrFail($id);
        // }
        // unset($documento);
        return view('supervision.registro_ejecucion')->with('contrato', $documento);
    }

    public function registrarAvanceMes($id, Request $request)
    {
        //$this->authorizeForUser(Auth::user(), 'supervisar', $doc);
        $this->validate($request, [
            'mes' => 'required',
            'productos' => 'required|array',
            //'pagos' => 'required|array',
        ]);
        //var_dump($request->all());
        return response()->json([
            'success' => true,
            'id' => $id,
            'data' => \json_encode($request->all())
        ]);
        //exit();
        
        $documento = DocumentoBase::findOrFail($id);
        $contrato = NULL;

        if (is_a($documento, 'App\CartaAdjudicacion') && $documento->tipo == 'carta_adjudicacion') {
            $contrato = CartaAdjudicacion::findOrFail($id);
        }

        if (is_a($documento, 'App\Contrato') && $documento->tipo == 'contrato') {
            $contrato = Contrato::findOrFail($id);
        }
        unset($documento);
        return view('supervision.registro_ejecucion')->with('contrato', $contrato);
    }

    public function SupervisionDataApi($id){
        $contrato = DocumentoBase::find($id);
        //return $contrato->informacion_supervision();
        return response()->json($contrato->informacion_supervision())->setEncodingOptions(JSON_NUMERIC_CHECK);
    }


    public function storeRegistroApi(Request $request, $id)
    {
        $doc = DocumentoBase::findOrFail($id);

        $this->authorizeForUser(Auth::user(), 'supervisar', $doc);
        
        $this->ValidarRegistroAvanceNuevo($request, $doc);

        $transaccion = DB::transaction(function() use ($doc, $request){
            $registro = new RegistroAvance();
            $registro->orden = null;
            $registro->fecha_registro = $request->fecha;
            
            $productosRequest = $request->productos;

            $pids = array_map(function($p){
                return $p['producto_id'];
            }, $productosRequest);
            
            $doc->registro_avance()->save($registro);
            foreach ($doc->productos as $producto) {
                $productoAvance = new AvanceProducto();
                if(in_array($producto->id, $pids)){
                    $id = $producto->id;
                    $dataProductoAvance = array_filter($productosRequest, function($productoRequest) use($id){
                        return $productoRequest['producto_id'] == $id;
                    });
                    
                    $dataProductoAvance = array_values($dataProductoAvance);
                    
                    $productoAvance->cantidad_registrada = $dataProductoAvance[0]['cantidad_registrada'];
                    $productoAvance->observaciones = $dataProductoAvance[0]['observaciones'];
                }else{
                    $productoAvance->cantidad_registrada = 0;
                    $productoAvance->observaciones = null;
                }
                $productoAvance->producto()->associate($producto);
                $productoAvance->registro()->associate($registro);
                $productoAvance->save();
            };
            $this->validarTotalPagado($request, $doc);
            $pagos = $request->pagos;            
            foreach ($pagos as $pago) {
                $nuevopago = new SolicitudPago();
                $nuevopago->numero_factura = $pago['numero_factura'];
                $nuevopago->cdp = $pago['cdp'];
                $nuevopago->valor_solicitado = $pago['valor_solicitado'];
                $nuevopago->observaciones = $pago['observaciones'];
                $nuevopago->registro()->associate($registro);
                $nuevopago->save();
            }
        }); 

        return "Registro guardado correctamente";
    }

    public function updateRegistroApi(Request $request, DocumentoBase $contrato, $registroId)
    {
        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);
        
        $registro = $contrato->registro_avance()->findOrFail($registroId);

        if($registro->procesado == 1){
            return response("El registro no puede editarse porque ya está procesado", 422);
        }

        $this->updateRegistroFn($request, $contrato, $registroId);
        return "Registro actualizado";
    }
    

    public function procesarRegistroApi(Request $request, DocumentoBase $contrato, $registroId)
    {
        //\sleep(1);
        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);        
        $registro = $contrato->registro_avance()->findOrFail($registroId);
        
        //if($registro->procesado == 1){
        //    return response("El registro no puede editarse porque ya está procesado", 422);
        //}

        $this->updateRegistroFn($request, $contrato, $registroId);
        
        $registro->orden = ($contrato->ultimoNumeroInforme() + 1);
        $registro->procesado = 1;
        $registro->save();
        //return $registro;
        return "Registro Procesado, puede generar un informe.";
    }

    public function deleteRegistroApi(Request $request, DocumentoBase $contrato, $registroId)
    {
        //Checkear si el usuario esta autorizado para esto
        //throw new \Exception("Error totalmente intencional y a proposito", 1);
        
        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);
        
        $registro = $contrato->registro_avance()->find($registroId); 

        if($registro->procesado == 1){
            return response("El registro no puede eliminarse porque ya está procesado", 422);
        }
        
        $registro->delete();
        return 204;
        //return $registro;
    }


    public function ValidarRegistroAvanceNuevo(Request $request, DocumentoBase $doc)
    {
        //Habilitar fechas distintas a mes actual
        $fechaactual = new \DateTime($request->fecha);
        
        $hoy = clone $fechaactual;
        $primerDia = clone $fechaactual->modify("first day of this month"); 
        $ultimoDia = clone $fechaactual->modify("last day of this month"); 

        $primerDia->modify('-1 day');
        $ultimoDia->modify('+1 day');

        $today = $hoy->format('Y-m-d');
        $ini = $primerDia->format('Y-m-d');
        $fin = $ultimoDia->format('Y-m-d');

        $first = date("Y-m-d", strtotime("first day of this month"));
        $last = date("Y-m-d", strtotime("last day of this month"));
        
        $fechaUltimoRegistro = $doc->ultimoRegistroRatificado() == null ? '2000-01-01' : $doc->ultimoRegistroRatificado()->fecha_registro;

        $this->validate($request, [
            'pagos' => 'array',
            'pagos.*.valor_solicitado' => 'numeric',
            'pagos.*.numero_factura' => 'unique:solicitud_pagos,numero_factura',
            'productos' => 'array',
            'fecha' => 'required|date|after:'.$ini.'|after:'.$fechaUltimoRegistro.'|before:'.$fin.'',
        ]);
    }

    public function ValidarRegistroAvance(Request $request, DocumentoBase $doc, $regid)
    {
        // NO TENGO NI LA MAS REMOTA PUTA IDEA PARA QUE HICE ESTO PERO CREO QUE ES IMPORTANTE, LO MANTENDRE COMENTADO
        
        // //$fechaactual = new \DateTime();
        // //Habilitar fechas distintas a mes actual
        // $fechaactual = new \DateTime($request->fecha);
        
        // $hoy = clone $fechaactual;
        // $primerDia = clone $fechaactual->modify("first day of this month"); 
        // $ultimoDia = clone $fechaactual->modify("last day of this month"); 

        // $primerDia->modify('-1 day');
        // $ultimoDia->modify('+1 day');

        // $today = $hoy->format('Y-m-d');
        // $ini = $primerDia->format('Y-m-d');
        // $fin = $ultimoDia->format('Y-m-d');

        // $first = date("Y-m-d", strtotime("first day of this month"));
        // $last = date("Y-m-d", strtotime("last day of this month"));
        
        // $fechaUltimoRegistro = $contrato->ultimoRegistroRatificado() == null ? '1999-01-01' : $contrato->ultimoRegistroRatificado()->fecha_registro;
        
        
        //Habilitar fechas distintas a mes actual
        $fechaactual = new \DateTime($request->fecha);
        
        $hoy = clone $fechaactual;
        $primerDia = clone $fechaactual->modify("first day of this month"); 
        $ultimoDia = clone $fechaactual->modify("last day of this month"); 

        $primerDia->modify('-1 day');
        $ultimoDia->modify('+1 day');

        $today = $hoy->format('Y-m-d');
        $ini = $primerDia->format('Y-m-d');
        $fin = $ultimoDia->format('Y-m-d');

        $first = date("Y-m-d", strtotime("first day of this month"));
        $last = date("Y-m-d", strtotime("last day of this month"));
        
        $fechaUltimoRegistro = $doc->ultimoRegistroRatificado() == null ? '2000-01-01' : $doc->ultimoRegistroRatificado()->fecha_registro;

        $this->validate($request, [
            'pagos' => 'array',
            'pagos.*.valor_solicitado' => 'numeric',
            //'pagos.*.numero_factura' => 'unique:solicitud_pagos,numero_factura',
            'pagos.*.numero_factura' => 'validar_numero_factura:'.$regid,
            'productos' => 'array',
            'fecha' => 'required|date|after:'.$ini.'|after:'.$fechaUltimoRegistro.'|before:'.$fin.'',
        ],[
            'pagos.*.numero_factura.validar_numero_factura' => 'El numero de factura :nfactura ya está en uso.'
        ]);
    }


    public function validarTotalPagado(Request $request, DocumentoBase $contrato)
    {
        $yaPagado = $contrato->totalPagadoRegistros();
        $this->validate($request, [
            'pagos' => 'maximo_pago:'.$contrato->valor.','.$yaPagado.'',
        ]);
    }

    private function updateRegistroFn(Request $request, DocumentoBase $contrato, $registroId){
        
        $this->ValidarRegistroAvance($request, $contrato, $registroId);
        
        $transaccion = DB::transaction(function() use ($contrato, $request, $registroId){
            $registro = $contrato->registro_avance()->findOrFail($registroId);
            $registro->fecha_registro = $request->fecha;
            $productosRequest = $request->productos;

            $pids = array_map(function($p){
                return $p['producto_id'];
            }, $productosRequest);

            foreach ($contrato->productos as $producto) {
                $productoAvance = $registro->avance_productos()->where('producto_id', $producto->id)->first();
                if(in_array($producto->id, $pids)){
                    $id = $producto->id;
                    $dataProductoAvance = array_filter($productosRequest, function($productoRequest) use($id){
                        return $productoRequest['producto_id'] == $id;
                    });
                    
                    $dataProductoAvance = array_values($dataProductoAvance);
                    
                    $productoAvance->cantidad_registrada = $dataProductoAvance[0]['cantidad_registrada'];
                    $productoAvance->observaciones = $dataProductoAvance[0]['observaciones'];
                }else{
                    $productoAvance->cantidad_registrada = 0;
                    $productoAvance->observaciones = null;
                }
                //$productoAvance->producto()->associate($producto);
                //$productoAvance->registro()->associate($registro);
                $productoAvance->save();
            };
            $registro->save();
            //Pagos
            $this->validarTotalPagado($request, $contrato);
            
            $registro->solicitud_pagos()->delete();
            
            
            $pagos = $request->pagos;            
            foreach ($pagos as $pago) {
                $nuevopago = new SolicitudPago();
                $nuevopago->numero_factura = $pago['numero_factura'];
                $nuevopago->cdp = $pago['cdp'];
                $nuevopago->valor_solicitado = $pago['valor_solicitado'];
                $nuevopago->observaciones = $pago['observaciones'];
                $nuevopago->registro()->associate($registro);
                $nuevopago->save();
            }
        });
    }


    public function informeHtmlRegistroApi(DocumentoBase $contrato, $regid)
    {
        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);
        //$this->authorizeForUser(User::find(4), 'supervisar', $contrato);
        $registro = $contrato->registro_avance()->find($regid);
        if($registro->informe){
            return $registro->informe;
        }
        $informeVacio = new InformeSupervision();
        $informeVacio->contenido = $registro->InformeHtml();
        $informeVacio->confirmado = 0;
        $informeVacio->registro_id = $registro->id;
        $informeVacio->numero = $registro->orden;
        $informeVacio->cumplido = NULL;
        $informeVacio->soportes = NULL;
        return $informeVacio;
        //return $regid;
    }

    

    public function informeHtmlStoreApi(Request $request, DocumentoBase $contrato, $regid)
    {
        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);
        
        $registro = $contrato->registro_avance()->findOrFail($regid);
        
        if($registro->informe){
            \App::abort(403, 'El registro ya tiene un informe creado y no puede crear otro');
            //return \Response::make(['errors'=>'Error', 'message'=>'El informe ya fue creado'], 500);
        }        
        
        $nuevoInforme = new InformeSupervision();
        $nuevoInforme->numero = $request->numero;
        $nuevoInforme->contenido = $request->contenido;
        
        $this->save_facturas($request->facturas, $registro);
        
        $informe = $registro->informe()->save($nuevoInforme);
        
        $this->save_cumplidos_soportes($request, $informe);
    }

    public function informeHtmlUpdateApi(Request $request, DocumentoBase $contrato, $regid)
    {
        //return var_dump($request->all());

        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);
        
        $registro = $contrato->registro_avance()->findOrFail($regid);

        $informe = $registro->informe()->findOrFail($request->id);
        
        if ($informe->confirmado){
            \App::abort(403, 'El informe actualmente está procesado y no puede modificarse');
        }
        
        $informe->contenido = $request->contenido;
        
        $this->save_facturas($request->facturas, $registro);

        $this->save_cumplidos_soportes($request, $informe);

        $informe->save();
    }

    public function informeHtmlProcesarApi(Request $request, DocumentoBase $contrato, $regid)
    {
        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);
        
        $registro = $contrato->registro_avance()->findOrFail($regid);

        $informe = $registro->informe()->findOrFail($request->id);
        
        if ($informe->confirmado){
            \App::abort(403, 'El informe actualmente está procesado y no puede modificarse');
        }
        
        $informe->contenido = $request->contenido;

        $this->save_facturas($request->facturas, $registro);

        foreach ($registro->solicitud_pagos as $solpago) {
            if ($solpago->factura_file == NULL) {
                \App::abort(403, 'Debe adjuntar un documento en formato PDF por cada solicitud de pago registrada.');
            }
        }
        
        if($request->file('cumplido') == NULL && $informe->cumplido == NULL){
            \App::abort(403, 'El cumplido a satisfacción es un documento obligatorio.');
        }

        $this->save_cumplidos_soportes($request, $informe);
        
        $informe->confirmado = 1;
        $registro->estado_cont  = 0;
        $informe->save();
        $registro->save();

        User::find(1)->notify(new NuevoInformeProcesado($informe));
    }

    private function save_facturas($facturas, $registro){
        if ($facturas) {
            foreach ($facturas as $index => $factura) {
                $file = file_get_contents($factura['file']);
                if ($factura['file']->getClientMimeType() != "application/pdf") {
                    \App::abort(403, 'Los archivos deben ser pdf.');
                }
                $solp = $registro->solicitud_pagos()->findOrFail($factura['id']);
                $nombre = $factura['id']."-".$solp->numero_factura.".pdf";
                $solp->factura_file = $nombre;
                $solp->save();
                Storage::disk('facturas')->put($nombre, $file);
                unset($file);
                unset($solp);
            }
        }
    }

    private function save_cumplidos_soportes(Request $request, $informe){
        if($request->file('cumplido')){
            $file = file_get_contents($request->cumplido);
            $nombre = $informe->id." - Cumplido informe No {$informe->numero}.pdf";
            $informe->cumplido = $nombre;
            Storage::disk('cumplidos')->put($nombre, $file);
        }
        
        if($request->file('soportes')){
            $file = file_get_contents($request->soportes);
            $nombre = $informe->id." - Soportes informe No {$informe->numero}.pdf";
            $informe->soportes = $nombre;
            Storage::disk('soportes')->put($nombre, $file);
        }

        $informe->save();
    }


    public function generarinforme(DocumentoBase $contrato, $regid)
    {
        //return $contrato;// = DocumentoBase::find($id);
        //return $regid;
        $this->authorizeForUser(Auth::user(), 'supervisar', $contrato);
        $registro = $contrato->registro_avance()->findOrFail($regid);
        //return $id;
        return view('supervision.generarinforme', ['registro'=>$registro]);
    }

    public function vistaPreviaInforme(Request $request)
    {
        $html = $request->contenido;
        $this->generarInformePDF($html);
    }

    public function storeInformeApi(Request $request, RegistroAvance $registro)
    {
        return $registro;
    }

    public function validar_nuevo_doc(Request $request){
        $this->validate($request, [
            'tercero_id' => 'required|numeric|exist_composite:tercero,id,tipo_id,'.$request->tercero_tipo.'',
            'tercero_tipo' => 'required|numeric',
            'tipo_cto' => 'required|numeric|exists:tipo_contrato,id',
            'modalidad_cto' => 'required|numeric|exists:modalidad_seleccion,id',
            'valor_cto' => 'required|numeric|min:0',
            'cdp_cto' => 'required',
            'compra' => 'required',
            'compra.*' => 'required',
        ],[
            'tercero_id.required' => 'El tercero es obligatorio.',
            'tercero_id.exist_composite' => 'El tercero no existe.',
            'tipo_cto.exists' => 'El tipo de contrato seleccionado no existe.',
            'modalidad_cto.exists' => 'La modalidad seleccionada no existe.',
            'valor_cto.required' => 'Elvalor del contrato es obligatorio.',            
            'valor_cto.min' => 'Elvalor del contrato debe ser mínimo $0.',            
            'cdp_cto.required' => 'El CDP es obligatorio.',
        ]);
    }

    public function validar_cuerpo_cto(Request $request){
        $this->validate($request, [
            'encabezado' => 'required',
            'clausula' => 'key_in_array:objeto',
            'clausula' => 'key_in_array:obligaciones',
            'obligaciones.obligacion_gnrl' => 'required',
            'obligaciones.obligacion_espf' => 'required',
            'clausula.*' => 'required',
            'nombre_contratante' => 'required',
            'cargo_contratante' => 'required',
            'nombre_contratista' => 'required',
            'cargo_contratista' => 'required',
        ],[
            'encabezado.required' => 'El encabezado y consideraciones son obligatorias.',
            'clausula.key_in_array' => 'La clausulas de objeto y obligaciones son obligatorias. Falta: :param',
            'obligaciones.obligacion_gnrl.required' => 'Se necesita al menos una obligacion general',
            'obligaciones.obligacion_espf.required' => 'Se necesita al menos una obligacion especifica',
        ]);
    }

    public function validar_cuerpo_carta(Request $request){
        $this->validate($request, [
            'html_carta' => 'required',
        ],[
            'html_carta.required' => 'La carta necesita un contenido.',
        ]);
    }

    public function NotificarDocProcesado($id='')
    {
        $doc = DocumentoBase::findOrFail($id);
        $rol = \App\Rol::where('name', 'cc')->first();
        if ($rol->users()->count() > 0) {
            $users_cc = $rol->users()->get();
            foreach ($users_cc as $user_cc) {
                $user_cc->notify(new CheckDoc(url('contratacion/contratos/'.$id), $doc->tercero->NombreCompleto()));
            }
        }else{
            Auth::user()->notify(new NoCoordinador());

        }
    }

    public function ver_pdf($id){
        $doc = DocumentoBase::findOrFail($id);
        switch ($doc->tipo) {
            case 'carta_adjudicacion':
                $doc = CartaAdjudicacion::findOrFail($id);
                break;
            case 'contrato':
                $doc = Contrato::findOrFail($id);
                break;            
            default:
                # code...
                break;
        }
        return $doc->pdf();
    }



    //  #### ###### #####   ####   #### #### ####
    // ##  ##  ##   ##  ## ##  ## ##  ## ## ##  ##
    // ##  ##  ##   ##  ## ##  ## ##     ## ##
    // ##  ##  ##   #####  ##  ##  ####  ##  ####
    // ##  ##  ##   ####   ##  ##     ## ##     ##
    // ##  ##  ##   ## ##  ##  ## ##  ## ## ##  ##
    //  ####   ##   ##  ##  ####   #### #### ####

    public function crear_otrosi($id)
    {
        return "Crear otrsi al contrato ".$id;
    }

    public function guardar_otrosi(Request $request)
    {
        return "Guardar otrosi ".$id;
    }

}
