<?php

namespace App\Http\Controllers;

use App\DocumentoBase;
use App\Contrato;
use App\User;
use App\Persona;

use Illuminate\Http\Request;

class ImpresionesController extends Controller
{
    //ImpresionesController@ImprimirDocumentoBase
    public function ImprimirDocumentoBase($id)
    {
        $documento = DocumentoBase::findOrFail($id);
        ini_set('max_execution_time', 300);
        //$pdf = new \mPDF('c','A4','12','Arial', 22, 15, 1, 1, 5, 0);
        $param = [
			'mode' => 'c',
			'format' => 'A4',
			'default_font_size' => 12,
			'default_font' => 'Arial',
			'margin_left' => 22,
			'margin_right' => 15,
			'margin_top' => 1,
			'margin_bottom' => 1,
			'margin_header' => 5,
			'margin_footer' => 0,
			'orientation' => 'P',
        ];
        $pdf = new \Mpdf\Mpdf($param);
        $pdf->setAutoTopMargin = 'stretch';
        $pdf->setAutoBottomMargin = 'stretch';
        $header = \View::make('contratos.plantilla_pdf_encabezado',['contrato' => $documento])->render();
        $footer = \View::make('contratos.plantilla_pdf_pie')->render();
        $html = \View::make('contratos.plantilla_pdf_cuerpo', ['contrato' => $documento])->render();
        $pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);
        $pdf->WriteHTML($html);
        return $pdf->Output();
    }

    public function ImprimirMemorando($id)
    {
    	$documento = DocumentoBase::findOrFail($id);
    	//exit($documento->tercero);
        if ($documento->estado_doc != 6) {
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El no está legalizado"]);
        }
        ini_set('max_execution_time', 300);
        //$pdf = new \mPDF('c','A4','12','Arial', 35, 30, 42.5, 35, 2, 0);
        $param = [
			'mode' => 'c',
			'format' => 'A4',
			'default_font_size' => 12,
			'default_font' => 'Arial',
			'margin_left' => 22,
			'margin_right' => 15,
			'margin_top' => 1,
			'margin_bottom' => 1,
			'margin_header' => 5,
			'margin_footer' => 0,
			'orientation' => 'P',
        ];
        $pdf = new \Mpdf\Mpdf($param);
        //$pdf->setAutoTopMargin = 'stretch';
        //$pdf->setAutoBottomMargin = 'stretch';
        $header = \View::make('contratos.memorando_supervision.plantilla_encabezado')->render();
        $footer = \View::make('contratos.memorando_supervision.plantilla_pie')->render();
        $html = \View::make('contratos.memorando_supervision.plantilla_cuerpo', ['documento' => $documento])->render();
        //exit($html);
        $pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);
        $pdf->WriteHTML($html);
        return $pdf->Output();
    }

    public function UserInfo($id)
    {
		$user = User::with('persona')
		->with('roles')->get()->find($id);
		return $user->toJson();
    }

    public function PersonInfo($id)
    {
		$persona = Persona::with('user')->with('sexo')->with('cargo')->with('contrato')->get()->find($id);
		return $persona->toJson();
    }

    public function ContractInfo($id)
    {
		$doc = Contrato::with('clausulas')
		->with('obligaciones')
		->with('polizas')
		->with('productos')
		->with('compra')
		->with('proceso')
		->with('revisado_por')
		->with('proyectado_por')
		->with('super')
		->get()
		->find($id);
		return $doc->toJson();
   }
}
