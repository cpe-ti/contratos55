<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InformeSupervision;
use App\User;

use App\Notifications\InformeRechazado;

class InformesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:menu-gaf');
        $this->middleware('permission:consultar-informe', ['only' => ['show', 'showAPI', 'showAprobarCont', 'showAprobarTes']] );
        $this->middleware('permission:aprobar-rechazar-informe-cont', ['only' => ['aprobarContAPI', 'rechazarContAPI']] );
        $this->middleware('permission:aprobar-rechazar-informe-tes', ['only' => ['aprobarTesAPI', 'rechazarTesAPI']] );
        // $this->middleware('permission:consultar-informe', ['only' => ['show']] );
        // $this->middleware('permission:consultar-informe', ['except' => ['index', 'show']] );
        //$this->middleware('permission:menu-administrar');

        // aprobarTesAPI
        // aprobarContAPI
        // rechazarTesAPI
        // rechazarContAPI
    }

    public function index()
    {
        return view("gaf.informes.inicio");
    }

    public function show(InformeSupervision $informe)
    {
        return view('gaf.informes.informe', ['informe' => $informe]);
    }

    public function showAprobarCont(InformeSupervision $informe)
    {
        return view('gaf.informes.aprobarcont', ['informe_id' => $informe->id]);
    }

    public function showAprobarTes(InformeSupervision $informe)
    {
        if ($informe->registro->estado_cont != 1) {
            \App::abort(404, 'Aun no ha sido aprobado por cont');
        }
        return view('gaf.informes.aprobartes', ['informe_id' => $informe->id]);
    }

    public function create()
    {
        return "Crear";
    }

    public function store()
    {
        return "Guardar";
    }
    
    public function edit()
    {
        return "Mostrando Vista Edicion";
    }
    
    public function update()
    {
        return "Actualizando Edicion";
    }

    // APIS
    public function indexAPI()
    {
        $informes = InformeSupervision::all(['id','numero','confirmado','registro_id','created_at','updated_at'])->where('confirmado', 1);
        return $informes;
    }
    
    public function showAPI(InformeSupervision $informe)
    {
        $informe = $informe->makeHidden(['registro']);
        return $informe;
    }

    public function aprobarTesAPI(Request $request, InformeSupervision $informe)
    {
        if ($informe->aprobado_tes) {
            return "El informe ya esta aprobado, no puede aprobarse nuevamente";
        }
        $informe->registro->estado_tes = 1;
        $informe->registro->comentario_ayf = $request->comentarios;
        $informe->registro->save();
    }

    public function aprobarContAPI(Request $request, InformeSupervision $informe)
    {
        if ($informe->aprobado_cont) {
            return "El informe ya esta aprobado, no puede aprobarse nuevamente";
        }
        $informe->registro->estado_cont = 1;
        $informe->registro->comentario_ayf = $request->comentarios;
        $informe->registro->save();
    }

    public function rechazarTesAPI(Request $request, InformeSupervision $informe)
    {
        $informe->registro->estado_tes = 0;
        $informe->registro->estado_cont = 0;
        $informe->registro->procesado = 0;
        $informe->registro->comentario_ayf = $request->comentarios;
        $informe->registro->save();
        $informe->confirmado = false;
        $informe->save();
        $user = $informe->registro->contrato->super->user;
        $user->notify(new InformeRechazado($informe));
    }

    public function rechazarContAPI(Request $request, InformeSupervision $informe)
    {
        // return "Perras";
        if ($informe->aprobado_cont) {
            return "El informe ya esta aprobado, no puede rechazarse";
        }
        $informe->registro->estado_tes = 0;
        $informe->registro->estado_cont = 0;
        $informe->registro->procesado = 0;
        $informe->registro->comentario_ayf = $request->comentarios;
        $informe->registro->save();
        $informe->confirmado = false;
        $informe->save();
        $user = $informe->registro->contrato->super->user;
        $user->notify(new InformeRechazado($informe));
    }
}
