<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ObligacionesController extends Controller
{
    public function index()
    {
        return "Index";
    }

    public function show($id)
    {
        return "Mostrando OBL ".$id;
    }

    public function create()
    {
        return "Crear";
    }

    public function store()
    {
        return "Guardar";
    }
    
    public function edit()
    {
        return "Mostrando Vista Edicion";
    }
    
    public function update()
    {
        return "Actualizando Edicion";
    }
}
