<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DocumentoBase;
use App\Contrato;
use App\CartaAdjudicacion;
use App\Otrosi;

use App\Http\Requests;

class OtrosisController extends Controller
{
    public function index()
    {
        return view('otrosis.inicio');
    }

    public function showAll()
    {
        return view("otrosis.todos");
    }
    
    public function show($id)
    {
        return "Mostrando EM ".$id;
    }

    public function select()
    {
        $todos = DocumentoBase::otrosiables(['id','tipo','tercero_id', 'tipo_tercero', 'consecutivo_cpe', 'valor']);
        return view("otrosis.nuevo.seleccionar", ['contratos' => $todos]);
    }

    public function create($id)
    {
        $doc = DocumentoBase::otrosiables()->get($id);
        if ($doc == NULL) {
            abort(404);
        }
        
        $tipo = get_class($doc);
        switch ($tipo) {
            case 'App\Contrato':
                return view('otrosis.nuevo.crear_contrato', ['doc'=>$doc]);
                break;
            case 'App\CartaAdjudicacion':
                return view('otrosis.nuevo.crear_carta', ['doc'=>$doc]);
                break;            
            default:
                abort(404);
                break;
        }

        
        return view('otrosis.nuevo.crear', ['doc'=>$doc]);
    }

    public function store()
    {
        return "Guardar";
    }
    
    public function edit()
    {
        return "Mostrando Vista Edicion";
    }
    
    public function update()
    {
        return "Actualizando Edicion";
    }
}
