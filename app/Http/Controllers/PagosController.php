<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('permission:revisar-sol-pagos', ['only' => ['index']]);
        //$this->middleware('permission:menu-administrar');
    }

    public function index()
    {
        return view("gaf.pagos.inicio");
    }

    public function show($id)
    {
        return "Mostrando Proceso ".$id;
    }

    public function create()
    {
        return "Crear";
    }

    public function store()
    {
        return "Guardar";
    }
    
    public function edit()
    {
        return "Mostrando Vista Edicion";
    }
    
    public function update()
    {
        return "Actualizando Edicion";
    }
}
