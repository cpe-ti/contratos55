<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Persona;

use App\Sexo;

use App\Rol;

class PersonasController extends Controller
{
    public function index()
    {
        return view('personas.todas', ['personas' => Persona::all()]);
    }

    public function listado()
    {
        return "listado de m";
        //return view('terceros.lista', ['terceros' => Tercero::all()]);
    }
    
    public function create()
    {
        return view('personas.nueva', ['roles' => Rol::all(), 'sexo' => Sexo::all()]);
    }

    
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'persona_id' => 'required|digits_between:7,11|unique:persona,id',
            'primer_nombre' => 'required|alpha',
            'segundo_nombre' => 'alpha',
            'primer_apellido' => 'required|alpha',
            'segundo_apellido' => 'alpha',
            'sexo_id' => 'required|exists:sexo,id',
            'telefono' => 'regex:/^((([0-9]){3}([-\s])?){1,2}([0-9]){4})$/',
            'email' => 'required|email',
            'rol_predeterminado' => 'exists:roles,id',
        ],[
            'persona_id.unique' => 'Este número de identificación ya se encuentra registrado.',
            'persona_id.digits_between' => 'El número de identificación debe contener entre 7 y 11 dígitos.',
            'persona_id.required' => 'El número de identificación es obligatorio.',
            'rol_predeterminado.exists' => 'Este tipo de rol no existe en la base de datos.',
            'telefono.regex' => 'El telefóno tiene un formato inválido.<br>Formatos aceptados: xxx-xxxx o xxx-xxx-xxxx.',
        ]);        
        $persona = new Persona;
        $persona->id = $request->persona_id;
        $persona->primer_nombre = strtolower($request->primer_nombre);
        $persona->segundo_nombre = strtolower($request->segundo_nombre);
        $persona->primer_apellido = strtolower($request->primer_apellido);
        $persona->segundo_apellido = strtolower($request->segundo_apellido);
        $persona->sexo_id = $request->sexo_id;
        $persona->telefono = $request->telefono;
        $persona->email = $request->email;
        $persona->rol_predeterminado = $request->rol_predeterminado;
        $persona->save();

        return response()->json([
            "mensaje" => 'Nuevo Participante Creado Correctamente',
            "success" => true
        ], 200);
    }

    
    public function show($id)
    {
    	return view('personas.persona', ['persona' => Persona::findOrFail($id)]);
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update($id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
