<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Requests;

use App\Producto;

use App\DocumentoBase;
use App\Contrato;

use App\TipoUnidadProducto;

class ProductosController extends Controller
{
    //
    public function addProductos($id)
    {
        // $contrato = Contrato::findOrFail($id);
        $contrato = DocumentoBase::findOrFail($id);
        $unidades = TipoUnidadProducto::all();
        
        if($contrato->productos->count() > 0){
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => "El contrato ya tiene productos asignados"]);
        }
        return view('productos.agregar', ['contrato' => $contrato, 'unidades' => $unidades]);
    }
    
    public function saveProductos(Request $request, $id)
    {
        // $contrato = Contrato::findOrFail($id);
        $contrato = DocumentoBase::findOrFail($id);
        if($contrato->productos->count() > 0){
            if ($request->wantsJson()) {
                return \Response::json([
                    'success' => false,
                    'mensaje' => "Ya se crearon los productos",
                ], 403);
            }
            return Redirect::back()->withErrors(['msg', "Ya se crearon los productos"]);
        }
        $this->validate($request, [
            'producto.*.nombre' => 'bail|required',
            'producto.*.descripcion' => 'bail|required',
            'producto.*.unidad' => 'bail|required',
            'producto.*.cantidad' => 'bail|required|numeric|unidad_medida',
            'producto.*.ponderacion' => 'bail|required|numeric|between:0.01,100|total_ponderacion',
        ],[
            'producto.*.nombre.required' => ' El nombre es obligatorio',
            'producto.*.descripcion.required' => ' Ingrese una descripción',
            'producto.*.unidad.required' => ' Seleccione una unidad de medida',
            'producto.*.cantidad.required' => ' Debe ingresar una cantidad',
            'producto.*.cantidad.numeric' => ' La cantidad debe ser númerica',
            'producto.*.cantidad.unidad_medida' => ' Si la unidad de medida es % la cantidad debe ser 100',
            'producto.*.ponderacion.required' => ' Ingrese una ponderación',
            'producto.*.ponderacion.numeric' => ' la ponderación debe ser un numero',
            'producto.*.ponderacion.between' => ' La ponderación debe ser un valor entre 0.01 y 100',
            'producto.*.ponderacion.total_ponderacion' => ' La suma de la ponderación de todos los productos debe ser exactamente 100',
        ]);
        //Transaccion guardar productos
        //print_r($request->all());
        //exit(0);
        DB::transaction(function() use ($request, $id)
        {
            foreach ($request->all()['producto'] as $key => $value) {
                //Si es porcentaje convertir a decimal
                $value['cantidad'] = $value['unidad'] == 2 ? round($value['cantidad'], 2)/100 : $value['cantidad'];
                //Guardar producto
                $producto = new Producto;
                $producto->contrato_id = $id;
                $producto->nombre = $value['nombre'];
                $producto->descripcion = $value['descripcion'];
                $producto->unidad_id = $value['unidad'];
                $producto->cantidad = $value['cantidad'];
                $producto->ponderacion = (round($value['ponderacion'], 2)/100);
                if ($value['obligacion']) {
                    $producto->obligacion_id = $value['obligacion'];
                }
                
                $producto->save();
            }
        });
        if ($request->wantsJson()) {
            return response()->json([
                'success' => true,
                'mensaje' => 'Productos creados correctamente'
            ]);
        }
        return redirect('supervision/contratos/'.$id);
    }
}
