<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DocumentoBase;

use App\Contrato;


use Illuminate\Support\Facades\DB;


class ProgramacionFinancieraController extends Controller
{
    public function index()
    {
        return "Index";
    }

    public function show($id)
    {
        //return "Mostrando OBL ".$id;
    }

    public function create($id)
    {
        $doc = DocumentoBase::findOrFail($id);
        if ($doc->tiene_programacion_financiera()) {
            $mensaje = "El contrato ya tiene programacion financiera";
            //return view('errors.custom', ['titulo' => "Error", 'mensaje' => $mensaje]);
            return view('programacion.ver_programacion', ['contrato' => $doc]);
        }        
        return view('programacion.crear_programacion_financiera', ['contrato' => $doc]);
    }

    public function store(Request $request, $id)
    {
        $doc = DocumentoBase::findOrFail($id);
        if ($doc->tiene_programacion_financiera()) {
            if ($request->wantsJson()) {
                return \Response::json([
                    'success' => false,
                    'mensaje' => "Ya se creo una Programación",
                ], 403);
            }
            return Redirect::back()->withErrors(['msg', "Ya se creo una Programación"]);
        }
        $this->validate($request, [
            'pago.*.desembolso' => 'bail|required|numeric|mayor_que_menor_que:0,'.$doc->valor.'',
            'pago.*.producto' => 'bail|required',
            'pago.*.producto.*' => 'required|numeric|mayor_que:0',
            
        ],[
            'pago.*.desembolso.mayor_que_menor_que' => 'El valor del pago debe ser mayor que :numero_mayor_que y menor que :numero_menor_que',
            'pago.*.producto.required' => 'El pago debe tener al menos un producto', 
            'pago.*.producto.*.required' => 'El producto debe tener un valor',
            'pago.*.producto.*.mayor_que' => 'El valor del producto debe ser mayor que :mayor',           
            
        ]);
        //Evaluaciones con base de datos
        $this->validate($request, [
            'pago.*.producto.*' => 'key_exists:productos,id|pertenece_a:DocumentoBase,'.$id.',productos',
            'pago' => 'total_pagado:'.$doc->valor.'|total_productos:'.$id.'',
        ],[
            'pago.*.producto.*.key_exists' => 'El item :id no existe en la tabla de :table en nuestra base de datos',
            'pago.*.producto.*.pertenece_a' => 'El :modelo no tiene :relacion relacionado con el id :id',
            'pago.total_productos' => 'Deben usarsen todos los productos y sus cantidades exactas.',
            'pago.total_pagado' => 'La suma de todos los pagos debe ser :pagar la suma actual es :pagado.',
        ]);
        
        $pagos = $request->pago;
        //print_r($pagos);
        //exit();
        $transaccion = DB::transaction(function() use ($doc, $pagos){
            foreach ($pagos as $key => $pago) {
                $pagox = new \App\ProgramacionPago();                
                $pagox->desembolso = $pago['desembolso'];
                $pagox->orden = $key;
                $doc->pagos_programados()->save($pagox);
                foreach ($pago['producto'] as $producto_id => $cantidad) {
                    $producto = \App\Producto::find($producto_id);
                    $cantidad = $producto->unidad_id == 2 ? $cantidad/100 : $cantidad;// convertir a porcentaje x/100
                    $pagox->productos()->save( $producto, ['cantidad' => $cantidad]);
                }
            }
        });
        //Si todo salio OK
        return "Programación Guardada";
    }
    
    public function edit()
    {
        return "Mostrando Vista Edicion";
    }
    
    public function update()
    {
        return "Actualizando Edicion";
    }
}
