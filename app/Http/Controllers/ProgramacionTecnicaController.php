<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DocumentoBase;

use App\Contrato;

use App\Producto;

use App\ProgramacionProducto;

use Illuminate\Support\Facades\DB;

class ProgramacionTecnicaController extends Controller
{
    public function index()
    {
        return "Index";
    }

    public function show($id)
    {
        return "Mostrando OBL ".$id;
    }

    public function create($id)
    {
        $documento = DocumentoBase::find($id);
        if ($documento->tiene_programacion_tecnica()) {
            $mensaje = "El contrato ya tiene programacion técnica";
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => $mensaje]);
        }
        return view('programacion.crear_programacion_tecnica', ['contrato' => $documento]);
    }

    public function store(Request $request, $id)
    {
        $documento = DocumentoBase::findOrFail($id);
        if ($documento->tiene_programacion_tecnica()) {
            $mensaje = "El contrato ya tiene programacion técnica";
            if ($request->wantsJson()) {
                return \Response::json([
                    'success' => false,
                    'mensaje' => $mensaje,
                ], 403);
            }
            return view('errors.custom', ['titulo' => "Error", 'mensaje' => $mensaje]);
        }
        
        $data = json_decode($request->datos, TRUE);
        //Ordenar por mes
        //Reacomodar array dejando como llave el id del producto
        $programacion = Array();
        foreach ($data as $k=>$d) {
            foreach ($d as $p=>$c) {
                if (array_key_exists($p, $programacion)) {
                    $programacion[$p][$k] = $c;
                }else{
                    $programacion[$p] = [$k=>$c];
                }
            }
        }
        ksort($programacion);
        //print_r($programacion);
        unset($data);
        //Verificar si se usaron todos los productos
        if (!(count($programacion) == count($documento->productos))) {
            if ($request->wantsJson()) {
                return \Response::json([
                    'success' => false,
                    'mensaje' => "Faltan productor por prgramar",
                ], 403);
            }
            return Redirect::back()->withErrors(['msg', "Faltan productor por prgramar"]);
        }
        foreach ($programacion as $producto_id => $meses) {
            //Verificar que el producto pertenezca a este documento
            if (!(count($documento->productos->find($producto_id)))) {
                if ($request->wantsJson()) {
                    return \Response::json([
                        'success' => false,
                        'mensaje' => "El producto ".$producto_id." no pertenece a este contrato.",
                    ], 403);
                }
                return Redirect::back()->withErrors(['msg', 'El producto '.$producto_id." no pertenece a este contrato."]);
            }            
            $suma = 0;
            //Comprobar que no exista una programacion previa para este producto
            $producto = Producto::findOrFail($producto_id);
            if ($producto->esta_programado()) {
                if ($request->wantsJson()) {
                    return \Response::json([
                        'success' => false,
                        'mensaje' => "Ya hay una programacion para uno o mas productos seleccionados.",
                    ], 403);
                }
                return Redirect::back()->withErrors(['msg', "Ya hay una programacion para uno o mas productos seleccionados."]);
            }
            //Realizar la suma de las cantidades
            foreach ($meses as $mes => $cantidad) {
                $suma += $cantidad;
            }
            //Verificar que todas las cantidades hayan sido usadas
            if (!($producto->cantidad == ($suma/($producto->unidad_id == 2 ? 100 : 1)))) {
                if ($request->wantsJson()) {
                    return \Response::json([
                        'success' => false,
                        'mensaje' => "Debe usar todas las cantidades de cada producto",
                    ], 403);
                }
                return Redirect::back()->withErrors(['msg', "Debe usar todas las cantidades de cada producto"]);
            }
        }

        //Pasadas las verificaciones se procede a guardar
        //exit(0);
        $transaccion = DB::transaction(function() use ($programacion){
            foreach ($programacion as $id_p => $meses) {
                $producto = Producto::findOrFail($id_p);
                foreach ($meses as $mes => $cantidad) {
                    $pt = new ProgramacionProducto();
                    $pt->fecha = $mes;
                    $pt->cantidad = $producto->unidad_id == 2 ? ($cantidad/100) : $cantidad;
                    $producto->programacion_tecnica()->save($pt);
                }
            }
        });
        //Si la transaccion es exito mostrar mensaje
        if ($request->wantsJson()) {
            return \Response::json([
                'success' => true,
                'mensaje' => "Programación guardada!",
            ], 202);
        }
        return redirect()->back()->with('message', 'Programación guardada!');
    }
    
    public function edit()
    {
        return "Mostrando Vista Edicion";
    }
    
    public function update()
    {
        return "Actualizando Edicion";
    }
}
