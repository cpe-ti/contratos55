<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportesController extends Controller
{
    //
    public function index()
    {
        return view('reportes.inicio');
    }

    public function auditoria()
    {
        return view('reportes.auditoria.inicio');
    }
    public function contratacion()
    {
        return view('reportes.contratacion.inicio');
    }
    public function supervision()
    {
        return view('reportes.supervision.inicio');
    }

    public function presupuesto()
    {
        return view('reportes.presupuesto.inicio');
    }
}
