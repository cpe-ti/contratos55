<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Tercero;

use App\TipoPersona;

use Illuminate\Support\Facades\DB;

class TercerosController extends Controller
{
    
    public function index()
    {
        return view('terceros.todos', ['terceros' => Tercero::all()]);
    }

    public function listado()
    {
        //return view('contratos.todos', ['contratos' => Contrato::where('supervisor', Auth::user()->persona_id)->get()]);
        return view('terceros.lista', ['terceros' => Tercero::all()]);
    }
    
    public function create()
    {
        return view('terceros.nuevo', ['tipos' => TipoPersona::all()]);
    }

    
    public function store(Request $request)
    {
        //exit( print_r($request->all()) );
        $this->validate($request, [
            'id' => 'required|digits_between:7,11|numeric|unique:tercero,id,null,null,tipo_id,'.$request->tipo_id.'',
            'tipo_id' => 'required|exists:tipo_persona,id|numeric',
            'primer_nombre' => 'alpha|required_if:tipo_id,1|required_without_all:razon_social',
            'segundo_nombre' => 'alpha',
            'primer_apellido' => 'alpha|required_if:tipo_id,1|required_without_all:razon_social',
            'segundo_apellido' => 'alpha',
            'razon_social' => '',
            'direccion' => 'required',
            //'telefono' => 'regex:/^((([0-9]){3}([-\s])?){1,2}([0-9]){4})$/',
            'telefono' => 'required',
            'email' => 'required|email',
            'representante' => 'required_if:tipo_id,2|regex:/^[\pL\s\-]+$/u',
            'id_representante' => 'required_if:tipo_id,2|numeric',
        ],[
            'id.required' => 'Id Requerido (NIT o Cédula)',
            'id.unique' => 'Este tercero ya existe como '.TipoPersona::find($request->tipo_id)->tipo.'',
            'primer_nombre.required_if' => 'Primer Nombre es requerido cuando la Persona es Natural.',
            'primer_apellido.required_if' => 'Primer Apellido es requerido cuando la Persona es Natural.',
            'primer_nombre.required_without_all' => 'Primer Nombre es requerido cuando "Razón Social" no esta presente.',
            'segundo_nombre.required' => '',
            'primer_apellido.required_without_all' => 'Primer Apellido es requerido cuando "Razón Social" no esta presente.',
            'segundo_apellido.required' => '',
            'razon_social.required' => '',
            'telefono.required' => 'Telefono obligatorio',
            'email.email' => 'El correo no es válido',
            'email.required' => 'El correo es obligatorio',
            'direccion.required' => 'La dirección es obligatoria',
            'representante.required_if' => 'El representante legal es obligatorio cuando la persona es Jurídica',
            'id_representante.required_if' => 'El representante legal es obligatorio cuando la persona es Jurídica',
        ]);
        //exit( print_r($request->all()) );
        $transaccion = DB::transaction(function() use ($request){
            $tercero = new Tercero;
            $tercero->id = $request->id;
            $tercero->tipo_id = $request->tipo_id;
            $tercero->primer_nombre =  mb_strtolower($request->primer_nombre);
            $tercero->segundo_nombre = mb_strtolower($request->segundo_nombre);
            $tercero->primer_apellido = mb_strtolower($request->primer_apellido);
            $tercero->segundo_apellido = mb_strtolower($request->segundo_apellido);
            $tercero->razon_social = mb_strtolower($request->razon_social);
            $tercero->direccion = mb_strtoupper($request->direccion);
            $tercero->telefono = mb_strtoupper($request->telefono);
            $tercero->email = mb_strtolower($request->email);
            $tercero->representante = $request->tipo_id == 2 ? mb_strtolower($request->representante) : NULL;
            $tercero->id_representante = $request->tipo_id == 2 ? $request->id_representante : NULL;
            $tercero->save();
        });
        if ($request->wantsJson()) {
            return response()->json([
                'success' => true,
                'mensaje' => 'Nuevo tercero creado correctamente'
            ]);
        }
        return redirect('terceros');
    }

    public function show($id, $tipo)
    {
        return view('terceros.tercero', ['tercero' => Tercero::where('id', '=', $id)->where('tipo_id', '=', $tipo)->first()]);
    }
    
    //tercero persona natural
    public function showN($id)
    {
    	return view('terceros.tercero', ['tercero' => Tercero::where('id', '=', $id)->where('tipo_id', '=', 1)->first()]);
    }
    
    //tercero persona juridica
    public function showJ($id)
    {
        return view('terceros.tercero', ['tercero' => Tercero::where('id', '=', $id)->where('tipo_id', '=', 2)->first()]);
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update($id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }

    public function inline_nuevo()
    {
        return view('terceros.inline-nuevo', ['tipos' => TipoPersona::all()]);
    }
    public function save_inline_nuevo()
    {
        # code...
    }
}
