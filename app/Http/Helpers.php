<?php   
function ImgBase64($ruta) {        
    $tipo = pathinfo($ruta, PATHINFO_EXTENSION);
    $data = file_get_contents($ruta);
    $base64 = 'data:image/' . $tipo . ';base64,' . base64_encode($data);
    return $base64;
}
function svgRawContent($ruta) {        
    $svg = fopen($ruta, 'r');
    return stream_get_contents($svg);
}

function crearRuta(...$segmentos){
    return join(DIRECTORY_SEPARATOR, $segmentos);
}