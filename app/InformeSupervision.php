<?php

namespace App;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Support\Facades\DB;

class InformeSupervision extends Model implements AuditableContract
{
    use Auditable;
    protected $table = "informes_supervision";
    protected $guarded = [];

    protected $casts = [
        'id' => 'integer',
        'numero' => 'integer',
        'confirmado' => 'boolean',
        'registro_id' => 'integer',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'contenido',
    // ];

    //Custom attribute
    protected $appends = ['sol_pagos', 'cumplido_url', 'soportes_url', 'aprobado_tes', 'aprobado_cont'];
    
    public function registro()
    {
        return $this->belongsTo('App\RegistroAvance', 'registro_id', 'id');
    }

    public function getSolPagosAttribute()
    {
        return $this->registro->solicitud_pagos;
    }

    public function getCumplidoUrlAttribute()
    {
        if($this->cumplido){
            return route('cumplido', ['pdf'=>$this->cumplido]);
        }
        return NULL;
    }

    public function getAprobadoContAttribute()
    {
        return (bool) $this->registro->estado_cont;
    }

    public function getAprobadoTesAttribute()
    {
        return (bool) $this->registro->estado_tes;
    }

    public function getSoportesUrlAttribute()
    {
        if($this->soportes){
            return route('soportes', ['pdf'=>$this->soportes]);
        }
        return NULL;
    }
}