<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Justificacion extends Model
{
    //nombre de tabla
    protected $table = 'justificaciones';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function cdp()
    {
        return $this->belongsTo('App\ProvisionPresupuestal');
    }
}
