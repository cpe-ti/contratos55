<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ModalidadSeleccion extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'modalidad_seleccion';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];
    
    //relaciones
    public function contrato()
    {
        return $this->hasMany('App\Contrato');
    }

    public function compra()
    {
        return $this->hasMany('App\Compra');
    }
}
