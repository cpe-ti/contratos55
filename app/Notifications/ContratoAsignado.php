<?php

namespace App\Notifications;

use App\DocumentoBase;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContratoAsignado extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(DocumentoBase $documento)
    {
        $this->doc = $documento;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //$pdf = new \mPDF('c','A4','12','Arial', 35, 30, 42.5, 35, 2, 0);
        $param = [
			'mode' => 'c',
			'format' => 'A4',
			'default_font_size' => 12,
			'default_font' => 'Arial',
			'margin_left' => 22,
			'margin_right' => 15,
			'margin_top' => 1,
			'margin_bottom' => 1,
			'margin_header' => 5,
			'margin_footer' => 0,
			'orientation' => 'P',
        ];
        $pdf = new \Mpdf\Mpdf($param);
        $pdf->setAutoTopMargin = 'stretch';
        $pdf->setAutoBottomMargin = 'stretch';
        $header = \View::make('contratos.memorando_supervision.plantilla_encabezado')->render();
        $footer = \View::make('contratos.memorando_supervision.plantilla_pie')->render();
        $html = \View::make('contratos.memorando_supervision.plantilla_cuerpo', ['documento' => $this->doc])->render();
        $pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);
        $pdf->WriteHTML($html);
        
        return (new MailMessage)
                    ->subject('Ha sido asignado(a) como supervisor de un contrato.')
                    ->line('Usted ha sido designado como supervisor del contratro '.$this->doc->consecutivo_cpe." - ".$this->doc->tercero->NombreCompleto())
                    ->line('Adjunto encontrara la carta de designación de supervisión.')
                    ->action('Ver Contrato', route('supervision.contrato', ['id' => $this->doc->id]))
                    ->line('Gracias por usar ContractCPE!')
                    ->attachData($pdf->Output('', 'S'), 'Memorando_de_asignación.pdf');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
