<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\InformeSupervision;

class InformeRechazado extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(InformeSupervision $informe)
    {
        $this->informe = $informe;
        $this->consecutivo = $informe->registro->contrato->consecutivo_cpe;
        $this->contrato = $informe->registro->contrato->NombreRapido();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $idcto = $this->informe->registro->contrato->id;
        $ruta = route('supervision.registrar-avance', ['id'=>$idcto]);
        $comentarios = $this->informe->registro->comentario_ayf;
        return (new MailMessage)
                    ->line('Su informe de supervision fue rechazado: '.$this->contrato)
                    ->line('Comentarios: '.$comentarios)
                    ->line('Para ver el informe en el aplicativo haga click en el boton: ')
                    ->action('informe de supervisión N° '.$this->informe->numero.' del contrato '.$this->consecutivo, $ruta)
                    ->line('Gracias!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
