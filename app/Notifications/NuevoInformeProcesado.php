<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\InformeSupervision;

class NuevoInformeProcesado extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(InformeSupervision $informe)
    {
        $this->informe = $informe;
        $this->consecutivo = $informe->registro->contrato->consecutivo_cpe;
        $this->contrato = $informe->registro->contrato->NombreRapido();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Se ha procesado un nuevo informe de supervisión para el contrato: '.$this->contrato)
                    ->line('Para ver el informe en el aplicativo haga click en el boton: ')
                    ->action('informe de supervisión N° '.$this->informe->numero.' del contrato '.$this->consecutivo, 'https://laravel.com')
                    ->line('Gracias!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
