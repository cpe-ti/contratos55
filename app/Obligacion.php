<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Obligacion extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'obligaciones';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function tipo()
    {
        return $this->belongsTo('App\TipoObligacion', 'tipo_obligacion_id', 'id');
    }

    public function contrato()
    {
        return $this->belongsTo('App\DocumentoBase', 'contrato_id', 'id');
    }

    public function Productos()
    {
        return $this->hasMany('App\Producto', 'obligacion_id', 'id');
    }
}
