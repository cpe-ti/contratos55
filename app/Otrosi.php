<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

class Otrosi extends DocumentoBase
{
    protected static $singleTableType = 'otrosi';

	// //Custom attribute
    // protected $appends = ['tipo_padre', 'tipo_documento']; 

	// public function getTTipoPadre()
    // {
    //     return '$this';
    // }

	// //Custom attribute
    // //protected $appends = ['tipo_documento']; 

	// public function getTipoDocumento()
    // {
	// 	$clase = get_class($this);
	// 	switch ($clase) {
	// 		case 'App\Contrato':
	// 			return "Contrato";
	// 			break;
	// 		case 'App\CartaAdjudicacion':
	// 			return "Carta de Adjudicación";
	// 			break;
	// 		case 'App\Otrosi':
	// 			return "Otrosí";
	// 			break;			
	// 		default:
	// 			return "Documento Base";
	// 			break;
	// 	}
	// 	return 'palito';
    // }

	//
	//Crear un otrosi a partir del Documento base pasado como reporte
	public static function crear(DocumentoBase $documento)
	{
		$otrosi  = $documento->replicate();
        return $otrosi;
	}
	
	public function documento()
	{
		return $this->belongsTo('App\DocumentoBase', 'contrato_id', 'id');
	}

	public function GetTipoClase()
	{
		return $this->tipo_padre;
	}

}
