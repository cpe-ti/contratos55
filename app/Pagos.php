<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Pagos extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'pagos';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function contrato()
    {
        return $this->belongsTo('App\Contrato');
    }
}
