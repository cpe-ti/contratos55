<?php 

namespace App;

use Zizaco\Entrust\EntrustPermission;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Permiso extends EntrustPermission implements AuditableContract
{
	use Auditable;
	public function grupo()
    {
        return $this->belongsTo('App\Grupo');
    }
}