<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Persona extends Model implements AuditableContract
{
    use Auditable;
    use Notifiable;
    //nombre de tabla
    protected $table = 'persona';

    //problema con bigint id primary key
    protected $casts = ["id"=>"bigInt"];

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //Relaciones
    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function sexo()
    {
        return $this->belongsTo('App\Sexo');
    }

    public function cargo(){
        return $this->belongsTo('App\Cargo');
    }    

    public function doc_revisado()
    {
        return $this->hasMany('App\DocumentoBase', 'reviso', 'id');
    }
    
    public function doc_proyectado()
    {
        return $this->hasMany('App\DocumentoBase', 'proyecto', 'id');
    }

    public function contrato()
    {
        return $this->hasMany('App\DocumentoBase', 'supervisor', 'id');
    }
    //Funciones y metodos especificos
    public function NombreCompleto() {
        return ucfirst($this->primer_nombre).($this->segundo_nombre == "" ? "" : " ".ucfirst($this->segundo_nombre))." ".ucfirst($this->primer_apellido).ucfirst($this->segundo_apellido == "" ? "" : " ".ucfirst($this->segundo_apellido));
    }

    public function NombreyCargo() {
        $pn = $this->primer_nombre;
        $sn = $this->segundo_nombre;
        $pa = $this->primer_apellido;
        $sa = $this->segundo_apellido;
        $w = "";
        switch ($this->sexo_id) {
            case 1:
                $w = $this->cargo->cargoM;
                break;
            case 2:
                $w = $this->cargo->cargoF;
                break;
            default:
                $w = $this->cargo->cargo;
                break;
        }
        $str = implode(' ', array($pn,$sn,$pa,$sa))." - ".$w;
        $str = preg_replace('/\s+/', ' ',$str);
        return ucwords($str);

        //return ucfirst($this->primer_nombre).($this->segundo_nombre == "" ? "" : " ".ucfirst($this->segundo_nombre))." ".ucfirst($this->primer_apellido).ucfirst($this->segundo_apellido == "" ? "" : " ".ucfirst($this->segundo_apellido));
    }

    public function proceso()
    {
        return $this->belongsToMany('App\Proceso', 'lideres_procesos', 'lider_id', 'proceso_id');
    }

    public function rol_pre()
    {
        return $this->rol_predeterminado;;
    }

    //public function routeNotificationForMail()
    //{
    //    return 'test@cpe.gov.co';
    //    return $this->email_address;
    //}
}
