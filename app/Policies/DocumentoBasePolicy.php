<?php

namespace App\Policies;

use App\User;
use App\DocumentoBase;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentoBasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the documentoBase.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentoBase  $documentoBase
     * @return mixed
     */
    public function view(User $user, DocumentoBase $documentoBase)
    {
        //
    }

    /**
     * Determine whether the user can create documentoBases.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the documentoBase.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentoBase  $documentoBase
     * @return mixed
     */
    public function update(User $user, DocumentoBase $documentoBase)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can delete the documentoBase.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentoBase  $documentoBase
     * @return mixed
     */
    public function delete(User $user, DocumentoBase $documentoBase)
    {
        //
        return $user->persona_id == $documentoBase->supervisor;
    }


    public function supervisar(User $user, DocumentoBase $documentoBase)
    {
        return $user->persona_id == $documentoBase->supervisor;
    }
}
