<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Poliza extends Model implements AuditableContract
{
    use Auditable;

    protected $table = "poliza";

    protected $guarded = [];

    public function documento()
	{
		return $this->belongsTo('App\DocumentoBase', 'doc_id', 'id');
	}

	public function garantias()
	{
		return $this->hasMany('App\Garantia', 'poliza_id', 'id');
	}
}
