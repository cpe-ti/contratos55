<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Presupuesto extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'rubro_presupuesto';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //Relaciones
    public function compra()
    {
        return $this->hasMany('App\Compra', 'rubro_id', 'id');
    }
	
	public function vigencia()
    {
        return $this->belongsTo('App\Vigencia', 'vigencia_id', 'id');
    }
}