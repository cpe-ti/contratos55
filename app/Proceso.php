<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Proceso extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'proceso';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    public function compra()
    {
        return $this->hasMany('App\Compra');
    }

    public function lider()
    {
        return $this->belongsToMany('App\Persona', 'lideres_procesos', 'proceso_id', 'lider_id');
    }
}
