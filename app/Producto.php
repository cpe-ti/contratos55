<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Producto extends Model implements AuditableContract
{
    use Auditable;
   	//nombre de tabla
    protected $table = 'productos';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //Custom attribute
    protected $appends = ['total_cant_pago', 'cantidad_formato']; 

    //protected $dateFormat = 'Y-m-d H:i';

    protected $casts = [
        'id' => 'integer',
        'contrato_id' => 'integer',
        'unidad_id' => 'integer',
        'cantidad' => 'double',
        'obligacion_id' => 'integer',
    ];

    public function getTotalCantPagoAttribute()
    {
        if($this->pivot == NULL){
            return false;
        }else{
            $orden = $this->programacion_pago()->find($this->pivot->pago_programado_id)->orden;
            
            $total = DB::table('documento_base')
            ->join('programacion_pagos', 'documento_base.id', '=', 'programacion_pagos.doc_id')
            ->join('pago_programado_productos', 'programacion_pagos.id', '=', 'pago_programado_productos.pago_programado_id')
            ->select(DB::raw('sum(pago_programado_productos.cantidad) as suma'))
            ->where('documento_base.id', '=', $this->contrato_id)
            ->where('pago_programado_productos.producto_id', '=', $this->id)
            ->where('programacion_pagos.orden', '<=', $orden)
            ->first()->suma;
            return $total;
        }
    }
    
    public function getCantidadFormatoAttribute(){
        return ($this->unidad->tipo == "porcentaje") ? ($this->cantidad * 100) : $this->cantidad;
    }



    //relaciones
    public function contrato()
    {
        return $this->belongsTo('App\DocumentoBase', 'contrato_id', 'id');
    }

    public function unidad()
    {
        return $this->belongsTo('App\TipoUnidadProducto', 'unidad_id', 'id');
    }

    public function programacion_tecnica()
    {
        return $this->hasMany('App\ProgramacionProducto', 'producto_id', 'id');   
    }

    public function obligacion()
    {
        return $this->belongsTo('App\Obligacion', 'obligacion_id', 'id');
    }

    public function programacion_pago()
    {
        return $this->belongsToMany('App\ProgramacionPago', 'pago_programado_productos', 'producto_id', 'pago_programado_id')->withPivot('cantidad');
    }
	
    public function esta_programado()
    {
        if (count($this->programacion_tecnica)) {
            return true;
        } else {
            return false;
        }        
    }

    public function esta_programado_pago()
    {
        if (count($this->programacion_pago)) {
            return true;
        } else {
            return false;
        }        
    }

    public function fecha_cumplimiento($cantidad = 0)
    {        
        print_r($this->programacion_tecnica);
    }

    public function porcentaje($cantidad = 0)
    {
        return (($cantidad*100)/$this->cantidad);
    }

    public function ponderacion($cantidad = 0)
    {
        return (($cantidad*100)/$this->cantidad) * $this->ponderacion;
    }

    //╦═╗╔═╗╔═╗╦╔═╗╔╦╗╦═╗╔═╗  ╔╦╗╔═╗  ╔═╗╦  ╦╔═╗╔╗╔╔═╗╔═╗  ╦ ╦  ╔═╗╦ ╦╔═╗╔═╗╦═╗╦  ╦╦╔═╗╦╔═╗╔╗╔
	//╠╦╝║╣ ║ ╦║╚═╗ ║ ╠╦╝║ ║   ║║║╣   ╠═╣╚╗╔╝╠═╣║║║║  ║╣   ╚╦╝  ╚═╗║ ║╠═╝║╣ ╠╦╝╚╗╔╝║╚═╗║║ ║║║║
	//╩╚═╚═╝╚═╝╩╚═╝ ╩ ╩╚═╚═╝  ═╩╝╚═╝  ╩ ╩ ╚╝ ╩ ╩╝╚╝╚═╝╚═╝   ╩   ╚═╝╚═╝╩  ╚═╝╩╚═ ╚╝ ╩╚═╝╩╚═╝╝╚╝
    public function avance()
    {
        return $this->hasMany('App\AvanceProducto', 'producto_id', 'id');
    }
}
