<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;



class ProgramacionPago extends Model implements AuditableContract
{
    use Auditable;
	
	protected $table = 'programacion_pagos';

    //protected $appends = ['total_pagado', 'productos_pago']; 
    protected $appends = ['total_pagado']; 

    public function getTotalPagadoAttribute(){
        $total = DB::table('programacion_pagos')
            ->select(DB::raw('sum(programacion_pagos.desembolso) as suma'))
            ->where('programacion_pagos.doc_id', '=', $this->doc_id)
            ->where('programacion_pagos.orden', '<=', $this->orden)
            ->first()->suma;
        return $total;
    }

    //public function getProductosPagoAttribute(){
    //    return $this->documento->productos;
    //}

    public function productos()
    {
        return $this->belongsToMany('App\Producto', 'pago_programado_productos', 'pago_programado_id', 'producto_id')->withPivot('cantidad');
    }

    public function documento()
    {
    	return $this->belongsTo('App\DocumentoBase', 'doc_id', 'id');
    }

    
}
