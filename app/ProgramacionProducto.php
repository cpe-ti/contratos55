<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ProgramacionProducto extends Model implements AuditableContract
{
	use Auditable;
	
	protected $table = 'programacion_productos';
	
	//Custom attribute
    protected $appends = ['total_cant_prog', 'cantidad_formato'];

	public function getTotalCantProgAttribute(){
        //return "Fuck you";
        //SELECT SUM(programacion_productos.cantidad) FROM `programacion_productos`
		//JOIN productos on programacion_productos.producto_id = productos.id
		//where programacion_productos.producto_id = 1 and fecha <= '2018-05-01'

		$total = DB::table('programacion_productos')
            ->join('productos', 'programacion_productos.producto_id', '=', 'productos.id')
            ->select(DB::raw('sum(programacion_productos.cantidad) as suma'))
            ->where('programacion_productos.producto_id', '=', $this->producto_id)
            ->where('programacion_productos.fecha', '<=', $this->fecha)
            ->first()->suma;
		return $total;
	}
	
	public function getCantidadFormatoAttribute(){
        return ($this->producto->unidad->tipo == "porcentaje") ? ($this->cantidad * 100) : $this->cantidad;
    }

	public function producto()
	{
		return $this->belongsTo('App\Producto', 'producto_id', 'id');
	}

	// public function contrato()
	// {
	// 	return $this->belongsTo('App\DocumentoBase', 'contrato_id', 'id');
	// }

	public function ponderado_mes()
	{
		return $this->producto->ponderacion($this->cantidad);
	}

	public function porcentaje_mes()
	{
		return $this->producto->porcentaje($this->cantidad);
	}
}
