<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
//use App\User;
use App\Persona;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!function_exists("buscar_llave")) {
            # code...
            function buscar_llave($array, $llave) {
                if (is_array($array)){
                    if (array_key_exists($llave, $array)){
                        return true;
                    }else{
                        foreach($array as $deep) {
                            if (buscar_llave($deep, $llave) === true){
                                return true;
                            };
                        }
                        return false;
                    }
                }else{
                    return false;
                }
            }
        }
        Validator::extend('key_in_array', function($attribute, $value, $parameters, $validator) {            
            $validator->addReplacer('key_in_array', function ($message, $attribute, $rule, $parameters) use ($value) {
                return str_replace(':param', $parameters[0], $message);
            });            
            return buscar_llave($value, $parameters[0]);
        });
        Validator::extend('fecha_mayor_que', function($attribute, $value, $parameters, $validator) {            
            return strtotime($validator->getData()[$parameters[0]]) > strtotime($value);
        });
        Validator::extend('persona_libre', function($attribute, $value, $parameters, $validator) {
            //Parametro[0] es el id que se exceptua
            $tipin = \App\Persona::find($value);
            if (array_key_exists(0, $parameters)) {
                if (is_null($tipin)) {
                    return true;
                }
                if (is_null($tipin->user)) {
                    return true;
                }
                if($tipin->id == $parameters[0]){
                    return true;
                }
            }
            if (is_null($tipin)) {
                return true;
            }
            if (is_null($tipin->user)) {
                return true;
            }
            return false;
        });
        //validacion de cantidad respecto a unidad de medida
        Validator::extend('unidad_medida', function($attribute, $value, $parameters, $validator) {
            $campo_name = $attribute;
            $temp = explode('.', $campo_name);
            unset($temp[count($temp) - 1]);
            $campo_name = implode('.', $temp).'.unidad';
            $unidad = array_get( $validator->getData(), $campo_name );
            return !($unidad == 2 && $value != 100);
        });
        //Validacion de suma total ponderada
        Validator::extend('total_ponderacion', function($attribute, $value, $parameters, $validator) {
            $p = array_get( $validator->getData(), 'producto' );
            $suma = 0;
            foreach ($p as $key => $value) {
                $suma += $value['ponderacion'];
            }
            return $suma == 100;
        });

        //Validar existencia llave compuesta
        Validator::extend('exist_composite', function($attribute, $value, $parameters, $validator) {
            //exit($attribute);
            //exit($value);
            $tabla = $parameters[0];
            $col1 = $parameters[1];
            $col2 = $parameters[2];
            $value2 = $parameters[3];
            //SELECT * FROM contratos.tercero WHERE (id, tipo_id) = (1057015139, 1);
            $terceros = DB::table($tabla)->where([
                [$col1, '=', $value],
                [$col2, '=', $value2],
            ])->count();

            return ($terceros > 0);
        });

        //validar algo aun no se que
        Validator::extend('exist_array', function($attribute, $value, $parameters, $validator) {            
            print_r($value);
            echo isset($value->producto) ? "Si" : "No";
            //echo array_key_exists('producto', $value) ? "Si" : "No";
            return false;            
        });
        //Validar mayor que
        Validator::extend('mayor_que', function($attribute, $value, $parameters, $validator) {            
            $validator->addReplacer('mayor_que', function($message, $attribute, $rule, $parameters) use ($validator) {
                $message = str_replace(':mayor', $parameters[0], $message);
                return $message;
            });
            return ($value > $parameters[0]);
        });
        //Validar menor que
        Validator::extend('menor_que', function($attribute, $value, $parameters, $validator) {            
            $validator->addReplacer('menor_que', function($message, $attribute, $rule, $parameters) use ($validator) {
                $message = str_replace(':menor', $parameters[0], $message);
                return $message;
            });
            return ($value > $parameters[0]);
        });
        //Validar mayor que menor que
        Validator::extend('mayor_que_menor_que', function($attribute, $value, $parameters, $validator) {            
            $validator->addReplacer('mayor_que_menor_que', function($message, $attribute, $rule, $parameters) use ($validator) {
                $message = str_replace(':numero_mayor_que', $parameters[0], $message);
                $message = str_replace(':numero_menor_que', $parameters[1], $message);
                return $message;
            });
            return ($value > $parameters[0] && $value <= $parameters[1]);
        });        
        //Validar que la llave de un array exista en una BD 
        Validator::extend('key_exists', function($attribute, $value, $parameters, $validator) {            
            $tmp = explode('.',$attribute);
            $key = end($tmp);
            $validator->addReplacer('key_exists', function($message, $attribute, $rule, $parameters) use ($validator, $key) {
                $message = str_replace(':table', $parameters[0], $message);
                $message = str_replace(':id', $key, $message);
                return $message;
            });
            $row = DB::table($parameters[0])
                ->where($parameters[1], '=', $key)
                ->first();
            if (is_null($row)) {
                return false;                
            } else {
                return true;
            }
        });        
        //Validar pertenecia de modelo
        Validator::extend('pertenece_a', function($attribute, $value, $parameters, $validator) {            
            $tmp = explode('.',$attribute);
            $key = end($tmp);
            $clase_padre = '\App\\'.$parameters[0];
            $relacion = $parameters[2];

            $validator->addReplacer('pertenece_a', function($message, $attribute, $rule, $parameters) use ($validator, $key) {
                $message = str_replace(':modelo', $parameters[0], $message);
                $message = str_replace(':relacion', $parameters[2], $message);
                $message = str_replace(':id', $key, $message);
                return $message;
            });
            $modelo_padre = $clase_padre::find($parameters[1]);            
            return count($modelo_padre->$relacion->find($key));
        });
        //Prueba
        Validator::extend('total_pagado', function($attribute, $value, $parameters, $validator) {            
            $suma = 0;
            foreach ($value as $key => $val) {
                $suma += $val['desembolso'];
            }
            $validator->addReplacer('total_pagado', function($message, $attribute, $rule, $parameters) use ($validator, $suma) {
                $message = str_replace(':pagado', "$ ".number_format($suma, 2, ',', '.'), $message);
                $message = str_replace(':pagar', "$ ".number_format($parameters[0], 2, ',', '.'), $message);
                return $message;
            });
            return $suma == $parameters[0];
        });

        //
        Validator::extend('total_productos', function($attribute, $value, $parameters, $validator) {            
            $productos = \App\DocumentoBase::findOrFail($parameters[0])->productos;
            $productos = $productos->keyBy('id');
            $doc_pid = $productos->pluck('cantidad','id')->toArray();
            $input_pid = array();
            //Normalizar las unidades porcentaje 1 a 100
            foreach ($doc_pid as $id => $cant) {
                $doc_pid[$id] = $productos->get($id)->unidad_id == 2 ? $doc_pid[$id]*100 : $doc_pid[$id]; 
            }

            unset($productos);

            //Sumar todos los productos enviados en el form
            foreach ($value as $key => $val) {
                foreach ($val['producto'] as $i => $v) {
                    if (array_key_exists($i,  $input_pid)) {
                        $input_pid[$i] += $v;
                    }else{
                        $input_pid[$i] = $v;
                    }
                }
            }
            //obtener la dferencia entre los arrays creados
            $diferencia = array_diff($doc_pid, $input_pid);
            return !count($diferencia);
        });




        //               ___     __          __                                        _      __                                                      _      _           
        //  _   ______ _/ (_)___/ /___ _____/ /___  ________  _____   ________  ____ _(_)____/ /__________  _____   _______  ______  ___  ______   __(_)____(_)___  ____ 
        // | | / / __ `/ / / __  / __ `/ __  / __ \/ ___/ _ \/ ___/  / ___/ _ \/ __ `/ / ___/ __/ ___/ __ \/ ___/  / ___/ / / / __ \/ _ \/ ___/ | / / / ___/ / __ \/ __ \
        // | |/ / /_/ / / / /_/ / /_/ / /_/ / /_/ / /  /  __(__  )  / /  /  __/ /_/ / (__  ) /_/ /  / /_/ (__  )  (__  ) /_/ / /_/ /  __/ /   | |/ / (__  ) / /_/ / / / /
        // |___/\__,_/_/_/\__,_/\__,_/\__,_/\____/_/   \___/____/  /_/   \___/\__, /_/____/\__/_/   \____/____/  /____/\__,_/ .___/\___/_/    |___/_/____/_/\____/_/ /_/ 
        //                                                                   /____/                                        /_/                                           
    
    
        //               ___     __          __                                        _      __                                                      _      _           
        //  _   ______ _/ (_)___/ /___ _____/ /___  ________  _____   ________  ____ _(_)____/ /__________  _____   _______  ______  ___  ______   __(_)____(_)___  ____ 
        // | | / / __ `/ / / __  / __ `/ __  / __ \/ ___/ _ \/ ___/  / ___/ _ \/ __ `/ / ___/ __/ ___/ __ \/ ___/  / ___/ / / / __ \/ _ \/ ___/ | / / / ___/ / __ \/ __ \
        // | |/ / /_/ / / / /_/ / /_/ / /_/ / /_/ / /  /  __(__  )  / /  /  __/ /_/ / (__  ) /_/ /  / /_/ (__  )  (__  ) /_/ / /_/ /  __/ /   | |/ / (__  ) / /_/ / / / /
        // |___/\__,_/_/_/\__,_/\__,_/\__,_/\____/_/   \___/____/  /_/   \___/\__, /_/____/\__/_/   \____/____/  /____/\__,_/ .___/\___/_/    |___/_/____/_/\____/_/ /_/ 
        //                                                                   /____/                                        /_/                                           
    
        Validator::extend('validar_numero_factura', function($attribute, $value, $parameters, $validator) {            
            // var_dump($attribute);
            // var_dump($value);
            // var_dump($parameters[0]);
            // exit(0);
            // var_dump($value['numero_factura']);
            // var_dump($parameters[0]);
            // var_dump($validator);
            $tabla = 'solicitud_pagos';
            $registroId = $parameters[0];
            $factura = strtolower($value);
            $results = DB::table($tabla)->where('registro_id', '!=', $registroId)->where('numero_factura', $factura)->first();
            //$results = DB::select( DB::raw("SELECT * FROM $tabla WHERE numero_factura = '$factura' AND registro_id != $registroId") );
            $validator->addReplacer('validar_numero_factura', function($message, $attribute, $rule, $parameters) use ($validator, $factura) {
                $message = str_replace(':nfactura', $factura, $message);
                return $message;
            });
            return empty($results);
            // exit(0);
        });

        Validator::extend('solicitud_pagos', function($attribute, $value, $parameters, $validator) {            
            //\var_dump($attribute);
            //\var_dump($value['numero_factura']);
            //\var_dump($parameters[0]);
            //\var_dump($validator);
            $tabla = $parameters[0];
            $registroId = array_key_exists(1, $parameters) ? $parameters[1] : '*' ;
            $factura = strtolower($value['numero_factura']);
            $results = DB::table($tabla)->where('registro_id', '!=', $registroId)->where('numero_factura', $factura)->first();
            //$results = DB::select( DB::raw("SELECT * FROM $tabla WHERE numero_factura = '$factura' AND registro_id != $registroId") );
            $validator->addReplacer('solicitud_pagos', function($message, $attribute, $rule, $parameters) use ($validator, $factura) {
                $message = str_replace(':nfactura', $factura, $message);
                return $message;
            });
            return empty($results);
            //exit(0);
        });

        Validator::extend('maximo_pago', function($attribute, $value, $parameters, $validator) {            
            //\var_dump($attribute);
            $pagos = $value;
            $valormax = $parameters[0];
            $yapagado = $parameters[1];
            
            foreach ($pagos as $pago) {
                $yapagado += $pago['valor_solicitado'];
            }

            $excedente = $yapagado - $valormax;

            $validator->addReplacer('maximo_pago', function($message, $attribute, $rule, $parameters) use ($validator, $valormax, $excedente) {
                $message = str_replace(':maximo_a_pagar', "$ ".number_format($valormax, 2, ',', '.'), $message);
                $message = str_replace(':pagado', "$ ".number_format($parameters[1], 2, ',', '.'), $message);
                $message = str_replace(':excedente', "$ ".number_format($excedente, 2, ',', '.'), $message);
                return $message;
            });
            //return false;      
            return $valormax >= $yapagado;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    
    
}
