<?php

namespace App\Providers;


use App\DocumentoBase;
use App\Policies\DocumentoBasePolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        DocumentoBase::class => DocumentoBasePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('supervisar', function($user, $documentoBase){
            return $user->persona_id == $documentoBase->supervisor;
        });
    }
}
