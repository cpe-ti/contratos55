<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ProvisionPresupuestal extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'provision_presupuestal';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function estudio()
    {
        return $this->belongsTo('App\EstudioMercado');
    }

    public function contratos()
    {
        return $this->belongsToMany('App\Contrato', 'contrato_cdp', 'cdp_id', 'contrato_id');
    }

    public function rp()
    {
        return $this->hasMany('App\RegistroPresupuestal');
    }

    public function justificacion()
    {
        return $this->hasOne('App\Justificacion');
    }
}
