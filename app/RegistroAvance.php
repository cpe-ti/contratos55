<?php

namespace App;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Support\Facades\DB;

class RegistroAvance extends Model implements AuditableContract
{
    use Auditable;
    protected $table = "registro_avances";
    protected $guarded = [];
    //id
    //orden
    //procesado
    //fecha_registro
    //contrato_id
    protected $casts = [
        'id' => 'integer',
        'orden' => 'integer',
        'procesado' => 'integer',
        'contrato_id' => 'integer',
    ];

    //Custom attribute
    protected $appends = ['contrato_nombre']; 

    public function getContratoNombreAttribute()
    {
        return $this->contrato->NombreRapido();
    }

    public function contrato()
    {
        return $this->belongsTo('App\DocumentoBase', 'contrato_id', 'id');
    }

    public function informe()
    {
        return $this->hasOne('App\InformeSupervision', 'registro_id', 'id');
    }

    public function avance_productos()
    {
        return $this->hasMany('App\AvanceProducto', 'registro_id', 'id');
    }

    public function solicitud_pagos()
    {
        return $this->hasMany('App\SolicitudPago', 'registro_id', 'id');
    }



    public function registrosProcesadosAnteriores()
    {
        //$registros = RegistroAvance::with('solicitud_pagos','avance_productos')->where('orden','<=', $this->orden)->where('procesado',1)->get();
        $registros = RegistroAvance::where('orden','<=', $this->orden)->where('procesado',1)->orderBy('orden')->get();
        return $registros;
    }


    public function RegistroAnterior()
    {
        $registro = RegistroAvance::where('orden','<', $this->orden)->where('procesado',1)->orderBy('orden','desc')->first();
        return $registro;
    }


    public function pagosHastaAqui()
    {
        $registros = $this->registrosProcesadosAnteriores();
        $pagos = Array();
        foreach ($registros as $registro) {
            foreach ($registro->solicitud_pagos as $pago) {
                array_push($pagos, $pago);                
            }
        }
        return $pagos;
    }

    public function avancesHastaAqui()
    {
        $registros = $this->registrosProcesadosAnteriores();
        $avances = Array();
        foreach ($registros as $registro) {
            foreach ($registro->avance_productos as $avance) {
                array_push($avances, $avance);                
            }
        }
        return $avances;
    }


    public function InformeHtml()
	{
        if($this->informe == null){
            $vistaInforme = \View::make('supervision.plantillaInformePDF', ['registro' => $this]);
            return $vistaInforme->render();  
        }

        if (count($this->informe))
        {
            return $this->informe->contenido;
        }

        $vistaInforme = \View::make('supervision.plantillaInformePDF', ['registro' => $this]);
        return $vistaInforme->render();        
	}
    
}
