<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class RegistroPresupuestal extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'registro_presupuestal';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //Relaciones
    public function cdp()
    {
        return $this->belongsTo('App\ProvisionPresupuestal');
    }

    public function contrato()
    {
        return $this->belongsTo('App\Contrato');
    }
}
