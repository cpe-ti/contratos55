<?php 
namespace App;

use Zizaco\Entrust\EntrustRole;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Rol extends EntrustRole implements AuditableContract
{
	use Auditable;
	public function users()
	{
		return $this->belongsToMany(app('config')->get('auth.providers.users.model'),app('config')->get('entrust.role_user_table'),app('config')->get('entrust.role_foreign_key'),app('config')->get('entrust.user_foreign_key'));
	}
}