<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Support\Facades\DB;

class SolicitudPago extends Model implements AuditableContract
{
    use Auditable;
    protected $table = "solicitud_pagos";
    protected $guarded = [];

    protected $casts = [
        'id' => 'integer',
        'valor_solicitado' => 'double',
        'registro_id' => 'integer',
    ];

    //Custom attribute
    protected $appends = ['url']; 
    
    public function getUrlAttribute()
    {
        if($this->factura_file){
            return route('facturas', ['pdf'=>$this->factura_file]);
        }
        return NULL;
    }

    public function registro()
    {
        return $this->belongsTo('App\RegistroAvance', 'registro_id', 'id');
    }
}
