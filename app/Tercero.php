<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Tercero extends Model implements AuditableContract
{
    use Auditable;
    //nombre de tabla
    protected $table = 'tercero';

    //problema con bigint id primary key
    protected $casts = ["id"=>"bigInt","id_representante"=>"bigInt"];

    protected $appends = ['nombre']; 

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //Attr especiales
    public function getNombreAttribute()
    {
        return $this->NombreCompleto();
    }


    //relaciones
    public function tipo()
    {
        return $this->belongsTo('App\TipoPersona', 'tipo_id', 'id');
    }

    public function contratos()
    {
        return $this->hasMany('App\DocumentoBase', 'tercero_id', 'id');
    }

    public function documentos()
    {
        return $this->hasMany('App\DocumentoBase', 'contrato_id', 'id');
    }
    
    //Funciones y metodos especificos
    public function NombreCompleto() {
        if ($this->razon_social == '' || $this->razon_social == NULL) {
            return ucfirst($this->primer_nombre).($this->segundo_nombre == "" ? "" : " ".ucfirst($this->segundo_nombre))." ".ucfirst($this->primer_apellido).ucfirst($this->segundo_apellido == "" ? "" : " ".ucfirst($this->segundo_apellido));
        }else{
            return ucfirst($this->razon_social);
        }
    }

    public function TipoPersona(){
        return $this->tipo->tipo;
    }

    public function iconoSVG(){
        switch ($this->tipo_id) {
            case 1:
                return file_get_contents(public_path().'/img/iconos/terceros/natural.svg');
                break;
            case 2:
                return file_get_contents(public_path().'/img/iconos/terceros/juridica.svg');
                break;
            default:
                return file_get_contents(public_path().'/img/iconos/notfound/3.svg');
                break;
        }
    }

    public function RepresentanteLegal(){
        switch ($this->tipo->id) {
            case 1:
                return $this->NombreCompleto();
                break;
            case 2:
                return $this->representante;
                break;            
            default:
                return "Tipo No especificado";
                break;
        }     
    }

    public function IdRepresentante(){
        switch ($this->tipo->id) {
            case 1:
                return $this->id;
                break;
            case 2:
                return $this->id_representante;
                break;            
            default:
                return "Tipo No especificado";
                break;
        }     
    }
}
