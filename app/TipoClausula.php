<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TipoClausula extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'tipo_clausula';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function clausulas()
    {
        return $this->hasMany('App\Clausula', 'tipo_clausula_id', 'id');
    }
}
