<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TipoContrato extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'tipo_contrato';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function contratos()
    {
        return $this->hasMany('App\Contrato');
    }
}
