<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TipoObligacion extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'tipo_obligacion';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function obligacion()
    {
        return $this->hasMany('App\Obligacion', 'tipo_obligacion_id', 'id');
    }
}
