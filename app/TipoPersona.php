<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TipoPersona extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'tipo_persona';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    //relaciones
    public function tercero()
    {
        return $this->hasMany('App\Tercero', 'tipo_id', 'id');
    }
}
