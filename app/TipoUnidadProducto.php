<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class TipoUnidadProducto extends Model implements AuditableContract
{
	use Auditable;
    //nombre de tabla
    protected $table = 'tipo_unidad_producto';

    //Prevenir Asignacion masiva en:
    protected $guarded = [];

    protected $casts = [
        'id' => 'integer',
    ];

    //relaciones
    public function producto()
    {
        return $this->hasMany('App\Producto', 'unidad_id', 'id');
    }
}
