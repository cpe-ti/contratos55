<?php

namespace App\Traits;

//Prueba con CssToInlineStyles
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

trait InformeSupervisionPDF
{
    public function generarInformePDF($html = "<h1>Prueba</h1>", $invalid = true)
    {
        ini_set('max_execution_time', 300);
        //$pdf = new \Mpdf('c','A4','12','Arial', 22, 15, 1, 1, 5, 0);
		$param = [
			'mode' => 'c',
			'format' => 'A4',
			'default_font_size' => 12,
			'default_font' => 'Arial',
			'margin_left' => 22,
			'margin_right' => 15,
			'margin_top' => 1,
			'margin_bottom' => 1,
			'margin_header' => 5,
			'margin_footer' => 0,
			'orientation' => 'P',
		];
        $pdf = new \Mpdf\Mpdf($param);

        $pdf->SetWatermarkText('NO APROBADO');
        $pdf->showWatermarkText = $invalid;

        $pdf->setAutoTopMargin = 'stretch';
        $pdf->setAutoBottomMargin = 'stretch';
        $header = \View::make('supervision.plantilla_encabezado')->render();
        $footer = \View::make('supervision.plantilla_pie')->render();
        $cssToInlineStyles = new CssToInlineStyles();		
		// $html = $request->contenido;
		//Hacer que todo el CSS sea inline
		$html = $cssToInlineStyles->convert($html);
		$pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);
        $pdf->WriteHTML($html);
        return $pdf->Output();
    }
}