<?php

namespace App;

use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class User extends Authenticatable implements AuditableContract
{
    use Auditable;
    use Notifiable;
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'persona_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Relaciones
    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }

    //public function rol()
    //{
    //    return $this->belongsTo('App\Rol');
    //}

    //Funciones y metodos especificos
    //public function RolCompleto()
    //{
    //    if ($this->rol == NULL) {
    //        return "Rol no asignado";
    //    }
    //    return $this->rol->rol;
    //}

    public function NombreCompleto() {
        return ucfirst($this->persona->primer_nombre).($this->persona->segundo_nombre == "" ? "" : " ".ucfirst($this->persona->segundo_nombre))." ".ucfirst($this->persona->primer_apellido).ucfirst($this->persona->segundo_apellido == "" ? "" : " ".ucfirst($this->persona->segundo_apellido));
    }

    //public function IsAdmin()
    //{
    //    if ($this->rol->id === 1 || $this->rol->id === 2) {
    //        return true;
    //    }
    //    return false;
    //}

    //public function IsRoot()
    //{
    //    if ($this->rol === 1) {
    //        return true;
    //    }
    //    return false;
    //}

    //public function TieneRol()
    //{
    //    if ($this->rol === NULL) {
    //        return false;
    //    }
    //    return true;
    //}
}
