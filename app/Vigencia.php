<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vigencia extends Model
{
    //
    protected $table = 'vigencias';

    public function rubros()
    {
        return $this->hasMany('App\Presupuesto', 'vigencia_id', 'id');
    }
}
