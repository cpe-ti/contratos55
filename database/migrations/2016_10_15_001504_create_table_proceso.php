<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProceso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proceso', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('area_id')->unsigned()->nullable();
            $table->foreign('area_id')->references('id')->on('area')->onDelete('SET NULL');
            $table->string('proceso')->unique();
            $table->timestamps();
        });

        //Tabla intermedia Procesos lideres
        Schema::create('lideres_procesos', function (Blueprint $table) {
            $table->bigInteger('lider_id')->unsigned()->index();
            $table->integer('proceso_id')->unsigned()->index();
            $table->foreign('lider_id')->references('id')->on('persona')->onDelete('cascade');
            $table->foreign('proceso_id')->references('id')->on('proceso')->onDelete('cascade');
            $table->primary(['lider_id', 'proceso_id']);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('proceso');
        Schema::dropIfExists('lideres_procesos');
    }
}
