<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRubroPresupuesto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rubro_presupuesto', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('cod')->unique();
            $table->string('nombre');
            $table->decimal('valor_inicial', 15, 2);
            $table->decimal('valor_actual', 15, 2);
            $table->integer('set_left');
            $table->integer('set_right');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('rubro_presupuesto');
    }
}
