<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComprasPaa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_de_compras', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id')->unsigned();
            $table->integer('id_oasis')->unique();
            $table->integer('rubro_id')->unsigned()->nullable();
            $table->foreign('rubro_id')->references('id')->on('rubro_presupuesto')->onDelete('SET NULL');
            $table->string('descripcion');
            $table->decimal('valor', 15, 2);
            $table->integer('proceso_id')->unsigned()->nullable();
            $table->foreign('proceso_id')->references('id')->on('proceso')->onDelete('SET NULL');
            $table->integer('modalidad_id')->unsigned()->nullable();
            $table->foreign('modalidad_id')->references('id')->on('modalidad_seleccion')->onDelete('SET NULL');
            $table->integer('estado_id')->unsigned()->nullable();
            $table->foreign('estado_id')->references('id')->on('estado_compra')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('plan_de_compras');
    }
}
