<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCdp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provision_presupuestal', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('justificacion_id')->unsigned()->nullable();
            $table->foreign('justificacion_id')->references('id')->on('justificacion')->onDelete('SET NULL');
            $table->integer('estudio_id')->unsigned()->nullable();
            $table->foreign('estudio_id')->references('id')->on('estudio_mercado')->onDelete('SET NULL');
            $table->string('descripcion');
            $table->decimal('valor', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('provision_presupuestal');
    }
}
