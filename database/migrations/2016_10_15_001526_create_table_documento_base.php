<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentoBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Tabla documento base para: ctos otrosis etc
        Schema::create('documento_base', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_oasis')->unsigned()->nullable()->index();
            $table->string('consecutivo_cpe')->nullable();
            $table->string('tipo')->index();
            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')->references('id')->on('documento_base')->unsigned()->onDelete('NO ACTION')->onUpdate('NO ACTION');
            //$table->integer('ultimo_otsi')->unsigned()->nullable();
            $table->integer('tipo_contrato_id')->unsigned()->nullable();
            $table->foreign('tipo_contrato_id')->references('id')->on('tipo_contrato')->onDelete('SET NULL');
            $table->integer('modalidad_id')->unsigned()->nullable();
            $table->foreign('modalidad_id')->references('id')->on('modalidad_seleccion')->onDelete('SET NULL');
            $table->bigInteger('tercero_id')->unsigned()->index()->nullable();
            $table->integer('tipo_tercero')->unsigned()->index()->nullable();
            $table->foreign(['tercero_id', 'tipo_tercero'])->references(['id', 'tipo_id'])->on('tercero')->onDelete('SET NULL');
            $table->decimal('valor', 20, 2);
            $table->bigInteger('supervisor')->unsigned()->nullable()->index();
            $table->foreign('supervisor')->references('id')->on('persona')->onDelete('SET NULL');
            $table->date('fecha_suscripcion')->nullable();
            $table->date('fecha_legalizacion')->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_terminacion')->nullable();
            $table->integer('estado_id')->unsigned()->index()->nullable();
            $table->foreign('estado_id')->references('id')->on('estado_contrato')->onDelete('SET NULL');
            $table->integer('estado_doc')->unsigned()->index()->nullable();
            $table->foreign('estado_doc')->references('id')->on('estado_documento')->onDelete('SET NULL');
            $table->string('rp')->nullable();
            $table->date('fecha_rp')->nullable();
            $table->string('cdp')->nullable();
            $table->boolean('activo')->nullable()->default(false);
            $table->longText('encabezado_consideraciones')->nullable();
            //Firmas contrato
            $table->string('nombre_contratante')->nullable();
            $table->string('cargo_contratante')->nullable();
            $table->string('nombre_contratista')->nullable();
            $table->string('cargo_contratista')->nullable();
            $table->bigInteger('reviso')->unsigned()->nullable();
            $table->foreign('reviso')->references('id')->on('persona')->unsigned()->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->bigInteger('proyecto')->unsigned()->nullable();
            $table->foreign('proyecto')->references('id')->on('persona')->unsigned()->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->string('url_secop')->nullable();            
            //Carta de adjudicacion//10-04-2017
            $table->longText('html_carta')->nullable();
            //observaciones aprobo rechazo
            $table->longText('observaciones')->nullable();
            $table->timestamps();         
        });
        //Tabla intermedia CDP Contratos
        Schema::create('documento_cdp', function (Blueprint $table) {
            $table->integer('doc_id')->unsigned()->index();
            $table->integer('cdp_id')->unsigned()->index();
            $table->foreign('doc_id')->references('id')->on('documento_base')->onDelete('cascade');
            $table->foreign('cdp_id')->references('id')->on('provision_presupuestal')->onDelete('cascade');
            $table->primary(['doc_id', 'cdp_id']);
            //$table->timestamps();
        });

        //Tabla intermedia Procesos lideres
        Schema::create('documento_compra', function (Blueprint $table) {
            $table->integer('doc_id')->unsigned()->index();
            $table->integer('compra_id')->unsigned()->index();
            $table->foreign('doc_id')->references('id')->on('documento_base')->onDelete('cascade');
            $table->foreign('compra_id')->references('id')->on('plan_de_compras')->onDelete('cascade');
            $table->primary(['doc_id', 'compra_id']);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento_base');
        Schema::dropIfExists('documento_cdp');
        Schema::dropIfExists('documento_compra');
    }
}
