<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_presupuestal', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')->references('id')->on('documento_base')->onDelete('SET NULL');
            $table->integer('cdp_id')->unsigned()->nullable();
            $table->foreign('cdp_id')->references('id')->on('provision_presupuestal')->onDelete('SET NULL');
            $table->string('descripcion');
            $table->decimal('valor', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('registro_presupuestal');
    }
}
