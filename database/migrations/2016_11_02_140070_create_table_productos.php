<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')->references('id')->on('documento_base')->onDelete('SET NULL');
            $table->string('nombre');
            $table->string('descripcion');
            $table->integer('unidad_id')->unsigned()->nullable();
            $table->foreign('unidad_id')->references('id')->on('tipo_unidad_producto')->onDelete('SET NULL');
            $table->integer('cantidad');
            $table->decimal('ponderacion', 3, 2);
            $table->integer('obligacion_id')->unsigned()->nullable();
            $table->foreign('obligacion_id')->references('id')->on('obligaciones')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
