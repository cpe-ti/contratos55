<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProgramacionPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacion_pagos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('doc_id')->unsigned()->nullable();
            $table->foreign('doc_id')->references('id')->on('documento_base')->onUpdate('cascade')->onDelete('SET NULL');
            $table->decimal('desembolso', 20, 2);
            $table->integer('orden')->unsigned();
            $table->timestamps();
        });

        // Tabla intermedia pago - productos
        Schema::create('pago_programado_productos', function (Blueprint $table) {
            $table->integer('pago_programado_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->decimal('cantidad', 20, 2)->unsigned()->nullable();
            $table->foreign('pago_programado_id')->references('id')->on('programacion_pagos')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->primary(['pago_programado_id', 'producto_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programacion_pagos');
        Schema::dropIfExists('pago_programado_productos');
    }
}
