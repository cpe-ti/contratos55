<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClausulas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clausulas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')->references('id')->on('documento_base')->onDelete('SET NULL');
            $table->integer('tipo_clausula_id')->unsigned()->nullable();
            $table->foreign('tipo_clausula_id')->references('id')->on('tipo_clausula')->onDelete('SET NULL');
            $table->longText('contenido_html')->nullable();
            $table->integer('ordinal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clausulas');
    }
}
