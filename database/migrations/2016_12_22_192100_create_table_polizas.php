<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePolizas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poliza', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('doc_id')->unsigned()->nullable();
            $table->foreign('doc_id')->references('id')->on('documento_base')->onDelete('SET NULL');
            $table->string('aseguradora')->nullable();
            $table->integer('numero_poliza')->unsigned()->index()->nullable();
            $table->date('fecha_expedicion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poliza');
    }
}
