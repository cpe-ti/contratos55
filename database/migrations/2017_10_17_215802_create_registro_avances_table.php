<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroAvancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_avances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orden')->nullable()->unsigned();
            $table->boolean('procesado')->default(false);
            $table->date('fecha_registro')->nullable();
            $table->integer('contrato_id')->unsigned()->index()->nullable();
            $table->foreign('contrato_id')->references('id')->on('documento_base')->onDelete('SET NULL');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_avances');
    }
}
