<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformesSupervisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informes_supervision', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero')->unsigned();            
            $table->longText('contenido')->nullable();
            $table->boolean('confirmado')->default(false);
            $table->integer('registro_id')->unsigned()->index()->nullable();
            $table->foreign('registro_id')->references('id')->on('registro_avances')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informes_supervision');
    }
}
