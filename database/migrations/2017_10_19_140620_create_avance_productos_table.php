<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvanceProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avance_productos', function (Blueprint $table) {
            $table->increments('id');            
            $table->double('cantidad_registrada')->unsigned();
            $table->string('observaciones')->nullable();
            $table->integer('producto_id')->unsigned()->index()->nullable();
            $table->integer('registro_id')->unsigned()->index()->nullable();
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('SET NULL');
            $table->foreign('registro_id')->references('id')->on('registro_avances')->onDelete('SET NULL');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avance_productos');
    }
}
