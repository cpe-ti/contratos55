<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_factura', 50)->unique();            
            $table->string('cdp')->nullable();
            $table->decimal('valor_solicitado', 20, 2)->unsigned();
            $table->string('observaciones')->nullable();
            $table->integer('registro_id')->unsigned()->index()->nullable();            
            $table->foreign('registro_id')->references('id')->on('registro_avances')->onDelete('SET NULL');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_pagos');
    }
}
