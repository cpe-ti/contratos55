<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtrosiOrdenToDocumentoBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documento_base', function($table) {
            //
            $table->integer('otrosi_orden')->unsigned()->nullable();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::table('users', function($table) {            
            $table->dropColumn('otrosi_orden');            
        });   
    }
}
