<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVigenciaToRubro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rubro_presupuesto', function($table) {
            // Añdir columna vigencia a tabla rubro
            $table->integer('vigencia_id')->unsigned()->nullable();
            $table->foreign('vigencia_id')->references('id')->on('vigencias')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rubro_presupuesto', function($table) {            
            $table->dropColumn('vigencia_id');            
        }); 
    }
}
