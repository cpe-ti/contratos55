<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsEstadoAyfRegistroavance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registro_avances', function($table) {
            $table->integer('estado_cont')->unsigned()->nullable();
            $table->integer('estado_tes')->unsigned()->nullable();
            $table->string('comentario_ayf')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registro_avances', function($table) {            
            $table->dropColumn('estado_cont');
            $table->dropColumn('estado_tes');
            $table->dropColumn('comentario_ayf');
        });
    }
}
