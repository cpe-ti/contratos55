<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCumplidoYSoportes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informes_supervision', function($table) {
            $table->string('cumplido')->nullable();
            $table->string('soportes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informes_supervision', function($table) {            
            $table->dropColumn('cumplido');
            $table->dropColumn('soportes');
        });
    }
}
