//Funcion inicializar para paginas cargadas con ajax
function inicializar (callback, callback2) {
	
	callback2 = typeof(callback2) != 'function' ? function(){} : callback2;
	if (window.jQuery) {  
		callback ();
	} else {
		window.addEventListener('load', function (argument) {
			callback ();
			console.log("callback 1");
		});		
		window.addEventListener('DOMContentLoaded', function() {
			callback2 ();
			console.log("callback 2");
		}, true);
	}	
}