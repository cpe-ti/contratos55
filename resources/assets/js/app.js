
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

//Errror catch global
window.onerror = function errorCatcher(errorMsg, url, lineNumber) {
    //alert("Ocurrió un error por favor recargue la página.\nError:\n" + errorMsg);
    //return false;
}

//Global data
var dtos = null;
var contractcpe_token;
//refreshCpeToken();
//registro endswith para internet explorer
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function (pattern) {
        var d = this.length - pattern.length;
        return d >= 0 && this.lastIndexOf(pattern) === d;
    };
}
//Registro de plugin eventos retrasados para mejorar el performance en resize
(function ($) {
    $.fn.onDelayed = function (nombre_evento, retraso_ms, callback) {
        var _timeout;
        this.on(nombre_evento, function (e) {
            if (!!_timeout) {
                clearTimeout(_timeout);
                //console.log('timer being re-set: ' + nombre_evento);
            } else {
                //console.log('timer being set for the first time: ' + nombre_evento);
            }
            _timeout = setTimeout(function () {
                callback(e);
            }, retraso_ms);
        });
    };
})(jQuery);


// Establecer tema bootstrap a las instacionas de select2 
// $.fn.select2.defaults.set("theme", "bootstrap");
/*
--EVENTOS
*/

//Evento carga de pagina completa
$(window).on('load', function () {
    $('#overlay_cargando').addClass('desvanecer-overlay');
    comprobar_menu();
    $('#overlay_cargando.desvanecer-overlay').on('animationend', function (e) {
        $("#overlay_cargando").off("animationend").remove();
    });
});

$(window).onDelayed('resize', 200, function () {
    comprobar_menu();
});
//Evento click en la capa oscura de fondo con el menu abierto
$("#overlay-menu").click(function (event) {
    event.preventDefault();
    event.stopPropagation();
    cerrar_menu(true);
});
//Evento click boton atras 
$("#boton_atras").click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    window.history.back();
});
//Evento click boton menu 
$("#boton_menu").click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    abrir_menu(true);
});
//Evento mostrar opciones usuario
$('body').on('click', '#menu-btn-opciones', function (event) {
    event.preventDefault();
    $(this).parent('#menu-seccion-principal').toggleClass('mostrar-opciones-usuario');
    //$(this).parent('#menu-user-options').toggleClass('menu-user-option-open');
});

//evento solo para links del menu 
$('#menu-lateral').on('click', 'a.link_dinamico', function (e) {
    e.preventDefault();
    e.stopPropagation();
    //valores por defecto si no hay data-atributes
    var new_url = $(this).attr('href') ? $(this).attr('href') : url_aplicacion;
    var titulo = $(this).attr('data-titulo') ? $(this).attr('data-titulo') : 'Repositorio CPE';
    var current_url = window.location.href;
    var new_url_menu = new_url + '#menu';
    /*Verificar si la url del link es igual a la url actual*/
    if ((current_url == new_url) || (current_url == new_url_menu)) {
        cerrar_menu(true);
    } else {
        $('#contenido-menu > ul > li').removeClass('menu-activo');
        $(this).parent('li').addClass('menu-activo');
        cerrar_menu(false, function (mobile) {
            if (mobile) {
                cargar_contenido_get(new_url, titulo, true, 'replace');
            } else {
                cargar_contenido_get(new_url, titulo, true, 'push');
            }
        });
    }
});
//Prevenir mantener foco en botones o enlaces para mejorar el desplazamiento en moviles
$('body').on('click', 'a, button, .btn', function (e) {
    $(this).blur();
    console.log('Blur');
});
//Evento click en cualquier link con clase 'link_dinamico'
$('body').on('click', 'a.link_dinamico', function (e) {
    e.preventDefault();
    e.stopPropagation();
    if ($(this).is('[disabled=disabled]')) {
        return false;
    }
    //valores por defecto si no hay data-atributes
    var new_url = $(this).attr('href') ? $(this).attr('href') : url_aplicacion;
    var titulo = $(this).attr('data-titulo') ? $(this).attr('data-titulo') : 'Repositorio CPE';
    var current_url = window.location.href;
    /*Verificar si la url del link es igual a la url actual*/
    if (current_url != new_url || $(this).attr('data-force-load') == 'true') {
        cargar_contenido_get(new_url, titulo, true, 'push');
    }
});
//evento para formularios
$('body').on('submit', '.form-dinamico', function (e) {
    e.preventDefault();
    //e.stopPropagation();
    var formulario = $(this)[0];
    var url_action = $(this).attr('action');
    var metodo = $(this).attr('method');
    var tipo_datos = "json";
    var datos = new FormData(formulario);
    var jform = $(this);
    $(':input', jform).attr("disabled", true);
    //$("#contenedor-pagina").css('overflow-y', 'hidden');
    enviar_formularios(url_action, metodo, tipo_datos, datos, function (respuesta) {
        console.log('Callback funcion enviar_formularios');
        console.log(respuesta);
        dtos = respuesta;
    });
});
//evento salir
window.onbeforeunload = function () {
    if ($(".prevent_lost")[0]) {
        return "Si sale ahora podria perder informacion ¿Desea salir?";
    }
};

//Evento popstate history (atras o adelante navegador)
window.addEventListener('popstate', function (e) {
    e.preventDefault();
    e.stopPropagation();
    console.log('pin');
    //
    if ($("#menu-lateral").is(".menu-abierto")) {
        cerrar_menu(false);
        return false;
    }
    if (window.location.href.endsWith('#modal')) {
        return false;
    }
    if (($(".bootbox").data('bs.modal') || {}).isShown) {
        cerrar_mensajes();
        return false;
    }
    //prevenir salir para formularios con informacion sensible
    if ($(".prevent_lost")[0]) {
        var r = confirm("Si sale ahora podria perder informacion ¿Desea salir?");
        if (r == false) {
            return false;
        }
    }
    if (e.state == null) {
        var url = window.location.href;
        var titulo = "Contract CPE";
    } else {
        url = typeof (e.state.url) == 'string' ? e.state.url : url_aplicacion;
        titulo = typeof (e.state.titulo) == 'string' ? e.state.titulo : 'Contract CPE';
    }
    if (url.endsWith('#menu')) {
        abrir_menu(false);
    } else {
        console.log(url);
        console.log(window.location.href);
        cargar_contenido_get(url, titulo, false, '', true);
    }
});
$("#overlay-menu").on('transitionend', function (event) {
    event.preventDefault();
    event.stopPropagation();
    if (!$(event.delegateTarget).is(".overlay-opacity")) {
        // statement
        $("#overlay-menu").removeClass('overlay-visible');
    }
});
/*prevenir disparar el evento de transicion del menu
con el evento de transicion de su contenido*/
$("#contenido-menu").on('transitionend', function (event) {
    event.preventDefault();
    event.stopPropagation();
});
/*
--FUNCIONES
*/
//Funcion es_mobile
window.es_mobile = function() {
    return ($(window).width() > 992) ? false : true;
}
//comprobar menu
window.comprobar_menu = function() {
    if (es_mobile() && window.location.href.endsWith('#menu') && !$("#menu-lateral").is(".menu-abierto")) {
        abrir_menu(false);
    }
    if (!es_mobile() && window.location.href.endsWith('#menu')) {
        cerrar_menu(true);
    }
}
//abrir menu
window.abrir_menu = function(push, callback) {
    push = typeof push == 'boolean' ? push : true;
    callback = typeof callback == 'function' ? callback : function () { };
    var mobile = es_mobile();
    //si es mobile y el menu no tiene la clase .menu_abierto
    if (mobile && !$("#menu-lateral").is(".menu-abierto")) {
        //añadir clases css
        $("#overlay-menu").addClass('overlay-visible overlay-opacity');
        $("#menu-lateral").addClass('menu-abierto');
        //si push es true y la ruta actual no termina en #menu
        if (push && !(window.location.href.endsWith('#menu'))) {
            var current_url = window.location.href;
            current_url += '#menu';
            history.pushState({ url: '#menu', titulo: 'menu' }, null, current_url);
        }
    } else {
        if ((window.location.href.endsWith('#menu'))) {
            window.history.back();
        }
    }
    callback(mobile);
}
//cerrar menu
window.cerrar_menu = function(back, callback) {
    back = typeof back == 'boolean' ? back : true;
    callback = typeof callback == 'function' ? callback : function () { };
    var mobile = es_mobile();
    if (back && window.location.href.endsWith('#menu')) {
        window.history.back();
    } else {
        $("#menu-lateral").removeClass('menu-abierto');
        $("#overlay-menu").removeClass('overlay-opacity');
        $("#menu-seccion-principal").removeClass('mostrar-opciones-usuario');
    }
    callback(mobile);
}
/*Funcion cambiar contenido de pagina del contenedor principal,
titulo y hacer push history (opcional)*/
window.cargar_contenido_get = function (url, titulo, make_history, history_method, fired_popstate) {
    if ($(".prevent_lost")[0] && !fired_popstate) {
        var r = confirm("Si sale ahora podria perder informacion ¿Desea salir?");
        if (r == false) {
            //window.history.go(1);
            return false;
        }
    }
    try {
        tinymce.remove();        
    } catch (error) {
        console.log(error);        
    }
    $("body").removeClass('mce-fullscreen');
    url = typeof url == 'string' ? url : '/';
    titulo = typeof titulo == 'string' ? titulo : 'Repositorio CPE';
    make_history = typeof make_history == 'boolean' ? make_history : true;
    history_method = history_method == 'push' || history_method == 'replace' ? history_method : 'push';
    $('#indicador_cargando').addClass('cargando-on');
    //alert("Cargando");
    //$.ajax
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'text',
        success: function (data) {
            if (make_history) {
                if (history_method == 'push') {
                    history.pushState({ url: url, titulo: titulo }, null, url);
                };
                if (history_method == 'replace') {
                    history.replaceState({ url: url, titulo: titulo }, null, url);
                }
            }
            cambiar_pagina(data);
        },
        error: function (data) {
            if (make_history) {
                if (history_method == 'push') {
                    history.pushState({ url: url, titulo: 'Error' }, null, url);
                };
                if (history_method == 'replace') {
                    history.replaceState({ url: url, titulo: 'Error' }, null, url);
                }
            }
            cambiar_pagina(data.readyState == 0 ? "No se pudo establecer una conexion<br/><pre>" + JSON.stringify(data, null, 4) + "</pre>" : data.responseText);
        }
    });
    // $.get(url, function (data) {

    // }, 'html').fail(function(data) {

    // });
};
//funcion para formularios
window.enviar_formularios = function (url_action, metodo, tipo_datos, datos, callback) {
    url_action = typeof (url_action) == 'string' || url_action != '' ? url_action : false;
    metodo = metodo.toUpperCase() == 'POST' || metodo.toUpperCase() == 'GET' ? metodo : 'GET';
    tipo_datos = typeof (tipo_datos) == 'string' ? tipo_datos : 'html';
    callback = typeof (callback) == 'function' ? callback : function () { };
    //Salir si la url_action no es valida
    if (url_action == false) {
        return false;
    }
    $('#indicador_cargando').addClass('cargando-on');
    var respuesta = null;
    $.ajax({
        url: url_action,
        type: metodo,
        dataType: tipo_datos,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log("success:");
            console.log(data);
            respuesta = { success: true, data: data };
        },
        error: function (data) {
            console.log("Fail:");
            console.log(data);
            respuesta = { success: false, data: data };
        },
        complete: function () {
            console.log("complete");
            $('#indicador_cargando').removeClass('cargando-on');
            callback(respuesta);
        }
    });
}
/*cambiar contenido hatml del contenedor principal con el que se 
pasa en el parametro - animacion fadein y fadeout*/
window.cambiar_pagina = function(contenido) {
    //console.log(contenido);
    $("#contenedor-pagina").removeClass('aparecer-pagina');
    $("#contenedor-pagina").addClass('desvanecer-pagina');
    $("#contenedor-pagina").on("animationend", function (e) {
        //Ir al incio antes de cambiar
        $('#contenedor-pagina').scrollTop(0);
        e.preventDefault();
        e.stopPropagation();
        if (e.originalEvent.animationName == "desvanecer-pagina") {
            $("#contenedor-pagina").html(contenido).promise().done(function () {
                cambiar_titulo($("#titulo_ajax").attr('data-titulo'));
                console.log($("#titulo_ajax").attr('data-titulo'));
            });
            $("#contenedor-pagina").removeClass('desvanecer-pagina');
            $("#contenedor-pagina").addClass('aparecer-pagina');
            //$('#indicador_cargando').removeClass('cargando-on');
        } else if (e.originalEvent.animationName == "aparecer-pagina") {
            $('#indicador_cargando').removeClass('cargando-on');
            $("#contenedor-pagina").off("animationend");
        }
    });
    //refreshCpeToken()
};

window.refreshCpeToken = function (callback) {
    var callback = callback || function () { console.log('nada') };
    $.get(url_aplicacion + '/refresh-csrf').done(function (data) {
        contractcpe_token = data; // the new token
        console.log(contractcpe_token);
        $("#token_salir").val(contractcpe_token);
        callback();
    });
}


window.cambiar_titulo = function(titulo) {
    document.title = titulo;
    $("#titulo-app").addClass('titulo-out');
    $("#titulo-app").on('transitionend', function (event) {
        event.preventDefault();
        if ($(event.delegateTarget).is(".titulo-out")) {
            $("#titulo-app").html(titulo).removeClass('titulo-out');
        } else {
            $("#titulo-app").off("transitionend");
        }
    });
    //$("#titulo-app").html(titulo);
}
//mensaje_alerta ("titulo", "mensaje").modal('show');
//mensaje_alerta ("titulo", "mensaje").modal('show');
//mensaje_alerta ("titulo", "mensaje").modal('show');

/* Funciones mensajes Modales
usar .modal('show'); al final de 
cada funcion para mostrar*/

//Alert
window.mensaje_alerta = function(titulo, mensaje, callback, callback_show, callback_close) {
    titulo = typeof (titulo) == 'string' ? titulo : 'titulo';
    mensaje = typeof (mensaje) == 'string' ? mensaje : 'mensaje';
    callback = typeof (callback) == 'function' ? callback : function () { };
    callback_show = typeof (callback_show) == 'function' ? callback_show : function () { };
    callback_close = typeof (callback_close) == 'function' ? callback_close : function () { };
    var modal = bootbox.alert({
        show: false,
        title: titulo,
        message: mensaje,
        className: "modal-ch",
        callback: callback
    });
    modal.on("show.bs.modal", function () {
        history.replaceState({ url: '#modal', titulo: 'Mensaje' }, null, window.location.href + "#modal");
        callback_show();
    });
    modal.on("hide.bs.modal", function () {
        history.replaceState({ url: window.location.href.split('#')[0], titulo: '' }, null, window.location.href.split('#')[0]);
        callback_close();
    });
    modal.cerrar = function () {
        modal.modal('hide');
    }
    return modal;
}
//Confirmacion
window.mensaje_confirmacion = function(titulo, mensaje, callback_true, callback_false, callback_show) {
    var modal = bootbox.confirm({
        show: false,
        title: titulo,
        message: mensaje,
        callback: function (resultado) {
            resultado ? callback_true(resultado) : callback_false(resultado);
        }
    });
    modal.on("show.bs.modal", function () {
        callback_show();
    });
    return modal;
}
//prompt
window.mensaje_prompt = function(titulo, callback_true, callback_false, callback_show) {
    var modal = bootbox.prompt({
        show: false,
        title: titulo,
        callback: function (respuesta) {
            respuesta ? callback_true(respuesta) : callback_false(respuesta);
        }
    });
    modal.on("show.bs.modal", function () {
        callback_show();
    });
    return modal;
}
//custom
window.mensaje_custom = function(titulo, mensaje, callback_escape, callback_show, clase, botones) {
    // botones = {success:{label:"Boton",className:"btn-success",callback:function(){}},"Boton 2":{className:"btn-danger",callback:function(){}},"Boton 3":function(){}}

    var modal = bootbox.dialog({
        title: titulo,
        message: mensaje,
        show: false,
        backdrop: true,
        closeButton: true,
        animate: true,
        className: clase,
        onEscape: callback_escape,
        buttons: botones
    });
    modal.on("show.bs.modal", function () {
        callback_show();
    });
    return modal;
}
//cerrar todos los modales abiertos
function cerrar_mensajes(argument) {
    //window.history.back();
    bootbox.hideAll();
}


window.mensaje_error = function(titulo = 'Error', mensaje = 'Error desconocido.', back = false) {
    bootbox.dialog({
        title: '<span style="color:#DE4141;">' + titulo + '</span>',
        message: '<div style="text-align: center; width:100%; color:#DE4141;"><i class="fa fa-exclamation-circle fa-4x" aria-hidden="true"></i></div>' + mensaje,
        onEscape: function () {

        },
        buttons: {
            success: {
                label: "Ok",
                className: "btn-danger",
                callback: function () {
                    back ? window.history.back() : back;
                },
            }
        }
    });

}

window.mensaje_correcto = function(titulo = 'Hecho', mensaje = 'Operacion completada correctamente.', back = true) {
    bootbox.dialog({
        title: titulo,
        message: '<div style="text-align: center; width:100%; color:#41DE5B;"><i class="fa fa-check-circle-o fa-4x" aria-hidden="true"></i></div>' + mensaje,
        onEscape: function () {

        },
        buttons: {
            success: {
                label: "Ok",
                className: "btn-success",
                callback: function () {
                    back ? window.history.back() : back;
                },
            }
        }
    });

}


/////
window.guid = function () {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

window.s4 = function() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

window.scrollTo = function(element, to, duration) {
    if (duration <= 0) return;
    var difference = to - element.scrollTop;
    var perTick = difference / duration * 10;

    setTimeout(function () {
        element.scrollTop = element.scrollTop + perTick;
        if (element.scrollTop == to) return;
        scrollTo(element, to, duration - 10);
    }, 10);
}

//NUMBER FORMATTER
Number.prototype.format = function (n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};