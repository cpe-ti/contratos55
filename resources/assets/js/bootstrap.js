
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.shuffle = require('shufflejs').default;

require('select2');
require('select2/dist/js/i18n/es.js');
import 'select2/dist/css/select2.css';
import 'select2-bootstrap-theme/dist/select2-bootstrap.css';

window.Inputmask = require('inputmask');

require("inputmask/dist/jquery.inputmask.bundle");

window.bootbox = require('bootbox');

window.moment = require('moment');

import tinymce from 'tinymce/tinymce';

require.context('file-loader?name=js/[path][name].[ext]&context=node_modules/tinymce!tinymce/skins', true, /.*/);

import 'tinymce/themes/modern/theme';

import 'tinymce/skins/lightgray/skin.min.css'
import 'tinymce/skins/lightgray/content.min.css'


// import 'tinymce/skins';
// require.context('tinymce/skins', true, /.*/);

//Plugins
import 'tinymce-i18n/langs/es.js';
import 'tinymce/plugins/paste/plugin';
import 'tinymce/plugins/link/plugin';
import 'tinymce/plugins/print/plugin';
import 'tinymce/plugins/hr/plugin';
import 'tinymce/plugins/autolink/plugin';
import 'tinymce/plugins/autoresize/plugin';
import 'tinymce/plugins/advlist/plugin';
import 'tinymce/plugins/anchor/plugin';
import 'tinymce/plugins/autolink/plugin';
import 'tinymce/plugins/charmap/plugin';
import 'tinymce/plugins/code/plugin';
// import 'tinymce/plugins/codesample/plugin';
import 'tinymce/plugins/colorpicker/plugin';
import 'tinymce/plugins/contextmenu/plugin';
import 'tinymce/plugins/directionality/plugin';
import 'tinymce/plugins/emoticons/plugin';
import 'tinymce/plugins/fullscreen/plugin';
import 'tinymce/plugins/hr/plugin';
import 'tinymce/plugins/image/plugin';
import 'tinymce/plugins/imagetools/plugin';
import 'tinymce/plugins/insertdatetime/plugin';
import 'tinymce/plugins/link/plugin';
import 'tinymce/plugins/lists/plugin';
import 'tinymce/plugins/media/plugin';
import 'tinymce/plugins/nonbreaking/plugin';
import 'tinymce/plugins/pagebreak/plugin';
import 'tinymce/plugins/paste/plugin';
import 'tinymce/plugins/preview/plugin';
import 'tinymce/plugins/print/plugin';
import 'tinymce/plugins/save/plugin';
import 'tinymce/plugins/searchreplace/plugin';
import 'tinymce/plugins/table/plugin';
import 'tinymce/plugins/template/plugin';
import 'tinymce/plugins/textcolor/plugin';
import 'tinymce/plugins/textpattern/plugin';
import 'tinymce/plugins/toc/plugin';
import 'tinymce/plugins/visualblocks/plugin';
import 'tinymce/plugins/visualchars/plugin';
import 'tinymce/plugins/wordcount/plugin';
// import 'tinymce/plugins/bdesk_photo/plugin';

// Initialize
// tinymce.init({
//     selector: '#description',
//     skin: false,
//     plugins: ['paste', 'link', 'autoresize']
// });


import Vue from 'vue';
// hacerlo global
window.Vue = Vue;

// import Handlebars from 'handlebars';
import Handlebars from 'handlebars/dist/handlebars.min.js';
// hacerlo global
window.Handlebars = Handlebars;

// require styles
require('swiper/dist/css/swiper.css');
const VueAwesomeSwiper = require('vue-awesome-swiper/ssr');
Vue.use(VueAwesomeSwiper);



Vue.directive('tinymce', {
    bind(el) {
        tinymce.init({
            target: el,
            theme: 'modern',
            //skin_url: '/css/tinymce/skins/lightgray' // point your tinymce skin directory here
            setup: function (editor) {
                console.log("Setup");
                editor.on('init', function () {
                    console.log("Init");
                });
            },
        })
    }
})

import InputMaskFV from 'inputmask'

Vue.directive('input-mask', {
    bind: function (el) {
        new InputMaskFV().mask(el);
    },
    update: function (value) {
        //console.log(value);
        //console.log(value.inputmask.unmaskedvalue());
    },
});

import VueCurrencyFilter from 'vue-currency-filter';
Vue.use(VueCurrencyFilter);


/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key',
//     cluster: 'mt1',
//     encrypted: true
// });
