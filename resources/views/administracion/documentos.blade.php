@extends('layouts.pagina_maestra')
@section('titulo')
Configuraciones de Documentos
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h1>Parámetros</h1>
	<a href="{{Request::url()}}/modalidades" class="link_dinamico btn-config col-sm-6 col-md-4">
		<div class="btn-inner">Modalidades</div>
	</a>
	<a href="{{Request::url()}}/modalidades" class="link_dinamico btn-config col-sm-6 col-md-4">
		<div class="btn-inner">Tipos de Contrato</div>
	</a>
	<a href="{{Request::url()}}/modalidades" class="link_dinamico btn-config col-sm-6 col-md-4">
		<div class="btn-inner">Terceros</div>
	</a>
	<a href="{{Request::url()}}/modalidades" class="link_dinamico btn-config col-sm-6 col-md-4">
		<div class="btn-inner">Otros</div>
	</a>
</div>
<style>
	.btn-config{
		margin: 5px 0px 5px 0px;
		text-align: center;
		text-decoration: none !important;
	}
	.btn-config>.btn-inner{
		text-decoration: none !important;
		background-color: #1A63A1;
		color: #FFFFFF;
		font-size: 12pt;
		padding: 15px;
		border-radius: 10px;
		margin: 0px 5px 0px 5px;
		-webkit-transition: all 100ms ease-in-out;
		   -moz-transition: all 100ms ease-in-out;
		    -ms-transition: all 100ms ease-in-out;
		     -o-transition: all 100ms ease-in-out;
		        transition: all 100ms ease-in-out;
	}
	.btn-config>.btn-inner:hover{
		background-color: #3886C8;		
		color: #FFFFFF;
	}
</style>
@endsection