@extends('layouts.pagina_maestra')
@section('titulo')
Editar Usuario
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h2>{{$usuario->NombreCompleto()}} - {{$usuario->email}}</h2>
	<form id="form-editar-usuario" action="{{ url('administrar/usuarios/'.$usuario->id.'/editar') }}" method="POST">
		{!!csrf_field()!!}
		<div class="panel panel-default" style="overflow: hidden;">
			<div class="panel-heading"><h4>Usuario:</h4></div>
			<div class="panel-body">
				<div class="form-group col-md-12">
					<label class="control-label" for="nombre">E-mail:</label>
					<input type="email" name="email" class="form-control" placeholder="e-mail" value="{{$usuario->email}}" />
					<span class="help-block">
						<strong class="email_error"></strong>
					</span>
				</div>		
				<div class="col-sm-8"><strong>Usuario Activo:</strong></div>
				<div class="col-sm-4 text-right">
					<label class="switch-checkbox">
						<input class="check-rol" type="checkbox" name="activo" {{ ($usuario->activo ? "checked" : "") }}/>
						<i class="tgl-i"></i>
					</label>
				</div>
			</div>
		</div>
		<div class="panel panel-default" style="overflow: hidden;">
			<div class="panel-heading"><h4>Persona:</h4></div>
			<div class="panel-body">
				<div class="form-group col-md-12">
					<label class="control-label" for="nombre">Persona:</label>
					<select name="persona" class="form-control" placeholder="Persona">
						<option value="{{$usuario->persona->id}}">{{$usuario->persona->id}} - {{$usuario->persona->NombreCompleto()}}</option>
						@forelse( $personas as $persona )
							@if(!($usuario->persona->id == $persona->id))
							<option value="{{$persona->id}}">{{$persona->id}} - {{$persona->NombreCompleto()}}</option>
							@endif
						@empty
						<option value="">No se encontraron personas</option>
						@endforelse
						<option value="1057014139">Yo no existo</option>
					</select>
					<span class="help-block">
						<strong class="persona_error"></strong>
					</span>
				</div>
			</div>
		</div>
		<div class="panel panel-default" style="overflow: hidden;">
			<div class="panel-heading"><h4>Roles:</h4></div>
			<div class="panel-body">
				@foreach ($roles as $rol)				
					<div class="col-sm-4"><strong>{{$rol->display_name}}</strong></div>
					<div class="col-sm-6">{{$rol->description}}</div>
					<div class="col-sm-2 text-right col-switch">
						<label class="switch-checkbox">
							<input class="check-rol" type="checkbox" name="rol[{{$rol->id}}]" {{ isset($usuario->roles->keyBy('id')[$rol->id]) ? 'checked' : '' }}/>
							<i class="tgl-i"></i>
						</label>
					</div>
					<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
					@if( !($loop->last) )
					<hr class="hr-no-margin">							
					@endif
				@endforeach
			</div>
		</div>
		<div class="row" id="debug_error">
			@foreach ($errors->all() as $error)
			    {{ $error }}<br/>
			@endforeach
		</div>
		<div class="form-group col-xs-12" style="padding-right: 0px; padding-left: 0px;">			
			<a href="{{ url('administrar/usuarios/'.$usuario->id) }}" id="boton-cancelar-edicion" class="link_dinamico btn btn-lg btn-danger">
				<i class="fa fa-close" aria-hidden="true"></i> Atras
			</a>
			<button type="submit" id="boton-crear-participante" class="btn btn-lg btn-primary pull-right">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>
				<span>&nbsp;&nbsp;Guardar</span>
			</button>			
		</div>
	</form>
</div>
<style>
	.hr-no-margin{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.panel-body > [class^="col-"], .panel-body > [class*=" col-"] {
		padding-left: 0px;
		padding-right: 0px;
	}
	.col-switch{
		margin-top: 10px;	
	}
</style>
<script>
	inicializar(function(){
		////Guardar Formulario
		$('#form-editar-usuario').on('submit', function(e) {
    	    e.preventDefault();
    	    e.stopPropagation();
    	    e.isDefaultPrevented = function () {
    	        return false;
    	    }
    	    var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			var jform = $(this);
			$(':input', jform).attr("disabled", true);	
			$(':checkbox', jform).addClass('disabled-tgl');	
			$(".help-block").addClass('help-block-visible');
			$('.form-group').removeClass("has-error").find('.help-block>strong').html('');
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				$(':input', jform).attr("disabled", false);
				$(':checkbox', jform).removeClass('disabled-tgl');
				console.log(respuesta);
				if ((respuesta.data.status == 200 && respuesta.data.statusText == "OK") || respuesta.data.success == true) {
					mensaje_alerta ("Hecho!", (respuesta.data.success ? respuesta.data.mensaje : "Cambios guardados correctamente")).modal('show');
				}else{
					if (respuesta.data.responseText) {
						$("#debug_error").html(respuesta.data.responseText);
					}
				}
			});
    	});
	});
</script>
@endsection