@extends('layouts.pagina_maestra')
@section('titulo')
Administración
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	{{-- <h1>Administrar</h1> --}}
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('administrar/participantes') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-address-book-o" aria-hidden="true"></i>
			</a>
			<div class="texto_item_menu">Participantes</div>			
		</div>
	</div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('administrar/usuarios') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-users" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Usuarios</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('administrar/roles-permisos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-key" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Roles y Permisos</span>
		</div>
	</div>	
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('administrar/documentos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-file-text-o" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Documentos</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('administrar/configuraciones') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-wrench" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Configuraciones</span>
		</div>
	</div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('administrar/auditoria') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-history" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Auditoría</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
</div>
@endsection
