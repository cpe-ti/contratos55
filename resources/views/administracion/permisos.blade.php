@extends('layouts.pagina_maestra')
@section('titulo')
Roles & Permisos
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h2>Rol: {{$rol->display_name}}</h2>
	<div class="contenedor-tabs-permisos">
		<form id="form-permisos-rol" action="{{ url('administrar/roles-permisos/'.$rol->id.'/editar') }}" method="POST">		
			{!!csrf_field()!!}
			<div class="" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="permisos-tabs" class="nav nav-tabs nav-tabs-responsive" role="tablist">
					@foreach ($grupos as $grupo)
					<li role="presentation" class="{{ ($loop->first ? 'active' : ($loop->index == 1 ? 'next' : '')) }}">
						<a href="#grupo-{{$grupo->id}}" id="grupo-{{$grupo->id}}-tab" role="tab" data-toggle="tab" aria-controls="grupo-{{$grupo->id}}" aria-expanded="true">
							<span class="text">{{$grupo->nombre}}</span>
						</a>
					</li>
					@endforeach
				</ul>
				<div id="permisos-tabs-content" class="tab-content">
					@foreach ($grupos as $grupo)
					<div role="tabpanel" class="tab-pane fade {{ ($loop->first ? 'in active' : '') }}" id="grupo-{{$grupo->id}}" aria-labelledby="grupo-{{$grupo->id}}-tab">
						<table class="table table-bordered table-striped table-hover">
							<thead>
								<th>Nombre</th>
								<th class="desc-per">Descripción</th>
								<th>Estado</th>
							</thead>
							@foreach ($grupo->permiso as $permiso)
							<tr id="permiso_{{$permiso->id}}">
								<td>{{$permiso->display_name}}</td>
								<td class="desc-per">{{$permiso->description}}</td>
								<td class="text-center" style="padding: 5px;">
									<label class="switch-checkbox">
										<input class="check-permiso" type="checkbox" name="permiso[{{$permiso->id}}]" {{ isset($rol->perms->keyBy('id')[$permiso->id]) ? 'checked' : '' }}/>
										<i class="tgl-i"></i>
									</label>
								</td>
							</tr>
							@endforeach
						</table>
					</div>					
					@endforeach
				</div>
			</div>
			<input type="hidden" name="rol" value="{{$rol->id}}">
			<div class="row">
				<div class="col-md-12">
					<button type="reset" id="boton-crear-participante" class="btn btn-danger btn-lg pull-left">
						<i class="fa fa-close" aria-hidden="true"></i>
						<span>&nbsp;&nbsp;Cancelar</span>
					</button>			
					<button type="submit" id="boton-crear-participante" class="btn btn-primary btn-lg pull-right">
						<i class="fa fa-floppy-o" aria-hidden="true"></i>
						<span>&nbsp;&nbsp;Guardar</span>
					</button>
				</div>
			</div>
			<div class="row">
				<p>&nbsp;</p>
			</div>
			<div class="errores">
				
			</div>
		</form>
	</div>
</div>
<style>
	/* ///////////////
	Pestañas Responsivas
	///////////////// */

	@media screen and (max-width: 768px) {
		.nav-tabs-responsive > li {
			display: none !important;
			width: 23%;
		}
		.desc-per{
			display: none;
		}
		.nav-tabs-responsive > li > a {
			max-width: 100%;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			word-wrap: normal;
			width: 100%;
			width: 100%;
			text-align: center;
			vertical-align: top;
		}
		.nav-tabs-responsive > li.active {
			width: 54%;
		}
		.nav-tabs-responsive > li.active:first-child {
			margin-left: 23%;
		}
		.nav-tabs-responsive > li.active, .nav-tabs-responsive > li.prev, .nav-tabs-responsive > li.next {
			display: block !important;
		}
		.nav-tabs-responsive > li.prev, .nav-tabs-responsive > li.next {
			-webkit-transform: scale(0.9);
			transform: scale(0.9);
		}
		.nav-tabs-responsive > li.next > a, .nav-tabs-responsive > li.prev > a {
			-webkit-transition: none;
			transition: none;
		}
		.nav-tabs-responsive > li.next > a .text, .nav-tabs-responsive > li.prev > a .text {
			display: none;
		}
		.nav-tabs-responsive > li.next > a:after, .nav-tabs-responsive > li.next > a:after, .nav-tabs-responsive > li.prev > a:after, .nav-tabs-responsive > li.prev > a:after {
			position: relative;
			top: 1px;
			display: inline-block;
			font-family: 'Glyphicons Halflings';
			font-style: normal;
			font-weight: 400;
			line-height: 1;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
		}
		.nav-tabs-responsive > li.prev > a:after {
			content: "\e079";
		}
		.nav-tabs-responsive > li.next > a:after {
			content: "\e080";
		}
		.nav-tabs-responsive > li.dropdown > a > .caret {
			display: none;
		}
		.nav-tabs-responsive > li.dropdown > a:after {
			content: "\e114";
		}
		.nav-tabs-responsive > li.dropdown.active > a:after {
			display: none;
		}
		.nav-tabs-responsive > li.dropdown.active > a > .caret {
			display: inline-block;
		}
		.nav-tabs-responsive > li.dropdown .dropdown-menu.pull-xs-left {
			left: 0;
			right: auto;
		}
		.nav-tabs-responsive > li.dropdown .dropdown-menu.pull-xs-center {
			right: auto;
			left: 50%;
			-webkit-transform: translateX(-50%);
			-moz-transform: translateX(-50%);
			-ms-transform: translateX(-50%);
			-o-transform: translateX(-50%);
			transform: translateX(-50%);
		}
		.nav-tabs-responsive > li.dropdown .dropdown-menu.pull-xs-right {
			left: auto;
			right: 0;
		}
	}
</style>
<script>
	inicializar(function(){
		///Pestañas responsivas
		$('.contenedor-tabs-permisos').on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
			var $target = $(e.target);
			var $tabs = $target.closest('.nav-tabs-responsive');
			var $current = $target.closest('li');
			var $parent = $current.closest('li.dropdown');
			$current = $parent.length > 0 ? $parent : $current;
			var $next = $current.next();
			var $prev = $current.prev();
			$tabs.find('>li').removeClass('next prev');
			$prev.addClass('prev');
			$next.addClass('next');
		});
		
		////Guardar Formulario
		$('#form-permisos-rol').on('submit', function(e) {
    	    e.preventDefault();
    	    e.stopPropagation();
    	    e.isDefaultPrevented = function () {
    	        return false;
    	    }
    	    var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			$(".help-block").addClass('help-block-visible');
			$('.form-group').removeClass("has-error").find('.help-block>strong').html('');
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				console.log(respuesta);
				if (respuesta.data.status == 200 && respuesta.data.statusText == "OK") {
					mensaje_alerta ("Hecho!", "Cambios guardados correctamente").modal('show');
				}else if(respuesta.data.success){
					if(respuesta.data.status == 403){
						mensaje_alerta ("Error!", respuesta.data.responseText).modal('show');
						formulario.reset();
					}else{
						mensaje_alerta ("Hecho!", respuesta.data.mensaje).modal('show');
					}
				}else{
					if (respuesta.data.responseText) {
						$("body").html(respuesta.data.responseText);
					}
				}
			});
    	});
	});
</script>
@endsection