@extends('layouts.pagina_maestra')
@section('titulo')
Roles & Permisos
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h1>Roles</h1>
	@foreach ($roles as $rol)
	<div class="col-sm-6 text-center col-roles">
		<div class="adm-rol-card">
			<div class="admn-rol-details rol_{{$rol->name}}">
				<p class="admn-rol-name"><strong>{{$rol->display_name}} ({{$rol->name}})</strong></p>
				<p class="admn-rol-detalles">{{$rol->description}}</p>
			</div>
			<a href="{{ Request::url().'/'.$rol->id.'/editar' }}" class="link_dinamico editar-rol">
				<i class="fa fa-key" aria-hidden="true"></i>
			</a>
		</div>
	</div>
	@endforeach
</div>
<style>
	.adm-rol-card{
		height: 150px;
		width: 100%;
		background-color: #F1F1F1;
		border-radius: 10px;
		display: flex;
		justify-content: space-around;
		margin: 20px 0px 20px 0px;
		-webkit-box-shadow: 0px 5px 20px -8px rgba(0,0,0,1);
		-moz-box-shadow: 0px 5px 20px -8px rgba(0,0,0,1);
		box-shadow: 0px 5px 20px -8px rgba(0,0,0,1);
	}
	.editar-rol{
		text-align: center;
		/*flex-grow: 2;*/
		width: 30px;
		min-width: 30px;
		height: 30px;
		margin: 0px 10px 0px 10px; 
		align-self: center;/*| flex-start | flex-end | center | baseline | stretch;*/
		line-height: 30px;
		font-size: 40px;
		color: #6F6F6F;
	}
	.admn-rol-details{
		background-color: #d5e1e9;
		text-align: justify;
		border-radius: 10px 0px 0px 10px;
		padding: 10px;
		padding-bottom: 0px;
		flex-grow: 3;
		display: flex;
		flex-direction: column;
		justify-content: center;
		min-width: 0;
	}
	.rol_root{
		background-color: #E5DC9E;
		color: #4D471F;
	}
	.admn-rol-details>p{
		/* white-space: nowrap; */
  		overflow: hidden;
  		text-overflow: ellipsis;
	}
	.admn-rol-name{
		white-space: nowrap !important;
		font-size: 20px;
	}
	.admn-rol-detalles{
		font-size: 13px;
	}
	@media (max-width: 767px) {
		.adm-rol-card{
			height: 130px;
		}
		.col-roles{
			padding-left: 0px !important;
			padding-right: 0px !important;
		}		
	}
	@media (min-width:768px) and (max-width: 991px) {
		.adm-rol-card{
			height: 170px;
		}
	}
	@media (min-width: 992px) and (max-width: 1199px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
@endsection