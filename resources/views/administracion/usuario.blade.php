@extends('layouts.pagina_maestra')
@section('titulo')
{!!$usuario->NombreCompleto()!!}
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1" id="pagina-usuario-adm">
	{{-- <h1>{{$usuario->NombreCompleto()}}</h1> --}}
	<hr>
	<h3>Persona:</h3>
	<p>Nombre: <strong>{{$usuario->NombreCompleto()}}</strong></p>
	<p>Id: <strong>{{$usuario->persona->id}}</strong></p>
	<h3>E-mail:</h3>
	<p><strong>{{$usuario->email}}</strong></p>
	<hr>
	<h3>Roles:</h3>
	@forelse($usuario->roles as $rol)
	<p>
		<strong>
			<a class="link_dinamico" href="{{ url('administrar/roles-permisos/'.$rol->id.'/editar') }}"><i class="fa fa-key" aria-hidden="true"></i> {{$rol->display_name}}</a>	
		</strong>
	</p>
	@empty
	<span>El usuario no tiene roles asignados.</span>
	@endforelse
	<h3>Habilidades:</h3>
	<div class="panel panel-default" style="overflow: hidden;">
		<div class="panel-body">
			@foreach(App\Permiso::all() as $permiso)
				@if($usuario->can($permiso->name))
					<span class="habilidad-tag">{{$permiso->display_name}}</span>
				@endif
			@endforeach
			@if(!($usuario->can(App\Permiso::all()->pluck('name')->toArray())))
				<span>Ninguna.</span>
			@endif
		</div>
	</div>
	<hr>
	<a href="{{ url('administrar/usuarios/'.$usuario->id.'/editar') }}" class="link_dinamico btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Usuario</a>
	<hr>
</div>
<style>
	.habilidad-tag{
		background-color: #207fb1;
		padding: 5px;
		border-radius: 6px;
		color: #FFFFFF;
		white-space: nowrap;
		line-height: 30px;
		transition: all 200ms;
		box-shadow: #000 0px 3px 5px -3px;
	}
	.habilidad-tag:hover{
		background-color: #1C648A;
		color: #FFFFFF;
	}
</style>
<script>
	inicializar(function(){
				
	});
</script>
@endsection