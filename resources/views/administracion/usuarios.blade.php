@extends('layouts.pagina_maestra')
@section('titulo')
Administrar Usuarios
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h1>Usuarios</h1>
	@foreach ($usuarios as $usuario)
	<div class="col-sm-6 text-center col-admn-users">
		<a href="{{ url('administrar/usuarios/'.$usuario->id) }}" class="link_dinamico admn-user-link">
			<div class="admn-user-card">
				<div class="admn-user-photo"></div>
				<div class="admn-user-details {{ ($usuario->hasRole('root') ? 'rol_root' : '') }}">
					<p class="admn-user-name"><strong>{{$usuario->persona->NombreCompleto()}}</strong></p>
					<p class="admn-user-mail">{{$usuario->email}}</p>
					<p class="admn-user-activo">{{ ($usuario->activo ? 'Activo' : 'Inactivo' ) }}</p>
					<p class="admn-user-rol">Roles:</p>
					<div class="rolito-papa text-left">
					@forelse($usuario->roles as $rol)
						<span class="rolito"><i class="fa fa-key" aria-hidden="true"></i> {{$rol->name}}</span>
					@empty
						<span>El usuario no tiene roles asignados.</span>
					@endforelse
					</div>
				</div>
			</div>
		</a>
	</div>
	@endforeach
</div>
<style>
	.rolito-papa{
		height: 50px;
		max-height: 50px;
		overflow: hidden;
	}
	.rolito{
		background-color: rgba(0, 0, 0, 0.2);
		padding: 3px;
		border-radius: 10px;
		white-space: nowrap;
	}
	.admn-user-card{
		height: 200px;
		width: 100%;
		background-color: #F1F1F1;
		border-radius: 10px;
		display: flex;
		flex-wrap: nowrap;
		justify-content: space-around;
		margin: 20px 0px 20px 0px;
		-webkit-box-shadow: 0px 5px 20px -8px rgba(0,0,0,1);
		-moz-box-shadow: 0px 5px 20px -8px rgba(0,0,0,1);
		box-shadow: 0px 5px 20px -8px rgba(0,0,0,1);
	}
	.admn-user-photo{
		background-color: #BEBEBE;
		text-align: center;
		/*flex-grow: 2;*/
		width: 100px;
		min-width: 100px;
		height: 100px;
		margin: 0px 10px 0px 10px; 
		border-radius: 100%;
		align-self: center;/*| flex-start | flex-end | center | baseline | stretch;*/
		line-height: 100px;
		font-size: 40px;
		color: #6F6F6F;
	}
	.admn-user-photo:before{
		content: '\f007';
    	font-family: 'FontAwesome';
    	font-style: normal;
    	font-weight: normal;
    	text-decoration: inherit;
	}
	.admn-user-details{
		background-color: #d5e1e9;
		color: #005083;
		text-align: center;
		border-radius: 0px 10px 10px 0px;
		padding: 10px;
		padding-bottom: 0px;
		flex-grow: 3;
		display: flex;
		flex-direction: column;
		justify-content: center;
		min-width: 0;
	}
	.rol_root{
		background-color: #E5DC9E;
		color: #4D471F;
	}
	.admn-user-details>p{
		/*white-space: nowrap !important;*/
		white-space: nowrap;
  		overflow: hidden;
  		text-overflow: ellipsis;
	}
	.admn-user-name{
		font-size: 20px;
		/* text-decoration: underline; */
	}
	.admn-user-mail{
		font-size: 13px;
	}
	.admn-user-rol{
		/* white-space: normal !important; */
		/* line-height: 25px; */
		font-size: 13px;
	}
	.admn-user-link, .admn-user-link:hover, .admn-user-link:active, .admn-user-link:visited{
		text-decoration: none !important;
	}
	@media (max-width: 767px) {
		.admn-user-card{
			height: 130px;
		}
		.col-admn-users{
			padding-left: 0px !important;
			padding-right: 0px !important;
		}
		.admn-user-photo{
			width: 70px;
			height: 70px;
			line-height: 70px;
			min-width: 70px;
		}
	}
	@media (min-width:768px) and (max-width: 991px) {
		.admn-user-photo{
			width: 90px;
			height: 90px;
			line-height: 90px;
			min-width: 90px;
		}
		.admn-user-card{
			height: 170px;
		}
	}
	@media (min-width: 992px) and (max-width: 1199px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
@endsection