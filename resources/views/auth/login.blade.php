@extends('layouts.pagina_maestra')
@section('titulo')
Iniciar Sesión
@endsection
@section('contenido')
<div class="col-md-12">
    <!-- <h1>Repositorio de Documentos</h1> -->
    <div class="row" id="row-login-form">
        <div class="col-xs-10 col-sm-6 col-xs-offset-1 col-sm-offset-3 col-form-ch" id="col-login">
            <h2>Iniciar Sesión</h2>
            <form id="form-login" role="form" method="POST" action="{{ url('/login') }}">
                {!!csrf_field()!!}
                <input type="hidden" name="test" value="{{url()->previous()}}">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label" for="email">E-Mail:</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="E-Mail" required="required"/>          
                    <span class="help-block">
                        <strong id="email-error">{{ $errors->first('email') }}</strong>
                    </span>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label" for="password">Contraseña:</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="*********" required="required"/>            
                    <span class="help-block">
                        <strong id="password-error">{{ $errors->first('password') }}</strong>
                    </span>
                </div>
                <div class="form-group" id="login-errors">
                    <span class="help-block">
                        <strong id="form-login-errors"></strong>
                    </span>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Recordarme
                        </label>                        
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-error-msg">
                        <p>Error</p>
                    </div>
                </div>
                <div class="form-group">
                    <a class="btn btn-link pull-left link_dinamico" href="{{ url('/password/reset') }}">Olvido su contraseña?</a>
                    <button type="submit" class="btn btn-primary pull-right">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        <span>&nbsp;&nbsp;Entrar</span>
                    </button>           
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    inicializar (function () {
        //Evento solo para el formulario login
        $('#form-login').on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            e.isDefaultPrevented = function () {
                return false;
            }
            $('.form-group').removeClass('has-error');
            // $(".help-block").removeClass('help-block-visible');
            $(".form-error-msg").removeClass('mostrar-errores');
            $('.help-block > strong').html('');
            //$(this).trigger(e);
            var formulario = $(this)[0];
            var url_action = $(this).attr('action');
            var metodo = $(this).attr('method');
            var tipo_datos = "json";
            var datos = new FormData(formulario);
            enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
                console.log(respuesta);
                console.log(respuesta.success);
                if (respuesta.success) {
                    //console.log(respuesta.data.success);
                    //console.log(respuesta.data.mensaje);                    
                    //Ocultar menu y cargar contenido de menu
                    $('#menu-lateral').addClass('ocultar-menu');
                    $('.ocultar-menu').on('transitionend webkitTransitionEnd oTransitionEnd', function () {
                        console.log('Evento');
                        $('#barra-seccion-usuario').html(respuesta.data.barrausuario);
                        $('#container-menu').html(respuesta.data.menu);                        
                        $('.ocultar-menu').off('transitionend webkitTransitionEnd oTransitionEnd');
                        $('#menu-lateral').removeClass('ocultar-menu');
                    });
                    cargar_contenido_get(respuesta.data.intento_url, 'Seguimiento a Contratos', true, 'push');
                }else{
                    $('.help-block > strong').html('');
                    var obj;
                    if (respuesta.data.status == 500){
                        $(".form-error-msg").html('<p>'+respuesta.data.responseText+'</p>').addClass('mostrar-errores');
                    }else{
                        obj = jQuery.parseJSON(respuesta.data.responseText);
                    }
                    $(".help-block").addClass('help-block-visible');
                    if (obj.email) {
                        $("#email").parent('.form-group').addClass("has-error");
                        $("#password").parent('.form-group').addClass("has-error");
                        $('#email-error').html(obj.email);
                    }
                    if (obj.password) {
                        $("#password").parent('.form-group').addClass("has-error");
                        $('#password-error').html(obj.password);
                    }
                    if (obj.error) {
                        $("#login-errors").addClass("has-error");
                        $('#form-login-errors').html(obj.error);
                    }                    
                }
            });
        });
    });
</script>
@endsection