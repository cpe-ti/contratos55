@extends('layouts.pagina_maestra')
@section('titulo')
Reestablecer Contraseña
@endsection
@section('contenido')
<div class="col-md-12">
    <div class="row" id="row-reset-pwd-mail-form">
        <div class="col-xs-10 col-sm-8 col-md-8 col-lg-6 col-xs-offset-1 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-form-ch" id="col-reset-pwd-mail">
            <h2>Reestablecer Contraseña</h2>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form id="form-reset-pwd-mail" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label" for="email">E-Mail:</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="E-Mail" value="{{ old('email') }}" required="required"/>          
                        <span class="help-block">
                            <strong id="email-error">{{ $errors->first('email') }}</strong>
                        </span>
                    @if ($errors->has('email'))
                    @endif
                </div>
                <div class="form-group" id="reset-errors">
                    <span class="help-block">
                        <strong id="form-reset-errors"></strong>
                    </span>
                </div>
                <div class="form-group">
                    <div class="form-error-msg">
                        <p>Error</p>
                    </div>
                </div>
                <div class="form-group">
                    <a class="btn btn-link pull-left link_dinamico" href="{{ url('/login') }}">Iniciar Sesión</a>
                    <button type="submit" class="btn btn-primary pull-right">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        <span>&nbsp;&nbsp;Enviar Correo</span>
                    </button>           
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    inicializar (function () {
        $("#form-reset-pwd-mail").on('submit', function(event) {
            event.preventDefault();
            var formulario = $(this)[0];
            var url_action = $(this).attr('action');
            var metodo = $(this).attr('method');
            var tipo_datos = "json";
            var datos = new FormData(formulario);
            $(':input, .btn').prop('disabled', true);
            enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
                console.log(respuesta);
                var titulo = "";
                var mensaje = "";
                var ok = false;
                if (respuesta.success == true) {
                    if (respuesta.data.success) {
                        titulo = "Hecho";
                        mensaje = respuesta.data.mensaje;                        
                        ok = true;
                    }else{
                        titulo = "Uy!";
                        mensaje = "Algo paso pero no sabemos que.<br>"+respuesta.data.responseText;                        
                        ok = false;
                    }
                }else {
                    switch (respuesta.data.status) {
                        case 100:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 101:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 102:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 103:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 200:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 201:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 202:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 203:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 204:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 205:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 206:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 207:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 208:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 300:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 301:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 302:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 303:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 304:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 305:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 306:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 307:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 308:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 400:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 401:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 402:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 403:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 404:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 405:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 406:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 407:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 408:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 409:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 410:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 411:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 412:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 413:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 414:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 415:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 416:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 417:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 418:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 422:
                            //Laravel validations
                            titulo = "Error";
                            mensaje = "*";
                            $.each(respuesta.data.responseJSON.errors, function(index, val) {
                                mensaje += "<strong>"+index+"</strong>:<br>";
                                $.each(val, function(i, v) {
                                    mensaje += v+"<br>";
                                });
                            });
                            break;
                        case 423:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 424:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 425:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 426:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 428:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 429:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 451:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 500:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText + "<br>"+respuesta.data.responseText;
                            break;
                        case 501:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 502:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 503:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 504:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 505:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 506:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 507:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 508:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 509:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 510:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 511:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                        case 512:
                            titulo = 'Error';
                            mensaje = respuesta.data.status + ' - ' + respuesta.data.statusText;
                            break;
                    }
                }
                history.pushState({url: window.location.href+"#modal", titulo:'mensaje'}, null, window.location.href+"#modal"); 
                bootbox.dialog({
                    title: titulo,
                    message: mensaje,
                    onEscape: function(){
                        
                    },
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn-success",
                            callback: function () {
                                window.history.back();
                                ok ? ok : $(':input, .btn').prop('disabled', false);
                                ok ? window.history.back() : ok;
                                return false;
                            }
                        }
                    }
                });
            });            
        });
    });
</script>
@endsection
