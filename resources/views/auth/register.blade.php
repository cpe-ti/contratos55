@extends('layouts.pagina_maestra')
@section('titulo')
Registro de Usuario
@endsection
@section('contenido')
<div class="col-md-12">
    <div class="row" id="row-register-form">
        <div class="col-xs-10 col-sm-8 col-xs-offset-1 col-sm-offset-2 col-form-ch" id="col-register">
            <h2>Registrarse</h2>
            <form id="form-registro" role="form" method="POST" action="{{ url('/register') }}">
                {!!csrf_field()!!}
                <div class="form-group{{ $errors->has('persona_id') ? ' has-error' : '' }}">
                    <label for="persona_id" class="control-label">Numero de Documento de Identidad:</label>
                    <input id="persona_id" type="number" class="form-control" name="persona_id" value="{{ old('persona_id') }}" required autofocus>
                    <span class="help-block">
                        <strong id="id-persona-error">{{ $errors->first('persona_id') }}</strong>
                    </span>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label" for="email">E-Mail:</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="E-Mail" required="required"/>          
                    <span class="help-block">
                        <strong id="email-error">{{ $errors->first('email') }}</strong>
                    </span>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label" for="password">Contraseña:</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="*********" required="required"/>
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="control-label">Confirmar contraseña</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="*********" required="required"/>
                    <span class="help-block">
                        <strong id="password-error">{{ $errors->first('password_confirmation') }}</strong>
                    </span>                    
                </div>
                <div class="form-group" id="login-errors">
                    <span class="help-block">
                        <strong id="form-login-errors"></strong>
                    </span>
                </div>
                <div class="form-group">
                    <div class="form-error-msg">
                        <p>Error</p>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        <span>&nbsp;&nbsp;Registrarse</span>
                    </button>           
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    inicializar (function () {
        //Evento solo para el formulario login
        $('#form-registro').on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            e.isDefaultPrevented = function () {
                return false;
            }
            //return false;
            $('.form-group').removeClass('has-error');
            $(".help-block").removeClass('help-block-visible');
            $(".form-error-msg").removeClass('mostrar-errores');
            //$(this).trigger(e);
            var formulario = $(this)[0];
            var url_action = $(this).attr('action');
            var metodo = $(this).attr('method');
            var tipo_datos = "json";
            var datos = new FormData(formulario);
            enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
                console.log(respuesta);
                console.log(respuesta.success);
                if (respuesta.success) {
                    $('.help-block > strong').html('');
                    console.log(respuesta.data.success);
                    console.log(respuesta.data.mensaje);
                    $(".form-error-msg").html('<p>'+respuesta.data.mensaje+'</p>').addClass('mostrar-errores');                    
                    $('#menu-lateral').addClass('ocultar-menu');
                    $('.ocultar-menu').on('transitionend webkitTransitionEnd oTransitionEnd', function () {
                        console.log('Evento');
                        $('#barra-seccion-usuario').html(respuesta.data.barrausuario);
                        $('#container-menu').html(respuesta.data.menu);                        
                        $('.ocultar-menu').off('transitionend webkitTransitionEnd oTransitionEnd');
                        $('#menu-lateral').removeClass('ocultar-menu');
                    });
                    cargar_contenido_get(respuesta.data.intento_url, '', false, 'replace');
                }else{
                    $('.help-block > strong').html('');
                    $(".form-error-msg").html('<p>'+respuesta.data.responseText+'</p>').addClass('mostrar-errores');
                    var obj = jQuery.parseJSON(respuesta.data.responseText);
                    console.log(obj);
                    $(".help-block").addClass('help-block-visible');
                    if (obj.persona_id) {
                        $("#persona_id").parent('.form-group').addClass("has-error");
                        $('#id-persona-error').html(obj.persona_id);
                    }                    
                    if (obj.email) {
                        $("#email").parent('.form-group').addClass("has-error");
                        $('#email-error').html(obj.email);
                    }
                    if (obj.password) {
                        $("#password-confirm").parent('.form-group').addClass("has-error");
                        $('#password-error').html(obj.password);
                    }
                    if (obj.error) {
                        $("#login-errors").addClass("has-error");
                        $('#form-login-errors').html(obj.error);
                        //$("#login-error-msg").html('<p>'+obj.error+'</p>').addClass('mostrar-errores');
                    }                                        
                }
            });
        });
    });
</script>
@endsection