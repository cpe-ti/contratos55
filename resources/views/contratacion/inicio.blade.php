@extends('layouts.pagina_maestra')
@section('titulo')
Contratación
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	{{-- <h1>Contratación</h1> --}}
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('contratacion/justificaciones') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-book" aria-hidden="true"></i>
			</a>
			<div class="texto_item_menu">Justificaciones</div>			
		</div>
	</div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('contratacion/contratos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-file-text-o" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Contratos</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('contratacion/otrosis') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-files-o" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Otrosis</span>
		</div>
	</div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('contratacion/cartas-adjudicacion') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-file-text-o" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Cartas de Adjudicación</span>
		</div>
	</div>	
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('contratacion/ordenes-compra') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-file-text-o" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Órdenes de compra</span>
		</div>
	</div>
</div>
@endsection