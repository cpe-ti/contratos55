@extends('layouts.pagina_maestra')
@section('titulo')
{{$contrato->NombreRapido()}}
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<div class="row">
		<div class="col-sm-3">
			<div class="contrato-icon">
				{{-- <img class="image-cto" src="{{URL::asset('img/iconos/contratos/contrato-personalizado.svg')}}" alt=""> --}}
				{!! str_replace("NCICON", substr($contrato->tercero->NombreCompleto(),0,35).'...', svgRawContent( public_path( crearRuta('img', 'iconos', 'contratos', 'contrato-personalizado.svg') ) ) ) !!}
				{{--  {!!str_replace("NCICON", substr($contrato->tercero->NombreCompleto(),0,35).'...', file_get_contents(URL::asset('img/iconos/contratos/contrato-personalizado.svg')))!!}  --}}
				{{-- {!!$contrato->icono()!!} --}}
			</div>
		</div>
		<div class="col-sm-9">
			<table style="width: 100%">
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>ID Oasis:</strong></td>
					<td>{{$contrato->id_oasis}}</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>Consecutivo CPE:</strong></td>
					<td>{{$contrato->consecutivo_cpe}}</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>Tercero:</strong></td>
					<td>{{$contrato->tercero->NombreCompleto()}}</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>Valor:</strong></td>
					@php
						$fmt = new NumberFormatter( 'es_CO', NumberFormatter::CURRENCY );
					@endphp
					<td>{{ "$ ".number_format($contrato->valor, 2, '.', ',') }}</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>Compra(s):</strong></td>
					<td>
						@foreach ($contrato->compra as $compra)
						{{$compra->id_oasis}}: {{$compra->descripcion}}
						@if($loop->index > 0)
						<br>
						@endif
						@endforeach
					</td>
				</tr>				
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>CDP:</strong></td>
					<td>{{$contrato->cdp}}</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>RP:</strong></td>
					<td>{{$contrato->rp}}</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>Modalidad de Contratación:</strong></td>
					<td>{{$contrato->modalidad->modalidad}}</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>Supervisor:</strong></td>
					<td>
						@if($contrato->supervisor)
						{{$contrato->persona->NombreCompleto()}}
						@else
						Sin supervisor
						@endif
					</td>
				</tr>
				<tr style="border-bottom: 1px solid #EAEAEA">
					<td style="padding-right: 10px; text-align: left;"><strong>Estado:</strong></td>
					<td>{{$contrato->estado->estado}}</td>
				</tr>
				<tr>
					<td style="padding-right: 10px; text-align: left;"><strong>URL SECOP:</strong></td>
					<td></b><a target="_blank" href="{{$contrato->url_secop}}">{{$contrato->url_secop}}</a></td>
				</tr>
				<tr>
					<td style="padding-right: 10px; text-align: left;"><strong>PDF:</strong></td>
					<td></b><a class="btn btn-danger" target="_blank" href="{{url('/imprimir-contrato')."/".$contrato->id}}">
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
					</a></td>
				</tr>
			</table>
		</div>
	</div>
	<hr/>
	@if (Request::segment(1) == 'gaf')

	@endif
	@if (Request::segment(1) == 'contratacion')
	<div class="row fila-acciones-contratacion">
		@php
		$estado = $contrato->estado_doc;
		@endphp
		<div class="col-md-12">
			<span>Contratación:{{Request::segment(1)}}</span>
		</div>
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/editar" class="link_dinamico btn btn-default" {{$estado != 1 && $estado != 2 && $estado != 4 ? 'disabled=disabled': ''}}>
				<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Contrato
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-btn">
			@if($estado == 1)
			<a href="{{ Request::url() }}/crear-cuerpo" class="link_dinamico btn btn-default">
				<i class="fa fa-file-text-o" aria-hidden="true"></i> Crear Cuerpo
			</a>
			@else
			<a href="{{ Request::url() }}/editar-cuerpo" class="link_dinamico btn btn-default" {{$estado != 2 && $estado != 4 ? 'disabled=disabled': ''}}>
				<i class="fa fa-file-text-o" aria-hidden="true"></i> Editar Cuerpo
			</a>
			@endif
		</div>
		@if($estado == 5 && Auth::user()->can('reversar-documento-aprobado'))
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/revertir-aprobacion" class="link_dinamico btn btn-danger">
				<i class="fa fa-undo" aria-hidden="true"></i> Revertir Aprobación
			</a>
		</div>
		@else
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/aprobar" class="link_dinamico btn btn-default" {{$estado != 3 ? 'disabled=disabled': ''}}>
				<i class="fa fa-check-square-o" aria-hidden="true"></i> Aprobación
			</a>
		</div>
		@endif
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/legalizar" class="link_dinamico btn btn-default" {{$estado != 5 ? 'disabled=disabled': ''}}>
				<i class="fa fa-gavel" aria-hidden="true"></i> Legalización
			</a>
		</div>
	</div>
	<div class="row fila-info-contratacion">
		<div class="col-md-12">
			<table class="table table-bordered table-hover">
				<tr>
					<td><strong>Creado Por:</strong></td>
					<td>{{ $contrato->CreadoPor() }}</td>
				</tr>
				@if ($estado != 1 && $estado != 2)
				<tr>
					<td><strong>Proyectó:</strong></td>
					<td>{{ $contrato->ProyectadoPor() }}</td>
				</tr>
				@endif
				<tr>
					<td><strong>Estado:</strong></td>
					<td class="{{$contrato->estado_documento->estado}}"><span>{{$contrato->estado_documento->estado}}</span></td>
				</tr>
				@if ($estado == 4)
				<tr>
					<td><strong>Rechazado por:</strong></td>
					<td><span>{{ $contrato->RechazadoPor() }}</span></td>
				</tr>
				<tr>
					<td><strong>Observaciones:</strong></td>
					<td><textarea class="form-control" readonly="readonly" style="max-width: 100%; resize:vertical;">{{$contrato->observaciones}}</textarea></td>
				</tr>
				@endif
				@if ($estado == 5)
				<tr>
					<td><strong>Aprobado por:</strong></td>
					<td><span>{{ $contrato->AprobadoPor() }}</span></td>
				</tr>
				@endif
			</table>
		</div>
	</div>
	@endif
	@if (Request::segment(1) == 'supervision')
	<div class="row fila-acciones-supervision">
		<div class="col-md-12">
			<span>Supervisión:</span>
		</div>
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/plazo-ejecucion" class="link_dinamico btn btn-default" {{$contrato->fecha_inicio && $contrato->fecha_terminacion ? 'disabled=disabled' : ''}}>
				<i class="fa fa-clock-o" aria-hidden="true"></i> Plazo de Ejecución
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/agregar-productos" class="link_dinamico btn btn-default" {{$contrato->productos->count() > 0 ? 'disabled=disabled' : ''}}>
				<i class="fa fa-shopping-cart" aria-hidden="true"></i> Productos
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/crear-programacion-tecnica" class="link_dinamico btn btn-default" {{$contrato->tiene_programacion_tecnica() ? 'disabled=disabled' : '' }}>
				<i class="fa fa-calendar" aria-hidden="true"></i> Prog. Técnica
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/crear-programacion-financiera" class="link_dinamico btn btn-default" >
				<i class="fa fa-money" aria-hidden="true"></i> Prog. Financiera
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-btn">
			<a href="{{ Request::url() }}/registrar-avance" class="link_dinamico btn btn-default" >
				<i class="fa fa-tachometer" aria-hidden="true"></i> Registro de avance
			</a>
		</div>
	</div>
	<div class="row fila-info-supervision">
		<div class="col-md-12">
			<table class="table table-bordered table-hover">
				
			</table>
		</div>
	</div>
	@endif
</div>
<style>
	td.rechazado>span{
		background-color: #FFA1A1;
		color: #A30000;
		text-transform: capitalize;
		padding-left:5px; 
		padding:5px;
		border-radius: 5px; 
	}
	td.aprobado>span{
		background-color: #98E3AD;
		color: #006C33;
		text-transform: capitalize;
		padding-left:5px; 
		padding:5px;
		border-radius: 5px; 
	}
	.col-btn{
		margin-top: 5px; 
		margin-bottom: 5px;
	}
	.contrato-icon{
		text-align: center;
		height: 180px;
		line-height: 180px;
		width: 100%;
		margin:0 auto;
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.contrato-icon>.image-cto, .contrato-icon>svg{
		width: 100%;
		max-height: 100%;
	}
	#chartsvg{
		max-width: 100%;
	}
	#chartsvg rect {
		fill: #0075BF;
	}
	#chartsvg text {
		fill: white;
		font: 25px sans-serif;
		text-anchor: end;
	}
	.tarjeta_producto{
		background-color: #595959;
		color: #FFFFFF;
		border-radius: 10px;
		padding: 15px;
		margin-top: 10px;
		margin-left: 0px !important;
		margin-right: 0px !important;
	}
	.tarjeta_producto p.detalle_producto{
		line-height: 90%;
	}
	.tarjeta_producto p.progreso{
		font-size: 40px;
		color: rgba(255, 255, 255, 0.5)
	}
	.fila-acciones-contratacion a.btn, .fila-acciones-supervision a.btn{
		width: 100%;
	}
</style>
<script type="text/javascript">
	inicializar(function(){
		
	});
</script>
@endsection