@extends('layouts.pagina_maestra')
@section('titulo')
Editar {{$contrato->NombreRapido()}}
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h3>Editar Contrato</h3>
	<form id="editar_contrato" action="{{Request::url()}}" method="POST" class="prevent_lost">
		{!!csrf_field()!!}
		<div class="panel panel-default panel-nuevo-cto">
			<div class="panel-heading">
				<h3 class="panel-title">Editar Datos de Contrato</h3>
			</div>
			<div class="panel-body">
				<div class="form-group col-md-6">
            		<label class="control-label" for="tipo_cto">Tipo de contrato:</label>
            		<select name="tipo_cto" class="form-control" id="tipo_cto" required="required">
            			<option value="">Seleccionar..</option>
            			@foreach ($tipo_cto as $tipo)
            			<option value="{{$tipo->id}}"{{$tipo->id == $contrato->tipo_contrato_id ? 'selected' : ''}}>{{$tipo->tipo}}</option>
            			@endforeach
            		</select>
            		<span class="help-block">
            			<strong id="tipo_cto_error">{{ $errors->first('tipo_cto') }}</strong>
            		</span>
            	</div>
            	<div class="form-group col-md-6">
            		<label class="control-label" for="modalidad_cto">Modalidad de contratación:</label>
            		<select name="modalidad_cto" class="form-control" id="modalidad_cto" required="required">
            			<option value="">Seleccionar..</option>
            			@foreach ($modalidades as $modalidad)
            			<option value="{{$modalidad->id}}"{{$modalidad->id == $contrato->modalidad_id ? 'selected' : ''}}>{{$modalidad->modalidad}}</option>
            			@endforeach
            		</select>
            		<span class="help-block">
            			<strong id="modalidad_cto_error">{{ $errors->first('modalidad_cto') }}</strong>
            		</span>
            	</div>
            	<div class="form-group col-md-12">
            		<label class="control-label" for="tercero_id">Tercero:</label>
            		<div class="input-group col-md-12">
            			<input type="number" name="tercero_id" class="form-control text-center" style="width: 100%;" id="tercero_id" placeholder="ID Tercero" readonly="readonly" value="{{$contrato->tercero_id}}" required="required"/>
            			<input type="number" name="tercero_tipo" class="form-control text-center" style="width: 0%; display: none;" id="tercero_tipo" placeholder="Tipo" readonly="readonly" value="{{$contrato->tipo_tercero}}" required="required"/>
            			<span class="input-group-btn">
            				<button class="btn btn-primary" id="boton-buscar-tercero" type="button">
            					<i class="fa fa-search" aria-hidden="true"></i>
            					<span class="texto-btn-hide">
            						Buscar
            					</span>
            				</button>
            			</span>
            		</div>
            		<span class="help-block">
            			<strong id="tercero_id_error">{{ $errors->first('tercero_id') }}</strong>
            			<strong id="tercero_id_error">{{ $errors->first('tercero_tipo') }}</strong>
            		</span>
            	</div>
            	<div class="form-group col-md-12">
            		<div id="nombre_tercero" class="well well-sm" style="margin-bottom: 0px;">
            			{{$contrato->tercero->NombreCompleto()}}
            		</div>
            	</div>
            	<div class="form-group col-md-6">
            		<label class="control-label" for="valor_cto">Valor del contrato:</label>
            		<div class="input-group">
            			<span class="input-group-addon" id="basic-addon1">
            				<i class="fa fa-usd" aria-hidden="true"></i>
            			</span>
            			<input name="valor_cto" class="form-control text-right" id="valor_cto" placeholder="Valor del contrato" data-inputmask="'alias': 'currency', 'autoUnmask': true" value="{{$contrato->valor}}" required="required"/>
            		</div>
            		<span class="help-block">
            			<strong id="valor_cto_error">{{ $errors->first('valor_cto') }}</strong>
            		</span>
            	</div>
            	<div class="form-group col-md-6">
            		<label class="control-label" for="cdp_cto">Provisión Presupuestal Nº:</label>
            		<input name="cdp_cto" class="form-control text-right" id="cdp_cto" placeholder="CDP" value="{{$contrato->cdp}}" required="required"/>
            		<span class="help-block">
            			<strong id="cdp_cto_error">{{ $errors->first('cdp_cto') }}</strong>
            		</span>
            	</div>
            	<div class="form-group col-md-12" id="parent_select_compras">
            		<label class="control-label" for="select_compras">Compra o Compras:</label>
            		<select name="compra[]" id="select_compras" class="form-control" style="width: 100%" multiple="multiple">{{-- required="required" --}}
            			@foreach($compras as $compra)
            				@php
            					$selected_c = in_array($compra->id, array_pluck($contrato->compra, 'id')) ? ' selected' : '';
            				@endphp
            				<option value="{{$compra->id}}"{{$selected_c}}>{{ $compra->descripcion }}</option>
            			@endforeach
            		</select>
            		<span class="help-block">
            			<strong id="compra_error">{{ $errors->first('compra') }}</strong>
            		</span>
            	</div>
            </div>
            <div class="panel-footer text-right">
            	<button type="submit" class="btn btn-primary">
            		<i class="fa fa-floppy-o" aria-hidden="true"></i>
            		<span>&nbsp;&nbsp;Guardar</span>
            	</button>				
            </div>
        </div>
    </form>
    <div>
    	<div class="col-md-12 text-center" id="success-message">
    		<i class="fa fa-check-circle" aria-hidden="true"></i><span> Datos Guardados</span>
    	</div>
    </div>
	<div id="errores"></div>
</form>
</div>
<style>
	#success-message{
		color: green;
		font-size: 18pt;
		display: none;
	}
	.nombre-compra{
		white-space: nowrap;
		flex-grow: 1;
		overflow: hidden;
    	text-overflow: ellipsis;
	}
	.compra-l{
		display: flex;
		flex-direction: row;
	}
	.compra-l>.id-compra{
		flex-grow: 0;
		font-size: 8pt;
		background-color: #1c5c76;;
		color: #FFFFFF;
		border-radius: 5px;
		padding: 3px;
		padding-right: 5px;
		padding-left: 5px;
	}
	.compra-numero{
		background-color: #326492;
		color: #FFFFFF;
		border-radius: 3px;
		padding-right: 5px;
		padding-left: 5px;
	}
</style>
<script type="text/javascript">
	inicializar(function(){
		$("#select_compras").select2({
			maximumSelectionLength: 5,
			dropdownParent: $("#parent_select_compras"),
			language: "es",
			placeholder: 'Seleccione una o mas compras',
			templateSelection: function(item){
				//return $(item.text+' <span class="compra-numero">'+item.id+'</span>');
				return $('<span><span class="compra-numero">'+item.id+'</span>&nbsp;&nbsp;'+item.text+'</span>');
			},
			templateResult: function (item) {
				return $('<div class="compra-l"><span class="nombre-compra">'+item.text+'</span><span class="id-compra">'+item.id+'</span></div>');
			},
			matcher: function(params, data) {
				if ($.trim(params.term) === '') {
					return data;
				}
				var termino = params.term.toUpperCase();
				var id = data.id.toUpperCase();
				var texto = data.text.toUpperCase();
				if(texto.indexOf(termino) >= 0 || id.indexOf(termino) >= 0){
					return data;
				}
				return null;
			}
		});
		
		
		$(":input").inputmask();
		

		//ençventos
		$("#boton-buscar-tercero, #tercero_id").on('click, keypress, focus', function(event) {
			event.preventDefault();
			mostrar_terceros();
		});
		$("#editar_contrato").on('submit', function(event) {
			event.preventDefault();
			event.stopPropagation();
			$("#valor_cto").inputmask('remove');
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			$(':input, .btn').prop('disabled', true);
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				$(":input").inputmask();
				var data = respuesta.data;
				if("responseText" in data){
					$(':input, .btn').prop('disabled', false);
					switch (data.status) {
						case 422:
							// errores validacion laravel
							// var errores = JSON.parse(data.responseText);
							var errores = data.responseJSON.errors;
							//console.log(errores);
							$.each(errores, function(index, val) {
								$("[name^="+index+"]").closest('.form-group').addClass('has-error');
								var er = "";
								$.each(val, function(index, val) {
									er += val+" ";
								});
								$("#"+index+"_error").html(er);								
							});
							break;
						case 403:
							$("#errores").html('<div class="alert alert-danger"><strong>Usted no tiene permiso para realizar cambios en esta sección</strong></div>');
							break;
						case 500:
							// statements_1
							$("#errores").html(respuesta.data.responseText);
							break;
						default:
							// statements_def
							break;
					}
				}else if (data.success == true) {
					$("#editar_contrato").removeClass('prevent_lost');
					$("#success-message").css('display', 'block');
					setTimeout(function() {window.history.back()}, 1500);
				}
				//console.log(respuesta);
			});
		});

		//Funciones
		function mostrar_terceros(){
			var modalLista = new bootbox.dialog({
    			title: 'Listado de terceros',
    			className: 'modal_lista_terceros',
    			message: '<p><i class="fa fa-spin fa-spinner"></i> Cargando...</p>',
    			buttons: {
					"Nuevo":{
						label: "<i class='fa fa-plus' aria-hidden='true'></i> Nuevo",
						className:"btn-primary pull-left",
						callback:function() {
							mostrar_form_nuevo_tercero();
							//return false;
						}
					},
					success: {
		 				label: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i> Seleccionar",
		 				className: "btn-success",
		 				callback: function () {
		 					$("#nombre_tercero").html('...');
		 					$("#tercero_id").val($(".tarjeta-tercero.selected").data('id'));
		 					$("#tercero_tipo").val($(".tarjeta-tercero.selected").data('tipo'));
		 					$("#nombre_tercero").html($(".tarjeta-tercero.selected").data('nombre'));
		 					console.log($(".tarjeta-tercero.selected").data('id'));
		 					console.log($(".tarjeta-tercero.selected").data('tipo'));
		 					console.log($(".tarjeta-tercero.selected").data('nombre'));
		 					$(this).blur();
		 					window.history.back();
		 					return false;
		 				}
		 			},
					//success: {
					//	label: "Ok",
					//	className: "btn-success",
					//	callback: function () {
					//		
					//	}
					//},
				}
			});
			modalLista.init(function(){
				history.pushState({url: window.location.href+"#modal", titulo:'mensaje'}, null, window.location.href+"#modal");
				$.ajax({
					url: '{{ url('terceros-lista') }}',
					type: 'GET',
					dataType: 'html',
				})
				.done(function(data) {
					console.log("success");
					//console.log(data);
					modalLista.find('.bootbox-body').html(data);
				})
				.fail(function(data) {
					console.log("error");
					console.log(data);
					if (data.status = 403) {
						modalLista.find('.bootbox-body').html(data.responseText);						
						return true;
					}
					modalLista.find('.bootbox-body').html(data.statusText+": "+data.status);
				})
				.always(function(data) {
					console.log("complete");
					//dialog.find('.bootbox-body').html(data);
				});
			});
		}
		function mostrar_form_nuevo_tercero(){
			var modalForm = new bootbox.dialog({
				title: 'Nuevo tercero',
    			className: 'modalForm_tercero',
    			message: '<p><i class="fa fa-spin fa-spinner"></i> Cargando...</p>',
    			buttons: {
					"Atras":{
						label: "<i class='fa fa-arrow-left' aria-hidden='true'></i> Atras",
						className:"btn-danger pull-left",
						callback:function() {
							//mostrar_form_nuevo_tercero();
							window.history.back();
							//bootbox.hideAll();
							return false;
						}
					},
				},
			});
			modalForm.on("shown.bs.modal", function() {
    		    $('body').addClass('modal-open');
    		});
			modalForm.init(function(){
				$(modalForm).on('hidden.bs.modal', function () {
				    mostrar_terceros();
				})
				$.ajax({
					url: '{{ url('terceros/inline_nuevo') }}',
					type: 'GET',
					dataType: 'html',
				})
				.done(function(data) {
					console.log("success");
					//console.log(data);
					modalForm.find('.bootbox-body').html(data);
				})
				.fail(function(data) {
					console.log("error");
					console.log(data);
					if (data.status = 403) {
						modalForm.find('.bootbox-body').html(data.responseText);						
						return true;
					}
					modalForm.find('.bootbox-body').html(data.statusText+": "+data.status);
				})
				.always(function(data) {
					console.log("complete");
					//dialog.find('.bootbox-body').html(data);
				});
			});
		}
	});
</script>
@endsection