<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Memorando de Supervisión - {{$documento->consecutivo_cpe}}</title>
	<style>
		body{
			font-family: 'Arial';
			font-size: 11pt;
			text-align: justify;
		}
		.cabeza-carta, .firma{
			width: 100%;
		}
		table.tabla-memo{
			width: 100%;
			font-size: 11pt;
			text-align: center;
		}
		table.tabla-memo td{
			font-size: 11pt;			
		}
		td.objeto{
			text-align: justify;
		}
		table.tabla-memo, table.tabla-memo th, table.tabla-memo td{
			border: 1px solid black;
			border-collapse: collapse;
		}
		.resaltado{
			/* background-color: #FCFF00; */
		}
	</style>
</head>
<body>
	<div class="cabeza-carta">		
		@php
			setlocale(LC_ALL, 'esp_esp', 'esp_spain', 'spanish_esp', 'spanish_spain');
			$fecha_leg = new DateTime($documento->fecha_legalizacion);
			$fecha_leg = strftime("%d de %B del %Y", $fecha_leg->getTimestamp());
			
			$sir = "Señor(a)";
			$sup = "Supervisor(a)";
			$des = "designado(a)";
			switch ($documento->super->sexo_id) {
				case 1:
					$sir = "Señor";
					$sup = "Supervisor";
					$des = "designado";
					break;
				case 2:
					$sir = "Señora";
					$sup = "Supervisora";
					$des = "designada";
					break;
				default:
					break;
			}
		@endphp
		<p>{{$sir}}: <span class="resaltado">{{$documento->super->NombreCompleto()}}.</span></p>
	</div>
	<div class="cuerpo-carta">
		<p>Me permito comunicarle que ha sido {{$des}} como {{$sup}} del siguiente contrato:</p>
		<table class="tabla-memo">
			<thead>
				<tr style="background-color: #A8A8A8;">
					<th>CONTRATO</th>
					<th>CONTRATISTA</th>
					<th>OBJETO</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{$documento->consecutivo_cpe}}</td>
					<td>{{$documento->tercero->NombreCompleto()}}</td>
					@if( get_class($documento) == App\CartaAdjudicacion::class )
						<td class="objeto">""</td>
					@else
						<td class="objeto">{!!$documento->ObjetoRaw()!!}</td>
					@endif
					{{-- <td class="objeto"></td> --}}
				</tr>
			</tbody>
		</table>
		<p>En su actuación como {{$sup}}, está en la obligación de mantener un estricto control sobre la ejecución del objeto contractual, por cuanto de conformidad con lo establecido en los artículos 51 y 56 de la Ley 80 de 1993, la Ley 734 de 2002 y la Ley 1474 de 2011, los manuales de Contratación y Supervisión e Interventoría de CPE, el supervisor responderá disciplinaria, civil y penalmente por sus acciones y omisiones en la actuación contractual, en los términos de la Constitución y la Ley.</p>
		<p>La referida obligación, deberá realizarse dando estricto cumplimiento al Manual de Supervisión e interventoría, así como al Manual de Contratación y se hace efectiva a través de los informes de supervisión los cuales deberán presentarse dentro del término que se señale en el clausulado contractual, en donde conste por ejemplo, el cumplimiento de su objeto, suscripción de actas de iniciación, suspensión, entrega del objeto contratado, seguimiento a la ejecución, terminación y liquidación de los contratos, y relación de pagos y saldos.</p>
		<p>De igual forma, dentro de sus obligaciones principales está la vigilancia del cumplimiento del plazo de ejecución, del objeto contractual, de las obligaciones del contrato y del cronograma de actividades, si lo hubiere, así como informar a la Coordinación de Contratación cualquier acontecimiento que impida el normal desarrollo del contrato, estudiar las solicitudes, sugerencias, reclamaciones y consultas del contratista, exigir la documentación e información que estime pertinente y necesaria para el cabal cumplimiento de sus obligaciones como supervisor, exigir la acreditación del cumplimiento de las obligaciones adquiridas con el sistema de seguridad social integral y parafiscales según sea el caso, aprobar las solicitudes de pago, y todas las demás obligaciones que deban cumplir las partes y/o el contratista, señaladas en el respectivo contrato o convenio.</p>
		<p>Para dar inicio a la ejecución del contrato, teniendo en cuenta que el mismo quedó debidamente legalizado el día <span class="resaltado">{{$fecha_leg}}</span>, deberá elaborar y suscribir la respectiva acta de inicio, la cual debe remitir de forma inmediata a la Coordinación de Contratación, para su publicación en el SECOP.</p>
		<p>Una vez finalice el contrato, deberá emitir un informe final de supervisión, expedir la certificación  final  de cumplimiento del objeto contractual.</p>
		<p>Todos los documentos antes citados deberán ser remitidos a la Coordinación de Contratación oportunamente para su revisión y trámite pertinente, con el fin de dar cumplimiento al procedimiento respectivo dentro de la ejecución y liquidación contractual.</p>
		
		<p>Cordialmente,</p>
	</div>
	<div class="firma">
		<p><strong>ALEXANDER JAIMES RODRIGUEZ</strong>
		<br>
		Secretario General
		<br>
		COMPUTADORES PARA EDUCAR</p>
		<p style="font-size: 8pt;">Proyectó: {{Auth::user()->persona->NombreyCargo()}}</p>		
	</div>
</body>
</html>