@extends('layouts.pagina_maestra')
@section('titulo')
Nuevo Contrato
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1" id="contenedor-nuevo-cto">
	@php
	$f = new NumberFormatter("es", NumberFormatter::SPELLOUT);
	$f->setTextAttribute(NumberFormatter::DEFAULT_RULESET, "%spellout-ordinal-feminine");
	for ($i=0; $i <= 100; $i++) { 
		$ordinales[$i] = $f->format($i);
	}
	@endphp
	<h2>Nuevo Contrato</h2>
	<form id="form-nuevo-cto" action="{{ Request::url() }}" method="POST" autocomplete="off">
		{!!csrf_field()!!}
		<div class="panel panel-default panel-nuevo-cto">
			<div class="panel-heading">
				<h3 class="panel-title">Datos de contrato</h3>
			</div>
			<div class="panel-body">
				<div class="form-group col-md-12">
					<label class="control-label" for="tercero_id">Tercero:</label>
					<div class="input-group col-md-12">
						<input type="number" class="form-control" style="width: 80%;" id="tercero_id" name="tercero_id" placeholder="ID Tercero" readonly="readonly"/>
						<input type="number" class="form-control" style="width: 20%;" id="tercero_tipo" name="tercero_tipo" placeholder="Tipo" readonly="readonly"/>
						<span class="input-group-btn">
							<button class="btn btn-primary" id="boton-buscar-tercero" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
								<span class="texto-btn-hide">
									Buscar
								</span>
							</button>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div id="nombre_tercero" class="well well-sm" style="margin-bottom: 0px;">
						...
					</div>
				</div>
				<div class="form-group col-md-6">
					<label class="control-label" for="consecutivo_cpe">Consecutivo CPE:</label>
					<input type="text" name="consecutivo_cpe" placeholder="Consecutivo CPE" class="form-control" id="consecutivo_cpe"/>			
				</div>
				<div class="form-group col-md-6">
					<label class="control-label" for="proceso_doc">Proceso perteneciente:</label>
					<select name="proceso_doc" class="form-control" id="proceso_doc">
						<option value="">Seleccionar..</option>
						@foreach ($procesos as $proceso)
						<option value="{{$proceso->id}}">{{$proceso->proceso}}</option>
						@endforeach
					</select>
				</div>
				{{-- <div class="form-group col-md-12">
					<label class="control-label" for="url">URL Secop:</label>
					<input type="text" name="url" placeholder="URL del contrato cargado en SECOP..." class="form-control" id="url"/>			
				</div> --}}
				<div class="form-group col-md-6">
					<label class="control-label" for="proceso_doc">Tipo de contrato:</label>
					<select name="tipo_contrato" class="form-control" id="proceso_doc">
						<option value="">Seleccionar..</option>
						<option value="">Prestación de servicios</option>
						<option value="">De obra o labor</option>
						<option value="">etc</option>
					</select>
				</div>
				<div class="form-group col-md-6">
					<label class="control-label" for="categoria_doc">Modalidad de contratación:</label>
					<select name="categoria_doc" class="form-control" id="categoria_doc">
						<option value="">Seleccionar..</option>
						@foreach ($modalidades as $modalidad)
						<option value="{{$modalidad->id}}">{{$modalidad->modalidad}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="nombre_doc">Valor:</label>
					<input type="number" name="nombre_doc" class="form-control" id="nombre_doc" placeholder="Valor del contrato"/>			
				</div>
				{{-- <div class="form-group col-md-4">
					<label class="control-label" for="url_doc">Fecha de Adjudicacion:</label>
					<input type="date" name="url_doc" class="form-control" id="url_doc" placeholder="fecha"/>			
				</div>
				<div class="form-group col-md-4">
					<label class="control-label" for="url_doc">Fecha de inicio:</label>
					<input type="date" name="url_doc" class="form-control" id="url_doc" placeholder="fecha"/>			
				</div>
				<div class="form-group col-md-4">
					<label class="control-label" for="url_doc">Fecha de terminacion:</label>
					<input type="date" name="url_doc" class="form-control" id="url_doc" placeholder="fecha"/>			
				</div> --}}
			</div>
		</div>
		<h3>Cuerpo del contrato:</h3>
		<div class="panel panel-default panel-cuerpo-cto">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>Encabezado y Consideraciones:</strong></h3>
			</div>
			<div class="panel-body" style="padding: 0;">
				<div class="form-group col-md-12" style="padding: 0; margin: 0;">
					<textarea class="form-control editable_text_area" name="encabezado" id="encabezado-contrato" cols="30" rows="10"></textarea>
				</div>
			</div>
		</div>
		<h3>Cláusulas</h3>
		<!-- Cláusulas -->
		<div id="clausulas-container">
			<div class="panel panel-default panel-clausula">
				<div class="panel-heading">
					<h3 class="panel-title"><strong>Cláusula {{$ordinales[1]}}: Objeto del contrato</strong></h3>
				</div>
				<div class="panel-body" style="padding: 0;">
					<div class="form-group col-md-12" style="padding: 0; margin: 0;">
						<textarea class="form-control editable_text_area" name="objeto" id="clausula-objeto" cols="30" rows="10">
							<strong>Cláusula primera objeto del contrato:</strong>
						</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row col-agregar-clausula" style="margin-left: 0px; margin-right: 0px;">				
			<div class="input-group form-group">
				<input type="text" id="nombre_clausuala" class="form-control" />
				<span class="input-group-addon" style="width:0px; padding-left:0px; padding-right:0px; border:none;"></span>
				<select id="tipo_clausula" class="form-control">
					<option value="">General</option>
					<option value="">Obligaciones</option>
					<option value="">ETC</option>
				</select>
				<span class="input-group-btn">
					<button type="button" id="boton_agregar_clausula" class="btn btn-default pull-left">
						<i class="fa fa-plus" aria-hidden="true"></i>
						<span>&nbsp;&nbsp;Agregar Cláusula</span>
					</button>
				</span>
			</div>
		</div>
		<div class="form-group col-xs-12" style="padding: 0px 0px 0px 0px;">			
			<button type="submit" class="btn btn-primary pull-right">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>
				<span>&nbsp;&nbsp;Guardar</span>
			</button>			
		</div>
		{{-- <div class="form-group col-xs-12" style="padding: 0px 0px 0px 0px;">			
			<a href="{{ url("contratacion/imprimir-contrato") }}" class="btn btn-primary pull-right">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>
				<span>&nbsp;&nbsp;Contrato</span>
			</a>			
		</div> --}}
	</form>
	<div id="errores"></div>
</div>
{{-- @include('terceros.lista', array('terceros'=>$terceros)) --}}
{{-- <div id="lista-terceros">
	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Tipo</th>
				<th>Nombre</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($terceros as $tercero)
				<tr>
					<td>{{$tercero->id}}</td>
					<td>{{$tercero->tipo->tipo}}</td>
					<td>{{$tercero->NombreCompleto()}}</td>
				</tr> 
			@endforeach
		</tbody>
	</table>
</div> --}}
<style>
	#contenedor-nuevo-cto{
		padding-right: 10px;
		padding-left: 10px;
	}
	.panel-nuevo-cto>.panel-body{
		padding-left: 5px; 
		padding-right: 5px; 
	}
	#form-nuevo-cto textarea { 
		resize:vertical;
	}	
	.bootstrap-tagsinput {
		width: 100% !important;
	}
	.modal_lista_terceros .bootbox-body{
		
	}
	@if(!Request::ajax())
	div.mce-fullscreen {
		top: 60px !important;
		left: 250px !important;
		width: calc(100% - 250px);
		display: flex;
		flex-direction: column;
		padding-bottom: 60px;
	}
	@media (max-width : 992px) {
		div.mce-fullscreen {
			top: 50px !important;
			left: 0px !important;
			width: 100%;
			display: flex;
			flex-direction: column;
			padding-bottom: 50px;
		}
	}
	@else
	div.mce-fullscreen {
		display: flex;
		flex-direction: column;
	}
	@media (max-width : 992px) {
		div.mce-fullscreen {
			display: flex;
			flex-direction: column;
		}
	}
	@endif
	body.mce-fullscreen #contenedor-pagina{
		overflow-y: inherit;
	}
	@media (max-width : 479px) {
		.texto-btn-hide {
			display: none;
		}
	}

	.mce-menubtn.mce-fixed-width span{
		width: auto !important;
	}
</style>
@include('plantillas.seccion_clausula_cto')
<script type="text/javascript">
	var ordinales = {!!json_encode($ordinales)!!};
	inicializar (function () {
		//Inicializar instacion de editor
		tinymce.init({
			//selector: '.editable_text_area',
			mode : "none",
			language: 'es',
			menubar: false,
			images_dataimg_filter: function(img) {
				return img.hasAttribute('internal-blob');
			},
			//height: 500,
			theme: 'modern',
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc',
			'bdesk_photo',
			],
			paste_retain_style_properties: "all",
			//| cut copy paste quickimage | styleselect insert 
			toolbar1: 'undo redo | alignleft aligncenter alignright alignjustify | bold italic underline | styleselect | fontselect | fontsizeselect | forecolor | backcolor | bullist | numlist | outdent | indent | table | link | bdesk_photo | code | fullscreen',
			//toolbar2: '',
			image_advtab: true,
			templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
			],
			setup: function (editor) {
				editor.on('change', function () {
					tinymce.triggerSave();
				});
			}
		});

		//Eventos

		$('#form-nuevo-cto').on('submit', function(e) {
			e.preventDefault();
			e.stopPropagation();
			e.isDefaultPrevented = function () {
				return false;
			}
			// retrigger with the exactly same event data
    		//$(this).trigger(e);
			
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);


			// // Display the key/value pairs
			// for (var dato of datos.entries()) {
			// 	console.log(dato[0]+ ', ' + dato[1]); 
			// }
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				console.log(respuesta);
				$("#errores").html(respuesta.data.responseText);
			});
		});



		$("#boton_agregar_clausula").on('click', function(event) {
			event.preventDefault();
			var clausula = Handlebars.compile(document.getElementById('template-clausula-contrato').innerHTML);
			var datos = {};
			var id = $(".panel-clausula").length;
			datos.id = (id+1);
			datos.ordinal = ordinales[datos.id];
			datos.nombre = $("#nombre_clausuala").val();
			console.log(datos);
			$('#clausulas-container').append(clausula(datos));
			tinyMCE.execCommand('mceAddEditor', false, "clausula-"+datos.id);
		});


		//Inicializar encabezado y objeto
		tinyMCE.execCommand('mceAddEditor', false, 'encabezado-contrato');
		tinyMCE.execCommand('mceAddEditor', false, 'clausula-objeto');

		$("#boton-buscar-tercero").on('click', function(event) {
			event.preventDefault();
			mostrar_terceros();
		});


		//Funciones
		function mostrar_terceros(){
			var modalLista = new bootbox.dialog({
				title: 'Listado de terceros',
				className: 'modal_lista_terceros',
				message: '<p><i class="fa fa-spin fa-spinner"></i> Cargando...</p>',
				buttons: {
					success: {
						label: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i> Seleccionar",
						className: "btn-success",
						callback: function () {
							$("#nombre_tercero").html('...');
							$("#tercero_id").val($(".tarjeta-tercero.selected").data('id'));
							$("#tercero_tipo").val($(".tarjeta-tercero.selected").data('tipo'));
							$("#nombre_tercero").html($(".tarjeta-tercero.selected").data('nombre'));
							console.log($(".tarjeta-tercero.selected").data('id'));
							console.log($(".tarjeta-tercero.selected").data('tipo'));
							console.log($(".tarjeta-tercero.selected").data('nombre'));
						}
					},
				}
			});
			modalLista.init(function(){
				history.pushState({url: window.location.href+"", titulo:'titulo'}, null, window.location.href+"");
				$.ajax({
					url: '{{ url('contratacion/terceros-lista') }}',
					type: 'GET',
					dataType: 'html',
				})
				.done(function(data) {
					console.log("success");
					//console.log(data);
					modalLista.find('.bootbox-body').html(data);
				})
				.fail(function(data) {
					console.log("error");
					console.log(data);
					modalLista.find('.bootbox-body').html(data.statusText+": "+data.status);
				})
				.always(function(data) {
					console.log("complete");
					//dialog.find('.bootbox-body').html(data);
				});
			});
		}
		
		//Funcion Agregar Clausula
		function agregar_clausula (nombre, tipo) {
			
		}

		function remover_clausula (elemento) {
			// body... 
		}

		function indexar_nombres () {
			// body... 
		}

	});
</script>
@endsection