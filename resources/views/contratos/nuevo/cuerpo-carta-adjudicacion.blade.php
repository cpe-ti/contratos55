@extends('layouts.pagina_maestra')
@section('titulo')
Generar Documento
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1 page-carta-adjudicacion" id="contenedor-nuevo-cto">
	@php
	setlocale(LC_ALL, 'esp_esp', 'esp_spain', 'spanish_esp', 'spanish_spain');
	//setlocale(LC_TIME, 'es-CO', 'es_CO');
	@endphp
	<form id="form-cuerpo-carta-adj" target="_blank" action="{{ Request::url() }}" method="POST" autocomplete="off" class="prevent_lost">
	{!!csrf_field()!!}
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4>Carta de Adjudicación</h4>
		</div>
		<div class="panel-body">
			<textarea name="html_carta" id="text-carta-adjudicacion" rows="60">
				@if($contrato->html_carta)
					{{$contrato->html_carta}}
				@else
				<p style="text-align: justify;">Bogotá D.C. {{ strftime("%d de %B del %Y", time()) }}.
				<br>
				<br>
				<br>
				<p>Señor(a)</p>
				<p><strong>{{ucwords($contrato->tercero->RepresentanteLegal())}}</strong></p>
				<p>C.C. {{$contrato->tercero->IdRepresentante()}}</p>
				@if($contrato->tercero->tipo_id == 2)
				<p>Representante Legal</p>
				<p><strong>{{ucwords($contrato->tercero->NombreCompleto())}}</strong></p>
				<p>Nit. {{$contrato->tercero->id}}</p>
				@endif
				<p>Dirección: {{$contrato->tercero->direccion}}</p>
				<p>Teléfono: {{$contrato->tercero->telefono}}</p>
				<p>Correo Electrónico: {{$contrato->tercero->email}}</p>
				<p>Bogotá D.C.</p>
				<br>
				<p style="text-align: justify;" style="line-height: 10.5pt;"><strong>Referencia:</strong> Aprobación de la oferta Proceso de Mínima Cuantía Invitación Pública No. <strong>XXX-XXX</strong> Consecutivo No. <strong>XXX-XXX</strong></p>
				<p>Apreciado Señor(a) <span>{{ucwords($contrato->tercero->RepresentanteLegal())}}</span>:</p>
				<p style="text-align: justify;">Atendiendo la recomendación del comité encargado de la evaluación de las propuestas, le informo que, <strong>{{ strtoupper($contrato->tercero->NombreCompleto())}}</strong> ha salido favorecido con la adjudicación del proceso de mínima cuantía No. <strong>XXX-XXX</strong>, la cual tiene por objeto “(…)  xxxxx (…)”</p>
				<p style="text-align: justify;">El contratista deberá cumplir con la totalidad de las especificaciones técnicas contenidas en el estudio previo y la invitación del proceso de mínima cuantía No. <strong>XXX-XXX</strong>, así como en la propuesta presentada por <strong>{{ strtoupper($contrato->tercero->NombreCompleto())}}</strong>.</p>
				@php
					$fmoneda = new NumberFormatter( 'es_CO', NumberFormatter::CURRENCY );
					$ftexto = new NumberFormatter("es_CO", NumberFormatter::SPELLOUT);
				@endphp
				<p style="text-align: justify;">Valor: El valor total de la presente adjudicación es de <strong>{{ strtoupper($ftexto->format($contrato->valor))}} PESOS M/CTE ({{ $fmoneda->formatCurrency($contrato->valor, 'COP') }}) INCLUIDO TODOS LOS IMPUESTOS A QUE HAYA LUGAR.</strong></p>
				<p style="text-align: justify;">Plazo: El término de ejecución y cumplimiento de la totalidad de las obligaciones adquiridas, con la adjudicación del presente proceso de selección, será de <strong>XXX XXX</strong>, contados a partir de la suscripción del acta de inicio, previo cumplimiento de los requisitos de perfeccionamiento y ejecución.</p>
				<p style="text-align: justify;">Lugar de ejecución: El domicilio contractual será XXX-XXX.</p>
				<p style="text-align: justify;">Forma de pago:....</p>
				<p style="text-align: justify;">La factura debe ir acompañada de la certificación de cumplimiento que expida la supervisión del contrato, de la certificación que acredite el pago por parte del contratista de aportes parafiscales relativos al Sistema de Seguridad Social Integral, así como los propios del Sena, ICBF y Cajas de Compensación Familiar, conforme a lo previsto en la normatividad vigente.</p>
				<p style="text-align: justify;">Garantías: De acuerdo a la invitación pública el contratista deberá constituir garantía única de cumplimiento con las siguientes coberturas:</p>
				<table style="width: 100%; border: 1px solid; border-collapse: collapse;">
					<thead>
						<tr>
							<th style="border: 1px solid;"><strong>Amparo</strong></th>
							<th style="border: 1px solid;"><strong>Porcentaje</strong></th>
							<th style="border: 1px solid;"><strong>Vigencia</strong></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="border: 1px solid;"></td>
							<td style="border: 1px solid;"></td>
							<td style="border: 1px solid;"></td>
						</tr>
					</tbody>
				</table>
				<p style="text-align: justify;">Estas garantías deberán ser allegadas a más tardar dentro de los tres (3) días hábiles siguientes a la publicación de esta comunicación, de no hacerlo se entenderá que desiste de su propuesta.</p>
				<p style="text-align: justify;">Esta adjudicación se notificará al oferente seleccionado, publicando una comunicación en la página web, www.colombiacompra.gov.co, con la publicación del documento antes enunciado, el proponente seleccionado queda informado de la aceptación de su oferta. La Invitación Pública, la comunicación de aceptación junto con la oferta constituyen para todos los efectos el contrato celebrado, con base en el cual se efectuará el respectivo registro presupuestal.</p>
				<p style="text-align: justify;">Finalmente, se le informa que el seguimiento y supervisión de la ejecución del contrato estará a cargo de la persona que para tal fin designe la Entidad, quien tendrá a su cargo la función de verificar el cumplimiento de las obligaciones establecidas en el presente proceso contractual, revisar los informes, formular las observaciones correspondientes y verificar el cumplimento de los requisitos para el pago de las facturas.</p>
				<p style="text-align: justify;">Cordial saludo,</p>
				<br>
				<p><strong>ALEXANDER JAIMES RODRIGUEZ</strong></p>
				<p>Secretario General</p>
				<p><strong>COMPUTADORES PARA EDUCAR</strong></p>
				<br>
				<br>
				<p style="font-size: 8pt;">Revisó: </p>
				<p style="font-size: 8pt;">Proyectó: {{Auth::user()->NombreCompleto()}}</p>	
				@endif
			</textarea>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-success pull-left" name="accion" value="1">
						<i class="fa fa-eye" aria-hidden="true"></i> <span class="text-btn">Vista Previa</span>
					</button>
					<div class="btn-group pull-right">
						<button type="submit" class="btn btn-primary boton_save" name="accion" value="2">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> <span class="text-btn">Guardar</span>
						</button>
						<button type="submit" class="btn btn-success boton_proeces" name="accion" value="3">
							<i class="fa fa-check-circle-o" aria-hidden="true"></i> <span>Procesar</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
	<hr>
</div>
<script type="text/javascript">
inicializar(function() {
	// alert("Test");
	tinymce.init({
			//selector: '.editable_text_area',
			is_ajax: {{Request::ajax() ? 'true' : 'false'}},//ajustar full scren
			//valid_elements: "li",
			//invalid_elements: "span",
			mode : "none",
			language: 'es',
			browser_spellcheck: true,
			menubar: false,
			images_dataimg_filter: function(img) {
				return img.hasAttribute('internal-blob');
			},
			content_style: ".mce-content-body {font-size:12pt; font-family: helvetica,sans-serif;}",
			//height: 500,
			theme: 'modern',
			plugins: [
				'advlist autolink lists link image charmap print preview hr anchor pagebreak',
				'searchreplace wordcount visualblocks visualchars code fullscreen',
				'insertdatetime media nonbreaking save table contextmenu directionality',
				'emoticons template paste textcolor colorpicker textpattern imagetools toc',
				// 'bdesk_photo',
			],
			paste_retain_style_properties: "all",
			//| cut copy paste quickimage | styleselect insert 
			toolbar1: 'undo redo | alignleft aligncenter alignright alignjustify | bold italic underline | styleselect | fontselect | fontsizeselect | forecolor | backcolor | bullist | numlist | outdent | indent | table | link | bdesk_photo | code | fullscreen',
			font_formats: 'Arial=helvetica,sans-serif; Helvetica=helvetica;',
			//toolbar2: '',
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					tinymce.triggerSave();
				});
			}
		});
		//Carta
		tinyMCE.execCommand('mceAddEditor', false, 'text-carta-adjudicacion');


		var accion_submit = 0;
		$(":submit","#form-cuerpo-carta-adj").click(function(e){
			accion_submit = $(this).val() ? $(this).val() : '0';
		});

		$("#form-cuerpo-carta-adj").on('submit', function(event) {
			if (accion_submit == 1) {
				return true;// salir si es vista previa
			}
			event.preventDefault();
			event.stopPropagation();
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			datos.append('accion', accion_submit);
			$(':input, .btn').prop('disabled', true);
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				console.log("probando");
				console.log(respuesta);
				switch (respuesta.data.status) {
					//informativos
					case 100:
					case 101:
					case 102:
					case 103:
						//Codigo
						$(':input, .btn').prop('disabled', false);
						mensaje_error('1XX','Informativos');
						break;
					//correctos
					case 200:
					case 201:
					case 202:
					case 203:
					case 204:
					case 205:
					case 206:
					case 207:
					case 208:
						//Codigo
						mensaje_correcto('Hecho', respuesta.data.responseText);
						break;
					//redirecciones
					case 300:
					case 301:
					case 302:
					case 303:
					case 304:
					case 305:
					case 306:
					case 307:
					case 308:
						//Codigo
						$(':input, .btn').prop('disabled', false);
						mensaje_error('3XX','Redirecciones');
						break;
					//errores de cliente
					case 400:
					case 401:
					case 402:
					case 403:
					case 404:
					case 405:
					case 406:
					case 407:
					case 408:
					case 409:
					case 410:
					case 411:
					case 412:
					case 413:
					case 414:
					case 415:
					case 416:
					case 417:
					case 418:
					case 422:
					case 423:
					case 424:
					case 425:
					case 426:
					case 428:
					case 429:
					case 451:
						//Codigo
						$(':input, .btn').prop('disabled', false);
						mensaje_error('4XX','Error de cliente: '+respuesta.data.responseText);
						break;
					//errores de servidor
					case 500:
					case 501:
					case 502:
					case 503:
					case 504:
					case 505:
					case 506:
					case 507:
					case 508:
					case 509:
					case 510:
					case 511:
					case 512:
						// Internal server error
						$(':input, .btn').prop('disabled', false);
						mensaje_error('Error Desconocido',respuesta.data.responseText);
						break;
					default:
						// statements_def
						if (respuesta.success && respuesta.data instanceof Object) {
							mensaje_correcto('Cuerpo Guardado');
						}else{
							$(':input, .btn').prop('disabled', false);
							mensaje_error('');
						}
						break;
				}
			});
		});
})
</script>
<style>
	#container-amparos{
		margin-bottom: 5px;
	}
	#text-carta-adjudicacion{
		width: 100%;
	}
	.cuerpo-carta{
		text-align: justify;
	}
	.page-carta-adjudicacion .panel-body:not(.otro){
		padding: 0px !important;
	}
	.page-carta-adjudicacion input:not(.form-control){
		/* border-radius: 5px; */
		border: none;
		border-bottom: 1px solid;
	}
	body.mce-fullscreen #contenedor-pagina{
		overflow-y: inherit;
	}

	@if(!Request::ajax())
	div.mce-fullscreen {
		top: 60px !important;
		left: 250px !important;
		width: calc(100% - 250px);
		display: flex;
		flex-direction: column;
		padding-bottom: 60px;
	}
	@media (max-width : 992px) {
		div.mce-fullscreen {
			top: 50px !important;
			left: 0px !important;
			width: 100%;
			display: flex;
			flex-direction: column;
			padding-bottom: 50px;
		}
	}
	@endif
	@media only screen and (max-width : 320px) {
		.text-btn{
			display: none;
		}
    }
</style>
@endsection