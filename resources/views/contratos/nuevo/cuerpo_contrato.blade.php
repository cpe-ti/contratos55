@extends('layouts.pagina_maestra')
@section('titulo')
Cuerpo del Documento
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1" id="contenedor-nuevo-cto">
	{{-- <h2>Cuerpo de Contrato</h2> --}}
	@php
	$f = new NumberFormatter("es", NumberFormatter::SPELLOUT);
	$f->setTextAttribute(NumberFormatter::DEFAULT_RULESET, "%spellout-ordinal-feminine");
	for ($i=0; $i <= 100; $i++) { 
		$ordinales[$i] = $f->format($i);
	}
	$id_cl = [];
	@endphp
	<form id="form-cuerpo-cto" action="{{ Request::url() }}" method="POST" autocomplete="off" class="prevent_lost">
		{!!csrf_field()!!}
		<div id="encabezado_cto">
			<strong>
				{{$contrato->tipo_contrato->tipo}} nº _______ suscrito entre computadores para educar y {{$contrato->tercero->NombreCompleto()}}
			</strong>
		</div>
		<br>
		<div class="panel panel-default panel-cuerpo-cto">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>Encabezado y Consideraciones:</strong></h3>
			</div>
			<div class="panel-body" style="padding: 0;">
				<div class="form-group col-md-12" style="padding: 0; margin: 0;">
					<textarea class="form-control editable_text_area" name="encabezado" id="encabezado-contrato" cols="30" rows="15">
						{{$contrato->encabezado_consideraciones}}
					</textarea>
				</div>
			</div>
		</div>
		<h3>Cláusulas:</h3>
		<div id="clausulas-container">			
			@if($contrato->clausulas->isEmpty())
				@php
					$id_cl[0] = uniqid('', true);
				@endphp
				<div class="panel panel-default panel-clausula">
					<div class="panel-heading">
						<h3 class="panel-title titulo-panel-clausula"><strong>CLÁUSULA <span class="ordinal-panel-clausula">{{mb_strtoupper($ordinales[1])}}</span> - OBJETO DEL CONTRATO:</strong></h3>
					</div>
					<div class="panel-body" style="padding: 0;">
						<div class="form-group col-md-12" style="padding: 0; margin: 0;">
							<textarea class="form-control editable_text_area" name="clausula[1][objeto]" id="{{ $id_cl[0] }}" cols="30" rows="10">
								<p>
									<strong>CLÁUSULA {{ mb_strtoupper($ordinales[1]) }} - OBJETO:</strong>&nbsp;
								</p>
							</textarea>
						</div>
					</div>
				</div>
			@endif
			@foreach($contrato->clausulas as $clausula)
				@php
					$id_cl[$loop->index] = uniqid('', true);
				@endphp				
				@if($clausula->tipo->id == 1)
					<div class="panel panel-default panel-clausula">
						<div class="panel-heading">
							<h3 class="panel-title titulo-panel-clausula"><strong>CLÁUSULA <span class="ordinal-panel-clausula">{{mb_strtoupper($ordinales[$clausula->ordinal])}}</span> - OBJETO DEL CONTRATO:</strong></h3>
						</div>
						<div class="panel-body" style="padding: 0;">
							<div class="form-group col-md-12" style="padding: 0; margin: 0;">
								<textarea class="form-control editable_text_area" name="clausula[{{$clausula->ordinal}}][objeto]" id="{{ $id_cl[$loop->index] }}" cols="30" rows="10">
									{{$clausula->contenido_html}}
								</textarea>
							</div>
						</div>
					</div>
				@elseif($clausula->tipo->id == 2)
					<div class="panel panel-default panel-clausula panel-clausula-obligaciones" id="clausula_obligaciones">
						<div class="panel-heading">
							<h3 class="panel-title titulo-panel-clausula"><strong>CLÁUSULA <span class="ordinal-panel-clausula">{{mb_strtoupper($ordinales[$clausula->ordinal])}}</span>: OBLIGACIONES DEL CONTRATISTA</strong></h3>
							<div class="close-button-clausula">
								<i class="fa fa-times-circle" aria-hidden="true"></i>
							</div>
						</div>
						<div class="panel-body">
							<p style="text-align: justify; font-size: 10pt; font-family: 'Helvetica';"><span>EL <strong>CONTRATISTA</strong>, se obliga a cumplir con el objeto del presente contrato, ejecutando, entre otras, las siguientes actividades, tal y como consta en la propuesta presentada por el <strong>CONTRATISTA</strong> y aceptada por el <strong>CONTRATANTE</strong>, la cual hace parte integrante de presente contrato:</span>
							<h5><strong>A) OBLIGACIONES ESPECÍFICAS:</strong></h5>
							<input type="hidden" name="clausula[{{$clausula->ordinal}}][obligaciones]" id="clausula-{{$clausula->ordinal}}" value="CLÁUSULA {{$ordinales[$clausula->ordinal]}}">
							<div id="seccion-obligaciones-espf">
								@foreach($contrato->obligaciones->where('tipo_obligacion_id', 2) as $obligacion)
									<div class="form-group col-md-12 obligacion-group">
										<div class="input-group">
											<span class="input-group-addon btn btn-primary numero-obligacion">
										    	{{$obligacion->ordinal}})
										    </span>
											<textarea style="resize: vertical;" name="obligaciones[obligacion_espf][]" class="form-control texto-obligacion" cols="30" readonly="true">{{$obligacion->obligacion}}</textarea>
											<span class="input-group-addon btn btn-danger close-button-obl">
										    	<i class="fa fa-times-circle" aria-hidden="true"></i>
										    </span>
										</div>
									</div>
								@endforeach
							</div>
							<div class="form-group col-md-12">
								<div class="input-group">
								    <textarea style="resize: vertical;" id="textarea_obligacion_spcf" class="form-control" cols="30" placeholder="Obligación"></textarea>
								    <span class="input-group-addon btn btn-primary btn-add-obl" id="boton_new_ob_espfca">
								    	<i class="fa fa-plus" aria-hidden="true"></i>
								    </span>
								</div>
							</div>
							<hr>
							<h5><strong>B) OBLIGACIONES GENERALES:</strong></h5>
							<div id="seccion-obligaciones-gnrl">
								@foreach($contrato->obligaciones->where('tipo_obligacion_id', 1) as $obligacion)
									<div class="form-group col-md-12 obligacion-group">
										<div class="input-group">
											<span class="input-group-addon btn btn-primary numero-obligacion">
										    	{{$obligacion->ordinal}})
										    </span>
											<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">{{$obligacion->obligacion}}</textarea>
											<span class="input-group-addon btn btn-danger close-button-obl">
										    	<i class="fa fa-times-circle" aria-hidden="true"></i>
										    </span>
										</div>
									</div>
								@endforeach
							</div>
							<div class="form-group col-md-12">
								<div class="input-group">
								    <textarea style="resize: vertical;" id="textarea_obligacion_gnrl" class="form-control" cols="30" placeholder="Obligación"></textarea>
								    <span class="input-group-addon btn btn-primary btn-add-obl" id="boton_new_ob_gnrl">
								    	<i class="fa fa-plus" aria-hidden="true"></i>
								    </span>
								</div>
							</div>
						</div>
					</div>
				@else
					<div class="panel panel-default panel-clausula">
						<div class="panel-heading">
							<h3 class="panel-title titulo-panel-clausula"><strong>CLÁUSULA <span class="ordinal-panel-clausula">{{mb_strtoupper($ordinales[$clausula->ordinal])}}</span></strong></h3>
							<div class="close-button-clausula">
								<i class="fa fa-times-circle" aria-hidden="true"></i>
							</div>
						</div>
						<div class="panel-body" style="padding: 0;">
							<div class="form-group col-md-12" style="padding: 0; margin: 0;">
								<textarea class="form-control editable_text_area" name="clausula[{{$clausula->ordinal}}]" id="{{ $id_cl[$loop->index] }}" cols="30" rows="10">
									{{$clausula->contenido_html}}
								</textarea>
							</div>
						</div>
					</div>
				@endif				
			@endforeach			
		</div>
		<div class="row" id="seccion-agregar-clausula">
			<label for="" style="margin-left: 15px;">Agregar cláusula</label>
			<div class="form-group col-md-11">
				<div class="input-group">
					<input type="text" class="form-control" id="nombre_nueva_clausula" placeholder="nombre de cláusula">
					<div class="input-group-btn">
						<select id="tipo_nueva_clausula" class="form-control" style="width: auto;">
							<option value="">Tipo de cláusula</option>
							<option value="1">General</option>
							<option value="2">Obligaciones</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group col-md-1 text-right">	
				<button type="button" id="boton_agregar_clausula" class="btn btn-primary">
					<i class="fa fa-plus" aria-hidden="true"></i>
				</button>
			</div>
		</div>
		<hr>
		<div class="row firmas">
			<label for="" class="col-md-12">Firmas:</label>
			<fieldset class="input-group-vertical col-md-6">
				<label for="">Contratante:</label>
				<div class="form-group">
					<label class="sr-only">Nombre Contratante</label>
					<input type="text" name="nombre_contratante" class="form-control" placeholder="Nombre Contratante"  value="{{$contrato->nombre_contratante ? $contrato->nombre_contratante : 'REYNEL FERNANDO BEDOYA RODRIGUEZ' }}">
				</div>
				<div class="form-group">
					<label class="sr-only">Cargo Contratante</label>
					<input type="text" name="cargo_contratante" class="form-control" placeholder="Cargo Contratante"  value="{{$contrato->cargo_contratante ? $contrato->cargo_contratante : 'Representante Legal'}}">
				</div>
			</fieldset>
			<fieldset class="input-group-vertical col-md-6">
				<label for="">Contratista: </label>
				<div class="form-group">
					<label class="sr-only">Nombre Contratista:</label>
					<input type="text" name="nombre_contratista" class="form-control" placeholder="Nombre Contratista" value="{{ $contrato->nombre_contratista != NULL ? $contrato->nombre_contratista : ( $contrato->tercero->tipo_id == 1 ? $contrato->tercero->NombreCompleto() : mb_strtoupper($contrato->tercero->representante)) }}">
				</div>
				<div class="form-group">
					<label class="sr-only">Cargo Contratista:</label>
					<input type="text" name="cargo_contratista" class="form-control" placeholder="Cargo Contratista" value="{{ $contrato->cargo_contratista != NULL ? $contrato->cargo_contratista : ( $contrato->tercero->tipo_id == 1 ? "Contratista" : "Representante Legal") }}">
				</div>
			</fieldset>
		</div>
		<div class="row error_srow">
			<div class="col-md-12" id="errors_cuerpo"></div>
		</div>
		<div class="row row-enviar">
			<div class="form-group col-md-12">
				<button formtarget="_blank" type="submit" class="btn btn-default pull-left boton_preview" name="accion" value="1">
					<i class="fa fa-eye" aria-hidden="true"></i> <span class="text-btn">Vista Previa</span>
				</button>
				<div class="btn-group pull-right">
					<button type="submit" class="btn btn-primary boton_save" name="accion" value="2">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <span class="text-btn">Guardar</span>
					</button>
					<button type="submit" class="btn btn-success boton_proeces" name="accion" value="3">
						<i class="fa fa-check-circle-o" aria-hidden="true"></i> <span>Procesar</span>
					</button>
				</div>
			</div>
		</div>
	</form>
	<hr>
</div>
@include('plantillas.seccion_clausula_cto')
@include('plantillas.seccion_clausula_obligaciones')
@include('plantillas.seccion_obligacion')
<script type="text/javascript">
	var ordinales = {!!json_encode($ordinales)!!};
	inicializar (function () {
		//Input mask 
		$(":input").inputmask();
		//Inicializar instacion de editor
		tinymce.init({
			//selector: '.editable_text_area',
			is_ajax: {{Request::ajax() ? 'true' : 'false'}},//ajustar full scren
			//valid_elements: "li",
			//invalid_elements: "span",
			mode : "none",
			language: 'es',
			browser_spellcheck: true,
			menubar: false,
			images_dataimg_filter: function(img) {
				return img.hasAttribute('internal-blob');
			},
			content_style: ".mce-content-body {font-size:12pt; font-family: helvetica,sans-serif;}",
			//height: 500,
			// theme: 'modern',
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools toc',
			
			],
			paste_retain_style_properties: "all",
			//| cut copy paste quickimage | styleselect insert 
			toolbar1: 'undo redo | alignleft aligncenter alignright alignjustify | bold italic underline | styleselect | fontselect | fontsizeselect | forecolor | backcolor | bullist | numlist | outdent | indent | table | link | code | fullscreen',
			font_formats: 'Arial=helvetica,sans-serif; Helvetica=helvetica;',
			//toolbar2: '',
			paste_data_images: true,
			image_advtab: true,
			setup: function (editor) {
				editor.on('change', function () {
					tinymce.triggerSave();
				});
			}
		});
		//Encabezado y objeto editable
		tinyMCE.execCommand('mceAddEditor', false, 'encabezado-contrato');
		
		@foreach($id_cl as $id)
			tinyMCE.execCommand('mceAddEditor', false, '{{$id}}');
		@endforeach

		// tinyMCE.execCommand('mceAddEditor', false, 'clausula-objeto');
		

		// Eventos
		$('#boton_agregar_clausula').on('click', function(e) {
    	    e.preventDefault();
    	    e.stopPropagation();
    	    e.isDefaultPrevented = function () {
    	        return false;
    	    }
    	    $(this).blur();
			if ($("#nombre_nueva_clausula").val() == '') {
				alert("Ingrese un nombre")
				return false;
			}
			if($("#tipo_nueva_clausula").val() == ''){
				alert("Seleccione un tipo")
				return false;				
			}

			var nombre = $("#nombre_nueva_clausula").val();
			var tipo = $("#tipo_nueva_clausula").val();

			agregar_clausula(nombre, tipo);
			
			$("#nombre_nueva_clausula").val('');
			$("#tipo_nueva_clausula").val('');

    	    
		});

		$("#tipo_nueva_clausula").on('change', function(event) {
			event.preventDefault();
			if($(this).val() == 2){
				$("#nombre_nueva_clausula").val('OBLIGACIONES DEL CONTRATISTA');
			}
		});

		$("#clausulas-container").on('keypress', '#textarea_obligacion_spcf, #textarea_obligacion_gnrl', function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
        	if (code == 13) {
        		$(this).next().click();
        		return false;
        	}
		});

		$("#clausulas-container").on('click', '#boton_new_ob_espfca', function(event) {
			event.preventDefault();
			var numero = $('#seccion-obligaciones-espf .texto-obligacion').length;
			//console.log(numero);
			var texto = $("#textarea_obligacion_spcf").val();
			if (texto == '') {
				return false;
			}
			agregar_obligacion_especifica(numero+1, texto);
			$("#textarea_obligacion_spcf").val('');
		});

		$("#clausulas-container").on('click', '#boton_new_ob_gnrl', function(event) {
			event.preventDefault();
			var numero = $('#seccion-obligaciones-gnrl .texto-obligacion').length;
			//console.log(numero);
			var texto = $("#textarea_obligacion_gnrl").val();
			if (texto == '') {
				return false;
			}
			agregar_obligacion_general(numero+1, texto);
			$("#textarea_obligacion_gnrl").val('');
		});

		$("#clausulas-container").on('click', '.close-button-clausula', function(event) {
			remover_clausula( $(event.currentTarget).closest('.panel-clausula')[0] );			
		});
		$("#clausulas-container").on('click', '.close-button-obl', function(event) {
			remover_obl($(event.currentTarget).closest('.obligacion-group'));			
		});

		//Submit button pressed
		var accion_submit = 0;
		$(":submit","#form-cuerpo-cto").click(function(e){
			accion_submit = $(this).val() ? $(this).val() : '0';
		});
		//submit
		$("#form-cuerpo-cto").on('submit', function(event) {
			//return true;// salir si es vista previa
			if (accion_submit == 1) {
				return true;// salir si es vista previa
			}
			event.preventDefault();
			event.stopPropagation();
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			datos.append('accion', accion_submit);
			$(':input, .btn').prop('disabled', true);
			//tinymce.activeEditor.getBody().setAttribute('contenteditable', false);//editores ineditables
			$("#errors_cuerpo").html('');
			//for (var dato of datos.entries()) {
			//	console.log(dato[0]+ ', ' + dato[1]); 
			//}
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				$(':input, .btn').prop('disabled', false);
				$("#errors_cuerpo").append("<hr/>");
				//tinymce.activeEditor.getBody().setAttribute('contenteditable', true);//editores editables
				console.log(respuesta);
				if (respuesta.success) {
					$(':input, .btn').prop('disabled', true);
					if("responseText" in respuesta.data){						
						$("#errors_cuerpo").append(respuesta.data.responseText);
					}else{
						$("#errors_cuerpo").append('<div class="alert alert-success"><strong>'+respuesta.data.mensaje+'</strong></div>');
						history.pushState({url: window.location.href+"#modal", titulo:'mensaje'}, null, window.location.href+"#modal");
						$(formulario).removeClass('prevent_lost');
						bootbox.dialog({
							title: "Hecho",
							message: respuesta.data.mensaje,
							onEscape: function(){
								return false;
							},
							buttons: {
								success: {
									label: "Ok",
									className: "btn-success",
									callback: function () {
										window.history.back();
										if (accion_submit == 3) {
											window.history.back();
										}else{
											$(formulario).addClass('prevent_lost');
											$(':input, .btn').prop('disabled', false);
										}
										return false;
									}
								}
							}
						});
						//setTimeout(function() {window.history.back()}, 1500);
					}
				}else{
					switch (respuesta.data.status) {
						case 422:
							// var obj = JSON.parse(respuesta.data.responseText);
							var obj = respuesta.data.responseJSON.errors;
							$.each(obj, function(index, val) {
								$.each(val, function(i, v) {
									console.log(v);
									$("#errors_cuerpo").append('<div class="alert alert-danger"><strong>'+v+'</strong></div>');
								});
							});
							break;
						case 200:
						case 201:
							$(':input, .btn').prop('disabled', true);
							$("#errors_cuerpo").append('<div class="alert alert-success"><strong>'+respuesta.data.responseText+'</strong></div>');
							break;
						case 500:
							$("#errors_cuerpo").append(respuesta.data.responseText);
							break;
						default:
							break;
					}
				}
			});
		});

		// Funciones

		function agregar_clausula(nombre, tipo) {
			if (tipo == 1) {
				var clausula = Handlebars.compile(document.getElementById('template-clausula-contrato').innerHTML);
				var datos = {};
				
				datos.id = guid();
				datos.numero = ($(".panel-clausula").length + 1);
				datos.ordinal = ordinales[datos.numero];
				datos.nombre = nombre;

				datos.ordinal = (datos.ordinal ? datos.ordinal.toUpperCase() : '');
				datos.nombre = (datos.nombre ? datos.nombre.toUpperCase() : '');

				$('#clausulas-container').append(clausula(datos));
				tinyMCE.execCommand('mceAddEditor', false, "clausula-"+datos.id);
			} else if(tipo == 2){
				if ( $(".panel-clausula-obligaciones").length > 0 ) {
					alert("Ya existe un panel de clausula de obligaciones.")
					return false;
				}
				var clausula = Handlebars.compile(document.getElementById('template-clausula-obligaciones').innerHTML);
				var datos = {};

				datos.id = guid();
				datos.numero = ($(".panel-clausula").length + 1);
				datos.ordinal = ordinales[datos.numero];
				datos.nombre = "OBLIGACIONES DEL CONTRATISTA";

				datos.ordinal = (datos.ordinal ? datos.ordinal.toUpperCase() : '');
				datos.nombre = (datos.nombre ? datos.nombre.toUpperCase() : '');
				
				$('#clausulas-container').append(clausula(datos));
			}
		}

		function remover_clausula (clausula) {
			var container = $(clausula).parent();
			$(clausula).remove();
			$.each(container.find('.panel-clausula'), function(index, panel) {
				$(panel).find(".ordinal-panel-clausula").text((ordinales[index+1]).toUpperCase());
				$.each($(panel).find('[name^="clausula["]'), function(i, el) {
					var old_name = el.getAttribute("name");
					var new_name = old_name.replace(/clausula\[[0-9]\]/i, "clausula["+(index+1)+"]");
					el.setAttribute("name", new_name);
					//console.log(old_name);
					//console.log(new_name);
				});
			});
		}

		function remover_obl (obl) {
			var container = $(obl).parent();
			$(obl).remove();
			$.each(container.find('.obligacion-group'), function(index, oblig) {
				$(oblig).find('.numero-obligacion').text((index+1)+")");
			});
		}

		function agregar_obligacion_especifica (numero, texto) {
			var clausula = Handlebars.compile(document.getElementById('template-obligacion').innerHTML);
			var datos = {};
			datos.numero = numero;
			datos.texto = texto;
			datos.inputname = "obligaciones[obligacion_espf][]";

			$('#seccion-obligaciones-espf').append(clausula(datos));
		}

		function agregar_obligacion_general (numero, texto) {
			var clausula = Handlebars.compile(document.getElementById('template-obligacion').innerHTML);
			var datos = {};
			datos.numero = numero;
			datos.texto = texto;
			datos.inputname = "obligaciones[obligacion_gnrl][]";

			$('#seccion-obligaciones-gnrl').append(clausula(datos));
		}

		function remover_obligacion (elemento) {
			
		}
	});
</script>

<style>
	.input-group-vertical {
		margin-bottom: 10px;
	}
	.input-group-vertical .form-control {
		border-radius: 0;
	}
	.input-group-vertical .form-group {
		margin-bottom: 0;
	}
	.input-group-vertical .form-group:not(:last-child) .form-control:not(:focus) {
		border-bottom-color: transparent;
	}
	.input-group-vertical .form-group:nth-child(2) .form-control {
		border-top-left-radius: 3px;
		border-top-right-radius: 3px;
	}
	.input-group-vertical .form-group:last-child .form-control {
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;
		top: -2px;
	}


	input#consecutivo_cpe{
		border:  none;
		border-bottom: 1px solid;
		width: 110px;
		text-align: center;
	}
	#encabezado_cto{
		text-transform: uppercase;
		font-family: 'arial';
		font-size: 14pt;
	}
	body.mce-fullscreen #contenedor-pagina{
		overflow-y: inherit;
	}
	.mce-txt{
		width: auto !important;
	}
	.panel-heading{
		position: relative;
	}
	.close-button-clausula{
		position: absolute;
		right: 10px;
		top: 0px;
		font-size: 27px;
		color: #B20000;
		cursor: pointer;
		z-index: 2;
	}
	
	.panel-clausula-obligaciones .input-group-addon.btn-primary{
		color: #fff !important;
    	background-color: #337ab7 !important;
    	border-color: #2e6da4 !important;
	}


	@if(!Request::ajax())
	div.mce-fullscreen {
		top: 60px !important;
		left: 250px !important;
		width: calc(100% - 250px);
		display: flex;
		flex-direction: column;
		padding-bottom: 60px;
	}
	@media (max-width : 992px) {
		div.mce-fullscreen {
			top: 50px !important;
			left: 0px !important;
			width: 100%;
			display: flex;
			flex-direction: column;
			padding-bottom: 50px;
		}
	}
	@endif

	/* LED 60 pulgadas */
    @media only screen and (max-width : 1200px) {

    }

    /* PC */
    @media only screen and (max-width : 992px) {

    }

    /* Tabletas */
    @media only screen and (max-width : 768px) {

    }

    /* telefonos */ 
    @media only screen and (max-width : 480px) {
		#encabezado_cto{
			font-size: 10pt;
		}
    }

    @media only screen and (max-width : 320px) {
		.text-btn{
			display: none;
		}
    }
</style>
@endsection