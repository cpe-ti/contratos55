@extends('layouts.pagina_maestra')
@section('titulo')
Legalización de contrato
@endsection
@section('contenido')
<div id="contenido_legalizacion_cto" class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<div class="colmd-12">
		<table class="table table-hover">
			<tbody>
				<tr>
					<td><strong>Tipo:</strong></td>
					<td>{{$contrato->tipo_contrato->tipo}}</td>
				</tr>
				<tr>
					<td><strong>Contrato Nº:</strong></td>
					<td>{{$contrato->consecutivo_cpe}}</td>
				</tr>
				<tr>
					<td><strong>Tercero:</strong></td>
					<td>{{$contrato->tercero->NombreCompleto()}}</td>
				</tr>
				<tr>
					<td><strong>Valor Contrato:</strong></td>
					<td>{{ "$ ".number_format($contrato->valor, 2) }}</td>
				</tr>
				<tr>
					<td><strong>CDP Nº:</strong></td>
					<td>{{$contrato->cdp}}</td>
				</tr>
				<tr>
					<td><strong>PDF:</strong></td>
					<td>
						<a class="btn btn-danger" href="{{url('/imprimir-contrato/'.$contrato->id)}}" target="_blank">
							<i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i>
						</a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<hr>
	<div class="row">
		<form id="form-legalizar-cto" target="_blank" action="{{ Request::url() }}" method="POST" autocomplete="off">
			{!!csrf_field()!!}
			<div class="form-group col-md-4">
				<label for="id_oasis">ID Oasis:</label>
				<input type="number" name="id_oasis" class="form-control text-right" placeholder="ID de OASIS (OCNT)" value="{{$contrato->id_oasis}}">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-4">
				<label for="id_oasis">Consecutivo CPE:</label>
				<input type="text" name="consecutivo_cpe" class="form-control text-right" id="consecutivo_cpe" placeholder="consecutivo" data-inputmask="'mask': '999-99[A]'" value="{{$contrato->consecutivo_cpe}}">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-4">
				<label for="rp">Número de Registro Presupuestal:</label>
				<input type="number" name="rp" class="form-control text-right" placeholder="Número de RP" value="{{$contrato->rp}}">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-6">
				<label for="fecha_rp">Fecha de Registro Presupuestal:</label>
				<input type="date" name="fecha_rp" class="form-control text-right" placeholder="Fecha de RP" value="{{$contrato->fecha_rp}}">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-6">
				<label for="fecha_suscripcion">Fecha de suscripción:</label>
				<input type="date" name="fecha_suscripcion" class="form-control text-right" placeholder="Fecha de suscripción" value="{{$contrato->fecha_suscripcion}}">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-6">
				<label for="fecha_suscripcion">Fecha de Legalización:</label>
				<input type="date" name="fecha_legalizacion" class="form-control text-right" placeholder="Fecha de legalización" value="{{$contrato->fecha_legalizacion}}">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-6">
				<label for="url_secop">URL Secop:</label>
				<input type="text" name="url_secop" class="form-control" placeholder="URL Secop" value="{{$contrato->url_secop}}">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-12" id="parent_select_supers">
				<label class="control-label" for="select_supers">Supervisor de contrato:</label>
				<select name="supervisor" id="select_supers" class="form-control" style="width: 100%" multiple="multiple">{{-- required="required" --}}
					@foreach($supervisores as $super)
					@if($super->id == $contrato->supervisor)
					<option value="{{$super->id}}" selected>{{$super->NombreCompleto()}}</option>
					@else
					<option value="{{$super->id}}">{{$super->NombreCompleto()}}</option>
					@endif
					@endforeach
				</select>
			</div>
			<div class="col-md-12">
				<hr>
			</div>
			<div id="seccion-polizas">
				@if(count($contrato->polizas))
					@foreach ($contrato->polizas as $poliza)
					<div class="form-group col-md-12 poliza">
						<div class="panel panel-default panel-poliza">
							<div class="panel-heading">
								<label for="">Póliza</label>
								<div class="row">
									<div class="form-group col-sm-5 col-xs-12">
										<label for="">Aseguradora:</label>
										<input type="text" class="form-control" name="polizas[{{ $loop->index }}][aseguradora]" placeholder="Aseguradora" value="{{$poliza->aseguradora}}" />
									</div>
									<div class="form-group col-sm-3 col-xs-6">
										<label for="">Nº Póliza:</label>
										<input type="number" class="form-control" name="polizas[{{ $loop->index }}][numero_poliza]" placeholder="Número Póliza" value="{{$poliza->numero_poliza}}" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label for="">Fecha Expedición:</label>
										<input type="date" class="form-control" name="polizas[{{ $loop->index }}][fecha_expedicion]" placeholder="Fecha de Expedición" value="{{$poliza->fecha_expedicion}}" />
									</div>
								</div>
							</div>
							<div class="panel-body garantias">
								@foreach ($poliza->garantias as $garantia)									
								<div class="row garantia">
									<div class="form-group col-sm-2 col-xs-12">
										<label for="">Amparo:</label>
										<input type="text" class="form-control" name="polizas[{{ $loop->parent->index }}][garantia][{{ $loop->index }}][amparo]" placeholder="(Garantía)" value="{{$garantia->amparo}}" />
									</div>
									<div class="form-group col-sm-3 col-xs-12">
										<label for="">Valor:</label>
										<input type="text" class="form-control valor-amparo" name="polizas[{{ $loop->parent->index }}][garantia][{{ $loop->index }}][valor_amparo]" data-inputmask="'alias': 'currency', 'autoUnmask': true" placeholder="Valor" value="{{$garantia->valor_amparo}}" />
									</div>
									<div class="form-group col-sm-3 col-xs-6">
										<label for="">Fecha inicio:</label>
										<input type="date" class="form-control" name="polizas[{{ $loop->parent->index }}][garantia][{{ $loop->index }}][fecha_inicio]" placeholder="Fecha de inicio" value="{{$garantia->fecha_inicio}}" />
									</div>
									<div class="form-group col-sm-3 col-xs-6">
										<label for="">Fecha terminación:</label>
										<input type="date" class="form-control" name="polizas[{{ $loop->parent->index }}][garantia][{{ $loop->index }}][fecha_fin]" placeholder="Fecha de terminación" value="{{$garantia->fecha_fin}}" />
									</div>
									<div class="form-group col-sm-1 col-xs-12">
										<label for="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
										<div class="btn btn-danger pull-right close-amparo">
											<i class="fa fa-trash-o" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-12">
										<div class="btn btn-default agregar-garantia pull-right">
											<i class="fa fa-plus" aria-hidden="true"></i> Agregar Garantía
										</div>
									</div>
								</div>
							</div>
							<div class="close-poliza">
								<i class="fa fa-times" aria-hidden="true"></i>
							</div>					
						</div>
					</div>
					@endforeach	
				@endif
			</div>
			<div class="col-md-12">
				<hr>
			</div>
			<div class="col-md-12" id="errors_legalizar">
				
			</div>
			<div class="col-md-12" style="margin-bottom: 10px;">
		  		<button type="button" class="btn btn-default" id="boton-agregar-poliza">
					<i class="fa fa-plus" aria-hidden="true"></i> Agregar Póliza
				</button>
				<div class="form-group pull-right">
					<div class="btn-group">
						<button type="submit" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" class="btn btn-primary" name="accion" value="1">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
						</button>
						<button type="submit" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" class="btn btn-success" name="accion" value="2">
							<i class="fa fa-check-circle-o" aria-hidden="true"></i> Legalizar
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.garantia{
		/* background-color: #C1DEE3;
		border: 1px solid #9BADB1;
		border-radius: 5px;
		margin-left: 0px !important;
		margin-right: 0px !important;
		margin-bottom: 10px !important;
		padding-top: 10px;
		position: relative; */
		/* position: relative; */
	}
	.panel-poliza{
		-webkit-animation: pol-in 500ms forwards;
		-moz-animation: pol-in 500ms forwards;
		-o-animation: pol-in 500ms forwards;
		animation: pol-in 500ms forwards;
	}
	@-webkit-keyframes pol-in {
		from { opacity: 0; -webkit-transform: translateY(10%); -webkit-transform: scale(0.8, 0.8); }
		to { opacity: 1; -webkit-transform: translateY(0%); -webkit-transform: scale(1, 1); }
	}
	@-o-keyframes pol-in {
		from { opacity: 0; -o-transform: translateY(10%); -o-transform: scale(0.8, 0.8); }
		to { opacity: 1; -o-transform: translateY(0%); -o-transform: scale(1, 1); }
	}
	@-moz-keyframes pol-in {
		from { opacity: 0; -moz-transform: translateY(10%); -moz-transform: scale(0.8, 0.8); }
		to { opacity: 1; -moz-transform: translateY(0%); -moz-transform: scale(1, 1); }
	}
	@keyframes pol-in {
		from { opacity: 0; transform: translateY(10%); transform: scale(0.8, 0.8); }
		to { opacity: 1; transform: translateY(0%); transform: scale(1, 1); }
	}
	.close-amparo{
		/* position: absolute; */
		/* right: 0px; */
	}
	.garantia input[readonly="readonly"]{
		background-color: #FFFFFF;
	}
	.close-poliza{
		position: absolute;
		right: -10px;
		top: -10px;
		background-color: #BC0000;
		color: #FFFFFF;
		cursor: pointer;
		width: 25px;
		height: 25px;
    	line-height: 25px;
    	text-align: center;
		border-radius: 100px;
	}
	.nombre-super{
		white-space: nowrap;
		flex-grow: 1;
		overflow: hidden;
    	text-overflow: ellipsis;
	}
	.supervisor-l{
		display: flex;
		flex-direction: row;
	}
	.supervisor-l>.id-super{
		flex-grow: 0;
		font-size: 8pt;
		background-color: #1c5c76;;
		color: #FFFFFF;
		border-radius: 5px;
		padding: 3px;
	}
</style>
@include('plantillas.poliza')
@include('plantillas.garantia')
<script>
	inicializar (function () {
		//dropdown supervisores
		$(":input").inputmask({placeholder: " "});
		$("#select_supers").select2({
			maximumSelectionLength: 1,
			dropdownParent: $("#parent_select_supers"),
			language: "es",
			placeholder: 'Buscar un supervisor por nombre o cédula',
			templateResult: function (item) {
				return $('<div class="supervisor-l"><span class="nombre-super">'+item.text+'</span><span class="id-super">'+item.id+'</span></div>');
			},
			matcher: function(params, data) {
				if ($.trim(params.term) === '') {
					return data;
				}
				var termino = params.term.toUpperCase();
				var id = data.id.toUpperCase();
				var texto = data.text.toUpperCase();
				if(texto.indexOf(termino) >= 0 || id.indexOf(termino) >= 0){
					return data;
				}
				return null;
			}
		});
		//eventos
		$("#boton-agregar-poliza").click(function(event) {
			var datos = {};
			var numero = $(".poliza").length;
			
			var html = document.getElementById('template-poliza').innerHTML;
			var newhtml = html.replace(/(polizas\[\])/g, "polizas["+numero+"]");
			
			//console.log(html);
			//console.log(newhtml);
			
			var poliza = Handlebars.compile(newhtml);
			$('#seccion-polizas').append(poliza);
			$(":input").inputmask({placeholder: " "});
			scrollTo($("#contenedor-pagina")[0], $("#contenedor-pagina>div").height(), 500);
		});
		$("#seccion-polizas").on('click', '.close-poliza', function(event) {
			event.preventDefault();
			$(event.currentTarget).closest('.poliza').remove();
			$(".poliza").each(function( index ) {
				//console.log( this.innerHTML );
				$.each($(this).find('[name^="polizas["]'), function(i, el) {
					var name = $(el).attr('name');
					var res = name.replace(/polizas\[[0-9]+\]/i, 'polizas['+index+']');
					$(el).attr('name', res);
				});
			});
		});
		//
		$("#seccion-polizas").on('click', '.agregar-garantia', function(event) {
			
			var polizas = document.getElementById( "seccion-polizas" );
			var panel = $(this).closest('.panel-poliza', polizas)[0];
			var container_garantias = $(panel).find('.garantias')[0];
			
			var p_number = $(".panel-poliza").index( panel );
			var g_number = $('.garantia',panel).length;
			
			var html = document.getElementById('template-garantia').innerHTML;
			var newhtml = html.replace(/(\[garantia\]\[\])/g, "[garantia]["+g_number+"]");
			var newhtml = newhtml.replace(/(polizas\[\])/g, "polizas["+p_number+"]");
			
			var garantia = Handlebars.compile(newhtml);
			
			$(container_garantias).append(garantia);
			$(":input").inputmask({placeholder: " "});
		});


		$("#seccion-polizas").on('click', '.close-amparo', function(event) {
			console.log($('.garantia').length);
			if (!($('.garantia').length > 1)) {
				$(this).addClass('disabled');
				return false;
			}
			var garantia_container = $(this).closest('.garantias')[0];
			$(this).closest('.garantia').remove();
			$(".garantia", garantia_container).each(function( index ) {
				$.each($(this).find('[name*="garantia["]'), function(i, el) {
					var name = $(el).attr('name');
					var res = name.replace(/\[garantia\]\[[0-9]+\]/, '[garantia]['+index+']');
					$(el).attr('name', res);
				});
			});
		});

		//pre evento a submit
		//Submit button pressed
		var accion_submit = 0;
		$(":submit","#form-legalizar-cto").click(function(e){
			accion_submit = $(this).val() ? $(this).val() : '0';
		});


		$("#form-legalizar-cto").on('submit', function(event) {
			event.preventDefault();
			event.stopPropagation();
			$(".valor-amparo").inputmask('remove');
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			datos.append('accion', accion_submit);
			$(':input, .btn').prop('disabled', true);
			$("#errors_legalizar").html('');
			$(".valor-amparo").inputmask();
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				$(':input, .btn').prop('disabled', false);
				if (respuesta.success) {
					$(':input, .btn').prop('disabled', true);
					if("responseText" in respuesta.data){						
						$("#errors_legalizar").append(respuesta.data.responseText);
					}else{
						$("#errors_legalizar").append('<div class="alert alert-success"><strong>'+respuesta.data.mensaje+'</strong></div>');
						setTimeout(function() {window.history.back()}, 1500);
					}
				}else{
					switch (respuesta.data.status) {
						case 404:
							$("#errors_legalizar").append(respuesta.data.responseText);
							break;
						case 422:
							// var obj = JSON.parse(respuesta.data.responseText);
							var obj = respuesta.data.responseJSON.errors;
							$.each(obj, function(index, val) {
								$.each(val, function(i, v) {
									console.log(v);
									$("#errors_legalizar").append('<div class="alert alert-danger"><strong>'+v+'</strong></div>');
								});
							});
							break;
						case 200:
						case 201:
							if (respuesta.data.mensaje && data.success) {
								$(':input, .btn').prop('disabled', true);
								$("#errors_legalizar").append('<div class="alert alert-success"><strong>'+respuesta.data.responseText+'</strong></div>');
								window.history.back();								
							}else{
								$("#errors_legalizar").append('<div class="alert alert-danger"><strong>'+respuesta.data.responseText+'</strong></div>');
							}
							break;
						case 500:
							$("#errors_legalizar").append(respuesta.data.responseText);
							break;
						default:
							break;
					}
				}
			});
		});
	});
</script>
@endsection