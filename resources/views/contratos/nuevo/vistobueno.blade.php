@extends('layouts.pagina_maestra')
@section('titulo')
Revisar
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1" id="contenedor-nuevo-cto">
	<h2>Revisar Contrato</h2>
	<div class="col-md-12">
		<table width="100%">
			<tr style="border-bottom: 1px solid #EAEAEA">
				<td style="padding-right: 10px; text-align: left;"><strong>Consecutivo CPE:</strong></td>
				<td>{{$contrato->consecutivo_cpe}}</td>
			</tr>
			<tr style="border-bottom: 1px solid #EAEAEA">
				<td style="padding-right: 10px; text-align: left;"><strong>Tercero:</strong></td>
				<td>{{$contrato->tercero->NombreCompleto()}}</td>
			</tr>
			<tr style="border-bottom: 1px solid #EAEAEA">
				<td style="padding-right: 10px; text-align: left;"><strong>Valor:</strong></td>
				<td>{{ "$ ".number_format($contrato->valor, 2) }}</td>
			</tr>
			<tr style="border-bottom: 1px solid #EAEAEA">
				<td style="padding-right: 10px; text-align: left;"><strong>CDP:</strong></td>
				<td>{{$contrato->cdp}}</td>
			</tr>
			<tr style="border-bottom: 1px solid #EAEAEA">
				<td style="padding-right: 10px; text-align: left;"><strong>Modalidad de Contratación:</strong></td>
				<td>{{$contrato->modalidad->modalidad}}</td>
			</tr>
		</table>
	</div>
	<div class="row cuerpo-doc">
		<div class="col-md-12 col-pdf">
			{{-- <iframe src="{{url('/imprimir-contrato/'.$contrato->id)}}" id="vista_previa_pdf" type="application/pdf" width="100%" height="700" frameborder="0">
				<h3>No soportado</h3>
			</iframe> --}}
			<object data="{{url('/imprimir-contrato/'.$contrato->id)}}" id="vista_previa_pdf" type="application/pdf" width="100%" height="700" standby="Cargando pdf">
				<h2>No soportado</h2>
			</object>
			{{-- <embed src="{{url('/imprimir-contrato/'.$contrato->id)}}" id="vista_previa_pdf" width="100%" height="700px;" type='application/pdf'> --}}
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center" id="success-message">
			<i class="fa fa-check-circle" aria-hidden="true"></i><span> Contrato Aprobado</span>
		</div>
	</div>
	<form id="form-aprobar-cto" action="{{ Request::url() }}" method="POST" autocomplete="off">
		<hr>
		<h3>Observaciones:</h3>
		<div class="row comentarios">
			<div class="col-md-12">
				<textarea class="form-control" name="observaciones" id="" cols="30" rows="5"></textarea>
			</div>
		</div>
		<div class="row">
			<div id="errores" class="col-md-12">
				
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				{!!csrf_field()!!}
				<div class="row row-enviar">
					<div class="form-group col-xs-6 text-left">
						<button type="submit" class="btn btn-danger" name="accion" value="2">
							<i class="fa fa-ban" aria-hidden="true"></i> Rechazar
						</button>					
					</div>
					<div class="form-group col-xs-6 text-right">
						<button type="submit" class="btn btn-success" name="accion" value="1">
							<i class="fa fa-check-circle" aria-hidden="true"></i> Aprobar
						</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<hr>
</div>
<style>
	#success-message{
		color: green;
		font-size: 18pt;
		display: none;
	}
	#contenedor-nuevo-cto{
		/* padding-right: 10px; */
		/* padding-left: 10px; */
	}
	.panel-nuevo-cto>.panel-body{
		padding-left: 5px; 
		padding-right: 5px; 
	}
	#form-nuevo-cto-fase-1 textarea { 
		resize:vertical;
	}
</style>
<script type="text/javascript">
	inicializar (function () {
		var accion_submit = 0;
		$(":submit","#form-aprobar-cto").click(function(e){
			accion_submit = $(this).val() ? $(this).val() : '0';
		});
		$("#form-aprobar-cto").on('submit', function(event) {
			event.preventDefault();
			event.stopPropagation();
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			datos.append('accion', accion_submit);
			$(':input, .btn').prop('disabled', true);
			$("#errors_legalizar").html('');
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				var mensaje = "";
				var ok = true;
				if("mensaje" in respuesta.data){
					mensaje = respuesta.data.mensaje;
				}else{
					mensaje = respuesta.data.responseText;
					$(':input, .btn').prop('disabled', false);
					ok = false;
				}
				bootbox.dialog({
					title: "Hecho",
					message: mensaje,
					onEscape: function(){
						
					},
					buttons: {
						success: {
							label: "Ok",
							className: "btn-success",
							callback: function () {
								ok ? window.history.back() : ok;
							}
						}
					}
				});
			});
		});
	});
</script>
@endsection