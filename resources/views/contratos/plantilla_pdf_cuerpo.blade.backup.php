<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>{{ mb_strtoupper($contrato->tercero->NombreCompleto()) }}</title>
	<style>
		body{
			font-family: 'Arial';
		}
	</style>
</head>
<body>
	{{--  <div id="cuerpo" style="page-break-before: avoid; line-height: 110%;">  --}}
	{{--  Se eliminaran todos los !important en el documento se crea un backup de la plantilla  --}}
	<div id="cuerpo" style="line-height: 110%;">
		{!! $contrato->encabezado_consideraciones !!}
		<h3 style="text-align: center;">CLÁUSULAS</h3>
		@foreach($contrato->clausulas as $key => $clausula)
			@if($clausula->tipo->tipo == 'obligaciones')
				@if($contrato->obligaciones)
				<p style="text-align: justify;"><strong style="text-transform: uppercase;">{{ $clausula->contenido_html }} - OBLIGACIONES DEL CONTRATISTA: </strong> <span style="font-size: 12.0pt; font-family: 'Helvetica';"><strong>EL CONTRATISTA,</strong> se obliga a cumplir con el objeto del presente contrato, ejecutando, entre otras, las siguientes actividades, tal y como consta en la propuesta presentada por la <strong>CONTRATISTA</strong> y aceptada por el <strong>CONTRATANTE</strong>, la cual hace parte integrante de presente contrato:</span></p>
				<p style="text-align: justify;">
					<strong>A)&nbsp;OBLIGACIONES ESPECÍFICAS:</strong>
					@foreach($contrato->obligaciones->where('tipo_obligacion_id', 2) as $obl)
					<strong>{{$obl->ordinal}})</strong> {{$obl->obligacion}}
					@endforeach
					<strong>B) OBLIGACIONES GENERALES:</strong>
					@foreach($contrato->obligaciones->where('tipo_obligacion_id', 1) as $obl)
					<strong>{{$obl->ordinal}})</strong> {{$obl->obligacion}}
					@endforeach
				</p>
				@endif
			@else
			{!! $clausula->contenido_html !!}                
			@endif
		@endforeach
		<p><span style="font-size: 12pt; font-family: helvetica, arial, sans-serif; color: black;">Para constancia, se firma por las partes <strong>CONTRATANTES</strong>, en Bogotá D.C., a los </span></p>
		<p> </p>
		<table style="width: 100%; ">
			<tbody>
				<tr>
					<td style="width: 50%;"><span style="font-family: helvetica, arial, sans-serif;"><strong><span style="font-size: 12pt;">EL CONTRATANTE:</span></strong></span></td>
					<td style="width: 50%;"><span style="font-family: helvetica, arial, sans-serif;"><strong><span style="font-size: 12pt;">EL CONTRATISTA:</span></strong></span></td>
				</tr>
				<tr>
					<td style="width: 50%;">
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</td>
					<td style="width: 50%;">
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td style="width: 50%; vertical-align: top !important;"><span style="text-transform: uppercase; vertical-align: top !important; font-size: 10.5pt; border-top: 2px solid;"><strong>{{$contrato->nombre_contratante}}</strong></span></td>
					<td style="width: 50%; vertical-align: top !important;"><span style="text-transform: uppercase; vertical-align: top !important; font-size: 10.5pt; border-top: 2px solid;"><strong>{{$contrato->nombre_contratista}}</strong></span></td>
				</tr>
				<tr style="vertical-align: top;">
					<td style="width: 50%; vertical-align: top !important;"> <span style="font-size: 10.5pt; font-family: helvetica, arial, sans-serif; text-transform: capitalize;">{{$contrato->cargo_contratante}}</span> </td>
					<td style="width: 50%; vertical-align: top !important;"> <span style="font-size: 10.5pt; font-family: helvetica, arial, sans-serif; text-transform: capitalize;">{{$contrato->cargo_contratista}}</span> </td>
				</tr>
				<tr>
					<td style="width: 50%;"> <span style="font-family: helvetica, arial, sans-serif;"><strong><span style="font-size: 10pt; text-transform: uppercase;">COMPUTADORES PARA EDUCAR</span></strong> </span> </td>
					<td style="width: 50%;"> <span style="font-family: helvetica, arial, sans-serif;"><strong><span style="font-size: 10pt; text-transform: uppercase;">{{$contrato->tercero->tipo->id == 2 ? $contrato->tercero->NombreCompleto() :  ""}}</span></strong> </span> </td>
				</tr>
			</tbody>
		</table>
		<div style="height: 30px;"></div>
		<div style="height: 20px; width: 100%;">
			<span style="font-size: 10pt; font-family: helvetica, arial, sans-serif;">Revisó: &nbsp;{{ $contrato->reviso() }}</span>
		</div>
		<div style="height: 20px; width: 100%;">
			<span style="font-size: 10pt; font-family: helvetica, arial, sans-serif;">Diana Marlén Pérez L. - Asesora Dirección Ejecutiva</span>
		</div>
		<div style="height: 20px; width: 100%;">
			<span style="font-size: 10pt; font-family: helvetica, arial, sans-serif;">Proyectó: {{ $contrato->proyecto() }}</span>
		</div>
	</div>
</body>
</html>