<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>{{ mb_strtoupper($contrato->tercero->NombreCompleto()) }}</title>
	<style>
		body{
			font-family: 'Arial';
			font-size: 12pt;
		}
		p{
			line-height: 100%;
		}
	</style>
</head>
<body>
	{!!$contrato->html_carta!!}
</body>
</html>