@extends('layouts.pagina_maestra')
@section('titulo')
Todos los Contratos ({{ count($contratos) }})
@endsection
@section('contenido')
<div id="contenido_lista_contratos" class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<div class="clearfix">
		<div class="input-group col-xs-12">
    	  	<div class="input-group">
				<input type="text" name="filter_busqueda_doc" id="filter_busqueda_doc" class="form-control" placeholder="Buscar"/>			
				<span class = "input-group-addon">
					<i class="fa fa-search" aria-hidden="true"></i>
				</span>
			</div>
    	  	@permission('crear-documento')
    	  		<span class="input-group-btn">
    	  		  	<a class="boton-nuevo-contrato btn btn-primary link_dinamico" href="{{ Request::url().'/nuevo' }}"></a>
    	  		</span>			    
			@endpermission
    	</div>
    	@if (Request::segment(1) == 'supervision')
    	<div class="input-group col-xs-12" id="filtro-estado" style="display: none;">
    	@else
    	<div class="row" id="filtro-estado">
    	@endif
    	<div class="col-md-12">    		
    		<div class="col-md-2 col-xs-4 col-check">
    			<label class="check-filtro checkbox-inline label-filtro label-creado"><input type="checkbox" value="1" checked="checked">Creado</label>
    		</div>
    		<div class="col-md-2 col-xs-4 col-check">
    			<label class="check-filtro checkbox-inline label-filtro label-guardado"><input type="checkbox" value="2" checked="checked">Cuerpo</label>
    		</div>
    		<div class="col-md-2 col-xs-4 col-check">
    			<label class="check-filtro checkbox-inline label-filtro label-revision"><input type="checkbox" value="3" checked="checked">En revisión</label>
    		</div>
    		<div class="col-md-2 col-xs-4 col-check">
    			<label class="check-filtro checkbox-inline label-filtro label-rechazado"><input type="checkbox" value="4" checked="checked">Rechazado</label>
    		</div>
    		<div class="col-md-2 col-xs-4 col-check">
    			<label class="check-filtro checkbox-inline label-filtro label-aprobado"><input type="checkbox" value="5" checked="checked">Aprobado</label>
    		</div>
    		<div class="col-md-2 col-xs-4 col-check">
    			<label class="check-filtro checkbox-inline label-filtro label-legalizado"><input type="checkbox" value="6" checked="checked">Legalizado</label>
    		</div>
			<div class="col-md-2 col-xs-4 col-check">
    			<label class="check-filtro checkbox-inline label-filtro label-mio"><input id="checkboxmio" type="checkbox" value="7">Mios</label>
    		</div>
    	</div>
    	</div>
	</div>
	{{-- <p>{{  }}</p> --}}
	<div class="row" id="row_all_contracts">
		@foreach ($contratos as $key => $contrato)
		@php
		$clase = '';
		switch ($contrato->estado_doc) {
			case 1:
				$clase = 'label-creado';
				break;
			case 2:
				$clase = 'label-guardado';
				break;
			case 3:
				$clase = 'label-revision';
				break;
			case 4:
				$clase = 'label-rechazado';
				break;
			case 5:
				$clase = 'label-aprobado';
				break;
			case 6:
				$clase = 'label-legalizado';
				break;			
			default:
				# code...
				break;
		}
		$mio = false;
		try {
			$mio = $contrato->audits()->with('user')->where('event','created')->first()->user->id == Auth::user()->id;
		} catch (\Exception $e) {
			//echo 'Caught \exception: ',  $e->getMessage(), "\n";
		}
		@endphp
		<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 col-contrato {{($key == count($contratos)-1) ? 'ch_sizer' : ''}}" data-tipo="{{$contrato->tipo}}" data-nombre="{{$contrato->NombreRapido()}}" data-estado="{{$contrato->estado_doc}}" data-mio="{{$mio}}">
			<a style="text-decoration:none;" class="link_dinamico a-contrato" href="{{ Request::url() }}/{{$contrato->id}}">
				{{-- <img class="image-cto" src="{{URL::asset('img/iconos/contratos/contrato-personalizado.svg')}}" alt=""> --}}
				{!! str_replace("NCICON", substr($contrato->tercero->NombreCompleto(),0,35).'...', svgRawContent( public_path( crearRuta('img', 'iconos', 'contratos', 'contrato-personalizado.svg') ) ) ) !!}
				{{--  {!!str_replace("NCICON", substr($contrato->tercero->NombreCompleto(),0,35).'...', file_get_contents(URL::asset('img/iconos/contratos/contrato-personalizado.svg')))!!}  --}}
			</a>
			<span class="label-contrato {{$clase}}">{{$contrato->NombreRapido()}}</span>
		</div>
		@endforeach		
	</div>
	@if($contratos->count() == 0)
		<div class="row">
			<div id="sin-contratos">
				{{-- {!!file_get_contents(URL::asset('img/iconos/notfound/4.svg'))!!} --}}
				{!! svgRawContent( public_path( crearRuta('img', 'iconos', 'notfound', '4.svg') ) ) !!}
				<p class="texto-empty text-center">Aqui no hay nada.</p>
			</div>			
		</div>
	@endif
</div>
<style>
	#row_all_contracts{
		margin-top: 15px;
	}
	.boton-nuevo-contrato:before {
	   	content: "Nuevo Contrato";
	}
	.col-contrato{
		-webkit-transition: -webkit-transform 500ms;
		-moz-transition: -moz-transform 500ms;
		-ms-transition: -ms-transform 500ms;
		-o-transition: -o-transform 500ms;
		transition: transform 500ms;
		will-change: transform;
		text-align: center;
		clear: none;
		height: 250px;
		border-radius: 10px;
		/*overflow: visible !important;*/
	}
	.col-contrato:hover{
		background-color: #AFC9D2;
	}
	a.a-contrato>.image-cto, a.a-contrato>svg{
		max-height: 80%;
   		text-decoration: none !important;
		margin-top: 10px;
		margin-bottom: 5px;
	}
	.label-contrato{
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;	
		font-family: 'arial';
		font-size: 13px;
		line-height: 13px;
		padding-top: 5px;
		padding: 5px;
		text-align: center;
		word-wrap: break-word;
		/*word-break: break-all !important;*/
		display: inline-block;
		max-width: 110%;
		/* margin-left: -5%; */
		border-radius: 10px;
	}
	.label-filtro{
		border-radius: 5px;
		padding-left: 5px;
		padding-right: 5px;
		/* margin-left: 10px; */
	}
	.label-filtro>input{
		margin-left: -15px !important;
	}
	.label-creado{
		background-color: #70D2EB;
		color: #335E69;
	}
	.label-guardado{
		background-color: #64B8CE;
		color: #2F5660;
	}
	.label-revision{
		background-color: #FFF669;
		color: #767230;
	}
	.label-rechazado{
		background-color: #FF9191;
		color: #704343;
	}
	.label-aprobado{
		background-color: #92E89E;
		color: #446C4A;
	}
	.label-legalizado{
		
	}
	.check-filtro{
		width: 100%;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
	}
	#filtro-estado{
		margin-top: 5px;
	}
	.col-check{
		padding-left: 2px !important;
		padding-right: 2px !important;

	}
	#sin-contratos{
		text-align: center;
		margin-top: 13%;
	}
	#sin-contratos > svg{
		max-width: 250px;
	}
	#sin-contratos > .texto-empty{
		font-size: 28px;
    	color: #bababa;
	}
	@media (max-width : 479px) {
		.boton-nuevo-contrato:before {
		   	content: "\f055";
		   	font-family: FontAwesome;
    		font-style: normal;
    		font-weight: normal;
    		text-decoration: inherit;
		}
    }
	@media (min-width : 480px) and (max-width: 767px) {
		
	}
	@media (min-width:768px) and (max-width: 991px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199px) {
		  
	}
	@media (min-width: 1200px) {
		  
	}
</style>
<script>
	inicializar (function () {
		'use strict';
		var Shuffle = window.shuffle;
		var contratos = new Shuffle(document.getElementById('row_all_contracts'), {
			itemSelector: '.col-contrato',
			sizer: '.ch_sizer',
			speed: 400,
			useTransforms: true
		});
		function filtrar_contratos() {
			var nombre;
			nombre = $('#filter_busqueda_doc').val().toLowerCase();
			var estados;
			estados = $("#filtro-estado input:checkbox:checked").map(function(){
    			return $(this).val();
    		}).get();
			var filtermio = $("#checkboxmio").is(":checked");
			contratos.filter(function(element){
				var name = (($(element).data('nombre').toLowerCase()).indexOf(nombre) >= 0);
				var estado = (jQuery.inArray( $(element).data('estado').toString(), estados ) !== -1);
				var mio = true;
				if(filtermio){
					mio = $(element).data('mio') == 1;
				}
				return (name && estado && mio);
			});
		}
		//$('#filter_busqueda_doc').on('input', function() {
    	//	filtrar_contratos();
		//});
		$('#filter_busqueda_doc').onDelayed('input', {{ count($contratos) }}, function() {
			filtrar_contratos();			
		});

		$("#filtro-estado").on('change', 'input:checkbox', function(event) {
			event.preventDefault();
			filtrar_contratos();
		});
	});
</script>
@endsection