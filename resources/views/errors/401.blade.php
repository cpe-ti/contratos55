@extends('layouts.pagina_maestra')
@section('titulo')
No Autenticado
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<!-- <h1>Aqui no hay nada que ver, vuelve por donde viniste antes de que te haga mi perra.</h1> -->
	<div class="img404">
		<!-- <img src="{{\app\Http\FileHelper::ImgBase64(public_path().'/img/errores/prohibido.svg')}}" alt="Largate de aqui"> -->
		{!!file_get_contents(URL::asset('/img/errores/prohibido.svg'))!!}
	</div>
	<h1 class="error_centrado">Error.</h1>
	<h3 class="error_centrado">No se ha logeado en el sistema.</h3>
</div>
<style>
	.img404{
		margin: 0 auto;
		text-align: center;
		margin-top: 50px;
	}
	.img404>svg{
		max-width: 100%;		
		max-height: 300px;		
		border-radius: 10px;
	}
	.img404>img{
		max-width: 100%;		
		max-height: 300px;		
		border-radius: 10px;
	}
	.error_centrado{
		text-align: center;
	}
</style>	
@endsection