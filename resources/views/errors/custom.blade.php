@extends('layouts.pagina_maestra')
@section('titulo')
{{$titulo}}
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<!-- <h1>Aqui no hay nada que ver, vuelve por donde viniste antes de que te haga mi perra.</h1> -->
	<div class="row">
		<div class="col-xs-12 img404">
			<img src="{{ URL::asset('img/errores/error.svg') }}" alt="Largate de aqui">
		</div>
	</div>
	<div class="row text-center">
		<h1>Error.</h1>
		<h3>{{$mensaje}}</h3>
	</div>
</div>
<style>
	.img404{
		margin: 0 auto;
		text-align: center;
	}
	.img404>img{
		max-width: 100%;		
		border-radius: 10px;
		max-width: 200px;
	}
</style>
@endsection
