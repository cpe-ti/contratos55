@extends('layouts.pagina_maestra')
@section('titulo')
Administrativa y Financiera
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	{{-- <h1>Administrativa y Financiera</h1> --}}
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('gaf/cdp') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-credit-card" aria-hidden="true"></i>
			</a>
			<div class="texto_item_menu">Provisiones Presupuestales</div>			
		</div>
	</div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('gaf/rp') }}" class="link_dinamico icono_item_menu">
				{{-- <i class="fa fa-usd" aria-hidden="true"></i> --}}
				<i class="fa fa-link" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Registros Presupuestales</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('gaf/terceros') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-users" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Terceros</span>
		</div>
	</div>	
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('gaf/contratos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-files-o" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Contratos</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('gaf/pagos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-usd" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Pagos</span>
		</div>
	</div>
</div>
@endsection