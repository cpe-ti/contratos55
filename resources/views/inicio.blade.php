@extends('layouts.pagina_maestra')
@section('titulo')
Seguimiento a Contratos
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<!-- <h1>Repositorio de Documentos</h1> -->
	<div class="row" id="icono-repositorio-inicio">
		{{-- <img src="{{URL::asset('img/iconos/contratos/contrato-2.svg')}}" alt=""> --}}
		{!! svgRawContent( public_path( crearRuta('img', 'iconos', 'contratos', 'contrato-2.svg') ) ) !!}
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- <h2>REPOSITORIO CPE</h2> -->
			<p class="leyenda-inicio">Aquí podrá realizar el seguimiento a los <b>Contratos</b> que esten bajo <b>Su supervisión</b>.</p>
		</div>
	</div>
</div>
<style>
	.leyenda-inicio{
		margin-top: 50px;
		text-align: center;
		font-size: 20px;
	}
</style>
@endsection