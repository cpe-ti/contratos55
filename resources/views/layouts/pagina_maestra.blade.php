@if(!Request::ajax())
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="minimal-ui, width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="Seguimiento a Contratos CPE">
	<meta name="author" content="Cristian David Home">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge"/>

	<meta name="csrf-token" content="{{ csrf_token() }}">


	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />

	<meta name="apple-mobile-web-app-capable" content="yes">  
	<meta name="mobile-web-app-capable" content="yes">  
	<meta name="apple-mobile-web-app-title" content="Repositorio CPE">  
	<meta name="apple-mobile-web-app-status-bar-style" content="black-transparent">  
	<title>@yield('titulo')</title>
	<!-- Estilo inicial animacion y carga de pagina -->
	<style>
		html{
			-ms-content-zooming: none; /* Disables zooming */
    		touch-action: none; /* Disable any special actions on tap/touch */
		}
		body{
		    -webkit-touch-callout: none;
		    /* user-select: none; */
		}
		html, body{
			overflow: hidden;
			height: 100%;
			width: 100%;
			/* position: fixed; */
		}
		#overlay_cargando{
			background-color: #393939;
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			z-index: 1999;
		}
		.spin{
			width: 50px;
			height: 50px;
			border-style: solid;
			border-color: rgba(255, 255, 255, 0);
			border-top-color: #FFFFFF;
			border-bottom-color: #FFFFFF;
			border-radius: 100%;
			position: absolute;
			top: 45px;
			left: -55px;
			will-change: transform;
		}
		#overlay_cargando > .spin{
			width: 100px;
			height: 100px;
			border-style: solid;
			border-color: rgba(255, 255, 255, 0);
			border-top-color: #00B4FF;
			border-bottom-color: #00B4FF;
			border-radius: 100%;
			position: absolute;
			top: calc(50% - 50px);
			left: calc(50% - 50px);
			-webkit-animation: rotar 1s linear infinite;
			-moz-animation: rotar 1s linear infinite;
			-ms-animation: rotar 1s linear infinite;
			-o-animation: rotar 1s linear infinite;
			animation: rotar 1s linear infinite;            
		}
		@-webkit-keyframes rotar {
			from { -webkit-transform: rotate(0deg); }
			to { -webkit-transform: rotate(360deg); }
		}
		@-o-keyframes rotar {
			from { -o-transform: rotate(0deg); }
			to { -o-transform: rotate(360deg); }
		}
		@-moz-keyframes rotar {
			from { -moz-transform: rotate(0deg); }
			to { -moz-transform: rotate(360deg); }
		}
		@keyframes rotar {
			from { transform: rotate(0deg); }
			to { transform: rotate(360deg); }
		}
	</style>
	@if(!Auth::check())
	<script>
		// your "Imaginary javascript"
		//window.location.href = '{{url("login")}}';
		// or
		//window.location.href = '{{route("login")}}'; //using a named route
	</script>
	@endif
	<script>
		var url_aplicacion = '{{Request::root()}}';
	</script>
	<script src="{{ URL::asset('js/inicializar.js') }}"></script>
</head>
<body>
	<noscript>
	  	<style>#overlay_cargando { display:none !important; }</style>
	</noscript>
	<div id="overlay_cargando">
		<div class="spin"></div>
	</div>
	<div id="overlay-menu"></div>
	<div id="menu-lateral">	
		<div id="container-menu">
			@include('partials.menu')
		</div>
	</div>
	@include('partials.barra')
	<div id="contenedor-principal">
		<div id="contenedor-pagina" class="container-fluid col-no-padding scrollable">
			@yield('contenido')
		</div>
		<div id="indicador_cargando">
			<div class="spin"></div>
		</div>
	</div>
	<!-- CSS
	================================================== -->	
	{{--  <link rel="stylesheet" href="{{ URL::asset('librerias/viejo/animate.css/animate.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('librerias/viejo/select2/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('librerias/viejo/select2/css/select2-bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('librerias/viejo/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('librerias/viejo/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('librerias/viejo/bootstrap-fileinput/css/fileinput.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('librerias/viejo/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('librerias/viejo/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css') }}">  --}}
	
	
	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	{{--  <link rel="stylesheet" href="{{ URL::asset('css/pagina_maestra.css') }}">  --}}

    <!-- JAVASCRIPT
    ================================================== -->
	{{--  <script src="{{ URL::asset('librerias/moment/moment.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/jquery/jquery-2.2.4.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/smooth-scroll/jquery.smooth-scroll.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/bootstrap/js/bootstrap.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/bootstrap-fileinput/js/fileinput.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/bootstrap-fileinput/js/locales/es.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/bootbox/bootbox.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/shuffle/shuffle.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/jquery-matchHeight/jquery.matchHeight-min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/bootstrap-tagsinput/bootstrap-typeahead.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/D3/d3.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/handlebars/handlebars.min-latest.js') }}"></script>  --}}
    
    
    {{--  <script src="{{ URL::asset('librerias/inputmask/jquery.inputmask.bundle.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/select2/js/select2.full.min.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/select2/js/i18n/es.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/jquery-easing/jquery.easing.1.3.js') }}"></script>  --}}
    {{--  <script src="{{ URL::asset('librerias/jquery.scrollTo-2.1.2/jquery.scrollTo.min.js') }}"></script>  --}}

	{{--  <script src="{{ URL::asset('librerias/chartjs/chart.min.js') }}"></script>  --}}

	{{--  <script src="{{ URL::asset('librerias/vue/vue.js') }}"></script>  --}}

    {{--  <script src="{{ URL::asset('js/script.js') }}"></script>  --}}
    <script src="{{ URL::asset('js/app.js') }}"></script>

	{{--  <script src="{{ URL::asset('librerias/tinymce/tinymce.min.js') }}"></script>  --}}
	{{--  <script src="{{ URL::asset('librerias/tinymce/jquery.tinymce.min.js') }}"></script>  --}}
	{{-- <script src="{{ URL::asset('librerias/handlebars/handlebars.min.js') }}"></script> --}}
</body>
</html>
@else
@yield('contenido')
<div id="titulo_ajax" data-titulo="@yield('titulo')" style="display:none"></div>
<script>
	//Si no existe el contenedor-pagina no se cargo la pagina maestra, recargar la pagina
	if (!document.getElementById("contenedor-pagina")) {
		location.reload();
	}
</script>
@endif