<div id="barra-nombre-usuario">{{ Auth::check() ? Auth::user()->NombreCompleto() : 'Invitado' }}</div>
<div id="barra-img-usuario">
	<i class="fa fa-user-o" aria-hidden="true"></i>
</div>
<div id="barra-info-usuario">
	<div id="info-img-usuario">
		<i class="fa fa-user-o" aria-hidden="true"></i>
	</div>
	<div id="info-nombre-usuario">{{ Auth::check() ? Auth::user()->NombreCompleto() : 'Invitado' }}</div>
	@if(Auth::check())
		@if(count(Auth::user()->roles) > 0)
		<div id="info-rol-usuario">{{ count(Auth::user()->roles) > 1 ? 'Varios Roles' : Auth::user()->roles[0]->display_name }}</div>
		@else
		<div id="info-rol-usuario">No tiene rol</div>
		@endif
	@else
		<div id="info-rol-usuario">Sesión no iniciada</div>
	@endif
	<div id="info-mail-usuario">{{Auth::check() ? Auth::user()->email : ''}}</div>
	<hr>
	<div class="btn-group btn-group-justified" role="group" aria-label="...">
		@if(Auth::check())
			<a href="#" class="btn btn-danger" onclick="event.preventDefault();
				refreshCpeToken(function(){
					console.log($('#token_salir').val());
					document.getElementById('logout-form').submit();					
				});
				">
				<i class="fa fa-sign-out" aria-hidden="true"></i>
				Cerrar Sesión
			</a>
			<a href="{{ url('mi-perfil') }}" class="link_dinamico btn btn-primary">
				<i class="fa fa-cog" aria-hidden="true"></i>
				Opciones
			</a>	
		@else
			<a href="{{ url('login') }}" class="btn btn-primary link_dinamico">
				<i class="fa fa-sign-in" aria-hidden="true"></i> Iniciar Sesión
			</a>
			<a href="{{ url('register') }}" class="btn btn-default link_dinamico">
				<i class="fa fa-user-plus" aria-hidden="true"></i> Registrarse
			</a>
		@endif
	</div>
</div>