<div id="barra">
	<div id="boton_atras"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>
	<div id="titulo-app">@yield('titulo')</div>
	<div id="boton_menu"><i class="fa fa-bars" aria-hidden="true"></i></div>
	<div id="barra-seccion-usuario">
		@include('partials.barra-seccion-usuario')	
	</div>
</div>