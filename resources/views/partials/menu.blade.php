<div id="menu-seccion-usuario">
	<div id="img-perfil-usuario">
		<i class="fa fa-user-o" aria-hidden="true"></i>
	</div>
	@if(Auth::check())
	<span id="menu-usuario-nombre">{{ Auth::check() ? Auth::user()->NombreCompleto() : 'Invitado' }}</span>
	@if(count(Auth::user()->roles) > 0)
	<span id="menu-usuario-rol">{{ count(Auth::user()->roles) > 1 ? 'Varios Roles' : Auth::user()->roles[0]->display_name }}</span>
	@else
	<span id="menu-usuario-rol">No tiene un rol asignado</span>
	@endif
	@else
	<span id="menu-usuario-nombre">Invitado</span>
	<span id="menu-usuario-rol">Sesión no iniciada</span>
	@endif
</div>
<div id="menu-opciones-usuario">
	<a href="{{ url('mi-perfil') }}" class="link_dinamico opcion-usuario">
		<i class="fa fa-cog" aria-hidden="true"></i>
		<span class="opcion-leyenda">Opciones</span>
	</a>
	<a href="#" class="opcion-usuario rojo-opc" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
		<i class="fa fa-sign-out" aria-hidden="true"></i>
		<span class="opcion-leyenda">Salir</span>
	</a>
    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        <input id="token_salir" type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
</div>
<div id="menu-seccion-principal">
	<div id="menu-btn-opciones" class="{{ Auth::check() ? 'mostrar-boton-opc-menu' : '' }}">
		<i class="fa fa-chevron-down" aria-hidden="true"></i>
	</div>
	<div id="contenido-menu">
		<ul>
			@if(Auth::check())
				@if(true)
					<li class="separador"></li>
					<li class="{{ menu_activo('/') }}">
						<a class="link_dinamico" href="{{ url('') }}">
							<div><i class="fa fa-home" aria-hidden="true"></i> Inicio</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('administrar') }}">
						<a class="link_dinamico" href="{{ url('administrar') }}">
							<div><i class="fa fa-key" aria-hidden="true"></i> Administración</div>
						</a>
					</li>
					{{-- <li class="separador"></li> --}}
					{{-- <li class="{{ menu_activo('presupuesto') }}"> --}}
						{{-- <a class="link_dinamico" href="{{ url('presupuesto') }}"> --}}
							{{-- <div><i class="fa fa-money" aria-hidden="true"></i> Presupuesto</div> --}}
						{{-- </a> --}}
					{{-- </li> --}}
					<li class="separador"></li>
					<li class="{{ menu_activo('gaf') }}">
						<a class="link_dinamico" href="{{ url('gaf') }}">
							<div><i class="fa fa-briefcase" aria-hidden="true"></i> A&F</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('contratacion') }}">
						<a class="link_dinamico" href="{{ url('contratacion') }}">
							<div><i class="fa fa-university" aria-hidden="true"></i> Contratación</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('supervision') }}">
						<a class="link_dinamico" href="{{ url('supervision') }}">
							<div><i class="fa fa-eye" aria-hidden="true"></i> Supervisión</div>
						</a>
					</li>
					
					

					{{-- <li class="{{ menu_activo('/') }}">
						<a class="link_dinamico" href="{{ url('') }}">
							<div><i class="fa fa-home" aria-hidden="true"></i> Inicio</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('administrar') }}">
						<a class="link_dinamico" href="{{ url('administrar') }}">
							<div><i class="fa fa-key" aria-hidden="true"></i> Administración</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('contratos') }}">
						<a class="link_dinamico" href="{{ url('contratos') }}">
							<div><i class="fa fa-file-text-o" aria-hidden="true"></i> Contratos</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('terceros') }}">
						<a class="link_dinamico" href="{{ url('terceros') }}">
							<div><i class="fa fa-users" aria-hidden="true"></i> Terceros</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('provisiones') }}">
						<a class="link_dinamico" href="{{ url('provisiones') }}">
							<div><i class="fa fa-product-hunt" aria-hidden="true"></i> Provisiones</div>
						</a>
					</li>
					<li class="separador"></li>
					<li class="{{ menu_activo('pdc') }}">
						<a class="link_dinamico" href="{{ url('pdc') }}">
							<div><i class="fa fa-usd" aria-hidden="true"></i> Plan de compras</div>
						</a>
					</li> --}}
					<li class="separador"></li>
				@else
					<div class="mensaje-menu">Aun no tiene un Rol asignado por lo cual no podra realizar niguna acción por ahora, por favor contacte al administrador(a) del sistema</div>
				@endif
			@else
				<li class="menu_user_login">
					<a class="link_dinamico" href="{{ url('login') }}">
						<div><i class="fa fa-sign-in" aria-hidden="true"></i> Iniciar Sesión</div>
					</a>
				</li>
				<li class="menu_user_sign">
					<a class="link_dinamico" href="{{ url('register') }}">
						<div><i class="fa fa-user-plus" aria-hidden="true"></i> Registrarse</div>
					</a>
				</li>
				<div id="menu-candadito">
					<i class="fa fa-lock" aria-hidden="true"></i>
				</div>
			@endif
		</ul>
	</div>
</div>
@php
	function menu_activo($menu){
		return (0 === strpos(Request::path(), $menu) ? 'menu-activo' : '');
	}
@endphp