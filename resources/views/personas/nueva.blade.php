@extends('layouts.pagina_maestra')
@section('titulo')
Nueva Persona
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<br/>
	<form id="form-nuevo-participante" action="{{ url('administrar/participantes/nuevo') }}" method="POST" autocomplete="off">
		{!!csrf_field()!!}
		<div class="form-group col-md-12{{ $errors->has('persona_id') ? ' has-error' : '' }}">
			<label class="control-label" for="persona_id">Nº de Documento de Identidad:</label>
			<input type="number" name="persona_id" class="form-control" id="persona_id" placeholder="Identificación" required="required"/>
			<span class="help-block">
                <strong id="persona_id_error">{{ $errors->first('persona_id') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-6">
			<label class="control-label" for="primer_nombre">Primer Nombre:</label>
			<input type="text" name="primer_nombre" class="form-control" id="primer_nombre" placeholder="Primer Nombre" required="required"/>
			<span class="help-block">
                <strong id="primer_nombre_error">{{ $errors->first('primer_nombre') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-6">
			<label class="control-label" for="segundo_nombre">Segundo Nombre:</label>
			<input type="text" name="segundo_nombre" class="form-control" id="segundo_nombre" placeholder="Segundo Nombre"/>
			<span class="help-block">
                <strong id="segundo_nombre_error">{{ $errors->first('segundo_nombre') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-6">
			<label class="control-label" for="primer_apellido">Primer Apellido:</label>
			<input type="text" name="primer_apellido" class="form-control" id="primer_apellido" placeholder="Primer Apellido" required="required"/>
			<span class="help-block">
                <strong id="primer_apellido_error">{{ $errors->first('primer_apellido') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-6">
			<label class="control-label" for="segundo_apellido">Segundo Apellido:</label>
			<input type="text" name="segundo_apellido" class="form-control" id="segundo_apellido" placeholder="Segundo Apellido"/>
			<span class="help-block">
                <strong id="segundo_apellido_error">{{ $errors->first('segundo_apellido') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-4">
			<label class="control-label" for="telefono">Telefono:</label>
			<input type="text" name="telefono" class="form-control" id="telefono" placeholder="xxx-xxxx o xxx-xxx-xxxx"/>
			<span class="help-block">
                <strong id="telefono_error">{{ $errors->first('telefono') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-4">
			<label class="control-label" for="email">Correo:</label>
			<input type="text" name="email" class="form-control" id="email" placeholder="alguien@cpe.gov.co"/>
			<span class="help-block">
                <strong id="email_error">{{ $errors->first('email') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-4">
			<label class="control-label" for="sexo_id">Sexo:</label>
			<select name="sexo_id" class="form-control" id="sexo_id" required="required">
				<option value="">Seleccionar..</option>
				@foreach ($sexo as $sex)
					<option value="{{$sex->id}}">{{$sex->sexo}}</option>
				@endforeach
			</select>
			<span class="help-block">
                <strong id="sexo_id_error">{{ $errors->first('sexo_id') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-12">
			<label class="control-label" for="rol_predeterminado">Rol Predeterminado:</label><br>
			@foreach ($roles as $rol)
				<div class="radio" id="rol_predeterminado">
					<label>
						<input type="radio" name="rol_predeterminado" value="{{$rol->id}}">
						<strong>{{$rol->display_name}}</strong>						
					</label>
				</div>
			@endforeach
			<span class="help-block">
                <strong id="rol_predeterminado_error">{{ $errors->first('rol_predeterminado') }}</strong>
            </span>
		</div>
		<div class="form-group form-error">
            <span class="help-block">
                <strong class="form-error-msg"></strong>
            </span>
        </div>
		<div class="form-group col-xs-12">			
			<button type="submit" id="boton-crear-participante" class="btn btn-primary pull-right">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>
				<span>&nbsp;&nbsp;Crear nuevo participante</span>
			</button>			
		</div>
	</form>
</div>
<style>
	.form-group:last-child{
		padding-bottom: 50px !important;
	}
	#form-nuevo-participante textarea { 
		resize:vertical;
	}
	#form-nuevo-participante button[type=submit]{
		font-size: 20px !important;
	}
	#form-nuevo-participante button[type=submit]>span{
		font-size: 16px !important;
	}
	.modal_lista_terceros .bootbox-body{
		
	}
</style>
<script type="text/javascript">
	inicializar (function () {
		$('#form-nuevo-participante').on('submit', function(e) {
    	    e.preventDefault();
    	    e.stopPropagation();
    	    e.isDefaultPrevented = function () {
    	        return false;
    	    }
    	    var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			//var jform = $(this);
			//$(':input', jform).attr("disabled", true);
			$(".help-block").addClass('help-block-visible');
			$('.form-group').removeClass("has-error").find('.help-block>strong').html('');
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				$('#boton-crear-participante').blur();
				console.log(respuesta);
				//$("#logs").html(respuesta.data.responseText);
				if (respuesta.data.responseText) {
					var obj;
					try{
    				    obj = jQuery.parseJSON(respuesta.data.responseText);
    				}catch(e){
    				    console.log(e);
    				    obj = {fallido:"Ocurrio un error inesperado:"+e+"<br>"+respuesta.data.responseText};
    				}
                    if (obj.persona_id) {
                        $("#persona_id").parent('.form-group').addClass("has-error");
                        $('#persona_id_error').html(obj.persona_id);
                    }
                    if (obj.primer_nombre) {
                        $("#primer_nombre").parent('.form-group').addClass("has-error");
                        $('#primer_nombre_error').html(obj.primer_nombre);
                    }
                    if (obj.segundo_nombre) {
                        $("#segundo_nombre").parent('.form-group').addClass("has-error");
                        $('#segundo_nombre_error').html(obj.segundo_nombre);
                    }
                    if (obj.primer_apellido) {
                        $("#primer_apellido").parent('.form-group').addClass("has-error");
                        $('#primer_apellido_error').html(obj.primer_apellido);
                    }
                    if (obj.segundo_apellido) {
                        $("#segundo_apellido").parent('.form-group').addClass("has-error");
                        $('#segundo_apellido_error').html(obj.segundo_apellido);
                    }
                    if (obj.sexo_id) {
                        $("#sexo_id").parent('.form-group').addClass("has-error");
                        $('#sexo_id_error').html(obj.sexo_id);
                    }
                    if (obj.telefono) {
                        $("#telefono").parent('.form-group').addClass("has-error");
                        $('#telefono_error').html(obj.telefono);
                    }
                    if (obj.rol_predeterminado) {
                        $("#rol_predeterminado").parent('.form-group').addClass("has-error");
                        $('#rol_predeterminado_error').html(obj.rol_predeterminado);
                    }
                    if (obj.fallido) {
                        $(".form-error").addClass("has-error");
                        $('.form-error-msg').html(obj.fallido);
                    }
                    if (obj.error) {
                        $(".form-error").addClass("has-error");
                        $('.form-error-msg').html(obj.error);
                    }                    					
				}else if(respuesta.data.success) {
					bootbox.dialog({
						title: "Correcto",
						message: respuesta.data.mensaje,
						//message: "<pre>"+JSON.stringify(respuesta.data, null, 4)+"</pre>",
						onEscape: function(){
							//$("#contenedor-pagina").css('overflow-y', 'auto');
							//$(':input', jform).attr("disabled", false);
						},
						buttons: {
							success: {
								label: "Ok",
								className: "btn-success",
								callback: function () {
									
								}
							}
						}
					});
                    //alert(respuesta.data.mensaje);
                }
			});
    	});
	});		
</script>
@endsection