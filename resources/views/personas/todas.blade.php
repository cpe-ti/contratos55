@extends('layouts.pagina_maestra')
@section('titulo')
Participantes
@endsection
@section('contenido')
<div id="contenido_lista_contratos" class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	{{-- {{ print_r($personas)}} --}}
	<div class="clearfix">
		<div class="input-group col-xs-12">
    	  	<div class="input-group">
				<input type="text" name="filter_busqueda_persona" id="filter_busqueda_persona" class="form-control" placeholder="Buscar"/>			
				<span class = "input-group-addon">
					<i class="fa fa-search" aria-hidden="true"></i>
				</span>
			</div>
    	  	<span class="input-group-btn">
    	  	  	<a class="boton-nuevo-participante btn btn-primary link_dinamico" href="{{ Request::url().'/nuevo' }}"></a>
    	  	</span>
    	</div>
	</div>
	<hr>
	<div class="row" id="row_personas">
		@foreach ($personas as $key => $persona)
		<div class="col-sm-6 col-md-4 col-participante{{$loop->first ? ' ch_sizer' : ''}}" data-nombre="{{$persona->NombreCompleto()}}" data-id="{{$persona->id}}">
			<a href="{{ Request::url().'/'.$persona->id }}" class="tarjeta-participante link_dinamico">
				<div class="img">
					{{-- <i class="fa fa-id-badge" aria-hidden="true"></i> --}}
					{{-- <i class="fa fa-address-book-o" aria-hidden="true"></i> --}}
					{{-- <i class="fa fa-address-book" aria-hidden="true"></i> --}}
					{{-- <i class="fa fa-address-card-o" aria-hidden="true"></i> --}}
					{{-- <i class="fa fa-address-card" aria-hidden="true"></i> --}}
					{{-- <i class="fa fa-user-circle" aria-hidden="true"></i> --}}
					<i class="fa fa-user-circle-o" aria-hidden="true"></i>
					{{-- <i class="fa fa-user-o" aria-hidden="true"></i> --}}
					{{-- <i class="fa fa-user" aria-hidden="true"></i> --}}
				</div>
				<div class="detalles">
					<span class="nombre">{{ $persona->NombreCompleto() }}</span>
					<span class="id text-right">{{ $persona->id }}</span>
					@if($persona->user)
						<span class="usuario text-right"><i class="fa fa-user-o" aria-hidden="true"></i>: {{ $persona->user->email }}</span>
						<span class="rol text-right"><i class="fa fa-key" aria-hidden="true"></i>: {{ (count($persona->user->roles)>0 ? $persona->user->roles[0]->display_name : "No tiene Roles" ) }}</span>
					@else
						<span class="usuario text-right">No tiene usuario</span>
						<span class="rol text-right">Tampoco Roles</span>
					@endif
				</div>
			</a>
		</div>
		{{-- <div class="clearfix visible-md-6 visible-sm-6"></div> --}}
		@endforeach	
	</div>
</div>
<style>
	.col-participante{
		margin-top: 10px;
		margin-bottom: 10px;
		width: 100%;
	}
	.tarjeta-participante{
		/* height: 150px; */
		background-color: #F8F8F8;
		border-radius: 5px;
		box-shadow: 0px 3px 8px -3px #6F6F6F;
		transition: box-shadow 150ms linear;
		display: flex;
		flex-direction: row;
	}
	.tarjeta-participante:link, .tarjeta-participante:active, .tarjeta-participante:visited{
		text-decoration: none;
		color: initial;
	}
	.tarjeta-participante:hover{
		box-shadow: 0px 4px 10px -0px #6F6F6F;		
	}
	.tarjeta-participante>.img{
		background-color: #66a1b7;
		width: 60px;
		flex-grow: 0;
		flex-shrink: 0; 
		display: flex;
		flex-direction: column;
		justify-content: center;
		border-radius: 5px 0px 0px 5px;
	}
	.tarjeta-participante>.img>i{
		/* width: 50px; */
		/* height: 50px; */
		/* background-color: #B2B2B2; */
		/* border-radius: 50px; */
		color: #507f90;
		align-self: center;
		font-size: 50px;
		text-shadow: 0px 0.5px 0.5px #b2c6ce;
	}
	.tarjeta-participante>.detalles{
		/* background-color: #404040; */
		flex-grow: 1;
		flex-shrink: 1;
		padding: 10px;
		display: flex;
		flex-direction: column;
		overflow: hidden;
		
	}
	.tarjeta-participante>.detalles>span{
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
		font-size: 12px;
		line-height: 200%;
	}
	.tarjeta-participante>.detalles>span.nombre{
		font-size: 16px;
	}
	.boton-nuevo-participante:before {
	   	content: "Crear Nuevo";
	}
	@media (max-width : 479px) {
		.boton-nuevo-participante:before {
		   	content: "\f055";
		   	font-family: FontAwesome;
    		font-style: normal;
    		font-weight: normal;
    		text-decoration: inherit;
		}
    }
    @media (min-width : 480px) and (max-width: 767px) {
		
	}
	@media (min-width:768px) and (max-width: 991px) {
		.col-participante:nth-child(2n+1){
    		clear: left;
		}
	}
	@media (min-width: 992px) and (max-width: 1199px) {
		.col-participante:nth-child(3n+1){
    		clear: left;
		}
	}
	@media (min-width: 1200px) {
		.col-participante:nth-child(3n+1){
    		clear: left;
		}
	}
</style>
<script>
	inicializar (function () {
		'use strict';
		var Shuffle = window.shuffle;
		var personas = new Shuffle(document.getElementById('row_personas'), {
			itemSelector: '.col-participante',
			sizer: '.ch_sizer',
			speed: 500,
			useTransforms: true
		});
		function filtrar_personas() {
			var texto;
			texto = $('#filter_busqueda_persona').val().toLowerCase();
			personas.filter(function(element){
				var nombre = (($(element).data('nombre').toLowerCase()).indexOf(texto) >= 0);
				var id = (($(element).data('id').toString().toLowerCase()).indexOf(texto) >= 0);
				return (nombre || id);
			});
		}
		$('#filter_busqueda_persona').onDelayed('input', {{ count($personas) }}, function() {
			filtrar_personas();			
		});
	});
</script>
@endsection