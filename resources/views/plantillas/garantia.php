<script id="template-garantia" type="text/x-handlebars-template">
	<div class="row garantia">
		<div class="form-group col-sm-2 col-xs-12">
			<label for="">Amparo:</label>
			<input type="text" class="form-control" name="polizas[][garantia][][amparo]]" placeholder="(Garantía)" />
		</div>
		<div class="form-group col-sm-3 col-xs-12">
			<label for="">Valor:</label>
			<input type="text" class="form-control valor-amparo" name="polizas[][garantia][][valor_amparo]]" data-inputmask="'alias': 'currency', 'autoUnmask': true" placeholder="Valor" />
		</div>
		<div class="form-group col-sm-3 col-xs-6">
			<label for="">Fecha inicio:</label>
			<input type="date" class="form-control" name="polizas[][garantia][][fecha_inicio]]" placeholder="Fecha de inicio" />
		</div>
		<div class="form-group col-sm-3 col-xs-6">
			<label for="">Fecha terminación:</label>
			<input type="date" class="form-control" name="polizas[][garantia][][fecha_fin]]" placeholder="Fecha de terminación" />
		</div>
		<div class="form-group col-sm-1 col-xs-12">
			<label for="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<div class="btn btn-danger close-amparo pull-right">
				<i class="fa fa-trash-o" aria-hidden="true"></i>
			</div>
		</div>
	</div>
</script>