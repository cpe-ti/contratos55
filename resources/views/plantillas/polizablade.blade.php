<div class="form-group col-md-12 poliza">
	<div class="panel panel-default panel-poliza">
		<div class="panel-heading">
			<label for="">Póliza</label>
			<div class="row">
				<div class="form-group col-sm-5 col-xs-12">
					<label for="">Aseguradora:</label>
					<input type="text" class="form-control" name="polizas[][aseguradora]" placeholder="Aseguradora" />
				</div>
				<div class="form-group col-sm-3 col-xs-6">
					<label for="">Nº Póliza:</label>
					<input type="number" class="form-control" name="polizas[][numero_poliza]" placeholder="Número Póliza" />
				</div>
				<div class="form-group col-sm-4 col-xs-6">
					<label for="">Fecha Expedición:</label>
					<input type="date" class="form-control" name="polizas[][fecha_expedicion]" placeholder="Fecha de Expedición" />
				</div>
			</div>
		</div>
		<div class="panel-body garantias">
			<div class="row garantia">
				<div class="form-group col-sm-2 col-xs-12">
					<label for="">Amparo:</label>
					<input type="text" class="form-control" name="polizas[][garantia][0][amparo]" placeholder="(Garantía)" />
				</div>
				<div class="form-group col-sm-3 col-xs-12">
					<label for="">Valor:</label>
					<input type="text" class="form-control valor-amparo" name="polizas[][garantia][0][valor_amparo]" data-inputmask="'alias': 'currency', 'autoUnmask': true" placeholder="Valor" />
				</div>
				<div class="form-group col-sm-3 col-xs-6">
					<label for="">Fecha inicio:</label>
					<input type="date" class="form-control" name="polizas[][garantia][0][fecha_inicio]" placeholder="Fecha de inicio" />
				</div>
				<div class="form-group col-sm-3 col-xs-6">
					<label for="">Fecha terminación:</label>
					<input type="date" class="form-control" name="polizas[][garantia][0][fecha_fin]" placeholder="Fecha de terminación" />
				</div>
				<div class="form-group col-sm-1 col-xs-12">
					<label for="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<div class="btn btn-danger pull-right disabled">
						<i class="fa fa-trash-o" aria-hidden="true"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-12">
					<div class="btn btn-default agregar-garantia pull-right">
						<i class="fa fa-plus" aria-hidden="true"></i> Agregar Garantía
					</div>
				</div>
			</div>
		</div>
		<div class="close-poliza">
			<i class="fa fa-times" aria-hidden="true"></i>
		</div>					
	</div>
</div>