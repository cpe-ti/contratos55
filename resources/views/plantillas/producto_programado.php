<script id="template-producto-programado" type="text/x-handlebars-template">
	<div class="col-producto col-md-3" data-producto-id="{{this.id}}" data-producto-cant="{{this.cantidad_seleccionada}}">
		<div class="tarjeta-producto">
			<div class="close-pdto">
				<i class="fa fa-times-circle" aria-hidden="true"></i>
			</div>
			<div class="text-left">
				<span class="label-producto">{{this.nombre}}</span>
			</div>
			<hr class="hr-card-pr">
			<div class="text-right">
				<span class="label-cantidad">{{this.cantidad_seleccionada}}{{this.unidad_string}}</span>
			</div>
		</div>
	</div>
	<!-- <i class="fa fa-list-ol" aria-hidden="true"></i> -->
	<!-- <i class="fa fa-percent" aria-hidden="true"></i> -->
</script>