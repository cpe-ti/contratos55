<script id="template-clausula-contrato" type="text/x-handlebars-template">
	<div class="panel panel-default panel-clausula">
		<div class="panel-heading">
			<h3 class="panel-title titulo-panel-clausula"><strong>CLÁUSULA <span class="ordinal-panel-clausula">{{this.ordinal}}</span></strong></h3>
			<div class="close-button-clausula">
				<i class="fa fa-times-circle" aria-hidden="true"></i>
			</div>
		</div>
		<div class="panel-body" style="padding: 0;">
			<div class="form-group col-md-12" style="padding: 0; margin: 0;">
				<textarea class="form-control editable_text_area" name="clausula[{{this.numero}}]" id="clausula-{{this.id}}" cols="30" rows="10">
					<p>
						<strong>CLÁUSULA {{this.ordinal}} - {{this.nombre}}:</strong>&nbsp;
					</p>
				</textarea>
			</div>
		</div>
	</div>
</script>