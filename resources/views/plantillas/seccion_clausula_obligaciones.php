<script id="template-clausula-obligaciones" type="text/x-handlebars-template">
	<div class="panel panel-default panel-clausula panel-clausula-obligaciones" id="clausula_obligaciones">
		<div class="panel-heading">
			<h3 class="panel-title titulo-panel-clausula"><strong>CLÁUSULA <span class="ordinal-panel-clausula">{{this.ordinal}}</span>: OBLIGACIONES DEL CONTRATISTA</strong></h3>
			<div class="close-button-clausula">
				<i class="fa fa-times-circle" aria-hidden="true"></i>
			</div>
		</div>
		<div class="panel-body">
			<p style="text-align: justify; font-size: 10pt; font-family: 'Helvetica';"><span>EL <strong>CONTRATISTA</strong>, se obliga a cumplir con el objeto del presente contrato, ejecutando, entre otras, las siguientes actividades, tal y como consta en la propuesta presentada por el <strong>CONTRATISTA</strong> y aceptada por el <strong>CONTRATANTE</strong>, la cual hace parte integrante de presente contrato:</span>
			<h5><strong>A) OBLIGACIONES ESPECÍFICAS:</strong></h5>
			<input type="hidden" name="clausula[{{this.numero}}][obligaciones]" id="clausula-{{this.id}}" value="CLÁUSULA {{this.ordinal}}">
			<div id="seccion-obligaciones-espf">
				
			</div>
			<div class="form-group col-md-12">
				<div class="input-group">
				    <textarea style="resize: vertical;" id="textarea_obligacion_spcf" class="form-control" cols="30" placeholder="Obligación"></textarea>
				    <span class="input-group-addon btn btn-primary btn-add-obl" id="boton_new_ob_espfca">
				    	<i class="fa fa-plus" aria-hidden="true"></i>
				    </span>
				</div>
			</div>
			<hr>
			<h5><strong>B) OBLIGACIONES GENERALES:</strong></h5>
			<div id="seccion-obligaciones-gnrl">
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							1)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Prestar el servicio y/o hacer entrega del bien dentro del término, plazo y en los sitios previstos en el contrato . </textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							2)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Reemplazar cualquier empleado, supervisor o coordinador asignado, por solicitud escrita de Computadores para Educar, en forma inmediata.</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							3)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Dar cumplimiento a sus obligaciones con los sistemas de salud, riesgos profesionales, pensiones, y aportes parafiscales, de conformidad con lo establecido en las leyes 789 de 2002 y 828 de 2003. </textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							4)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Dar estricto cumplimiento a las cláusulas pactadas en el contrato  y lo consignado en su propuesta.  </textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							5)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Cumplir con las condiciones técnicas, económicas y comerciales presentadas en su propuesta. </textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							6)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Acatar la Constitución, la Ley y demás normas pertinentes </textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							7)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Atender oportunamente los requerimientos del supervisor del contrato generado por este proceso.</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							8)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Reportar, en caso de cualquier novedad o anomalía, de manera inmediata tal situación al Supervisor del contrato.</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							9)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Informar por escrito al supervisor del contrato generado por este proceso sobre los obstáculos, problemas o sugerencias que se encuentre en el desarrollo del presente contrato </textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							10)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Obrar con lealtad y buena fe en las distintas etapas contractuales, evitando dilaciones y entrabamientos que puedan presentarse. </textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							11)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Salvaguardar la  información confidencial que obtenga o conozca  en el desarrollo de sus actividades, salvo requerimiento  expreso de autoridad competente</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							12)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Toda la información o documentos que se produzcan en desarrollo del  proceso contractual, serán de uso exclusivo de COMPUTADORES PARA EDUCAR, obligándose desde ya el contratista a no utilizarlos para fines distintos a los previstos en el contrato, ni a divulgar la información que se le suministre, ni los resultados de su trabajo, conservando la confidencialidad de los mismos de conformidad  con la ley, so pena de las acciones civiles, administrativas o penales a que haya lugar.</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							13)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">No acceder a peticiones o amenazas de quienes actúan por fuera de la Ley, con el fin de obligarlos a hacer u omitir algún acto o hecho, debiendo informar inmediatamente a COMPUTADORES PARA EDUCAR, a través del responsable del control de ejecución acerca de la ocurrencia de tales peticiones o amenazas, y a las demás autoridades competentes para que se adopten las medidas y correctivos que fueren necesarios</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							14)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Dar cumplimiento a las obligaciones establecidas en el protocolo de comunicaciones de Computadores Para Educar, documento el cual se anexa y hace parte integra del contrato.</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
				<div class="form-group col-md-12 obligacion-group">
					<div class="input-group">
						<span class="input-group-addon btn btn-primary numero-obligacion">
							15)
						</span>
						<textarea style="resize: vertical;" name="obligaciones[obligacion_gnrl][]" class="form-control texto-obligacion" cols="30" readonly="true">Todas las demás que se deriven de la naturaleza del presente proceso, aunque no estén especificadas y que se requieran para el cabal y correcto cumplimiento del objeto del mismo.</textarea>
						<span class="input-group-addon btn btn-danger close-button-obl">
							<i class="fa fa-times-circle" aria-hidden="true"></i>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group col-md-12">
				<div class="input-group">
				    <textarea style="resize: vertical;" id="textarea_obligacion_gnrl" class="form-control" cols="30" placeholder="Obligación"></textarea>
				    <span class="input-group-addon btn btn-primary btn-add-obl" id="boton_new_ob_gnrl">
				    	<i class="fa fa-plus" aria-hidden="true"></i>
				    </span>
				</div>
			</div>
		</div>
	</div>
</script>