<script id="template-obligacion" type="text/x-handlebars-template">
	<div class="form-group col-md-12 obligacion-group">
		<div class="input-group">
			<span class="input-group-addon btn btn-primary numero-obligacion">
		    	{{this.numero}})
		    </span>
			<textarea style="resize: vertical;" name="{{this.inputname}}" class="form-control texto-obligacion" cols="30" readonly="true">{{this.texto}}</textarea>
			<span class="input-group-addon btn btn-danger close-button-obl">
		    	<i class="fa fa-times-circle" aria-hidden="true"></i>
		    </span>
		</div>
	</div>
</script>