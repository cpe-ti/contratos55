<script id="template-seccion-pago" type="text/x-handlebars-template">
	<div class="panel panel-default panel-pago animated fadeIn">
		<button class="boton-removethis-fields">
			<i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
		</button>
		<div class="panel-heading">
			<div class="row">				
				<div class="col-md-3">
					<h3 class="titulo-panel numero-pago">Pago X</h3>
				</div>
				<div class="form-group col-md-9">
					<label class="control-label" for="unidad">Desembolso:</label>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-dollar"></i>
						</span>
						<input type="text" name="pago[0][desembolso]" class="form-control desembolso-pago" data-inputmask="'alias': 'currency', 'autoUnmask': true" value="0" placeholder="Desembolso" autofocus="true">
					</div>
					<span class="help-block">
						<strong class="unidad_error"></strong>
					</span>
				</div>
			</div>		
		</div>
		<div class="panel-body">
			<div class="row row-productos">
				
			</div>			
		</div>
		<div class="panel-footer">
			<form>
				<div class="row add-producto">
					<div class="form-group col-md-6">
						<label class="control-label">Producto:</label>
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-question-circle-o" aria-hidden="true"></i>
							</span>
							<select class="form-control lista_productos">
								<option value="">Seleccione un Producto</option>
								{{#each productos}}
								<option value="{{this.id}}">{{this.nombre}}</option>
								{{/each}}
							</select>
						</div>
						<span class="help-block">
							<strong class="nombre_error"></strong>
						</span>
					</div>
					<div class="form-group col-md-5">
						<label class="control-label">Cantidad (unidades o porcentaje):</label>
						<input type="number" class="form-control cantidad_producto text-right" placeholder="Cantidad o porcentaje">
						<span class="help-block">
							<strong class="producto_pago_error"></strong>
						</span>
					</div>
					<div class="form-group col-md-1 text-right">
						<label class="control-label col-md-12">&nbsp;</label>
						<button class="btn btn-primary btn-add-p">
							<i class="fa fa-plus"></i>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</script>