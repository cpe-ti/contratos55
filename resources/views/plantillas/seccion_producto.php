<script id="template-seccion-producto" type="text/x-handlebars-template">
	<div class="well panel-producto animated fadeIn">
		<h3 class="titulo-panel">Producto X</h3>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-4">
			<label class="control-label" for="nombre">Nombre:</label>
			<input type="text" name="producto[0][nombre]" class="form-control" placeholder="Nombre" />
			<span class="help-block">
				<strong class="nombre_error"></strong>
			</span>
		</div>
		<div class="form-group col-md-8">
			<label class="control-label" for="descripcion">Descripción:</label>
			<textarea name="producto[0][descripcion]" rows="1" class="form-control" placeholder="Descripción" ></textarea>
			<span class="help-block">
				<strong class="descripcion_error"></strong>
			</span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-xs-12 col-md-4">
			<label class="control-label" for="unidad">Medida:</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-question-circle-o" aria-hidden="true"></i>
				</span>
				<select name="producto[0][unidad]" class="form-control" >
					<option value="">Seleccione una</option>
					{{#each unidades}}
					<option data-icono="{{this.icono}}" value="{{this.id}}">{{this.tipo}}</option>
					{{/each}}
				</select>
			</div>
			<span class="help-block">
				<strong class="unidad_error"></strong>
			</span>
		</div>
		<div class="form-group col-xs-6 col-md-4">
			<label class="control-label" for="cantidad">Cantidad:</label>
			<input type="text" name="producto[0][cantidad]" class="form-control text-right cantidad_producto" placeholder="Númerico" />
			<span class="help-block">
				<strong class="cantidad_error"></strong>
			</span>
		</div>
		<div class="form-group col-xs-6 col-md-4">
			<label class="control-label" for="unidad">Ponderación:</label>
			<div class="input-group">
				<input type="text" name="producto[0][ponderacion]" class="form-control text-right" placeholder="porcentaje" />
				<span class="input-group-addon">
					<i class="fa fa-percent" aria-hidden="true"></i>
				</span>
			</div>
			<span class="help-block">
				<strong class="unidad_error"></strong>
			</span>
		</div>
		<div class="form-group col-xs-12 col-md-12">
			<label class="control-label" for="unidad">Obligación Relacionada:</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-question-circle-o" aria-hidden="true"></i>
				</span>
				<select name="producto[0][obligacion]" class="form-control" >
					<option value="">Seleccione una</option>
					{{#each obligaciones}}
					<option value="{{this.id}}">{{this.obligacion}}</option>
					{{/each}}
				</select>
			</div>
			<span class="help-block">
				<strong class="unidad_error"></strong>
			</span>
		</div>
		<a href="#" class="boton-removethis-fields">
			<i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
		</a>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
	</div>
</script>