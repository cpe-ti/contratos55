<script id="template-seccion-producto-pago" type="text/x-handlebars-template">
	<div class="col-md-12 row-producto" data-p-id="{{producto_id}}" data-p-cant="{{cantidad}}">
		<div class="row">						
			<input type="hidden" name="pago[{{pago_index}}][producto][{{producto_id}}]" value="{{cantidad}}">
			<div class="col-md-6 pago-producto">
				<strong>Producto: </strong>{{producto_nombre}}
			</div>
			<div class="col-md-5 pago-cantidad">
				<strong>Cantidad: </strong>{{cantidad}} {{unidad}}
			</div>
			<div class="col-md-1 text-right producto-borrar">
				<button class="btn btn-danger borrar-p">
					<i class="fa fa-trash"></i>
				</button>
			</div>
		</div>
	</div>
</script>