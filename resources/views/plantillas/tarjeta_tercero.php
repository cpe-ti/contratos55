<script id="template-tarjeta-tercero" type="text/x-handlebars-template">
  	<div class="col-xs-12 col-md-6 col-tercero" data-nombre="{{nombre}} {{id}}">
		<div class="tarjeta-tercero" style="background-color: {{color}};">
			<div class="detalles">
				<p class="nombre">{{nombre}}</p>
				<p class="id">{{id}}</p>
				<p class="tipo">{{tipo}}</p>
			</div>
			<a href="{{url}}" class="ver-tercero link_dinamico">
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</div>
	</div>
</script>