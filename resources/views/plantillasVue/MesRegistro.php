<script type="text/x-template" id="mes-registro-avance-vue">
    <div>
        <div class="row contenedor-paneles-registro">            
            <div v-if="registros.length" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <panel-registro-avance v-for="(registro, index) in registros" 
                v-on:delete-registro="borrar_registro(index)"
                :index="index"
                :id="registro.id"
                :orden="registro.orden"
                :key="index"
                :fecha_registro="registro.fecha_registro" 
                :habilitado="meshabil" 
                :productos="productos" 
                :fechaini="fechaini" 
                :fechafin="fechafin"
                :productos_avance="registro.avance_productos"
                :pagos_avance="registro.solicitud_pagos"
                :nuevo="registro.nuevo ? registro.nuevo : false" 
                :procesado="registro.procesado"
                :url="url">
                </panel-registro-avance>
            </div>            
            <div v-else class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-warning">
                    <strong>No hay registros!</strong> No se han cargado registros en este periodo.
                </div>
            </div>
        </div>
        <div class="row" v-if="!meshabil">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
                    <strong>No puede registrar información en esete periodo!</strong>
                </div>
            </div>
        </div>
        <!-- <hr> -->
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-lg" @click="agregar_registro" :disabled="!meshabil">
                    <span><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar registro de avance</span>
                </button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
               <h4>Productos Esperados:</h4>
               <div v-if="productos_programados.length">
                    <ul class="list-group">
                        <li class="list-group-item" v-for="p in productos_programados">{{p.producto.nombre}} <span class="badge">{{p.cantidad_formato}} {{p.producto.unidad.abrev}}</span></li>
                    </ul>
                </div>
                <div class="alert alert-info" v-else>
                   <span>No hay productos programados!</span>
                </div>
            </div>
        </div>
        <br>
    </div>
</script>