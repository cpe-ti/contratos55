<script type="text/x-template" id="pago-avance-vue">
    <div class="list-group-item lsflx">
        <div class="lsflx-left">
            <div class="pagolsflx">
                <!-- <h4 class="list-group-item-heading">Factura Nº: <span class="badge" style="text-transform:uppercase; background-color: #005f94;">{{factura}}</span></h4> -->
                <span class="lsflx-item"><strong>Factura Nº: </strong><span style="text-transform:uppercase;">{{factura}}</span></span>
                <span class="lsflx-item badge">CDP: {{cdp}}</span>
                <span class="lsflx-item badge" style="background-color: green;">{{valor | currency("$", ",", 2, ".") }}</span>
                <!-- <span class="" style="font-size: 18px; color: green;"> -->
                    <!-- <i class="fa fa-usd" aria-hidden="true"></i> -->
                <!-- </span> -->
            </div>
            <p v-if="observaciones" class="list-group-item-text"><strong>Observaciones: </strong>{{observaciones}}</p>
        </div>
        <button type="button" class="lsflx-right" @click="$emit('delete-pago-solicitado')" :disabled="guardando || !mesActual || procesado==1">
            <i class="fa fa-trash-o"></i>
        </button>
    </div>
</script>