<script type="text/x-template" id="panel-registro-avance-vue">
<div class="container-panel">
   <div class="panel panel-default panel-reg-avance" v-bind:class="{ 'panel-nuevo': panelSinGuardar }"  style="position: relative; box-shadow: 0 5px 20px -3px black;">
       <div v-if="guardando" class="cargando-panel">
           <div class="spinner">
            <i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw"></i>
           </div>
       </div>
      <div class="btn-delete-panel-avance" @click="borrar_registro" >X</div>
      <!-- <div class="btn-delete-panel-avance" @click="$emit('delete-registro')" >X</div> -->
      <div class="panel-heading">
         <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h3 class="text-info">Informe Numero:  {{orden}}</h3>
                    <h4 class="text-info"><i class="fa fa-calendar" aria-hidden="true"></i> {{fecha}}</h4>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Fecha de corte:</label>
                    <input type="date" :min="fechaini" :max="fechafin" v-model="fecha" class="form-control text-right" name="" id="" :disabled="!habilitado || (procesado==1)">
                 </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <div class="panel-body">
         <h4 class="encabezado_registro">Registro de Avance (Productos): <span v-if="!productos_avance.length" class="badge pull-right"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> No se han agregado productos!</span></h4>
            <div class="list-group">
                <producto-avance v-for="(p, index) in productos_avance" 
                v-on:delete-avance-producto="borrar_avance_producto(index)"
                :producto_id="p.producto_id"
                :cantidad_registrada="p.cantidad_registrada"
                :observaciones ="p.observaciones"
                :guardando="guardando" 
                :mesActual="habilitado" 
                :procesado="procesado"
                :key="index">
                </producto-avance>
            </div>
         <!-- <transition-group name="list" tag="div" class="list-group"> -->
         <!-- </transition-group> -->
        <!-- <button class="btn btn-default" @click="mostrarOcultos">Mostrar</button> -->
         <!-- <div v-if="!productos_avance.length" class="alert alert-info">
            <strong><i class="fa fa-archive" aria-hidden="true"></i> No se han agregado productos!</strong>
         </div> -->
         <form action="#" method="POST" @submit.prevent.stop="agregar_producto">
            <div class="row">
               <div class="seccion-agregar-p">
                  <div class="seccion-agregar-p-inputs">
                     <div class="col-sm-6 form-group">
                        <label for="">Producto:</label>
                        <select required="required" ref="listaProductos" class="form-control" v-model="nuevo_producto.producto_id" :disabled="!habilitado || (procesado==1)">
                           <option v-for="opc in productos_libres" :value="opc.id">{{opc.nombre}}</option>
                        </select>
                     </div>
                     <div class="col-sm-6 form-group">
                        <label for="">Cantidad:</label>
                        <div class="input-group">
                           <input required="required" type="text" v-input-mask 
                           data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '', 'placeholder': '0'" 
                           class="form-control text-right"
                           v-on:input="setCantidad"
                           v-on:change="setCantidad"
                           v-bind:value="cantidadMasked"
                           autocomplete="off" 
                           :disabled="!habilitado || (procesado==1)">
                           <span class="input-group-addon text-capitalize">{{abrev}}</span>
                        </div>
                     </div>
                      <!-- <span>{{cantidad}}</span>                      -->
                     <div class="col-sm-12 form-group">
                        <label for="">Observaciones:</label>
                        <textarea style="resize: vertical;" class="form-control" cols="30" rows="2" v-model="nuevo_producto.observaciones" :disabled="!habilitado || (procesado==1)"></textarea>
                     </div>
                  </div>
                  <div class="seccion-agregar-p-boton">
                     <button type="submit" class="btn btn-primary" :disabled="!habilitado || (procesado==1)">
                     <i class="fa fa-plus"></i>
                     </button>
                  </div>
               </div>
            </div>
         </form> 
      </div>
      <hr style="margin: 0px;">
      <div class="panel-heading">
         <h4 class="encabezado_registro">Solicitud de pagos: <span v-if="!pagos_avance.length" class="badge pull-right"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> No se han agregado pagos!</span></h4>
      </div>
      <div class="panel-body">
         <div class="list-group">
            <pago-avance v-for="(pg, index) in pagos_avance" 
            v-on:delete-pago-solicitado="borrar_solicitud_pago(index)"
            :valor="pg.valor_solicitado"
            :factura="pg.numero_factura"
            :cdp="pg.cdp"
            :observaciones="pg.observaciones"
            :index="index" 
            :key="index" 
            :procesado="procesado"
            :guardando="guardando" 
            :mesActual="habilitado"></pago-avance>
         </div>
         <form action="#" method="POST" @submit.prevent.stop="agregar_pago">
            <div class="row">
               <div class="seccion-agregar-p">
                  <div class="seccion-agregar-p-inputs">
                    <div class="col-sm-6 col-md-4 form-group">
                        <label for="">Factura Nº:</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-barcode" aria-hidden="true"></i></span>
                           <input 
                           required="required" 
                           type="text"
                           style="text-transform:uppercase;"  
                           class="form-control text-right"
                           v-model="nuevo_pago.numero_factura"
                           autocomplete="off" 
                           :disabled="!habilitado || (procesado==1)">
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 form-group">
                        <label for="">Provisión Presupuestal:</label>
                        <div class="input-group">
                           <span class="input-group-addon text-capitalize">CDP</span>
                           <input required="required" type="text" 
                           class="form-control text-right" 
                           v-model="nuevo_pago.cdp"
                           autocomplete="off" 
                           :disabled="!habilitado || (procesado==1)">
                        </div>
                     </div>
                     <div class="col-sm-12 col-md-4 form-group">
                        <label for="">Valor del pago:</label>
                        <div class="input-group">
                           <span class="input-group-addon text-capitalize">$</span>
                           <input required="required" type="text" 
                           v-input-mask data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '', 'placeholder': '0'" 
                           class="form-control text-right" 
                           v-on:input="setValor"
                           v-bind:value="valorMasked" 
                           autocomplete="off" 
                           :disabled="!habilitado || (procesado==1)">
                        </div>
                     </div>
                     <div class="col-sm-12 form-group">
                        <label for="">Observaciones:</label>
                        <textarea style="resize: vertical;" class="form-control" cols="30" rows="2" v-model="nuevo_pago.observaciones" :disabled="!habilitado || (procesado==1)"></textarea>
                     </div>
                     <!-- <div class="col-sm-6 form-group">
                        <label for="">Fecha:</label>
                        <div class="input-group">
                           <span class="input-group-addon text-capitalize">
                           <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                           </span>
                           <input required="required" type="date" :min="fechaini" :max="fechafin" class="form-control text-right" v-model="nuevo_pago.fecha" autocomplete="off" :disabled="!habilitado || (procesado==1)">
                        </div>
                     </div> -->
                  </div>
                  <div class="seccion-agregar-p-boton">
                     <button type="submit" class="btn btn-primary" :disabled="!habilitado || (procesado==1)">
                     <i class="fa fa-plus"></i>
                     </button>
                  </div>
               </div>
            </div>
         </form>
      </div>
      <div class="panel-footer pie-panel-avance">
        <div class="row">
            <div v-if="respuesta.complete && respuesta.success" class="alert alert-success">
                <button type="button" class="close" @click="ocultarMensaje" aria-hidden="true">&times;</button>
                <strong>Hecho!</strong> {{respuesta.texto}}
            </div>
            <div v-if="respuesta.complete && !respuesta.success" class="alert alert-danger">
                <button type="button" class="close" @click="ocultarMensaje" aria-hidden="true">&times;</button>
                <strong>Error!</strong> {{respuesta.textoError}}
            </div>
        </div>
        <a :href="urlInforme" class="btn btn-default btn-informe link_dinamico" :disabled="!procesado">
           <span><i class="fa fa-clipboard" aria-hidden="true"></i> Generar Informe</span>
        </a>
        <div class="btn-group pull-right btn-grupo-guardar">
            <button v-if="panelSinGuardar" type="button" class="btn btn-primary" @click="guardarRegistro" :disabled="!habilitado || (procesado == 1) ">
                <span v-if="guardando"><i class="fa fa-refresh fa-spin fa-fw"></i> Guardando...</span>
                <span v-else><i class="fa fa-floppy-o"></i> Guardar</span>
            </button>
            <button v-else type="button" class="btn btn-primary" @click="actualizarRegistro" :disabled="!habilitado || (procesado == 1) ">
                <span v-if="guardando"><i class="fa fa-refresh fa-spin fa-fw"></i> Actualizando...</span>
                <span v-else><i class="fa fa-floppy-o"></i> Actualizar</span>
            </button>
            <button type="button" v-if="!nuevo" class="btn btn-success" @click="procesarRegistro" :disabled="!habilitado || (procesado == 1) ">
                <span><i class="fa fa-floppy-o"></i> Procesar</span>
            </button>
        </div>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
</script>