<script type="text/x-template" id="producto-avance-vue">
    <div v-bind:class="{'avance-oculto':oculto}" class="list-group-item lsflx">
        <div class="lsflx-left">
            <span class="badge pull-right">{{cantidad_registrada | currency("", ",", 2, ".") }} {{unidad}}</span>
            <h4 class="list-group-item-heading"><i class="fa fa-archive" aria-hidden="true"></i> {{nombre}}</h4>
            <p v-if="observaciones" class="list-group-item-text"><strong>Observaciones: </strong>{{observaciones}}</p>
        </div>
        <button type="button" class="lsflx-right" @click="$emit('delete-avance-producto')" :disabled="guardando || !mesActual || procesado==1">
            <i class="fa fa-trash-o"></i>
        </button>
    </div>
</script>