@extends('layouts.pagina_maestra')
@section('titulo')
Presupuesto
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	{{-- <h1>Presupuesto</h1> --}}
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('presupuesto/rubros') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-sitemap" aria-hidden="true"></i>
			</a>
			<div class="texto_item_menu">Rubros</div>			
		</div>
	</div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('presupuesto/plan-de-compras') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-shopping-bag" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Plan de Compras</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('presupuesto/areas') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Areas</span>
		</div>
	</div>	
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('presupuesto/procesos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-cogs" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Procesos</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
</div>
@endsection