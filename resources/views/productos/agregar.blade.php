@extends('layouts.pagina_maestra')
@section('titulo')
Agregar Productos
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h3 style="white-space: nowrap; text-overflow: ellipsis; width: 100%; overflow: hidden;">Contrato: {{$contrato->NombreRapido()}}</h3>
	<form id="form-agregar-productos" action="{{ Request::url() }}" method="POST" autocomplete="off">
		{!!csrf_field()!!}
		<div class="form-group form-error">
			<span class="help-block">
				<strong class="form-error-msg"></strong>
			</span>
		</div>
		<div class="form-group col-xs-12">			
			<button id="boton-add-fields" class="btn btn-lg btn-primary">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</button>
			<button type="submit" id="boton-crear-participante" class="btn btn-lg btn-primary pull-right">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>
				<span>&nbsp;&nbsp;Guardar</span>
			</button>			
		</div>
	</form>
	<hr>
</div>
<style>
	.boton-removethis-fields{
		border: 0px;
		position: absolute;
		top: 1px;
		right: 3px;
		padding: 0;
		color: #C10000;
		text-decoration: none;
	}
	.boton-removethis-fields:link, .boton-removethis-fields:visited, .boton-removethis-fields:hover, .boton-removethis-fields:active{
		color: #C10000;
	}	
	.panel-producto{
		position: relative;
		padding: 0px !important;
		padding-top: 0px !important;
		padding-bottom: 10px !important;
	}
	.titulo-panel{
		margin-left: 15px;
	}
	.form-group{
		margin-bottom: 0px !important;
	}
	.form-group>textarea{
		min-width: 100%;
		max-width: 100%;
		/* height: 34px !important; */
		resize: vertical !important;
	}
	form>.form-group:last-child{
		padding-bottom: 50px;
	}
	.animar{

	}
</style>

@include('plantillas.seccion_producto') 
<script type="text/javascript">
	var unidades = {!!json_encode($unidades)!!};	
	var obligaciones = {!!json_encode($contrato->obligaciones->where('tipo_obligacion_id','2'))!!};
	var dataset = {unidades, obligaciones};
	//console.log(dataset);
	//console.log(obligaciones);
	inicializar (function () {
		agregar_panel();
		$("#form-agregar-productos").on('change', 'select', function(event) {
			event.preventDefault();
			var opcion = $("option:selected", this); 
			var icono = opcion.data("icono");
			$(this).parent().find(".input-group-addon").html(icono);
			if (opcion.val() == 2) {
				var inpcant = $(this).closest('.panel-producto').find(".cantidad_producto");
				inpcant.val(100).disabled = true;
			}
		});
		
		$('#form-agregar-productos').on('click', '#boton-add-fields', function(e)
		{
			e.preventDefault();
			agregar_panel();
			document.querySelector("#contenedor-pagina").scrollTop = $("#contenedor-pagina>div").height();

		}).on('click', '.boton-removethis-fields', function(e)
		{
			e.preventDefault();
			if ($(".panel-producto").length > 1) {
				$(this).closest(".panel-producto").remove();
				indexar_nombre_productos();
			}
		});
		///Envio del formulario
		$('#form-agregar-productos').on('submit', function(e) {
			e.preventDefault();
			e.stopPropagation();
			e.isDefaultPrevented = function () {
				return false;
			}
			indexar_nombre_productos();
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			//var jform = $(this);
			//$(':input', jform).attr("disabled", true);
			$(".help-block").addClass('help-block-visible');
			$('.form-group').removeClass("has-error").find('.help-block>strong').html('');
			
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				console.log(respuesta);
				//$("#logs").html(respuesta.data.responseText);
				if (respuesta.data.responseText) {
					var obj;
					try{
						obj = jQuery.parseJSON(respuesta.data.responseText);
					}catch(e){
						//console.log(e);
						obj = {fallido:"Ocurrio un error inesperado, no identificado: "+e+"<br>"+respuesta.data.responseText};
					}                    
					//console.log(obj);
					var obj2 = {};
					$.each(obj, function(index, value) {
					    var split = index.split('.');
					    var textobj = '';
					    var tailtextobj = '';
					    for (var i = 0; i < split.length; i++) {
						    textobj += '{"'+split[i]+'":';
						    tailtextobj += '}';
						}
						textobj = textobj+'"'+escape(value)+'"'+tailtextobj;
						var tempobj = jQuery.parseJSON(textobj);
					    obj2 = deepExtend(obj2, tempobj);
					});
					console.log(obj2);
					if (obj.fallido) {
						$(".form-error").addClass("has-error");
						$('.form-error-msg').html(obj.fallido);
					}
					if (obj.error) {
						$(".form-error").addClass("has-error");
						$('.form-error-msg').html(obj.error);
					}
					if (obj2.producto) {
						$.each(obj2.producto, function(index, value) {
							$.each(value, function(indexv, valuev) {
								var name = 'producto\\['+index+'\\]\\['+indexv+'\\]';
								var input = $('[name='+name+']');
								input.closest(".form-group").addClass("has-error").find('.help-block>strong').html(unescape(valuev));
							});
						});
						//$(".form-error").addClass("has-error");
						//$('.form-error-msg').html(obj.error);
					}
                }else if(respuesta.success) {
                	$(".form-error").addClass("has-error");
                	$('.form-error-msg').html(respuesta.data);
                	//console.log(respuesta.data);
                	bootbox.dialog({
                		title: "Correcto",
                		message: respuesta.data.mensaje,
						//message: "<pre>"+JSON.stringify(respuesta.data, null, 4)+"</pre>",
						onEscape: function(){
							//$("#contenedor-pagina").css('overflow-y', 'auto');
							//$(':input', jform).attr("disabled", false);
						},
						buttons: {
							success: {
								label: "Ok",
								className: "btn-success",
								callback: function () {
									setTimeout(function() {window.history.back()}, 1500);
								}
							}
						}
					});					
                }
            });
		});

		function indexar_nombre_productos(){
			$(".panel-producto").each( function(indexp, element) {
				$(this).children('.titulo-panel').html('Producto '+(indexp+1));
				$(element).find(".form-control[name^='producto']").each( function(indexi, element) {
					var no = $(element).attr('name');
					var nr = no.replace(/producto\[[0-9]+\]/i, "producto["+indexp+"]");
					$(element).attr('name', nr);
				});
			});
		}

		function agregar_panel(){
			var p_pdcto = Handlebars.compile(document.getElementById('template-seccion-producto').innerHTML);
			$(p_pdcto(dataset)).insertBefore('.form-error');
			indexar_nombre_productos();
		}

		function deepExtend(destination, source) {
			for (var property in source) {
				if (source[property] && source[property].constructor &&	source[property].constructor === Object) {
					destination[property] = destination[property] || {};
					arguments.callee(destination[property], source[property]);
				} else {
					destination[property] = source[property];
				}
			}
			return destination;
		};

		$(".select_prueba").select2({
			//maximumSelectionLength: 1,
			//dropdownParent: $("#parent_select_supers"),
			language: "es",
			placeholder: 'Buscar un supervisor por nombre o cédula',
			templateResult: function (item) {
				return $('<div class="supervisor-l"><span class="nombre-super">'+item.text+'</span></div>');
			},
			matcher: function(params, data) {
				if ($.trim(params.term) === '') {
					return data;
				}
				var termino = params.term.toUpperCase();
				var texto = data.text.toUpperCase();
				if(texto.indexOf(termino) >= 0){
					return data;
				}
				return null;
			}
		});
	});
</script>
@endsection