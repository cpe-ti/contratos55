@extends('layouts.pagina_maestra')
@section('titulo')
Programar Pagos
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h3 style="white-space: nowrap; text-overflow: ellipsis; width: 100%; overflow: hidden;">Contrato: {{$contrato->NombreRapido()}}</h3>
	<form id="form-agregar-pagos" action="{{ Request::url() }}" method="POST" autocomplete="off">
		{!!csrf_field()!!}
		<div class="form-group unidades-display">
			<div class="container-units">
				<div class="badge-unit badge-valor" id="badge-valor">
					<span>
						Valor: 
					</span>
					<span id="valor_text">{{"$ ".number_format($contrato->valor, 2, ',', '.')}}</span>
				</div> 
				@foreach($contrato->productos as $producto)
				<div class="badge-unit" id="badge-u-{{$producto->id}}" data-p-tipo={{$producto->unidad_id}}>
					<span class="nombre">{{$producto->nombre}}: </span>
					<span class="cantidad">
						{{$producto->unidad_id == 2 ? $producto->cantidad * 100 : $producto->cantidad}}						
					</span>
					<span class="unidad">{{$producto->unidad_id == 2 ? "%" : "Uds"}}</span>
				</div>
				@endforeach
			</div>
		</div>
		<div class="form-group form-error">
			<span class="help-block">
				<strong class="form-error-msg"></strong>
			</span>
		</div>
		<div class="form-group col-xs-12">			
			<div class="row">
			<button id="boton-add-fields" class="btn btn-lg btn-primary">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</button>
			<button type="submit" id="boton-crear-participante" class="btn btn-lg btn-primary pull-right">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>
				<span>&nbsp;&nbsp;Guardar</span>
			</button>			
			</div>
		</div>
	</form>
	<hr>
	<pre id="log">
		
	</pre>
</div>
<style>
	.container-units{
		background-color: #E5E5E5;
		border-radius: 10px;
		display: flex;
		padding: 10px;
		flex-direction: row;
		/* justify-content: space-around; */
		justify-content: flex-start;
		flex-wrap: wrap;
	}
	.badge-unit{
		background-color: #005A9F;
		color: #FFFFFF;
		padding: 5px;
		border-radius: 10px;
		margin: 5px; 
	}
	.badge-valor{
		background-color: #439F00;
	}
	.badge-valor.notok{
		background-color: #EB0000;
	}
	.empty{
		background-color: #527A99;
		color: #E5E5E5;
	}
	.boton-removethis-fields{
		border: 0px;
		width: 25px;
		height: 25px;
		position: absolute;
		top: -5px;
		right: -5px;
		padding: 0;
		color: #C10000;
		background-color: transparent;
		text-decoration: none;
		z-index: 10;
	}
	.boton-removethis-fields:link, .boton-removethis-fields:visited, .boton-removethis-fields:hover, .boton-removethis-fields:active{
		color: #C10000;
	}	
	.panel-pago{
		position: relative;
		/* padding: 0px !important; */
		/* padding-top: 0px !important; */
		/* padding-bottom: 10px !important; */
	}
	.panel-pago>.panel-body{
		padding-top: 0px;
		padding-bottom: 0px;
	}
	.titulo-panel{
		margin-left: 15px;
	}
	.form-group{
		margin-bottom: 0px !important;
	}
	.form-group>textarea{
		min-width: 100%;
		max-width: 100%;
		/* height: 34px !important; */
		resize: vertical !important;
	}
	form>.form-group:last-child{
		padding-bottom: 50px;
	}
	.row-producto{
		margin-top: 10px;
		padding-bottom: 5px;
	}
	.row-producto:not(:last-child){
		border-bottom: 1px solid #DCDCDC;
	}
	.animar{

	}
</style>
@include('plantillas.seccion_pago') 
@include('plantillas.seccion_producto_pago') 
<script type="text/javascript">
	var unidades = {};
	var productos = {!!json_encode($contrato->productos->keyBy('id'))!!};	
	var dataset = {valor_cto: {{$contrato->valor}}, unidades, productos};
	inicializar (function () {
		Inputmask.extendDefaults({
		  'autoUnmask': true
		});
		agregar_panel();
		
		
		$('#form-agregar-pagos').on('click', '#boton-add-fields', function(e)
		{
			e.preventDefault();
			agregar_panel();
			document.querySelector("#contenedor-pagina").scrollTop = $("#contenedor-pagina>div").height();

		}).on('click', '.boton-removethis-fields', function(e)
		{
			e.preventDefault();
			if ($(".panel-pago").length > 1) {
				$(this).closest(".panel-pago").remove();
				indexar_nombre_productos();
				actualizar_valor();
				actualizar_productos();
			}
		});
		//Agregar productos
		$('#form-agregar-pagos').on('click', '.panel-pago .btn-add-p', function(e)
		{
			e.preventDefault();
			agregar_producto($(this).closest('.panel-pago'));
		});
		//Borrar producto
		$('#form-agregar-pagos').on('click', '.borrar-p', function(e) {
			event.preventDefault();
			remover_producto($(e.currentTarget).closest('.row-producto').first());
		});

		$('#form-agregar-pagos').on('keyup', '.desembolso-pago', function(e)
		{
			var suma = 0;
			$(".desembolso-pago").each( function(indexp, elemento) {
				
				var val = parseFloat($(elemento).inputmask('unmaskedvalue'));
				suma = suma + (isNaN(val) ? 0 : val);
				
			});
			if (suma>dataset.valor_cto) {
				$("#badge-valor").addClass('notok');
			}else{
				$("#badge-valor").removeClass('notok');
			}
			$("#valor_text").html("$ "+(dataset.valor_cto - suma).format(2, 3, '.', ','));
			
		});

		///Envio del formulario
		$('#form-agregar-pagos').on('submit', function(e) {
			e.preventDefault();
			e.stopPropagation();
			e.isDefaultPrevented = function () {
				return false;
			}
			
			indexar_nombre_productos();
			
			$(':input').inputmask('remove');
			
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			$(':input, .btn', formulario).attr("disabled", true);
			$(".help-block").addClass('help-block-visible');
			$('.form-group').removeClass("has-error").find('.help-block>strong').html('');
			
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				$(':input').inputmask();
				console.log(respuesta);
				//$("#logs").html(respuesta.data.responseText);
				$(':input, .btn', formulario).attr("disabled", false);
				$("#log").html(respuesta.data.responseText);				
            });
		});

		function indexar_nombre_productos(){
			$(".panel-pago").each( function(indexp, element) {				
				$(this).find('.numero-pago').first().html('Pago '+(indexp+1));
				$(element).find("input[name^='pago']").each( function(indexi, element) {
					var no = $(element).attr('name');
					var nr = no.replace(/pago\[[0-9]+\]/i, "pago["+(indexp+1)+"]");
					$(element).attr('name', nr);
				});
			});
		}

		function agregar_panel(){
			var p_pago = Handlebars.compile(document.getElementById('template-seccion-pago').innerHTML);
			$(p_pago(dataset)).insertBefore('.unidades-display').find('.desembolso-pago').focus();
			indexar_nombre_productos();
			$(":input").inputmask();
		}

		function agregar_producto(panel) {
			var p_id = $(panel).find('.lista_productos').first().val();
			var p_nombre = $(panel).find('.lista_productos>option:selected').first().text();
			var p_cant = $(panel).find('.cantidad_producto').first().val();
			var pago_index = ($(panel).find('.desembolso-pago').last().attr("name")).match(/pago\[[0-9]+\]/)[0];
			pago_index = pago_index.match(/[0-9]+/)[0];
			var error = false;			
			//
			if (p_id == "" || p_cant <= 0) {
				alert("Seleccione un producto e ingrese una cantidad");
				return false;
			}
			$.each($(panel).find('.row-producto'), function(index, el) {
				if(p_id == $(el).attr('data-p-id')){
					error = true;
					alert("Este producto ya esta usado para este pago")
					return false;
				}
			});			
			if (error) {
				return false;
			}			
			var datos = {pago_index: pago_index, producto_id: p_id, producto_nombre: p_nombre, cantidad: p_cant, unidad: (dataset.productos[p_id].unidad_id == 2 ? "%" : "Uds")};
			var suma_p = 0;
			var restante = 0;
			var m = dataset.productos[p_id].unidad_id == 2 ? 100 : 1;
			$(".row-producto[data-p-id='"+p_id+"']").each(function(index, el) {
				suma_p = suma_p + parseFloat($(el).attr('data-p-cant'));				
			});
			suma_p += parseFloat(p_cant);
			restante = ((dataset.productos[p_id].cantidad*m)-suma_p);
			if (restante < 0) {
				alert("Esto excede la cantidad de este producto.");
				return false;
			}
			var producto = Handlebars.compile(document.getElementById('template-seccion-producto-pago').innerHTML);
			$(panel).find('.row-productos').first().append(producto(datos));

			if (restante == 0) {
				$("#badge-u-"+p_id).addClass('empty');	
			}else {
				$("#badge-u-"+p_id).removeClass('empty');				
			}
			$("#badge-u-"+p_id+">.cantidad").html(restante);
			//Focus
			$(panel).find('.lista_productos').focus();
			$(panel).closest('.panel-pago').find('.cantidad_producto').val('');
		}

		function remover_producto(producto) {			
			var p_id = producto.attr('data-p-id');
			var cant_p = parseFloat( producto.attr('data-p-cant'));
			var cant_restante = parseFloat($("#badge-u-"+p_id+">.cantidad").text());
			var nueva_cant = (cant_p+cant_restante);

			$("#badge-u-"+p_id+">.cantidad").html(nueva_cant);
			$("#badge-u-"+p_id).removeClass('empty');
			producto.remove();
		}

		function actualizar_valor(){
			var valor = dataset.valor_cto;
			var suma = 0;
			$(".desembolso-pago").each(function(i, el) {
				var val = parseFloat($(el).inputmask('unmaskedvalue'));
				suma = suma + (isNaN(val) ? 0 : val);
			});
			if (suma>valor) {
				$("#badge-valor").addClass('notok');
			}else{
				$("#badge-valor").removeClass('notok');
			}
			$("#valor_text").html("$ "+(valor - suma).format(2, 3, '.', ','));
		}

		function actualizar_productos(){
			$.each(dataset.productos, function(index, p) {
			    var cantidad = p.unidad_id == 2 ? p.cantidad*100 : p.cantidad;
			    $(".row-producto[data-p-id='"+p.id+"']").each(function(i, el) {
					cantidad = cantidad - parseFloat($(el).attr('data-p-cant'));
				});
				$("#badge-u-"+p.id+">.cantidad").html(cantidad);
				$("#badge-u-"+p.id).removeClass('empty');
			});
		}

		function deepExtend(destination, source) {
			for (var property in source) {
				if (source[property] && source[property].constructor &&	source[property].constructor === Object) {
					destination[property] = destination[property] || {};
					arguments.callee(destination[property], source[property]);
				} else {
					destination[property] = source[property];
				}
			}
			return destination;
		};
	});
</script>
@endsection