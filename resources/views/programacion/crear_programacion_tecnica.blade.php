@extends('layouts.pagina_maestra')
@section('titulo')
Programar Ejecución Técnica
@endsection
@section('contenido')
<div id="contenido_programacion_tecnica" class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">	
	@php
		$inicio = new DateTime($contrato->fecha_inicio);
		$inicio->modify('first day of this month');
		$fin = new DateTime($contrato->fecha_terminacion);
		$fin->modify('first day of next month');
		$intervalo = DateInterval::createFromDateString('1 month');
		$periodo = new DatePeriod($inicio, $intervalo, $fin);
		setlocale(LC_TIME, 'es-CO', 'es_CO');
		//$fechas[];
	@endphp
	{{-- {{$contrato->productos}} --}}
	<div id="lista_productos">
		
	</div>
	<div class="row">
		<div class="col-xs-6 col-md-2 meses-encabezado">
			<div class="col-titulo">Mes</div>
		</div>
		<div class="col-xs-6 col-md-10">
			<div class="col-titulo">Productos</div>
		</div>
	</div>
	@foreach($periodo as $mes)
	@php
	$fechas[] = $mes->format('Y-m-d H:i:s');
	@endphp
	<div class="row">
		<div class="col-xs-12 col-md-2 col-meses">
			<div class="mes">{{ ucfirst(utf8_encode(strftime("%b %Y", $mes->getTimeStamp()))) }}</div>
		</div>
		<div class="col-xs-12 col-md-10 col-productos">
			<div class="row fila-agregar" data-date="{{ $mes->format('Y-m-d H:i:s') }}">
				<form action="nothing.php" method="POST" class="fake-form" autocomplete="off">
					<div class="form-group col-md-6 col-xs-7">
						<label class="sr-only" for="">Producto:</label>
						<select name="producto" class="form-control select-producto" id="" required>
							<option value="">Seleccionar Producto</option>
							@foreach($contrato->productos as $producto)
							<option value="{{$producto->id}}">{{$producto->nombre}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-4 col-xs-5">
						<label class="sr-only" for="">Cantidad:</label>
						<input type="number" name="cantidad" class="form-control producto-cantidad" id="" placeholder="Cantidad" required>
					</div>
					<div class="form-group col-md-2 col-xs-12">
						<label class="sr-only" for=""></label>
						<button type="submit" class="btn btn-primary boton-agregar-producto">Agregar</button>
					</div>
				</form>
			</div>				
			<div class="row fila-productos" data-date="{{ $mes->format('Y-m-d H:i:s') }}">

			</div>				
		</div>
	</div>
	<hr>
	@endforeach
	<div class="row">
		<form action="{{ Request::url() }}" method="POST" id="programacion_tecnica">
			{!!csrf_field()!!}
			<input type="hidden" id="datos_field" name="datos" value="">
			<div class="form-group col-md-2 col-xs-12">
				<button type="submit" class="btn btn-primary boton-agregar-producto">Guardar Programación</button>
			</div>
		</form>
	</div>
	<pre id="prevdata"></pre>
	<pre id="pampum"></pre>
</div>
<style>
	#lista_productos{
		background-color: #E8E8E8;
		padding: 7px;
		border-radius: 5px;
		z-index: 10;
		transition: all 500ms;
	}
	.p_bloque{
		background-color: #2060A9;
		color: #FCFCFC;
		padding: 5px;
		border-radius: 5px;
		white-space: nowrap;
		margin: 2px;
		display: inline-block;
	}
	.p_bloque_terminado{
		background-color: #8D9AA9 !important;
		color: #DBDBDB !important;
	}
	.col-titulo{
		/* background-color: #285F8D; */
		color: #285F8D;
		padding: 10px;
		margin:0; 
		margin-top: 5px;
		margin-bottom: 5px;
	}
	.mes{
		/* height: 100px; */
		border-radius: 5px;
		background-color: #B3D5EC;
		color: #3476A0;
		font-size: 18px;
		padding: 20px 10px 20px 10px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	.fila-agregar{
		padding-top: 5px;
	}
	.tarjeta-producto{
		padding: 10px;
		background-color: #F4F4F4;
		border: 1px solid #E7E7E7;
		border-radius: 5px;
		margin-top: 5px;
		margin-bottom: 5px;
		/* margin: 5px; */
		-webkit-animation: card-pdto-in 200ms ease-in-out forwards;
		-moz-animation: card-pdto-in 200ms ease-in-out forwards;
		-ms-animation: card-pdto-in 200ms ease-in-out forwards;
		-o-animation: card-pdto-in 200ms ease-in-out forwards;
		animation: card-pdto-in 200ms ease-in-out forwards;
	}
	.close-pdto{
		position: absolute;
		top: 0;
		right: 5px;
		color: #8B8B8B;
		cursor: pointer;
		font-size: 18px;
	}
	.close-pdto:hover{
		color: #CD1A1A;
	}
	.label-cantidad{
		background-color: #2373BD;
		color: #FFFFFF;
		padding: 4px;
		border-radius: 5px;
		white-space: nowrap;
	}
	.hr-card-pr{
		margin: 2px;
		margin-bottom: 7px;
		border-color: #dcdcdc;
	}
	
	@-webkit-keyframes card-pdto-in {
		0% { opacity: 0; transform: scale(0.7, 0.7);}
		100% { opacity: 1; transform: scale(1, 1); }
	}
	@-moz-keyframes card-pdto-in {
		0% { opacity: 0; transform: scale(0.7, 0.7);}
		100% { opacity: 1; transform: scale(1, 1); }
	}
	@-ms-keyframes card-pdto-in {
		0% { opacity: 0; transform: scale(0.7, 0.7);}
		100% { opacity: 1; transform: scale(1, 1); }
	}
	@-o-keyframes card-pdto-in {
		0% { opacity: 0; transform: scale(0.7, 0.7);}
		100% { opacity: 1; transform: scale(1, 1); }
	}
	@keyframes card-pdto-in {
		0% { opacity: 0; transform: scale(0.7, 0.7);}
		100% { opacity: 1; transform: scale(1, 1); }
	}

	@media (max-width : 479px) {
		
	}
	@media (min-width : 480px) and (max-width: 767px) {
		
	}
	@media (min-width:768px) and (max-width: 991px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199px) {
		
	}
	@media (min-width: 1200px) {
		
	}
	@media (min-width: 992px){
		.col-producto:nth-child(4n+1){
			clear: left;
		}
	}
</style>
@include('plantillas.producto_programado')
<script>
	inicializar (function () {
		var productos = {!!json_encode($contrato->productos->keyBy('id'))!!};
		console.log(productos);
		var programacion = {};	
		
		mostrar_data();
		actualizar_lista();
		
		$("#programacion_tecnica").on('submit', function(e) {
			e.preventDefault();
			e.stopPropagation();
			e.isDefaultPrevented = function () {
				return false;
			}
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			var jform = $(this);
			//$(':input', '#contenido_programacion_tecnica').attr("disabled", true);	
			//$(".help-block").addClass('help-block-visible');
			//$('.form-group').removeClass("has-error").find('.help-block>strong').html('');
			

			//for (var par of datos.entries()) {
			//    console.log(par[0]+ ', ' + par[1]); 
			//}

			//return false;

			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				//$(':input', '#contenido_programacion_tecnica').attr("disabled", false);
				console.log(respuesta);
				$("#pampum").html(respuesta.data.responseText);
				//if ((respuesta.data.status == 200 && respuesta.data.statusText == "OK") || respuesta.data.success == true) {
				//	mensaje_alerta ("Hecho!", (respuesta.data.success ? respuesta.data.mensaje : "Cambios guardados correctamente")).modal('show');
				//}else{
				//	if (respuesta.data.responseText) {
				//		$("#debug_error").html(respuesta.data.responseText);
				//	}
				//}
			});
		});
		
		$(".fake-form").on('submit', function(e) {
			e.preventDefault();
			e.stopPropagation();
			e.isDefaultPrevented = function () {
				return false;
			}
			agregar_producto($(this)[0]);
		});
		
		$("#contenido_programacion_tecnica").on('click', '.close-pdto', function(event) {
			event.preventDefault();
			/* Act on the event */
			remover_producto($(this).closest(".col-producto")[0]);
		});

		function actualizar_lista(){
			$("#lista_productos").html("");
			$.each( productos, function( key, value ) {
				if(!value.convertido){
					value.cantidad = value.unidad_id == 2 ?  value.cantidad*100 : value.cantidad;
					value.convertido = true;
					value.unidad_string = value.unidad_id == 2 ?  "%" : "";
				}
				$("#lista_productos").append('<span class="p_bloque'+(value.cantidad == 0 ? ' p_bloque_terminado' : '')+'">'+value.nombre+': '+ value.cantidad+value.unidad_string+'</span>');
			});
			$("#datos_field").val(JSON.stringify(programacion));
		}

		function agregar_producto(elemento){
			var fila_agregar = $(elemento).closest(".fila-agregar");
			var fila_productos = fila_agregar.next( ".fila-productos" );
			var id = Number(fila_agregar.find('.select-producto').val());
			var cant =  Number(fila_agregar.find('.producto-cantidad').val());
			
			productos[id].cantidad_seleccionada = cant;
			
			if ($('*[data-producto-id="'+id+'"]', fila_productos).length > 0) {
				alert("Este producto ya fue utilizado para este perido de tiempo");
				return false;
			}

			if (productos[id].cantidad_seleccionada > productos[id].cantidad) {
				alert("la cantidad excede las unidades restantes de este producto: "+productos[id].cantidad+productos[id].unidad_string);
				return false;
			}

			if (!(productos[id].cantidad_seleccionada > 0)) {
				alert("La cantidad debe ser mayor a 0");
				return false;				
			}
			
			productos[id].cantidad -= productos[id].cantidad_seleccionada;
			
			var p = Handlebars.compile(document.getElementById('template-producto-programado').innerHTML);
			fila_productos.append(p(productos[id]));

			if (!programacion[fila_agregar.data('date')]) {
				programacion[fila_agregar.data('date')] = {};
			}
			programacion[fila_agregar.data('date')][id] = cant;			
			
			mostrar_data();
			actualizar_lista();
			elemento.reset();
		}

		function remover_producto(elemento){
			var id = $(elemento).data('producto-id');
			var cant = $(elemento).data('producto-cant');			
			var fila_p = $(elemento).closest(".fila-productos");
			var date = fila_p.data('date');
			
			productos[id].cantidad += cant;
			
			$(elemento).remove();
			
			if($(".col-producto", fila_p).length > 0){
				delete programacion[date][id];
			}else{
				delete programacion[date];
			}			
			mostrar_data();
			actualizar_lista();
		}

		function mostrar_data(){
			$("#prevdata").html(JSON.stringify(programacion));
		}
	});
</script>
@endsection