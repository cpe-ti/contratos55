@extends('layouts.pagina_maestra')
@section('titulo')
Programación del Contrato 
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h2>Ejecucion programada</h2>
	@php
		setlocale(LC_TIME, 'es-CO', 'es_CO');
		// $suma = 0;
		// $array_json = Array();
		// $lastl = 0;
		// $programacion['tecnica'] = array();
		// $programacion['financiera'] = array();

		// $suma_pond = 0;

		// //llenar el dataset con la programaciond de productos

		// foreach($contrato->meses() as $mes){
		//     $mestxt = strftime("%d-%m-%Y", $mes->getTimeStamp());
		//     $programacion_tecnica = $contrato->programacion_tecnica()->whereDate('fecha','=', $mes)->get();
		//     $programacion['tecnica'][$mestxt]['programacion'] = $programacion_tecnica;
		//     $suma = 0;
		// 	foreach ($programacion['tecnica'][$mestxt]['programacion'] as $prog_t) {
		// 		$suma += $prog_t->ponderado_mes();				
		// 	}
		// 	$suma_pond += $suma;
		// 	$programacion['tecnica'][$mestxt]['ponderado_mes'] = $suma;
		// 	$programacion['tecnica'][$mestxt]['avance_mes'] = $suma_pond;
		// }
		// //Chartjs
		// foreach($programacion['tecnica'] as $mes => $datos){
		// 	$array_json[$mes] = $datos['avance_mes'];
		// }

	
		// foreach($contrato->meses() as $mes){
		// 	//print_r( $contrato->programacion_tecnica()->whereDate('fecha','=', $mes)->get()->toArray() );
		// }
		// foreach ($contrato->pagos_programados()->with('productos')->orderBy('orden', 'ASC')->get() as $pago) {
		// 	print_r($pago->toArray());
		// }
		
		
		
		//foreach ($contrato->pagos_programados()->orderBy('orden', 'ASC')->get() as $pago) {
		//}


		// foreach ($contrato->programacion_completa() as $mestext => $mes) {
		// 	echo $mestext;
		// 	echo $mes['prog_tec']['ponderado_mes']."   ";
		// 	echo $mes['prog_tec']['avance_mes']."   ";
		// 	echo "</br>";
		// }

		$array_pagos = Array();
		foreach ($contrato->pagos_programados()->with('productos')->orderBy('orden', 'ASC')->get() as $pago) {
			//$array_pagos[$pago->id];
			//echo $pago;
		}
	@endphp
		
	<pre>
		{{-- {{ print_r( $contrato->pagos_programados()->with('productos')->orderBy('orden', 'ASC')->get()->toArray() ) }} --}}
	</pre>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>PAGO</th>
				<th>PRODUCTOS</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($contrato->pagos_programados()->with('productos')->orderBy('orden', 'ASC')->get() as $pago)
			<tr>
				<td>{{ $pago->orden }}</td>
				<td>
				@foreach ($pago->productos as $prd)
					{{$prd->nombre}}: {{$prd->total_cant_pago}}
					<br>
				@endforeach
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>MES</th>
				<th>PROG TEC</th>
				<th>PAGO</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($contrato->meses() as $mes)
				<tr>
					<td>{{ strftime("%d-%m-%Y", $mes->getTimeStamp()) }}</td>
					<td>
						@foreach ( $contrato->programacion_tecnica()->whereDate('fecha','=', $mes)->get() as $pr )
							{{ $pr->producto->nombre }}: {{ $pr->total_cant_prog }}
							<br>
						@endforeach
					</td>
					<td>
						@foreach ( $contrato->programacion_tecnica()->whereDate('fecha','=', $mes)->get() as $pr )
							@foreach ($contrato->pagos_programados()->with('productos')->orderBy('orden', 'ASC')->get() as $pago)
								@foreach ($pago->productos as $prd)
									@if ($prd->id == $pr->producto->id)
										{{$prd->nombre}}: {{$prd->total_cant_pago}}
									@else
										
									@endif									
									<br>
								@endforeach
								{{ $pago->orden }}
							@endforeach
						@endforeach
					</td>
				</tr>				
			@endforeach
		</tbody>
	</table>
	<canvas id="grafico_ejecucion_tecnica">
		
	</canvas>
</div>
<script>
	inicializar (function () {
		// var data = {
		// 	labels: {},
		// 	datasets: [{
		// 		label: "PROGRAMACIÓN TECNICA PROGRAMADA",
		// 		fill: false,
		// 		lineTension: 0.1,
		// 		backgroundColor: "rgba(75,192,192,0.4)",
		// 		borderColor: "rgba(75,192,192,1)",
		// 		borderCapStyle: 'butt',
		// 		borderDash: [10, 2],
		// 		borderDashOffset: 0.0,
		// 		borderJoinStyle: 'miter',
		// 		pointBorderColor: "rgba(75,192,192,1)",
		// 		pointBackgroundColor: "#fff",
		// 		pointBorderWidth: 1,
		// 		pointHoverRadius: 10,
		// 		pointHoverBackgroundColor: "rgba(75,192,192,1)",
		// 		pointHoverBorderColor: "rgba(220,220,220,1)",
		// 		pointHoverBorderWidth: 2,
		// 		pointRadius: 10,
		// 		pointHitRadius: 10,
		// 		data: {},
		// 		spanGaps: false,
		// 	}]
		// };
		// var ctx = document.getElementById("grafico_ejecucion_tecnica").getContext("2d");
		// var myLineChart = new Chart(ctx, {
		// 	type: 'line',
		// 	data: data,
		// 	options: {
		// 		tooltips: {
		// 			callbacks: {
		// 				label: function(tooltipItem, data) {
		// 					return Number(tooltipItem.yLabel).toFixed(1).replace(/[.,]0$/, "")+"%";
		// 				}
		// 			}
		// 		},
		// 		scales: {
		// 			yAxes: [{
		// 				ticks: {
		// 					min: 0,
		// 					max: 100,
		// 					callback: function(value){return value+ "%"}
		// 				},  
		// 				scaleLabel: {
		// 					display: true,
		// 					labelString: "Percentage"
		// 				}
		// 			}]
		// 		}
		// 	}
		// });
	});
</script>	
@endsection