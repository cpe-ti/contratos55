@extends('layouts.pagina_maestra') 
@section('titulo') 
Generar Informe de Supervisión
@endsection 
@section('contenido') 
@php
setlocale(LC_ALL, 'esp_esp', 'esp_spain', 'spanish_esp', 'spanish_spain'); //setlocale(LC_TIME, 'es-CO', 'es_CO');
$contrato = $registro->contrato;
@endphp
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1" id="contenedor-informe">
	<form id="form-cuerpo-carta-adj" target="_blank" action="{{ route('informe.vistaprevia') }}" method="POST" autocomplete="off" class="prevent_lostXXX">
		{!!csrf_field()!!}
		<div class="panel panel-default">
			<div class="panel-heading" @click="cambio">
				<h4>Informe de supervisión N° @{{numero}}</h4>
			</div>
			<div class="panel-body" style="padding: 0px !important;">
				<textarea id="text-informe-supervision" name="contenido" rows="50" v-on:change="update" :value="contenido">
					CARGANDO CONTENIDO...
				</textarea>
			</div>
			<div class="panel-footer">
				<div v-if="(solpagos.length > 0)" class="row">
					<div class="col-lg-12">
						<h4>Cargar Facturas de solicitudes de pagos</h4>
					</div>
					<div class="col-lg-12">
						<ul class="list-group">
							<li class="list-group-item" v-for="(p, index) in solpagos">
								<span>Factura N°: @{{p.numero_factura}}:</span>
								<div class="input-group">
									<label class="input-group-btn">
										<span class="btn" :class="{'btn-primary': !p.nombre, 'btn-success': p.nombre}" :disabled="guardando || procesado">
											<i class="fa fa-folder-open"></i>
											<input type="file" :name="'factura-'+p.id" v-on:change="previewFiles(index, p.id, $event)" accept=".pdf" style="display: none;" :disabled="guardando || procesado">
										</span>
									</label>
									<input type="text" :value="p.nombre || p.factura_file" class="form-control" placeholder="Seleccione un archivo..." readonly>
									<div class="input-group-btn" v-if="p.factura_file">
										<span class="btn btn-success">
											<i class="fa fa-check" aria-hidden="true"></i>
											Guardado
										</span>
										<a :href="p.url" target="_blank" class="btn btn-default">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h4>Cargar cumplido a satisfacción y demas soportes</h4>
					</div>
					<div class="col-lg-12">
						<ul class="list-group">
							<li class="list-group-item">
								<span>Cumplido a satisfacción:</span>
								<div class="input-group">
									<label class="input-group-btn">
										<span class="btn" :class="{'btn-primary': !cumplido_file.nombre, 'btn-success': cumplido_file.nombre}" :disabled="guardando || procesado">
											<i class="fa fa-folder-open"></i>
											<input type="file" v-on:change="cumplidoChange($event)" accept=".pdf" style="display: none;" :disabled="guardando || procesado">
										</span>
									</label>
									<input type="text" :value="cumplidoNombre" class="form-control" placeholder="Seleccione un archivo..." readonly>
									<div class="input-group-btn" v-if="cumplido">
										<span class="btn btn-success">
											<i class="fa fa-check" aria-hidden="true"></i>
											Guardado
										</span>
										<a :href="cumplido_url" target="_blank" class="btn btn-default">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</li>
							<li class="list-group-item">
								<span>Soportes (si son requeridos):</span>
								<div class="input-group">
									<label class="input-group-btn">
										<span class="btn" :class="{'btn-primary': !soportes_file.nombre, 'btn-success': soportes_file.nombre}" :disabled="guardando || procesado">
											<i class="fa fa-folder-open"></i>
											<input type="file" v-on:change="soporteChange($event)" accept=".pdf" style="display: none;" :disabled="guardando || procesado">
										</span>
									</label>
									<input type="text" :value="soportesNombre" class="form-control" placeholder="Seleccione un archivo..." readonly>
									<div class="input-group-btn" v-if="soportes">
										<span class="btn btn-success">
											<i class="fa fa-check" aria-hidden="true"></i>
											Guardado
										</span>
										<a :href="soportes_url" target="_blank" class="btn btn-default">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-success pull-left">
							<i class="fa fa-eye" aria-hidden="true"></i>
							<span class="text-btn">Vista Previa</span>
						</button>
						<div class="btn-group pull-right">
							<button v-if="id==null" class="btn btn-primary boton_save" @click="guardarInforme" :disabled="guardando || procesado">
								<i class="fa fa-floppy-o" aria-hidden="true"></i>
								<span class="text-btn">Guardar</span>
							</button>
							<button v-else class="btn btn-primary boton_save" @click="actualizarInforme" :disabled="guardando || procesado">
								<i class="fa fa-floppy-o" aria-hidden="true"></i>
								<span class="text-btn">Actualizar</span>
							</button>
							<button v-if="guardado" class="btn btn-success boton_proeces" @click="procesarInforme" :disabled="guardando || procesado">
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								<span>Procesar</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<style>
	body.mce-fullscreen #contenedor-pagina{
		overflow-y: inherit;
	}

	@if(!Request::ajax())
	div.mce-fullscreen {
		top: 60px !important;
		left: 250px !important;
		width: calc(100% - 250px);
		display: flex;
		flex-direction: column;
		padding-bottom: 60px;
	}
	@media (max-width : 992px) {
		div.mce-fullscreen {
			top: 50px !important;
			left: 0px !important;
			width: 100%;
			display: flex;
			flex-direction: column;
			padding-bottom: 50px;
		}
	}
	@endif
	@media only screen and (max-width : 320px) {
		.text-btn{
			display: none;
		}
    }
</style>
<script type="text/javascript">
	inicializar(function() {
		const InformeApp = new Vue({
            el: '#contenedor-informe',
			data: {
				id: null,
				contenido: "",
				confirmado: 0,
				numero: null,
				registro_id: null,
				guardando: false,
				solpagos: [],
				facturas: [],
				cumplido: null,
				cumplido_url: null,
				cumplido_file: {
					nombre: null,
					file: null,
				},
				soportes: null,
				soportes_url: null,
				soportes_file: {
					nombre: null,
					file: null,
				}
			},
			beforeCreate : function() {
				console.log("Antes de la creacion");
			},
			created: function(){
				//this.update_data();
				console.log("Creado");
				
			},
			mounted: function(){
				console.log("Montado");
    			var vm = this;
				tinymce.init({
					selector: '#text-informe-supervision',
					is_ajax: {{Request::ajax() ? 'true' : 'false'}},//ajustar full scren
					//valid_elements: "li",
					//invalid_elements: "span",
					mode : "none",
					language: 'es',
					browser_spellcheck: true,
					menubar: false,
					images_dataimg_filter: function(img) {
						return img.hasAttribute('internal-blob');
					},
					content_style: ".mce-content-body {font-size:12pt; font-family: helvetica,sans-serif;}",
					//height: 500,
					theme: 'modern',
					plugins: [
					'advlist autolink lists link image charmap print preview hr anchor pagebreak',
					'searchreplace wordcount visualblocks visualchars code fullscreen',
					'insertdatetime media nonbreaking save table contextmenu directionality',
					'emoticons template paste textcolor colorpicker textpattern imagetools toc',
					'image paste code'
					],
					paste_retain_style_properties: "all",
					//| cut copy paste quickimage | styleselect insert 
					toolbar1: 'undo redo | alignleft aligncenter alignright alignjustify | bold italic underline | styleselect | fontselect | fontsizeselect | forecolor | backcolor | bullist | numlist | outdent | indent | table | link | bdesk_photo | code | fullscreen | image | paste',
					font_formats: 'Arial=helvetica,sans-serif; Helvetica=helvetica;',
					//toolbar2: '',
					image_advtab: true,
					paste_data_images: true,
					setup: function (editor) {
						editor.on('change', function () {
							console.log("Cambia TinyMCE");
							editor.save();
							editor.targetElm.dispatchEvent(new Event('change'));
						});
						editor.on('init', function () {
							console.log("Inicia TinyMCE");
							vm.update_data();
						});
					}
				});
			},
			methods: {
				cumplidoChange: function(e){
					this.cumplido_file.nombre = e.target.files[0].name;
					this.cumplido_file.file = e.target.files[0];
				},
				soporteChange: function(e){
					this.soportes_file.nombre = e.target.files[0].name;
					this.soportes_file.file = e.target.files[0];
				},
				previewFiles: function(index, id, e){
					//console.log(e.target.files[0]);
					this.$set(this.solpagos[index], 'nombre', e.target.files[0].name);
					this.$set(this.facturas, index, {facturaid: id, file: e.target.files[0]});
					//console.log(this.facturas);
					//this.solpagos[index].file = 
				},
				update_data: function(){
					console.log("Obteniendo datos");
					this.guardando = true;
					var url = "{{route('supervision.registros.htmlContentInforme',['contrato'=>$contrato,'registro'=>Request::route('registroid')])}}"
					console.log(url);
					axios.get(url)
                	.then(response => {
						console.log(response);
                	    this.confirmado = response.data.confirmado;
                	    this.contenido = response.data.contenido || "";
                	    this.id = response.data.id;
                	    this.numero = response.data.numero;
						this.registro_id = response.data.registro_id;
						this.solpagos = response.data.sol_pagos;
						this.cumplido = response.data.cumplido;
						this.soportes = response.data.soportes;

						this.cumplido_url = response.data.cumplido_url;
						this.soportes_url = response.data.soportes_url;

						this.$set(this.cumplido_file, 'nombre', null);
						this.$set(this.cumplido_file, 'file', null);
						this.$set(this.soportes_file, 'nombre', null);
						this.$set(this.soportes_file, 'file', null);						
						
						tinymce.get('text-informe-supervision').setContent(this.contenido);
                	})
                	.catch(e => {						
						console.log(e);
						bootbox.alert({
                	        title: e.response.data.errors,
                	        message: e.response.data.message || 'Ocurrió un error',
                	        backdrop: true
                	    });
                	}).then(()=>{
						console.log('Always');
						//console.log(this.contenido);						
						//tinymce.get('text-informe-supervision').setContent(this.contenido);
						this.guardando = false;
					})
				},
				update: function(evt){
					this.contenido = evt.target.value;
				},
				guardarInforme: function(e){
					this.guardando = true;
					e.preventDefault();

					const formData = new FormData();
					formData.append('numero', this.numero);
					formData.append('id', this.id);
					formData.append('contenido', this.contenido);
					//formData.append('facturas', null);
					this.facturas.forEach(function(item, index) {
						//console.log();
						formData.append('facturas['+index+'][file]', item.file);
						formData.append('facturas['+index+'][id]', item.facturaid);
					});					
					formData.append('cumplido', this.cumplido_file.file);
					formData.append('soportes', this.soportes_file.file);
					//formData.append('facturas[0]', this.facturas[0]);

					axios.post("{{route('supervision.registros.nuevoInformeHtml',['contrato'=>$contrato,'registro'=>Request::route('registroid')])}}", formData)      
                    .then(response => {
                        console.log(response);
						bootbox.alert({
                        	title: 'Informe Creado',
                        	message: 'Operación exitosa	',
                        	// message: response.data,
                        	backdrop: true
                    	});
						this.update_data();
                    })
                    .catch(e => {
                        console.log(e);
                        console.log(e.response);
						bootbox.alert({
                        	title: e.response.data.errors,
                        	message: e.response.data.message || 'Ocurrió un error',
                        	backdrop: true
                    	});
                    }).then(() => {
						this.guardando = false;
                    });
				},
				actualizarInforme: function(e){
					e.preventDefault();
					this.guardando = true;
					
					const formData = new FormData();
					formData.append('numero', this.numero);
					formData.append('id', this.id);
					formData.append('contenido', this.contenido);
					//formData.append('facturas', null);
					this.facturas.forEach(function(item, index) {
						//console.log();
						formData.append('facturas['+index+'][file]', item.file);
						formData.append('facturas['+index+'][id]', item.facturaid);
					});
					formData.append('cumplido', this.cumplido_file.file);
					formData.append('soportes', this.soportes_file.file);
					axios.post("{{route('supervision.registros.actualizarInformeHtml',['contrato'=>$contrato,'registro'=>Request::route('registroid')])}}", formData)      
                    .then(response => {
                        console.log(response);
						bootbox.alert({
                        	title: 'Informe Actualizado',
                        	message: 'Operación exitosa	',
                        	backdrop: true
                    	});
						this.update_data();
                    })
                    .catch(e => {
                        console.log(e);
                        console.log(e.response);
						bootbox.alert({
                        	title: e.response.data.errors,
                        	message: e.response.data.message || 'Ocurrió un error',
                        	backdrop: true
                    	});
                    }).then(() => {
                        this.guardando = false;
                    });
				},
				procesarInforme: function(e){
					e.preventDefault();
					vmapp = this;
					bootbox.confirm({
                        title: "Procesar Informe",
                        message: "Una vez procesado no se podra editar el contenido del informe\n Desea continuar?",
                        buttons: {
                            confirm: {
                                label: 'Si',
                                className: 'btn-danger'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-default'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                //vmapp.guardando = true;
								vmapp.guardando = true;					
								const formData = new FormData();
								formData.append('numero', vmapp.numero);
								formData.append('id', vmapp.id);
								formData.append('contenido', vmapp.contenido);
								//formData.append('facturas', null);
								vmapp.facturas.forEach(function(item, index) {
									//console.log();
									formData.append('facturas['+index+'][file]', item.file);
									formData.append('facturas['+index+'][id]', item.facturaid);
								});	
								formData.append('cumplido', vmapp.cumplido_file.file);
								formData.append('soportes', vmapp.soportes_file.file);							
								axios.post("{{route('supervision.registros.procesarInformeHtml',['contrato'=>$contrato,'registro'=>Request::route('registroid')])}}", formData)      
                    			.then(response => {
                    			    console.log(response);
									bootbox.alert({
                    			    	title: 'Informe Procesado',
                    			    	message: 'Este informe no podrá editarse más',
                    			    	backdrop: true
                    				});
									vmapp.update_data();
                    			})
                    			.catch(e => {
                    			    console.log(e.response);
									bootbox.alert({
                    			    	title: e.response.data.errors,
                    			    	message: e.response.data.message || 'Ocurrió un error',
                    			    	backdrop: true
                    				});
                    			}).then(() => {
                    			    vmapp.guardando = false;
                    			});
                            }
                        }
                    });
				},
				cambio: function(){
					//this.confirmado = this.confirmado == 0 ? 1 : 0;
					//this.id = null;
				}
			},
			computed: {
				guardado: function(){
					return this.id != null;
				},
				procesado: function(){
					if(this.confirmado == 0){
						return false;
					}
					return this.confirmado == 1;
				},
				cumplidoNombre: function(){
					if (this.cumplido_file.nombre) {
						return this.cumplido_file.nombre
					}
					return this.cumplido;
				},
				soportesNombre: function(){
					if (this.soportes_file.nombre) {
						return this.soportes_file.nombre
					}
					return this.soportes;
				}
			},
			watch: {
				procesado: function(val){
					console.log("Watchmen back");
					if(val){
						tinymce.get('text-informe-supervision').setMode('readonly');
					}else{
						tinymce.get('text-informe-supervision').setMode('design');
					}
				},
				guardando: function(val){
					if(val){
						$('#indicador_cargando').addClass('cargando-on');
					}else{
						$('#indicador_cargando').removeClass('cargando-on');						
					}
				},
				contenido: function(val){
					console.log("Contenido cambio");
				}
			}
		});
})

</script>
@endsection