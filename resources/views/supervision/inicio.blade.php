@extends('layouts.pagina_maestra')
@section('titulo')
Supervisión
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	{{-- <h1>Supervisión</h1> --}}
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('supervision/contratos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-file-text-o" aria-hidden="true"></i>
			</a>
			<div class="texto_item_menu">Contratos</div>			
		</div>
	</div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('supervision/reportes') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-bar-chart" aria-hidden="true"></i>
				{{-- <i class="fa fa-pie-chart" aria-hidden="true"></i> --}}
			</a>
			<span class="texto_item_menu">Reportes</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('supervision/incumplimientos') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-info-circle" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Incumplimientos</span>
		</div>
	</div>	
	<div class="col-xs-6 col-lg-4 text-center col-menu-btn">
		<div class="item_menu">
			<a href="{{ url('supervision/sanciones') }}" class="link_dinamico icono_item_menu">
				<i class="fa fa-gavel" aria-hidden="true"></i>
			</a>
			<span class="texto_item_menu">Sanciones</span>
		</div>
	</div>
	<div class="clearfix visible-xs"></div>
</div>
@endsection