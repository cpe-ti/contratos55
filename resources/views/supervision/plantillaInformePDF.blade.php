@php
	$contrato = $registro->contrato;
	$fmt = new NumberFormatter( 'es_CO', NumberFormatter::CURRENCY );	
@endphp
<body style="font-size: 11pt; font-family: 'Arial Narrow'">
	<h3 class="titulo" style="font-size:11pt;text-align:center;">INFORME No {{$registro->orden}} DEL {{mb_strtoupper($contrato->tipo_contrato->tipo)}} No. {{$contrato->consecutivo_cpe}} SUSCRITO ENTRE COMPUTADORES PARA EDUCAR Y {{mb_strtoupper($contrato->tercero->NombreCompleto())}}</h3>
	<ul class="lista-check" style="margin-top:0;padding-left:2em;list-style:none;list-style-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIEgAACBIB4XPaOAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAABxSURBVBiVhc6hDcJQFAXQQ4IkgK6hHtAswBDtPF2hqqmrwuLZoJIFMExAkBTzFCn/P3tP7n0L+StxzKETBixzaMQ2hQ54YJ9COzxR/waXCFYxc0c/19BiQoVbTK7nYIE3XvjgnPqridYuhcR/V2z+gS/EHxG314vYNgAAAABJRU5ErkJggg==');">
		<li>
			<strong>
				<i>Información general del Contrato:</i>
			</strong>
		</li>
	</ul>
	<table class="tabla-info-cto" style="width:100%;border:1px solid black;border-collapse:collapse;">
		<thead>
			<tr>
				<th style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;text-align:left;">Nº contrato:</th>
				<th style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:100;">{{$contrato->consecutivo_cpe}}</th>
				<th style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;text-align:left;">Tipo de contrato:</th>
				<th style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:100;">{{$contrato->tipo_contrato->tipo}}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Contratista:</td>
				<td colspan="3" style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;">{{$contrato->tercero->NombreCompleto()}}</td>
			</tr>
			<tr>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Objeto del Contrato:</td>
				<td colspan="3" style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:justify;">{!!$contrato->Objeto()!!}</td>
			</tr>
			<tr>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Certificado de Disponibilidad Presupuestal - CDP</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{$contrato->cdp}}</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Registro Presupuestal:</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{$contrato->rp}}</td>
			</tr>
			<tr>
				<td rowspan="2" style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Valor inicial del Contrato:</td>
				<td rowspan="2" style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{"$ ".number_format($contrato->valor, 2, '.', ',')}}</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Otrosí No. (Adiciones):</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">N/A</td>
			</tr>
			<tr class="row-end">
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Valor final del contrato:</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{"$ ".number_format($contrato->valor, 2, '.', ',')}}</td>
			</tr>
			<tr>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Fecha suscripción:</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{$contrato->fecha_suscripcion}}</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Fecha de inicio:</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{$contrato->fecha_inicio}}</td>
			</tr>
			<tr>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Plazo de ejecución inicial:</td>
                @php
                    $datetime1 = new DateTime($contrato->fecha_inicio);
                    $datetime2 = new DateTime($contrato->fecha_fin);
                    $interval = $datetime1->diff($datetime2);
                @endphp
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{$interval->format('%y años %m meses y %d dias')}}</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Fecha de legalización:</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{$contrato->fecha_legalizacion}}</td>
			</tr>
			<tr>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Otrosí No. (Prórrogas):</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">N/A</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;font-weight:bold;">Plazo de ejecución final:</td>
				<td style="border:1px solid black;border-collapse:collapse;padding-left:5pt;padding-right:5pt;text-align:center;font-style:italic;">{{$contrato->fecha_fin}}</td>
			</tr>
		</tbody>
	</table>
	<br>
	<ul class="lista-check" style="margin-top:0;padding-left:2em;list-style:none;list-style-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIEgAACBIB4XPaOAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAABxSURBVBiVhc6hDcJQFAXQQ4IkgK6hHtAswBDtPF2hqqmrwuLZoJIFMExAkBTzFCn/P3tP7n0L+StxzKETBixzaMQ2hQ54YJ9COzxR/waXCFYxc0c/19BiQoVbTK7nYIE3XvjgnPqridYuhcR/V2z+gS/EHxG314vYNgAAAABJRU5ErkJggg==');">
		<li>
			<strong>
				<i>Verificación financiera a la fecha:</i>
			</strong>
		</li>
	</ul>
	<table class="table-v-finan" style="width:70%;margin: 0 auto;border:1px solid black;border-collapse:collapse;">
		<thead style="background-color: #77d4ff;">
			<tr style="background-color: #77d4ff;">
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Descripción general</th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Valor</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Valor inicial del contrato</td>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;text-align:right;">{{"$ ".number_format($contrato->valor, 2, '.', ',')}}</td>
			</tr>
			<tr>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Adiciones (cuando aplique)</td>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;text-align:right;">$ 0</td>
			</tr>
			<tr>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Valor total del contrato</td>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;text-align:right;">{{"$ ".number_format($contrato->valor, 2, '.', ',')}}</td>
			</tr>
			<tr>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Valor Ejecutado</td>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;text-align:right;">$ xx.xxx.xxx</td>
			</tr>
			<tr>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Valor Pagado</td>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;text-align:right;">{{"$ ".number_format($contrato->totalPagadoRegistros(), 2, '.', ',')}}</td>
			</tr>
		</tbody>
	</table>
	<br>
	<ul class="lista-check" style="margin-top:0;padding-left:2em;list-style:none;list-style-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIEgAACBIB4XPaOAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAABxSURBVBiVhc6hDcJQFAXQQ4IkgK6hHtAswBDtPF2hqqmrwuLZoJIFMExAkBTzFCn/P3tP7n0L+StxzKETBixzaMQ2hQ54YJ9COzxR/waXCFYxc0c/19BiQoVbTK7nYIE3XvjgnPqridYuhcR/V2z+gS/EHxG314vYNgAAAABJRU5ErkJggg==');">
		<li>
			<strong>
				<i>Relación de pagos:</i>
			</strong>
		</li>
	</ul>
	<table class="table-relacion-pagos" style="width:100%; border:1px solid black;border-collapse:collapse;">
		<thead style="background-color:#77d4ff;">
			<tr style="background-color:#77d4ff;">
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">No.</th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Fecha de Pago</th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Factura No. (cuando aplique) </th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Concepto del pago</th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Valor</th>
			</tr>
		</thead>
		<tbody>
			@php
				$acum = 0;
			@endphp
			@foreach($registro->pagosHastaAqui() as $pago)
				@php
					$acum += $pago->valor_solicitado;
				@endphp
				<tr>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">{{$loop->iteration}}</td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">{{$pago->registro->fecha_registro}}</td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">{{$pago->numero_factura}}</td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">{{$pago->observaciones}}</td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;text-align:right;">{{ "$ ".number_format($pago->valor_solicitado, 2, '.', ',') }}</td>
				</tr>
			@endforeach
			
			<tr style="background-color:#d8d8d8;">
				<td colspan="3" style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">Total pagado</td>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;"></td>
				<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;text-align:right;">{{ "$ ".number_format($acum, 2, '.', ',') }}</td>
			</tr>
		</tbody>
	</table>
	<p style="font-family:'Arial Narrow';font-size:11pt;text-align:justify;">De conformidad con los documentos que obran en el expediente del contrato y los cumplidos a satisfacción expedidos por el
		supervisor, a continuación, se relacionan los pagos realizados durante la ejecución del contrato:</p>
	<ul class="lista-check" style="margin-top:0;padding-left:2em;list-style:none;list-style-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIEgAACBIB4XPaOAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAABxSURBVBiVhc6hDcJQFAXQQ4IkgK6hHtAswBDtPF2hqqmrwuLZoJIFMExAkBTzFCn/P3tP7n0L+StxzKETBixzaMQ2hQ54YJ9COzxR/waXCFYxc0c/19BiQoVbTK7nYIE3XvjgnPqridYuhcR/V2z+gS/EHxG314vYNgAAAABJRU5ErkJggg==');">
		<li>
			<strong>
				<i>Ejecución de Obligaciones:</i>
			</strong>
		</li>
	</ul>
	<table class="table-obligaciones" style="width:100%;border:1px solid black;border-collapse:collapse;">
		<thead style="background-color:#d8d8d8;">
			<tr style="background-color:#d8d8d8;">
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">OBLIGACIÓN</th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">ACTIVIDAD / BIENES ENTREGADOS</th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">ESTADO</th>
				<th style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">OBSERVACIONES</th>
			</tr>
		</thead>
		<tbody>
			@if( count($contrato->obligaciones) == 0)
				<tr>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse; text-align:justify;"></td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;"></td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;"></td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;"></td>
				</tr>
			@endif
			@foreach($contrato->obligaciones as $obligacion)
				<tr>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse; text-align:justify;">{{$obligacion->obligacion}}</td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">
						@foreach($obligacion->Productos as $producto)
							{{$producto->nombre}}
						@endforeach
					</td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;"></td>
					<td style="padding-left:5pt;padding-right:5pt;border:1px solid black;border-collapse:collapse;">
						@foreach($obligacion->Productos as $producto)
							{{$producto->nombre}}:{{$producto->avance()->where('registro_id', $registro->id)->first()->observaciones}}
						@endforeach
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<p style="font-family:'Arial Narrow';font-size:11pt;text-align:justify;">En ejecución del {{$contrato->tipo_contrato->tipo}} No. {{$contrato->consecutivo_cpe}} suscrito entre
		<strong>COMPUTADORES PARA EDUCAR</strong> y <strong>{{mb_strtoupper($contrato->tercero->NombreCompleto())}}</strong>, en el periodo comprendido entre el {{($registro->RegistroAnterior() ? $registro->RegistroAnterior()->fecha_registro : $contrato->fecha_inicio )}} y el {{ $registro->fecha_registro}}, se desarrollaron
		las siguientes actividades y/o se entregaron los siguientes bienes, como se relaciona a continuación:</p>

	<p>Actividades:</p>	

	<ul class="lista-check" style="margin-top:0;padding-left:2em;list-style:none;list-style-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIEgAACBIB4XPaOAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAABxSURBVBiVhc6hDcJQFAXQQ4IkgK6hHtAswBDtPF2hqqmrwuLZoJIFMExAkBTzFCn/P3tP7n0L+StxzKETBixzaMQ2hQ54YJ9COzxR/waXCFYxc0c/19BiQoVbTK7nYIE3XvjgnPqridYuhcR/V2z+gS/EHxG314vYNgAAAABJRU5ErkJggg==');">
		<li>
			<strong>
				<i>CUMPLIMIENTO DE LA LEY 789 DE 2002:</i>
			</strong>
		</li>
	</ul>
	<p style="font-family:'Arial Narrow';font-size:11pt;text-align:justify;">Que de conformidad con las disposiciones del artículo 50 de la Ley 789 de 2002,
		<strong>{{mb_strtoupper($contrato->tercero->NombreCompleto())}}</strong> certificó que cumple con sus obligaciones con el sistema de seguridad social integral, en lo
		que respecta a los aportes al sistema de salud, pensiones, riesgos profesionales y aportes a las cajas de compensación
		familiar, ICBF y SENA, conforme a los requisitos exigidos en la Ley, soportados en el documento adjunto de pago de parafiscales.</p>
	<p style="font-family:'Arial Narrow';font-size:11pt;text-align:justify;">De acuerdo con la documentación y evidencias que obran en el expediente del contrato, el contratista
		<strong>cumplió</strong> con las obligaciones establecidas en el Contrato de prestación de servicios No. {{$contrato->consecutivo_cpe}}, ejecutando
		las actividades de acuerdo con las condiciones del contrato. Por lo anterior, se puede realizar el pago de la cuenta de
		cobro de fecha XX-XX-XXXX.</p>
	<p style="font-family:'Arial Narrow';font-size:11pt;text-align:justify;">En constancia se firma a los @{{fecha_formato}}.</p>
	<p style="font-family:'Arial Narrow';font-size:11pt;text-align:justify;">Revisó y aprobó:</p>
	<br>
	<br>
	<div>
		<strong>{{$contrato->super->NombreCompleto()}}</strong>
	</div>
	<div>
		<strong>Supervisor</strong>
	</div>
	<div>
		<strong>{{$contrato->super->cargo->cargo}}</strong>
	</div>
	<div>
		<strong>Computadores para Educar</strong>
	</div>
	<br>
	<br>
	<div>Proyectado por: </div>
	<div>Con copia a la carpeta del Contrato No. {{$contrato->consecutivo_cpe}}</div>
</body>