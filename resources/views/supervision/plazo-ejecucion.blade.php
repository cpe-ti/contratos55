@extends('layouts.pagina_maestra')
@section('titulo')
Agregar Plazo de Ejecución
@endsection
@section('contenido')
<div id="contenido_legalizacion_cto" class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<h3>Agregar fecha de inicio y terminación al contrato</h3>
	<hr>
	<div class="row">
		<form id="form-plazo-ejecucion" target="_blank" action="{{ Request::url() }}" method="POST" autocomplete="off">
			{!!csrf_field()!!}
			<div class="form-group col-md-6">
				<label for="fecha_inicio">Fecha de Inicio:</label>
				<input type="date" id="fecha_inicio" name="fecha_inicio" class="form-control text-right" placeholder="Fecha de Inicio" v-model="fechaini">{{-- required="required" --}}
			</div>
			<div class="form-group col-md-6">
				<label for="fecha_fin">Fecha de Terminación:</label>
				<input type="date" id="fecha_fin" name="fecha_fin" class="form-control text-right" placeholder="Fecha de Terminación" v-model="fechafin" >{{-- required="required" --}}
			</div>
			<div class="col-md-12" style="font-size: 14pt;"><strong>Plazo de Ejecución: </strong><span id="plazo">@{{diferencia}}</span></div>			
			<div class="col-md-12" id="errors_plazo_ejec">
				
			</div>
			<div class="form-group col-md-12 text-right">
				<button type="submit" class="btn btn-primary">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar Fechas
				</button>
			</div>
		</form>
	</div>
</div>
<style>
	
</style>

<script>
	inicializar (function () {
		//dropdown supervisores

		var plazo = new Vue({
            el: '#contenido_legalizacion_cto',
            data: {
                fechaini: moment(new Date()).format('YYYY-MM-DD'),
				fechafin: moment(new Date()).format('YYYY-MM-DD'),
            },
			computed: {
				diferencia: function(){ 
					var a = moment(this.fechafin);
					var b = moment(this.fechaini);
					var years = a.diff(b, 'year');
					b.add(years, 'years');
					var months = a.diff(b, 'months');
					b.add(months, 'months');
					var days = a.diff(b, 'days');
					return years + ' Años ' + months + ' Meses ' + days + ' días';
				},
				fechainif: function(){ return moment(this.fechaini).format('DD/MM/YYYY') },
				fechafinf: function(){ return moment(this.fechafin).format('DD/MM/YYYY') },
			},
            methods: {
                
            }
        })
		$("#form-plazo-ejecucion").on('submit', function(event) {
			event.preventDefault();
			event.stopPropagation();
			var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			$(':input, .btn').prop('disabled', true);
			$("#errors_plazo_ejec").html('');
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				$(':input, .btn').prop('disabled', false);
				if (respuesta.success) {
					$(':input, .btn').prop('disabled', true);
					if("responseText" in respuesta.data){						
						$("#errors_plazo_ejec").append(respuesta.data.responseText);
					}else{
						$("#errors_plazo_ejec").append('<div class="alert alert-success"><strong>'+respuesta.data.mensaje+'</strong></div>');
						setTimeout(function() {window.history.back()}, 1500);
					}
				}else{
					switch (respuesta.data.status) {
						case 422:
						// var obj = JSON.parse(respuesta.data.responseText);
						var obj = respuesta.data.responseJSON.errors;
						$.each(obj, function(index, val) {
							$.each(val, function(i, v) {
								console.log(v);
								$("#errors_plazo_ejec").append('<div class="alert alert-danger"><strong>'+v+'</strong></div>');
							});
						});
						break;
						case 200:
						case 201:
						$(':input, .btn').prop('disabled', true);
						$("#errors_plazo_ejec").append('<div class="alert alert-success"><strong>'+respuesta.data.responseText+'</strong></div>');
						break;
						case 500:
						$("#errors_plazo_ejec").append(respuesta.data.responseText);
						break;
						default:
						break;
					}
				}
			});
		});
	});
</script>
@endsection