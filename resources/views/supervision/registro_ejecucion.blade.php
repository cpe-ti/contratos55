@extends('layouts.pagina_maestra') 
@section('titulo')
Registrar Avance Contrato {{$contrato->consecutivo_cpe}}
@endsection 
@section('contenido')
@php
$data['meses'] = $contrato->informacion_supervision();
//foreach ($data['meses'] as $key => $mes) {
//    $x = $contrato->programacion_tecnica()->whereDate('fecha', '=', date($mes["date"]))->with('producto')->get();
//    //array_push($data['meses'][$key], ['productos' => $x]);
//    $data['meses'][$key]['productos'] = $x;
//}
//echo "<pre>";
////var_dump($data['meses']);
//echo json_encode($data['meses']);
//echo "</pre>";
//exit();
$data['productos'] = $contrato->productos;
@endphp
<div id="contenido_legalizacion_cto" class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
    <div class="container-fluid" id="registro_avance">
        {{--  <div class="btn btn-default" @click="reloadData">Recargar datos</div>  --}}
        <div class="row">
            <div v-swiper:mySwiper="swiperOption">
                <div class="swiper-pagination swiper-pagination-bullets" slot="pagination"></div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide" v-for="(mes, index) in meses" :data-label-pagination="mes.date">
                        <mes-registro
                        :index="index"
                        :key="index"
                        :registrosmes="mes.registro_avance"
                        :fechaini="mes.dateini"
                        :fechafin="mes.datefin"
                        :meshabil="mes.meshabil"
                        :productos_programados="mes.productos_programados"
                        :productos="productos">
                        </mes-registro>
                    </div>
                </div>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</div>
<style>
    .cargando-panel{
        width: 100%;
        height: 100%;
        background-color: rgba(255, 255, 255, 0.5);
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 4;
        cursor: not-allowed;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
    .cargando-panel>.spinner{
        color:#3097d1;
    }
    .panel-reg-avance{
        position: relative;
    }
    .panel-reg-avance.panel-nuevo{
        border: 3px solid #3097d1;
    }
    .btn-delete-panel-avance{
        position: absolute;
        width: 30px;
        right: -5px;
        top: -5px;
        height: 30px;
        background-color: #da0000;
        border-radius: 50px;
        color: white;
        text-align: center;
        line-height: 30px;
        cursor: pointer;
        z-index: 4;
    }
    .pie-panel-avance>.btn-informe{
        max-width: 33%;
        overflow: hidden;
        white-space: nowrap;
        /* display: block; */
        text-overflow: ellipsis;    
    }
    .pie-panel-avance>.btn-informe{
        max-width: 33%;
        overflow: hidden;
        white-space: nowrap;
        /* display: block; */
        text-overflow: ellipsis;    
    }
    .pie-panel-avance>.btn-grupo-guardar{
        max-width: 67%;
        overflow: hidden;
        display: flex;
        flex-direction: row;
        /* white-space: nowrap; */
        /* display: block; */
        /* text-overflow: ellipsis;     */
    }
    .pie-panel-avance>.btn-grupo-guardar>.btn{
        /* max-width: 50%; */
        overflow: hidden;
        white-space: nowrap;
        /* display: block; */
        text-overflow: ellipsis; 
    }
    .panel-reg-avance .encabezado_registro{
        display: flex;
        flex-direction: row;
        align-items: baseline;
        justify-content: space-between;
        flex-wrap: wrap;
    }
    .panel-reg-avance .encabezado_registro>.badge{
        background-color: rgb(255, 218, 92) !important;
        color: #a07c00 !important;
        font-size: 13px;
        padding: 7px;
        margin-top: 10px;
    }
    .seccion-agregar-p{
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
    }
    .seccion-agregar-p-inputs{
        flex-grow: 1;
    }
    .seccion-agregar-p>.seccion-agregar-p-boton{
        display: flex;
        flex-direction: column;
        /* background-color: darkgray; */
        align-self: normal;
        justify-content: center;
        padding: 15px;
        padding-left: 0px;
        padding-top: 25px;
    }
    .seccion-agregar-p>.seccion-agregar-p-boton>.btn{
        height:100%;
        /*font-size: 15pt;*/
    }
    .list-enter-active, .list-leave-active {
        transition: opacity 300ms;
    }
    .list-enter, .list-leave-to /* .list-leave-active below version 2.1.8 */ {
        opacity: 0;
    }
    .lsflx .list-group-item-heading{
        word-break: break-all;
        color: #005f94;
    }
    .lsflx .lsflx-item{
        font-size: 13px;
        margin: 2px;
    }
    .lsflx .lsflx-item.badge{
        background-color: #005f94;
    }
    .lsflx{
        display:flex !important;
        flex-direction: row;
        padding: 0px !important;
        justify-content: space-between;
        transition: all 500ms;
    }
    .lsflx.avance-oculto{
        /* height: 0px; */
        /* overflow: hidden; */
    }
    .lsflx .lsflx-left{
        padding: 10px;
        padding-top: 15px;
        padding-bottom: 15px;
        width: 100%;
    }
    .lsflx .lsflx-left>.pagolsflx{
        display: flex;
        align-items: baseline;
        justify-content: space-between;
        flex-wrap: wrap;
    }
    .lsflx .lsflx-right{
        padding: 10px;
        color: #bf5329;
        border: 1px solid #bf5329;
        display: flex;
        align-items: center;
        cursor: pointer;
        font-size: 15pt;
        margin: -1px;
        background-color: white;
    }
    .lsflx .lsflx-right:disabled{
        background-color: #dadada;
        color: #717171;
        border-color: #717171;
        cursor: not-allowed;
    }
    .list-group .lsflx:first-child>.lsflx-right{
        border-radius: 0px 4px 0px 0px;
    }
    .list-group .lsflx:last-child>.lsflx-right{
        border-radius: 0px 0px 4px 0px;
    }
    .list-group .lsflx:only-child>.lsflx-right{
        border-radius: 0px 4px 4px 0px;
    }
    .list-group-item-text{
        word-break: break-word;
    }
    .swiper-slide{
        padding-left: 15px !important;
        padding-right: 15px !important;
    }
    .swiper-button-prev{
        left:-50px;
    }
    .swiper-button-next{
        right:-50px;
    }
    .swiper-button-prev, .swiper-button-next{
        background-color: #ffffff;
        /* top: 270px; */
        color: white;
        box-shadow: 0px 2px 9px -1px black;
        width: 50px;
        height: 50px;
        border-radius: 30px;
        background-size: 15px;
    }
    .swiper-container{
        margin-left: -15px;
        margin-right: -15px;
        touch-action: unset;
    }
    .swiper-wrapper {
        padding-top:10px;
        touch-action: pan-y;
    }
     .swiper-pagination{
        top: 0px !important;
        bottom: unset !important;
        position: relative !important;
     }
     .swiper-pagination-bullet-custom {
        width: auto;
        margin:5px !important;
        border-radius: 10px;
        height: 20px;
        padding-left: 5px;
        padding-right: 5px;    
        text-align: center;
        line-height: 20px;
        font-size: 12px;
        color: #000;
        opacity: 1;
        background: rgba(0,0,0,0.2);
      }
      .swiper-pagination-bullet-custom.swiper-pagination-bullet-active {
        color:#fff;
        background: #007aff;
      }
    @media only screen and (max-width : 1200px) {
        .swiper-button-prev{
            left:0px;
        }
        .swiper-button-next{
            right:0px;
        }
    }

    @media only screen and (max-width : 992px) {
        .swiper-button-prev, .swiper-button-next{
            display: none;
        }
    }
    @media only screen and (max-width : 767px) {
        .swiper-button-prev, .swiper-button-next{
            display: none;
        }
    }

    @media only screen and (max-width : 480px) {

    }

    @media only screen and (max-width : 320px) {

    }
</style>
@include('plantillas.seccion_producto_vue')
@include('plantillasVue.MesRegistro')
@include('plantillasVue.PanelRegistroAvance')
@include('plantillasVue.ProductoAvance')
@include('plantillasVue.PagosAvance')
<script>
    inicializar(function() {
        //COMPONENTE MES (PERIODO)
        Vue.component('mes-registro', {
            template: '#mes-registro-avance-vue',
            props: ['registrosmes', 'fechaini', 'fechafin', 'meshabil', 'productos', 'productos_programados', 'url'],
            data() {
                return {
                    registros: this.registrosmes,
                    nuevoregistro: {},
                }
            },
            methods: {
                //REGISTROS
                agregar_registro: function(){
                    var nuevos = _.filter(this.registros, function(registro) { if (registro.nuevo) return registro }).length;
                    if(!(nuevos>0)){
                        this.registros.push({
                            avance_productos:[],
                            contrato_id:3,
                            created_at:null,
                            fecha_registro:this.fechafin,
                            id: null,
                            orden: null,
                            solicitud_pagos:[],
                            updated_at:null,
                            nuevo: true
                        });
                    }else{
                        bootbox.alert({
                            title: "Atención",
                            message: "No puede agregar otro registro hasta que el actual sea guardado o eliminado",
                            backdrop: true
                        });
                    }
                    this.$nextTick(function () {
                        window.dispatchEvent(new Event('resize'));
                    })
                },
                borrar_registro: function(index) {
                    this.registros.splice(index, 1);
                }
            },
            computed:{

            },
            mounted: function () {
                console.log('Mes montado');
                //window.dispatchEvent(new Event('resize'));
            },
            watch:{
                registrosmes: function(){
                    this.registros = this.registrosmes;
                }
            }

        });
        //COMPONENTE PANEL REGISTRO
        Vue.component('panel-registro-avance', {
            template: '#panel-registro-avance-vue',
            
            //'avance_productos','contrato_id','created_at','fecha_registro','id','orden'
            
            props: ['id','index', 'fecha_registro', 'habilitado', 'procesado', 'productos', 'productos_avance', 'pagos_avance', 'fechaini', 'fechafin', 'orden', 'nuevo'],
            data() {
                return {
                    localid: this.id,
                    fecha: this.fecha_registro,
                    pto_avance: this.productos_avance,
                    pg_avance: this.pagos_avance,
                    nuevo_producto:{
                        id: null,
                        producto_id: null,
                        cantidad_registrada: null,
                        observaciones: null,
                        registro_id: null,
                        created_at: null,
                        updated_at: null
                    },
                    nuevo_pago: {
                        id: null,
                        registro_id: null,
                        cdp: null,
                        numero_factura: null,
                        observaciones: null,
                        valor_solicitado: null,
                        created_at: null,
                        updated_at: null
                    },
                    respuesta: {
                        complete: false,
                        success: true,
                        texto: null,
                        textoError: null,
                    },
                    guardando: false,
                    cantidadMasked: '0',
                    valorMasked: '0'
                }
            },
            methods: {
                ocultarMensaje: function(){
                    this.respuesta.complete = false;
                    this.respuesta.success = true;
                    this.respuesta.texto = null;
                    this.respuesta.textoError = null;
                },
                mostrarOcultos: function(){
                    
                },
                // AVANCE DE PRODUCTOS
                agregar_producto: function () {
                    if (this.nuevo_producto.cantidad_registrada == 0){
                        return false;
                    }                    
                    this.nuevo_producto.id = this._uid;
                    //pto_avance
                    this.productos_avance.push(_.clone(this.nuevo_producto));                    
                    //set to null
                    this.nuevo_producto.id = this._uid;
                    this.nuevo_producto.producto_id = null;
                    this.nuevo_producto.cantidad_registrada = null;
                    this.nuevo_producto.observaciones = null;
                    this.nuevo_producto.registro_id = null;
                    this.nuevo_producto.created_at = null;
                    this.nuevo_producto.updated_at = null;
                    this.cantidadMasked = null;
                    //console.log(this.$refs);
                    this.$nextTick(function () {
                        this.$refs.listaProductos.selectedIndex = 0;
                        this.$refs.listaProductos.dispatchEvent(new Event('change'));
                        this.$refs.listaProductos.focus();
                    });
                },
                borrar_avance_producto: function(index){
                    this.productos_avance.splice(index,1);
                },
                //PAGOS SOLICITADOS
                agregar_pago: function(){
                    if (this.nuevo_pago.valor_solicitado == 0){
                        return false;
                    }
                    this.pagos_avance.push(_.clone(this.nuevo_pago));                    
                    //set to null
                    this.nuevo_pago.id = null;
                    this.nuevo_pago.registro_id = null;
                    this.nuevo_pago.cdp = null;
                    this.nuevo_pago.numero_factura = null;
                    this.nuevo_pago.observaciones = null;
                    this.nuevo_pago.valor_solicitado = null;
                    this.nuevo_pago.created_at = null;
                    this.nuevo_pago.updated_at = null;
                    this.valorMasked = null;    
                },
                borrar_solicitud_pago: function(index){
                    this.pagos_avance.splice(index,1);
                },
                //REGISTROS (GUARDAR, ACTUALIZAR, PROCESAR, ELIMINAR)
                guardarRegistro: function(){
                    this.guardando = true;
                    axios.post("{{route('supervision.registros.guardar',['contrato'=>$contrato])}}", {
                        fecha: this.fecha,
                        productos: this.productos_avance,
                        pagos: this.pagos_avance
                    })      
                    .then(response => {
                        console.log(response);
                        this.respuesta.texto = response.data;
                        this.respuesta.complete = true;
                        this.respuesta.success = true;
                        this.$root.reloadData();
                    })
                    .catch(e => {
                        console.log(e);
                        console.log(e.response);
                        this.respuesta.complete = true;
                        this.respuesta.success = false;
                        this.respuesta.textoError = e.response.data;
                        // this.errors.push(e);
                    }).then(() => {
                        //siempre
                        this.guardando = false;
                    });
                },
                actualizarRegistro: function(){
                    console.log('Actualizando');
                    this.guardando = true;
                    var url = "{{route('supervision.registros.actualizar',['contrato'=>$contrato, 'registro'=>''])}}/"+this.id;
                    axios.put(url, {
                        fecha: this.fecha,
                        productos: this.productos_avance,
                        pagos: this.pagos_avance
                    })
                    .then(response => {
                        console.log(response);
                        this.respuesta.texto = response.data;
                        this.respuesta.complete = true;
                        this.respuesta.success = true;
                        this.$root.reloadData();
                    })
                    .catch(e => {
                        console.log(e);
                        console.log(e.response);
                        this.respuesta.complete = true;
                        this.respuesta.success = false;
                        this.respuesta.textoError = e.response.data;
                        // this.errors.push(e);
                    }).then(() => {
                        //siempre
                        this.guardando = false;
                    });
                },
                procesarRegistro: function(){
                    var vmrg = this;
                    bootbox.confirm({
                        title: "Procesar Registro?",
                        message: "Una vez procesado no se podran editar los valores\n Desea continuar?",
                        buttons: {
                            confirm: {
                                label: 'Si',
                                className: 'btn-danger'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-default'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                vmrg.guardando = true;
                                var url = "{{route('supervision.registros.procesar',['contrato'=>$contrato, 'registro'=>''])}}/"+vmrg.id;
                                console.log(url);
                                axios.put(url, {
                                    fecha: vmrg.fecha,
                                    productos: vmrg.productos_avance,
                                    pagos: vmrg.pagos_avance
                                })
                                .then(response => {
                                    console.log(response.data);
                                    vmrg.respuesta.texto = response.data;
                                    vmrg.respuesta.complete = true;
                                    vmrg.respuesta.success = true;
                                    vmrg.$root.reloadData();
                                })
                                .catch(e => {
                                    console.log(e);
                                    console.log(e.response);
                                    vmrg.respuesta.complete = true;
                                    vmrg.respuesta.success = false;
                                    vmrg.respuesta.textoError = e.response.data;
                                }).then(()=>{
                                    vmrg.guardando = false;
                                });
                            }
                        }
                    });                    
                },
                borrar_registro:function(){
                    if(this.nuevo){
                        this.$emit('delete-registro');
                        this.$nextTick(function () {
                            window.dispatchEvent(new Event('resize'));
                        });                                    
                        return false;
                    }
                    var vmapp = this;
                    bootbox.confirm({
                        title: "Eliminar registro",
                        message: "Esta seguro de eliminar este registro?",
                        buttons: {
                            confirm: {
                                label: 'Si',
                                className: 'btn-danger'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-default'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                var url = "{{route('supervision.registros.eliminar',['contrato'=>$contrato,'registro'=>''])}}/"+vmapp.id;
                                console.log(url);
                                axios.delete(url, {
                                
                                })     
                                .then(response => {
                                    console.log(response);
                                    vmapp.$root.reloadData();
                                    vmapp.$nextTick(function () {
                                        window.dispatchEvent(new Event('resize'));
                                    });
                                })
                                .catch(e => {
                                    console.log(e);
                                    console.log(e.response);
                                    console.log(e.response.data);                        
                                    bootbox.alert({
                                        title: "Error",
                                        message: "Error al borrar el registro: "+(e.response.data.message ? e.response.data.message : e.response.data),
                                        backdrop: true
                                    });
                                }).then(() => {
                                    //siempre
                                    //vmapp.guardando = false;
                                });
                            }
                        }
                    });
                },
                //CANTIDADES SIN FORMATO 
                setCantidad: function(event){
                    this.nuevo_producto.cantidad_registrada = event.target.inputmask.unmaskedvalue();
                    this.cantidadMasked = event.target.value;
                },
                setValor: function(event){
                    this.nuevo_pago.valor_solicitado = event.target.inputmask.unmaskedvalue();
                    this.valorMasked = event.target.value;
                },
            },
            watch: {
                guardando:function(){
                    var el = this.$el;
                    //Vue.nextTick(function () {
                    this.$nextTick(function () {
                        el.scrollIntoView(false);
                    });
                },
                fecha_registro: function(){
                    this.fecha = this.fecha_registro; 
                }
            },
            computed:{
                urlInforme:function(){
                    return "{{route('informe',['id'=>$contrato->id, 'registro'=>''])}}/"+this.id;
                },
                panelSinGuardar: function(){
                    return this.nuevo;
                },
                abrev: function(){
                    if(this.nuevo_producto.producto_id == null){
                        return "";
                    }
                    
                    var idp = this.nuevo_producto.producto_id;
                    
                    var pdto = _.findLast(this.productos, function(p) { 
                        return p.id == idp; 
                    });
                    
                    console.log(pdto);
                    
                    if(pdto == undefined){
                        return "";
                    }
                    
                    return pdto.unidad.abrev;
                },
                productos_libres: function(){
                    var usedID = _.map(this.productos_avance, 'producto_id');
                    var plibres = _.filter(this.productos, function(p){
                        return !(_.includes(usedID, p.id));
                    });
                    //console.log(productosUsados);
                    return plibres;
                }
            },
            mounted: function () {
                console.log('Panel Montado');
                this.$nextTick(function () {
                    window.dispatchEvent(new Event('resize'));
                    if(this.localid == null){
                        this.localid = this._uid
                    }
                    this.key = this.localid;
                });
            },
        });
        //COMPONENTE PRODUCTO AVANCE
        Vue.component('producto-avance', {
            template: '#producto-avance-vue',
            props: ['procesado','producto_id','cantidad_registrada','observaciones','guardando', 'mesActual'],
            data() {
                return {}
            },
            methods: {
                mostrar:function(){
                    this.hidden = false;
                } 
            },
            computed:{
                oculto: function(){
                    return this.cantidad_registrada == 0;
                },
                unidad: function(){
                    console.log(this.$parent.productos);
                    var idp = this.producto_id;
                    var p = _.find(this.$parent.productos, function(p) {
                        return p.id === idp;
                    });
                    if(p == undefined)
                    {
                        return "";
                    }
                    console.log(p);
                    return p.unidad.abrev;
                },
                nombre: function(){
                    var idp = this.producto_id;
                    var p = _.find(this.$parent.productos, function(p) {
                        return p.id === idp;
                    });
                    if(p == undefined)
                    {
                        return "";
                    }
                    return p.nombre;
                }
            },
            mounted: function () {
                this.$nextTick(function () {
                    window.dispatchEvent(new Event('resize'));
                })
            },
        });
        //COMPONENTE SOLICITUD DE PAGO
        Vue.component('pago-avance', {
            template: '#pago-avance-vue',
            props: ['procesado','cdp','valor','observaciones','factura', 'index', 'guardando', 'mesActual'],
            data() {
                return {
                    
                }
            },
            mounted: function () {
                this.$nextTick(function () {
                    window.dispatchEvent(new Event('resize'));
                })
            },
            methods: {
                
            },
            computed:{

            }
        });
        //INSTANCIA PRINCIPAL DE VUE
        var appx = new Vue({
            el: '#registro_avance',
            data: {
                swiperOption: {
                    simulateTouch : false,
                    speed: 500,
                    slidesPerView: 1,
                    slidesPerView: 1,
                    slidesPerColumn: 1,
                    virtualTranslate: false,
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    paginationType : 'bullets',
                    effect: "slide",
                    centeredSlides: true,
                    prevButton: '.swiper-button-prev',
                    nextButton: '.swiper-button-next',
                    shortSwipes: true,
                    touchEventsTarget: "wrapper",
                    spaceBetween: 15,
                    longSwipes: true,
                    longSwipesRatio: 0.2,
                    longSwipesMs: 50,
                    autoHeight: true,
                    paginationBulletRender: function(swiper, index, className){
                        if(swiper.slides[index] == undefined){
                            return `<span class="${className} swiper-pagination-bullet-custom">${index}</span>`;
                        }
                        var label = swiper.slides[index].getAttribute("data-label-pagination");
                        var x = label.split("-");
                        var label = x[0]+"-"+x[1];
                        return `<span class="${className} swiper-pagination-bullet-custom">${label}</span>`;
                    }
                },
                //meses: {!! json_encode($data['meses']) !!},
                meses: {!! json_encode($data['meses']) !!},
                productos: {!! json_encode($data['productos']) !!},
                url: "{{ Request::url() }}",
            },
            created() {
                //axios.get("{{route('supervision.registros',['contrato'=>$contrato])}}")
                //.then(response => {
                //    this.meses = response.data
                //})
                //.catch(e => {
                //    console.log(e);
                //})
            },
            mounted: function () {
                this.myswiper.slideTo(this.mesActualIndex, 0);
                this.$nextTick(function () {
                    //console.log(this.meses);
                    window.dispatchEvent(new Event('resize'));
                })
            },
            methods: {
                getmesActual: function(){
                    //this.myswiper.slideTo(this.mesActualIndex, 600);
                    //console.log(this.mesActual);
                    return this.mesActual;
                },
                reloadData: function(){
                    //axios.get("{{url('api/supervision/contratos/'.$contrato->id.'/guardar-registro')}}")
                    //axios.get("{{route('supervision.registros',['contrato'=>$contrato])}}")
                    axios.get("{{url('api/supervision/contratos/'.$contrato->id.'/supervision-data')}}")
                    .then(response => {
                        // JSON responses are automatically parsed.
                        //console.log(response.data);
                        //this.meses = null;
                        this.meses = response.data;
                        //this.myswiper.slideTo(this.mesActualIndex, 0);
                    })
                    .catch(e => {
                        console.log(e);
                    })
                }
            },
            computed:{
                mesActual: function(){
                    //console.log(this.meses);
                    return this.meses.filter(function(e) { return e.meshabil == true; })[0];
                },
                mesActualIndex: function(){
                    index = this.meses.findIndex(e => e.meshabil == true);
                    return index;
                },
            }
        })
        console.log(appx);
        //console.log({!! json_encode($data['meses']) !!});
        //console.log({!! json_encode($data['productos']) !!});
        //Inputmask().mask(document.querySelectorAll("input"));
    });
</script>
@endsection