<div class="row">
	<div class="col-md-12">
		<form id="form-nuevo-tercero-inline" target="_blank" action="{{ url('terceros/inline_nuevo') }}" method="POST" autocomplete="off">
			{!!csrf_field()!!}
			<div class="form-group col-md-6">
				<label class="control-label" for="id_tercero_inline">Nº de identificación (NIT o C.C):</label>
				<input type="number" name="id" class="form-control" id="id_tercero_inline" placeholder="Identificación (Nit o Cédula)" required="required"/>
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_tipo_id">Tipo de persona:</label>
				<select name="tipo_id" class="form-control" id="tercero_tipo_id" required="required">
					<option value="">Seleccionar..</option>
					@foreach ($tipos as $tipo)
					<option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
					@endforeach
				</select>
			</div>		
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_primer_nombre">Primer Nombre:</label>
				<input type="texto" name="primer_nombre" class="form-control" id="tercero_primer_nombre" placeholder="Primer Nombre"/>
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_segundo_nombre">Segundo Nombre:</label>
				<input type="texto" name="segundo_nombre" class="form-control" id="tercero_segundo_nombre" placeholder="Segundo Nombre"/>
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_primer_apellido">Primer Apellido:</label>
				<input type="texto" name="primer_apellido" class="form-control" id="tercero_primer_apellido" placeholder="Primer Apellido"/>
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_segundo_apellido">Segundo Apellido:</label>
				<input type="texto" name="segundo_apellido" class="form-control" id="tercero_segundo_apellido" placeholder="Segundo Apellido"/>
			</div>
			<div class="form-group col-md-12">
				<label class="control-label" for="tercero_razon_social">Razón Social:</label>
				<input type="text" name="razon_social" class="form-control" id="tercero_razon_social" placeholder="Razon Social"/>			
			</div>
			<div class="form-group col-md-12">
				<label class="control-label" for="tercero_direccion">Dirección:</label>
				<input type="text" name="direccion" class="form-control" id="tercero_direccion" placeholder="Dirección"/>			
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_telefono">Teléfono:</label>
				<input type="text" name="telefono" class="form-control" id="tercero_telefono" placeholder="Teléfono"/>			
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_telefono">Correo:</label>
				<input type="text" name="email" class="form-control" id="tercero_correo" placeholder="Correo"/>			
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_telefono">Nombre Representante Legal:</label>
				<input type="text" name="representante" class="form-control" id="tercero_correo" placeholder="Obligatorio para persona Jurídica"/>			
			</div>
			<div class="form-group col-md-6">
				<label class="control-label" for="tercero_telefono">Cédula Representante Legal:</label>
				<input type="number" name="id_representante" class="form-control" id="tercero_correo" placeholder="Obligatorio para persona Jurídica"/>			
			</div>
			<div class="form-group col-xs-12" id="errores_inline_tercero">
				
			</div>
			<div class="form-group col-xs-12">			
				<button type="submit" id="submit_crear_tercero" class="btn btn-crear-tercero btn-primary pull-right">
					<i class="fa fa-floppy-o" aria-hidden="true"></i>
					<span>&nbsp;&nbsp;Crear</span>
				</button>			
			</div>
		</form>
	</div>
</div>
<style>
	.form-group:last-child {
    	padding-bottom: 0px;
    	margin-bottom: 0px;
    }
    .p-error{
    	color: #DA0000;
    }
    .p-success{
    	color: #00A22E;
    }
</style>
<script>
	inicializar (function () {
		$('#form-nuevo-tercero-inline').on('submit', function(e) {
    		e.preventDefault();
    		e.stopPropagation();
    		var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			var jform = $(this);
			$(':input', jform).attr("disabled", true);
			$("#errores_inline_tercero").html('');
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (datos) {
				if (datos.success == false && ('responseText' in datos.data)) {
					$(':input', jform).attr("disabled", false);
					var objerr = {};
					try {
						// objerr = JSON.parse(datos.data.responseText);
						objerr = datos.data.responseJSON.errors;
					} catch(e) {
						objerr = {fail: e, message: datos.data.responseText};
					}					
					$.each(objerr, function(index, val) {
						if (val.constructor === Array) {
							$.each(val, function(i, v) {
								$("#errores_inline_tercero").append('<p class="p-error">'+v+'</p>');
							});
						}else{
							$("#errores_inline_tercero").append('<p class="p-error">'+val+'</p>');
						}
					});
				}else if(datos.success){
					$("#errores_inline_tercero").append('<p class="p-success">'+datos.data.mensaje+'</p>');
					setTimeout(function() {
						//cerrar_mensajes();
						window.history.back();
					}, 1000);
				}
			});
    	});
	});
</script>