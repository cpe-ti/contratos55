<div class="row">
	@if($terceros->isEmpty())
		<div class="col-md-12">
			Aun no existen terceros en la base de datos!
		</div>
	@else
		<div class="col-md-12">
			<div class="input-group">
				<input type="text" name="tercero_id" class="form-control" id="buscar_tercero" placeholder="Buscar"/>			
				<span class = "input-group-addon">
			      <i class="fa fa-search" aria-hidden="true"></i>
			   	</span>
			</div>
		</div>
	@endif
</div>
<div class="row" id="listado_terceros">
@foreach ($terceros as $key => $tercero)
	<div class="col-md-12 col-tercero {{($key == count($terceros)-1) ? 'ch_sizer' : ''}}" data-nombre="{{$tercero->NombreCompleto()}} {{$tercero->id}}">
		<div class="tarjeta-tercero" tabindex="0" data-id="{{$tercero->id}}" data-tipo="{{$tercero->tipo_id}}" data-tipo-nombre="{{$tercero->tipo->tipo}}" data-nombre="{{$tercero->NombreCompleto()}} - {{$tercero->tipo->tipo}}">
			<div class="img-perfil">
				<div class="circle-perfil"></div>
			</div>	
			<div class="detalles">
				<span class="nombre">{{$tercero->NombreCompleto()}}</span>
				<span class="id">{{$tercero->id}}</span>
				<span class="tipo">{{$tercero->tipo->tipo}}</span>
			</div>
		</div>
	</div>
@endforeach
</div>
<style>
	#listado_terceros{
		/* max-height: 0px; */
		overflow-y: auto !important;
		overflow-x: hidden !important;
	}
	.col-tercero{
		width: 100%;
	}	
	.tarjeta-tercero{
		margin-top: 10px;
		margin-left: 2px;
		width: calc(100% - 4px);
		height: 80px;
		border-radius: 10px;
		/* background-color: #B4B4B4; */
		display: flex;
		flex-direction: row;
		flex-wrap: nowrap;
		overflow: hidden;
		cursor: pointer;
		-webkit-transition: all 250ms;
		-moz-transition: all 250ms;
		-ms-transition: all 250ms;
		-o-transition: all 250ms;
		transition: all 250ms;		
	}
	.tarjeta-tercero.selected{
		outline: 3px solid #006ECB;
		/* border: 3px solid #006ECB; */
	}
	.tarjeta-tercero:hover{
		-webkit-box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.75);
    	-moz-box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.75);
    	box-shadow: 0px 5px 10px 0px rgba(0,0,0,0.75);
    	-webkit-transform: translateY(-2px);
    	-moz-transform: translateY(-2px);
    	-ms-transform: translateY(-2px);
    	-o-transform: translateY(-2px);
    	transform: translateY(-2px);
	}
	.tarjeta-tercero:active{
		-webkit-box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.75);
    	-moz-box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.75);
    	box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.75);
		-webkit-transform: translateY(1px);
    	-moz-transform: translateY(1px);
    	-ms-transform: translateY(1px);
    	-o-transform: translateY(1px);
    	transform: translateY(1px);
    	-webkit-transition: all 100ms;
		-moz-transition: all 100ms;
		-ms-transition: all 100ms;
		-o-transition: all 100ms;
		transition: all 100ms;
	}
	.tarjeta-tercero>.img-perfil{
		background-color: #DBDBDB;
		width: 80px;
		min-width: 80px;
		display: flex;
		align-items: center;
  		justify-content: center;
  		border-radius: 10px 0px 0px 10px;
	}
	.tarjeta-tercero>.img-perfil>.circle-perfil{
		height: 60px;
		width: 60px;
		border-radius: 100px;
		background-color: #00675F;
	}
	.tarjeta-tercero>.detalles{
		background-color: #4D4D4D;
		flex-grow: 1;
		padding: 10px;
		display: flex;
		flex-direction: column;
		align-items: left;
  		justify-content: space-between;
  		color: #E5E5E5;
  		border-radius: 0px 10px 10px 0px;
	}
	.tarjeta-tercero>.detalles>span{
		white-space: nowrap;
  		/* overflow: hidden; */
		text-overflow: ellipsis;
	}
	.tarjeta-tercero>.detalles>span.nombre{
		/* font-size: 20px */
	}
	.tarjeta-tercero>.detalles>span.id{
		font-size: 11px
	}
	.tarjeta-tercero>.detalles>span.tipo{
		font-size: 11px
	}

	@media (max-width: 767px) {
		.tarjeta-tercero>.detalles>span.nombre{
			font-size: 12px;
		}
	}
	@media (min-width:768px) and (max-width: 991px) {
		.tarjeta-tercero>.detalles>span.nombre{
			/* font-size: 10px; */
		}
	}
	@media (min-width: 992px) and (max-width: 1199px) {
		.tarjeta-tercero>.detalles>span.nombre{
			/* font-size: 10px; */
		}
	}
	@media (min-width: 1200px) {
		.tarjeta-tercero>.detalles>span.nombre{
			/* font-size: 10px; */
		}
	}
</style>
<script>
	inicializar (function () {
		$('#listado_terceros').css('max-height', $(window).height() - 250);
		$(".tarjeta-tercero").on('click, focus', function(event) {
			event.preventDefault();
			$(".tarjeta-tercero").not(this).removeClass('selected');
			$(this).toggleClass('selected');
		});
		var ShuffleTercero = window.shuffle;
		var terceros = new ShuffleTercero(document.getElementById('listado_terceros'), {
			itemSelector: '.col-tercero',
			sizer: '.ch_sizer',
			speed: 300,
			useTransforms: true
		});
		function filtrar_terceros() {
			var nombre;
			nombre = $('#buscar_tercero').val().toLowerCase();
			terceros.filter(function(element){
				var b1 = (($(element).data('nombre').toLowerCase()).indexOf(nombre) >= 0);
				return b1;
			});
		}
		$('#buscar_tercero').onDelayed('input', {{ count($terceros) }}, function() {
			filtrar_terceros();
		});
		$('#listado_terceros').ready( function(){
			filtrar_terceros();
		})
	});
</script>