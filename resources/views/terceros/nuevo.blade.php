@extends('layouts.pagina_maestra')
@section('titulo')
Nuevo Tercero
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<br/>
	<form id="form-nuevo-tercero" action="{{ url('terceros/nuevo') }}" method="POST" autocomplete="off">
		{!!csrf_field()!!}
		<div class="form-group col-md-8{{ $errors->has('tercero_id') ? ' has-error' : '' }}">
			<label class="control-label" for="tercero_id">Nº de identificación (C.C o NIT sin dígito de verificación):</label>
			<input type="number" name="tercero_id" class="form-control" id="tercero_id" placeholder="Identificación" required="required"/>
			<span class="help-block">
                <strong id="tercero_id_error">{{ $errors->first('tercero_id') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-4">
			<label class="control-label" for="tipo_persona">Tipo de persona:</label>
			<select name="tipo_persona" class="form-control" id="tipo_persona" required="required">
				<option value="">Seleccionar..</option>
				@foreach($tipos as $tipo)
				<option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
				@endforeach
			</select>
			<span class="help-block">
                <strong id="tipo_persona_error">{{ $errors->first('tipo_persona') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-6">
			<label class="control-label" for="primer_nombre">Primer Nombre:</label>
			<input type="text" name="primer_nombre" class="form-control" id="primer_nombre" placeholder="Primer Nombre"/>
			<span class="help-block">
                <strong id="primer_nombre_error">{{ $errors->first('primer_nombre') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-6">
			<label class="control-label" for="segundo_nombre">Segundo Nombre:</label>
			<input type="text" name="segundo_nombre" class="form-control" id="segundo_nombre" placeholder="Segundo Nombre"/>
			<span class="help-block">
                <strong id="segundo_nombre_error">{{ $errors->first('segundo_nombre') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-6">
			<label class="control-label" for="primer_apellido">Primer Apellido:</label>
			<input type="text" name="primer_apellido" class="form-control" id="primer_apellido" placeholder="Primer Apellido"/>
			<span class="help-block">
                <strong id="primer_apellido_error">{{ $errors->first('primer_apellido') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-6">
			<label class="control-label" for="segundo_apellido">Segundo Apellido:</label>
			<input type="text" name="segundo_apellido" class="form-control" id="segundo_apellido" placeholder="Segundo Apellido"/>
			<span class="help-block">
                <strong id="segundo_apellido_error">{{ $errors->first('segundo_apellido') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-12">
			<label class="control-label" for="razon_social">Razón Social:</label>
			<input type="text" name="razon_social" class="form-control" id="razon_social" placeholder="Razon Social"/>
			<span class="help-block">
                <strong id="razon_social_error">{{ $errors->first('razon_social') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-6">
			<label class="control-label" for="telefono">Telefono:</label>
			<input type="text" name="telefono" class="form-control" id="telefono" placeholder="555-5555 o 555-555-5555"/>
			<span class="help-block">
                <strong id="telefono_error">{{ $errors->first('telefono') }}</strong>
            </span>
		</div>
		<div class="form-group col-md-6">
			<label class="control-label" for="correo">correo:</label>
			<input type="text" name="correo" class="form-control" id="correo" placeholder="alguien@cpe.gov.co"/>
			<span class="help-block">
                <strong id="correo_error">{{ $errors->first('correo') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group col-md-12">
			<label class="control-label" for="direccion">Dirección:</label>
			<input type="text" name="direccion" class="form-control" id="direccion" placeholder="Carrera 11 # 71-73"/>
			<span class="help-block">
                <strong id="direccion_error">{{ $errors->first('direccion') }}</strong>
            </span>
		</div>
		<div class="clearfix visible-xs visible-sm visible-md visible-lg"></div>
		<div class="form-group form-error">
            <span class="help-block">
                <strong class="form-error-msg"></strong>
            </span>
        </div>
		<div class="form-group col-xs-12">			
			<button type="submit" class="btn btn-primary pull-right">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>
				<span>&nbsp;&nbsp;Crear nuevo tercero</span>
			</button>			
		</div>
		<p id="logs"></p>
	</form>
</div>
<style>
	.form-group:last-child{
		padding-bottom: 50px;
	}
	#form-nuevo-tercero textarea { 
		resize:vertical;
	}
	#form-nuevo-tercero button[type=submit]{
		font-size: 20px !important;
	}
	#form-nuevo-tercero button[type=submit]>span{
		font-size: 16px !important;
	}
	.modal_lista_terceros .bootbox-body{
		
	}
	input[required], select[required]{
	    
	}
</style>
<script type="text/javascript">
	inicializar (function () {
		$('#form-nuevo-tercero').on('submit', function(e) {
    	    e.preventDefault();
    	    e.stopPropagation();
    	    e.isDefaultPrevented = function () {
    	        return false;
    	    }
    	    var formulario = $(this)[0];
			var url_action = $(this).attr('action');
			var metodo = $(this).attr('method');
			var tipo_datos = "json";
			var datos = new FormData(formulario);
			var jform = $(this);
			$(':input', jform).attr("disabled", true);
			$(".help-block").addClass('help-block-visible');
			$('.form-group').removeClass("has-error").find('.help-block>strong').html('');
			enviar_formularios (url_action, metodo, tipo_datos, datos, function (respuesta) {
				console.log(respuesta);
				$(':input', jform).attr("disabled", false);
				//$("#logs").html(respuesta.data.responseText);
				if (respuesta.data.responseJson) {
					var obj;
					try{
    				    obj = respuesta.data.responseJson.errors;
    				    // obj = jQuery.parseJSON(respuesta.data.responseText);
    				}catch(e){
    				    console.log(e); //error in the above string(in this case,yes)!
    				    obj = {fallido:"Ocurrio un error inesperado: "+e+"<br>"+respuesta.data.responseText};
    				}
                    if (obj.tercero_id) {
                        $("#tercero_id").parent('.form-group').addClass("has-error");
                        $("#tipo_persona").parent('.form-group').addClass("has-error");
                        $('#tercero_id_error').html(obj.tercero_id);
                    }
                    if (obj.primer_nombre) {
                        $("#primer_nombre").parent('.form-group').addClass("has-error");
                        $('#primer_nombre_error').html(obj.primer_nombre);
                    }
                    if (obj.segundo_nombre) {
                        $("#segundo_nombre").parent('.form-group').addClass("has-error");
                        $('#segundo_nombre_error').html(obj.segundo_nombre);
                    }
                    if (obj.primer_apellido) {
                        $("#primer_apellido").parent('.form-group').addClass("has-error");
                        $('#primer_apellido_error').html(obj.primer_apellido);
                    }
                    if (obj.segundo_apellido) {
                        $("#segundo_apellido").parent('.form-group').addClass("has-error");
                        $('#segundo_apellido_error').html(obj.segundo_apellido);
                    }
                    if (obj.correo) {
                        $("#correo").parent('.form-group').addClass("has-error");
                        $('#correo_error').html(obj.correo);
                    }
                    if (obj.telefono) {
                        $("#telefono").parent('.form-group').addClass("has-error");
                        $('#telefono_error').html(obj.telefono);
                    }
                    if (obj.fallido) {
                        $(".form-error").addClass("has-error");
                        $('.form-error-msg').html(obj.fallido);
                    }
                    if (obj.error) {
                        $(".form-error").addClass("has-error");
                        $('.form-error-msg').html(obj.error);
                    }                    					
				}else if(respuesta.data.success) {
					bootbox.dialog({
						title: "Hecho",
						message: (respuesta.data.mensaje ? respuesta.data.mensaje : "Nuevo Tercero creado."),
						onEscape: function(){
							//$("#contenedor-pagina").css('overflow-y', 'auto');
							alert("Ohhh");
						},
						buttons: {
							success: {
								label: "Ok",
								className: "btn-success",
								callback: function () {
									window.history.back();
								}
							}
						}
					});
                    //alert(respuesta.data.mensaje);
                }
			});
    	});
	});
</script>
@endsection