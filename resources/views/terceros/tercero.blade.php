@extends('layouts.pagina_maestra')
@section('titulo')
{{$tercero->NombreCompleto()}}
@endsection
@section('contenido')
<div class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<div class="row">
		<div class="col-xs-3">
			<div class="tercero-icon">
				{!! $tercero->iconoSVG() !!}
				{{-- {!!$tercero->icono()!!} --}}
			</div>
		</div>
		<div class="col-xs-9">
			<p><b>Nº Identificación: </b>{{$tercero->id}}</p>
			<p><b>Tipo: </b>{{$tercero->TipoPersona()}}</p>
			<p><b>Nombre: </b>{{$tercero->NombreCompleto()}}</p>
			<p><b>Dirección: </b>{{$tercero->direccion}}</p>
			<p><b>Telefono: </b>{{$tercero->telefono}}</p>
			<hr/>
		</div>
	</div>
	<hr/>
</div>
<style>
	.tercero-icon{
		text-align: center;
		height: 100px;
		width: 100%;
		margin:0 auto;
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.col-tags{
		/*margin-top: 15px;*/
		margin-bottom: 15px;
	}
	.tag{
		background-color: #1A85D7;
		color: #FFFFFF;
		padding: 10px;
		border-radius: 10px;
		line-height: 40px;
		white-space: nowrap;
	}
	.tercero-icon>svg{
		width: 100%;
		max-height: 100%;
	}
</style>
<script type="text/javascript">
	inicializar(function(){
		
	});
</script>
@endsection