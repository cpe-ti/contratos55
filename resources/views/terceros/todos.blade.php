@extends('layouts.pagina_maestra')
@section('titulo')
	Todos los Terceros
@endsection
@section('contenido')
<div id="contenido_lista_contratos" class="col-md-12 col-lg-10 col-md-offset-0 col-lg-offset-1">
	<div class="clearfix">
		<div class="input-group col-xs-12">
    	  	<div class="input-group">
				<input type="text" name="tercero_id" class="form-control input-lg" id="buscar_tercero" placeholder="Buscar"/>
				<span class = "input-group-addon">
					<i class="fa fa-search" aria-hidden="true"></i>
				</span>
			</div>
    	  	<span class="input-group-btn">
    	  	  	<a id="boton-nuevo-tercero" class="btn btn-primary link_dinamico btn-lg" href="{{ url('gaf/terceros/nuevo') }}"></a>
    	  	</span>
    	</div>
	</div>
	<div class="row" id="listado_terceros">
		@foreach ($terceros as $key => $tercero)
		<div class="col-xs-12 col-md-6 col-tercero {{($key == count($terceros)-1) ? 'ch_sizer' : ''}}" data-nombre="{{$tercero->NombreCompleto()}} {{$tercero->id}}">
			<div class="tarjeta-tercero" style="background-color: {{($tercero->tipo->id == 2 ? '#B9D2E3' : '#DFDFDF')}};">
				<div class="detalles">
					<p class="nombre">{{$tercero->NombreCompleto()}}</p>
					<p class="id">{{$tercero->id}}</p>
					<p class="tipo">{{$tercero->tipo->tipo}}</p>
				</div>
				<a href="{{ url('gaf/terceros/'.$tercero->id.'-'.$tercero->tipo->id) }}" class="ver-tercero link_dinamico">
					<i class="fa fa-chevron-right" aria-hidden="true"></i>
				</a>
			</div>
		</div>
		@endforeach
	</div>
</div>
<style>
	#listado_terceros{
		margin-top: 15px;
	}
	.col-tercero{
		width: 100%;
	}
	#boton-nuevo-tercero:before {
	   	content: "Crear tercero";
	}
	.tarjeta-tercero{
		background-color: #DFDFDF;
		height: 100px;
		margin-top: 10px;
		border-radius: 5px;
		display: flex;
		flex-direction: row;
		overflow: hidden;
	}
	.tarjeta-tercero>.detalles{
		padding-left: 10px;
		padding-right: 10px;
		flex-grow: 1;
		display: flex;
		flex-direction: column;
		overflow: hidden;
		justify-content: space-around;
	}
	.tarjeta-tercero>.detalles>p{
		text-overflow: ellipsis;
  		overflow: hidden;
  		white-space: nowrap;
  		max-width: 100%;
  		line-height: 100%;
		margin: 0px; 
	}
	.tarjeta-tercero>.detalles>p.nombre{
		font-size: 18px;
	}
	.tarjeta-tercero>.detalles>p.id{
		font-size: 13px;
	}
	.tarjeta-tercero>.detalles>p.tipo{
		font-size: 13px;
	}
	.tarjeta-tercero>.ver-tercero{
		background-color: #3B6086;
		color: #FFFFFF;
		width: 60px;
		min-width: 60px;
		display: flex;
		flex-direction: column;
		justify-content: center;
		text-decoration: none;
		font-size: 20px;
		-webkit-transition: all 200ms;
		-moz-transition: all 200ms;
		-ms-transition: all 200ms;
		-o-transition: all 200ms;
		transition: all 200ms;		
	}
	.tarjeta-tercero>.ver-tercero:hover{
		font-size: 30px;		
	}
	.tarjeta-tercero>.ver-tercero:active{
		font-size: 20px;		
	}
	.tarjeta-tercero>.ver-tercero>i{
		align-self: center;
	}
	
	/* Media querys */
	@media (max-width : 479px) {
		.tarjeta-tercero>.detalles>p.nombre{
			font-size: 15px;
		}
		.tarjeta-tercero>.detalles>p.id{
			font-size: 10px;
		}
		.tarjeta-tercero>.detalles>p.tipo{
			font-size: 10px;
		}
		.tarjeta-tercero{
			height: 80px;
		}
		#boton-nuevo-tercero:before {
		   	content: "\f055";
		   	font-family: FontAwesome;
    		font-style: normal;
    		font-weight: normal;
    		text-decoration: inherit;
		}
    }
	@media (min-width : 480px) and (max-width: 767px) {
		.tarjeta-tercero>.detalles>p.nombre{
			font-size: 17px;
		}
		.tarjeta-tercero>.detalles>p.id{
			font-size: 12px;
		}
		.tarjeta-tercero>.detalles>p.tipo{
			font-size: 12px;
		}
		.tarjeta-tercero{
			height: 90px;
		}
		#boton-nuevo-tercero:before {
		   	content: "\f055";
		   	font-family: FontAwesome;
    		font-style: normal;
    		font-weight: normal;
    		text-decoration: inherit;
		}
	}
	@media (min-width:768px) and (max-width: 991px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
@include('plantillas.tarjeta_tercero') 
<script>
	inicializar (function () {
		var ShuffleTercero = window.shuffle;
		var terceros = new ShuffleTercero(document.getElementById('listado_terceros'), {
			itemSelector: '.col-tercero',
			sizer: '.ch_sizer',
			speed: 300,
			useTransforms: true
		});
		//$("#boton-nuevo-tercero").click(function(e) {
		//	e.preventDefault();
    	//    e.stopPropagation();
    	//    e.isDefaultPrevented = function () {
    	//        return false;
    	//    }
		//	AgregarTarjeta({nombre:'Prueba', id:Date.now(), color:'#CF8A8A', tipo:'1', url:'personas-naturales/12158'});
		//});
		function AgregarTarjeta(datos) {
			var template = document.getElementById('template-tarjeta-tercero').innerHTML;
			var compiled_template = Handlebars.compile(template);
			var rendered = compiled_template(datos);
			var $elemento = $(rendered);
		  	var items = [$elemento[0]];
		  	items.forEach(function (item) {
		  		console.log(item);
		  		//$(terceros.element).append(item);
		  		$(terceros.element).append(item);
		  	});
		  	terceros.add(items);
		};

		function filtrar_terceros() {
			var nombre;
			nombre = $('#buscar_tercero').val().toLowerCase();
			terceros.filter(function(element){
				return (($(element).data('nombre').toLowerCase()).indexOf(nombre) >= 0);
			});
		}
		$('#buscar_tercero').onDelayed('input', {{ count($terceros) }}, function() {
			filtrar_terceros();
		});
	});
</script>
@endsection