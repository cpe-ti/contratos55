<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth', 'web']], function () {
    //Grupo Supervision
    Route::group(['prefix' => 'supervision', 'middleware' => ['permission:menu-supervision']], function () {
        //obtener datos de supervision de contrato X
        Route::get('/contratos/{contrato}/supervision-data','ContratosController@SupervisionDataApi')->where('contrato', '[0-9]+')->name('supervision.registros');
        //Guardar nuevo registro para contrato X
        Route::post('/contratos/{contrato}/guardar-registro','ContratosController@storeRegistroApi')->where('contrato', '[0-9]+')->name('supervision.registros.guardar');
        //Actualizar registro existente by ID
        Route::put('/contratos/{contrato}/actualizar-registro/{registro}','ContratosController@updateRegistroApi')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('supervision.registros.actualizar');
        //Procesar
        Route::put('/contratos/{contrato}/procesar-registro/{registro}','ContratosController@procesarRegistroApi')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('supervision.registros.procesar');
        //Borrar registro
        Route::delete('/contratos/{contrato}/eliminar-registro/{registro}','ContratosController@deleteRegistroApi')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('supervision.registros.eliminar');
        //HTML datos de informe
        Route::get('/contratos/{contrato}/informeHtml/{registro}','ContratosController@informeHtmlRegistroApi')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('supervision.registros.htmlContentInforme');
        //Crear informe HTML
        Route::post('/contratos/{contrato}/informeHtml/{registro}/guardar','ContratosController@informeHtmlStoreApi')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('supervision.registros.nuevoInformeHtml');
        //Actualizar informe HTML
        Route::post('/contratos/{contrato}/informeHtml/{registro}/actualizar','ContratosController@informeHtmlUpdateApi')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('supervision.registros.actualizarInformeHtml');
        //Procesar informe y final final no da mas
        Route::post('/contratos/{contrato}/informeHtml/{registro}/procesar','ContratosController@informeHtmlProcesarApi')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('supervision.registros.procesarInformeHtml');
    });

    Route::group(['auditoria' => 'supervision', 'middleware' => ['permission:menu-supervision']], function () {
        Route::get('/registro/{registro}/informe', 'AuditoriaController@informeRegistro')->where('registro', '[0-9]+')->name('auditoria.informe_supervision');
    });
    

    Route::group(['prefix' => 'gaf', 'middleware' => ['permission:*']], function () {
        Route::get('/informes-supervision','InformesController@indexAPI')->name('informe.todos');
        Route::get('/informes-supervision/{informe_procesado}','InformesController@showAPI')->where('informe_procesado', '[0-9]+')->name('informe.informe');
        Route::post('/informes-supervision/{informe_procesado}/aprobar-tes','InformesController@aprobarTesAPI')->where('informe_procesado', '[0-9]+')->name('informe.aprobar-tes');
        Route::post('/informes-supervision/{informe_procesado}/rechazar-tes','InformesController@rechazarTesAPI')->where('informe_procesado', '[0-9]+')->name('informe.rechazar-tes');
        Route::post('/informes-supervision/{informe_procesado}/aprobar-cont','InformesController@aprobarContAPI')->where('informe_procesado', '[0-9]+')->name('informe.aprobar-cont');
        Route::post('/informes-supervision/{informe_procesado}/rechazar-cont','InformesController@rechazarContAPI')->where('informe_procesado', '[0-9]+')->name('informe.rechazar-cont');
    });

});
