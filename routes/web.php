<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', function () {
    return view('inicio');
});

Route::get('/query', function () {
    return App\Compra::all();
})->where('query', '(.*)');

Auth::routes();

Route::get('/inicio', 'HomeController@index');

Route::get('refresh-csrf', function(){
    return csrf_token();
});

Route::get('tester/{id}', function($id){
    $clase = App\DocumentoBase::findOrFail($id)->tester_text();
    return $clase;
})->where('id', '[0-9]+');

//Route::get('/memo/{id}', 'ImpresionesController@ImprimirMemorando')->where('id', '[0-9]+');
//Route::get('/carta/{id}', 'DocumentosController@ImprimirCartaAdjudicacion')->where('id', '[0-9]+');

//Route::get('/doc/{id}', 'ImpresionesController@ImprimirDocumentoBase')->where('id', '[0-9]+');
//Route::get('/user_info/{id}', 'ImpresionesController@UserInfo')->where('id', '[0-9]+');
//Route::get('/persona_info/{id}', 'ImpresionesController@PersonInfo')->where('id', '[0-9]+');
//Route::get('/contract_info/{id}', 'ImpresionesController@ContractInfo')->where('id', '[0-9]+');

Route::group(['middleware' => ['auth', 'web']], function () {
    //Grupo Administrar
    Route::group(['prefix' => 'administrar', 'middleware' => ['permission:menu-administrar']], function () {
        //GET
        Route::get('/', 'AdminController@index');
        Route::get('/usuarios', 'AdminController@Usuarios')->middleware('permission:consultar-usuarios');
        Route::get('/usuarios/{id}', 'AdminController@VerUsuario')->where('id', '[0-9]+')->middleware('permission:consultar-usuarios');
        Route::get('/usuarios/{id}/editar', 'AdminController@EditarUsuario')->where('id', '[0-9]+')->middleware('permission:actualizar-usuarios');
        Route::get('/participantes', 'PersonasController@index')->middleware('permission:consultar-personas');
        Route::get('/participantes/{id}', 'PersonasController@show')->where('id', '[0-9]+')->middleware('permission:consultar-personas');
        Route::get('/participantes/{id}/editar', 'PersonasController@editar')->where('id', '[0-9]+')->middleware('permission:actualizar-personas');
        Route::get('/participantes/nuevo', 'PersonasController@create')->middleware('permission:crear-personas');
        Route::get('/roles-permisos', 'AdminController@VerRoles')->middleware('permission:consultar-roles');
        Route::get('/roles-permisos/nuevo', 'AdminController@CrearRoles')->name('rol.nuevo');
        Route::get('/roles-permisos/{rol_id}/editar', 'AdminController@EditarPermisos')->where('rol_id', '[0-9]+')->middleware('permission:actualizar-roles');
        Route::get('/historico-sistema', 'AdminController@ShowAudit');
        Route::get('/documentos', 'AdminController@documentos');
        //Route::get('/contratos/{id}', 'AdminController@VerContrato')->where('id', '[0-9]+');
        //Route::get('/contratos/{id}/editar', 'AdminController@EditarContrato')->where('id', '[0-9]+');
        //POST
        Route::post('/usuarios/{id}/editar', 'AdminController@GuardarUsuario')->where('id', '[0-9]+')->middleware('permission:actualizar-usuarios');
        Route::post('/participantes/{id}/editar', 'PersonasController@guardar')->where('id', '[0-9]+')->middleware('permission:actualizar-personas');
        Route::post('/participantes/nuevo', 'PersonasController@store')->middleware('permission:crear-personas');
        Route::post('/roles-permisos/{rol_id}/editar', 'AdminController@GuardarPermisos')->where('rol_id', '[0-9]+')->middleware('permission:actualizar-roles');        
        //Route::post('/contratos/{id}/editar', 'AdminController@EditarContrato')->where('id', '[0-9]+');
    });

    //Grupo Presupuesto
    Route::group(['prefix' => 'planeacion'], function () {
        //GET
        Route::get('/', function(){
            return view('presupuesto.inicio');
        })->name('planeacion.inicio');
        Route::get('/rubros', 'RubrosController@index');
        Route::get('/rubros/nuevo', 'RubrosController@create');
        Route::get('/rubros/{id}', 'RubrosController@show')->where('id', '[0-9]+');
        Route::get('/rubros/{id}/editar', 'RubrosController@edit')->where('id', '[0-9]+');
        Route::get('/plan-de-compras', 'ComprasController@index');
        Route::get('/plan-de-compras/nueva-compra', 'ComprasController@create');
        Route::get('/plan-de-compras/compra/{id}', 'ComprasController@show')->where('id', '[0-9]+');
        Route::get('/plan-de-compras/compra/{id}/editar', 'ComprasController@edit')->where('id', '[0-9]+');
        Route::get('/areas', 'AreasController@index');
        Route::get('/areas/{id}', 'AreasController@show')->where('id', '[0-9]+');
        Route::get('/areas/nueva', 'AreasController@create');
        Route::get('/areas/{id}/editar', 'AreasController@edit')->where('id', '[0-9]+');
        Route::get('/procesos', 'ProcesosController@index');
        Route::get('/procesos/nuevo', 'ProcesosController@create');
        Route::get('/procesos/{id}', 'ProcesosController@show')->where('id', '[0-9]+');
        Route::get('/procesos/{id}/editar', 'ProcesosController@edit')->where('id', '[0-9]+');
        //POST
        Route::post('/rubros/nuevo', 'RubrosController@store');
        Route::post('/rubros/{id}/editar', 'RubrosController@update')->where('id', '[0-9]+');
        Route::post('/plan-de-compras/nueva-compra', 'ComprasController@store');
        Route::post('/plan-de-compras/compra/{id}/editar', 'ComprasController@update')->where('id', '[0-9]+');
        Route::post('/areas/nueva', 'AreasController@store');
        Route::post('/areas/{id}/editar', 'AreasController@update')->where('id', '[0-9]+');
        Route::post('/procesos/nuevo', 'ProcesosController@store');
        Route::post('/procesos/{id}/editar', 'ProcesosController@update')->where('id', '[0-9]+');
    });

    //Grupo Administrativa y Financiera
    Route::group(['prefix' => 'gaf'], function () {
        //GET
        Route::get('/', function(){
            return view('gaf.inicio');
        });
        Route::get('/cdp', 'ProvisionesController@index');
        Route::get('/cdp/nuevo', 'ProvisionesController@create');
        Route::get('/cdp/{id}', 'ProvisionesController@show')->where('id', '[0-9]+');
        Route::get('/rp', 'RegPresupuestalesController@index');
        Route::get('/rp/nuevo', 'RegPresupuestalesController@create');
        Route::get('/rp/{id}', 'RegPresupuestalesController@show')->where('id', '[0-9]+');
        Route::get('/terceros', 'TercerosController@index')->middleware('permission:consultar-trcros');
        Route::get('/terceros/nuevo', 'TercerosController@create')->middleware('permission:crear-trcros');
        Route::get('/terceros/{id}-{tipo_id}', 'TercerosController@show')->where(['id'=>'[0-9]+', 'tipo_id'=>'[0-9]+']);
        Route::get('/terceros/{id}-{tipo_id}/editar', 'TercerosController@edit')->where(['id'=>'[0-9]+', 'tipo_id'=>'[0-9]+']);
        Route::get('/contratos', 'ContratosController@index')->middleware('permission:consultar-documento');
        Route::get('/contratos/{id}', 'ContratosController@show')->where('id', '[0-9]+')->middleware('permission:consultar-documento');
        Route::get('/contratos/{id}/registrar-pago', 'PagosController@create')->where('id', '[0-9]+')->middleware('permission:crear-pagos');
        Route::get('/contratos/{id}/asignar-rp', 'ContratosController@asignarRp')->where('id', '[0-9]+');
        
        Route::get('/pagos', 'PagosController@index');
        Route::get('/informes', 'InformesController@index')->name('gaf.informes');
        Route::get('/informes/{informe_procesado}', 'InformesController@show')->where('informe_procesado', '[0-9]+')->name('gaf.informes.informe');
        Route::get('/informes/{informe_procesado}/aprobacion-contaduria', 'InformesController@showAprobarCont')->where('informe_procesado', '[0-9]+')->name('gaf.informes.informe.aprobacion-contaduria');
        Route::get('/informes/{informe_procesado}/aprobacion-tesoreria', 'InformesController@showAprobarTes')->where('informe_procesado', '[0-9]+')->name('gaf.informes.informe.aprobacion-tesoreria');
        
        
        Route::get('/pagos/{id}', 'PagosController@show')->where('id', '[0-9]+');
        
        
        //POST        
        Route::post('/cdp/nuevo', 'ProvisionesController@store');
        Route::post('/rp/nuevo', 'RegPresupuestalesController@store');
        Route::post('/terceros/nuevo', 'TercerosController@store')->middleware('permission:crear-trcros');
        Route::post('/terceros/{id}-{tipo_id}/editar', 'TercerosController@update')->where(['id'=>'[0-9]+', 'tipo_id'=>'[0-9]+']);
        Route::post('/contratos/{id}/registrar-pago', 'PagosController@store')->where('id', '[0-9]+')->middleware('permission:crear-pagos');
        Route::post('/contratos/{id}/asignar-rp', 'ContratosController@registrarRP')->where('id', '[0-9]+');
    });
    
    Route::get('/imprimir-contrato/{id}', 'ContratosController@ver_pdf')->where('id', '[0-9]+');
    Route::get('/terceros-lista', 'TercerosController@listado')->middleware('permission:consultar-trcros');
    Route::get('/terceros/inline_nuevo', 'TercerosController@inline_nuevo')->middleware('permission:crear-trcros');
    Route::post('/terceros/inline_nuevo', 'TercerosController@store')->middleware('permission:crear-trcros');
    //Grupo Contratacion
    Route::group(['prefix' => 'contratacion', 'middleware' => ['permission:menu-cto']], function () {
    //Route::group(['prefix' => 'contratacion'], function () {
        //GET
        Route::get('/', function(){
            return view('contratacion.inicio');
        });        
        Route::get('/justificaciones','EstudiosDeMercadoController@index');
        Route::get('/justificaciones/nueva','EstudiosDeMercadoController@create');
        Route::get('/justificaciones/{id}','EstudiosDeMercadoController@show')->where('id', '[0-9]+');
        Route::get('/justificaciones/{id}/editar','EstudiosDeMercadoController@edit')->where('id', '[0-9]+');
        //
        Route::get('/contratos','ContratosController@index')->middleware('permission:consultar-documento');
        Route::get('/contratos/nuevo','ContratosController@create')->middleware('permission:crear-documento')->name('contratos.nuevo');
        Route::get('/contratos/{id}','ContratosController@show')->where('id', '[0-9]+')->middleware('permission:consultar-documento');
        Route::get('/contratos/{id}/editar','ContratosController@edit')->where('id', '[0-9]+')->middleware('permission:actualizar-documento');
        Route::get('/contratos/{id}/crear-cuerpo', 'ContratosController@generar_cuerpo')->where('id', '[0-9]+')->middleware('permission:actualizar-cuerpo-documento');
        Route::get('/contratos/{id}/editar-cuerpo', 'ContratosController@editar_cuerpo')->where('id', '[0-9]+')->middleware('permission:actualizar-cuerpo-documento');
        Route::get('/contratos/{id}/aprobar', 'ContratosController@aprobar')->where('id', '[0-9]+')->middleware('permission:aprobar-contrato');
        Route::get('/contratos/{id}/aprobar-direccion', 'ContratosController@aprobar_direccion')->where('id', '[0-9]+')->middleware('permission:aprobar-contrato');
        Route::get('/contratos/{id}/revertir-aprobacion', 'ContratosController@show_reversar')->where('id', '[0-9]+')->middleware('permission:reversar-documento-aprobado');
        Route::get('/contratos/{id}/legalizar', 'ContratosController@legalizar')->where('id', '[0-9]+');

        ///OTRO SI
        Route::get('/contratos/{id}/crear-otrosi', 'ContratosController@crear_otrosi')->where('id', '[0-9]+')->middleware('permission:crear-documento');;
        
        Route::get('/imprimir-contrato', 'ContratosController@imprimir')->where('id', '[0-9]+');        
        //POST
        
        Route::post('/justificaciones/nueva','EstudiosDeMercadoController@store');
        Route::post('/justificaciones/{id}/editar','EstudiosDeMercadoController@update')->where('id', '[0-9]+');
        //
        Route::post('/contratos/nuevo','ContratosController@store')->middleware('permission:crear-documento');
        Route::post('/contratos/{id}/crear-cuerpo', 'ContratosController@guardar_cuerpo')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/editar','ContratosController@update')->where('id', '[0-9]+')->middleware('permission:actualizar-documento');
        Route::post('/contratos/{id}/editar-cuerpo', 'ContratosController@update_cuerpo')->where('id', '[0-9]+')->middleware('permission:actualizar-documento');
        Route::post('/contratos/{id}/aprobar', 'ContratosController@save_aprobar')->where('id', '[0-9]+')->middleware('permission:aprobar-contrato');
        Route::post('/contratos/{id}/aprobar-direccion', 'ContratosController@save_aprobar_direccion')->where('id', '[0-9]+')->middleware('permission:aprobar-contrato');
        Route::post('/contratos/{id}/revertir-aprobacion', 'ContratosController@reversar_aprobado')->where('id', '[0-9]+')->middleware('permission:reversar-documento-aprobado');
        Route::post('/contratos/{id}/legalizar', 'ContratosController@StoreLegalizar')->where('id', '[0-9]+');
        
        //        
        //GURPO OTROSIS
        Route::get('/otrosis','OtrosisController@index')->name('otrosis.inicio');
        Route::get('/otrosis/nuevo','OtrosisController@select')->name('otrosis.nuevo');
        Route::get('/otrosis/nuevo/{doc}', 'OtrosisController@create')->where('doc', '[0-9]+')->name('otrosis.crear');
        Route::get('/otrosis/todos','OtrosisController@showAll')->name('otrosis.todos');
        Route::get('/otrosis/{otrosi}','OtrosisController@show')->where('otrosi', '[0-9]+')->name('otrosis.ver');
        Route::get('/otrosis/{id}/editar','OtrosisController@edit')->where('id', '[0-9]+')->name('otrosis.editar');        
        Route::post('/otrosis/nuevo','OtrosisController@store')->name('otrosis.guardar');
        Route::post('/otrosis/{id}/editar','OtrosisController@update')->name('otrosis.actualizar');

    });

    //Grupo Supervision
    Route::group(['prefix' => 'supervision', 'middleware' => ['permission:menu-supervision']], function () {
        //GET
        Route::get('/', function(){
            return view('supervision.inicio');
        });
        Route::get('/contratos','ContratosController@indexSuper');
        Route::get('/contratos/{id}','ContratosController@showSuper')->where('id', '[0-9]+')->name('supervision.contrato');
        Route::get('/contratos/{id}/productos','ProductosController@showProductos')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/agregar-productos','ProductosController@addProductos')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/obligaciones','ObligacionesController@index')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/agregar-obligaciones','ObligacionesController@create')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/editar-obligaciones','ObligacionesController@edit')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/plazo-ejecucion','ContratosController@plazo_ejecucion')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/crear-programacion-tecnica','ProgramacionTecnicaController@create')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/crear-programacion-financiera','ProgramacionFinancieraController@create')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/editar-programacion-tecnica','ProgramacionTecnicaController@edit')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/edita192.168.10.6r-programacion-financiera','ProgramacionFinancieraController@edit')->where('id', '[0-9]+');
        //Route::get('/contratos/ejecucion-todos','');
        Route::get('/contratos/{id}/ejecucion','ContratosController@ejecucion')->where('id', '[0-9]+');
        Route::get('/contratos/{id}/registrar-avance','ContratosController@registrarAvance')->where('id', '[0-9]+')->name('supervision.registrar-avance');
        //POST
        Route::post('/contratos/{id}/plazo-ejecucion','ContratosController@store_plazo_ejecucion')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/agregar-productos','ProductosController@saveProductos')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/agregar-obligaciones','ObligacionesController@store')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/editar-obligaciones','ObligacionesController@update')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/crear-programacion-tecnica','ProgramacionTecnicaController@store')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/crear-programacion-financiera','ProgramacionFinancieraController@store')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/editar-programacion-tecnica','ProgramacionTecnicaController@update')->where('id', '[0-9]+');
        Route::post('/contratos/{id}/editar-programacion-financiera','ProgramacionFinancieraController@update')->where('id', '[0-9]+');

        
        Route::get('/contratos/{contrato}/supervision/informe/{registroid}', 'ContratosController@generarinforme')->where('contrato', '[0-9]+')->where('registro', '[0-9]+')->name('informe');
        Route::post('/contratos/supervision/informe/vista-previa', 'ContratosController@vistaPreviaInforme')->name('informe.vistaprevia');
        
        Route::get('/reportes', 'UserController@profile');
        
        //Route::post('/contratos/{id}/ejecucion/registrar-avance','')->where('id', '[0-9]+');
        //Route::post('/contratos/{id}/registrar-avance','ContratosController@registrarAvanceMes')->where('id', '[0-9]+');
    });

    //Grupo auditoria
    Route::group(['prefix' => 'auditoria', 'middleware' => ['permission:menu-auditoria']], function () {
        //GET
        Route::get('/', function(){
            return view('auditoria.inicio');
        })->name('auditoria.inicio');

        //Route::get('/contratos','ContratosController@index')->middleware('permission:consultar-documento');
        Route::get('/contratos', 'ContratosController@index')->name('auditoria.contratos');
        Route::get('/contratos/{documento}', function(App\DocumentoBase $documento){
            return view('auditoria.contratos.documento', ['contrato' => $documento]);
        })->where('doc_id', '[0-9]+')->name('auditoria.contratos.contrato');

        
        Route::get('/contratos/{documento}/avance', 'AuditoriaController@avanceContrato')->where('doc_id', '[0-9]+')->name('auditoria.contratos.contrato.avance');
        
        
        Route::get('users/{id}', '$UserController@profile');
        
    });

    //Grupo reportes
    Route::group(['prefix' => 'reportes', 'middleware' => []], function () {
        //GET
        Route::get('/', 'ReportesController@index')->name('reportes.inicio');
        Route::get('/auditoria', 'ReportesController@auditoria')->name('reportes.auditoria');
        Route::get('/contratacion', 'ReportesController@contratacion')->name('reportes.contratacion');
        Route::get('/presupuesto', 'ReportesController@presupuesto')->name('reportes.contratacion');
        Route::get('/supervision', 'ReportesController@supervision')->name('reportes.supervision');
    });
});

//rutas pruebas
Route::get('informe/contratos/{contrato}', function(App\DocumentoBase $contrato){
    return view('supervision.plantillaInformePDF', ['contrato' => $contrato]);
    //return $contrato;
})->where('id', '[0-9]+');

Route::get('facturas/{pdf}', function ($pdf) {
    // $carpeta = "facturas/";
    if(Storage::disk('facturas')->exists( $pdf )){
        return response(Storage::disk('facturas')->get($pdf))->withHeaders(['Content-Type' => 'application/pdf']);
    }
    abort(404);
})->name('facturas');

Route::get('cumplido/{pdf}', function ($pdf) {
    // $carpeta = "facturas/";
    if(Storage::disk('cumplidos')->exists( $pdf )){
        return response(Storage::disk('cumplidos')->get($pdf))->withHeaders(['Content-Type' => 'application/pdf']);
    }
    abort(404);
})->name('cumplido');

Route::get('soportes/{pdf}', function ($pdf) {
    // $carpeta = "facturas/";
    if(Storage::disk('soportes')->exists( $pdf )){
        return response(Storage::disk('soportes')->get($pdf))->withHeaders(['Content-Type' => 'application/pdf']);
    }
    abort(404);
})->name('soportes');


Route::get('xxx', function () {
    return App\DocumentoBase::allActive()->toArray();
    return App\TipoUnidadProducto::all();
    return App\TipoUnidadProducto::all();
});